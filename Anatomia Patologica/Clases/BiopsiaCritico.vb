﻿Public Class BiopsiaCritico : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private consulta As String = ""
    Private GEN_IdUsuariosCritico, GEN_IdUsuarios_NotificadoBiopsias_Critico As Integer
    Private ANA_FechaBiopsias_Critico, ANA_ObservacionCritico, ANA_RespuestaBiopsias_Critico As String
    Private ANA_IdBiopsias_Critico As Integer
    Private ANA_CerradoBiopsias_Critico As String = "NO"
    Private ANA_LeidoBiopsias_Critico As String = "NO"

    Private ANA_FechaLeidoBiopsias_Critico As String = Nothing
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Function BiopsiaCritico(ByVal id_biopsia As Integer)

        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Dim datos As Boolean = False
        consulta = "SELECT * FROM ANA_Biopsias_Critico WHERE ANA_IdBiopsias_Critico=" & id_biopsia
        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        If tablas.Rows.Count > 0 Then
            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.Set_ANA_FechaBiopsias_Critico(filas.Item("ANA_FechaBiopsias_Critico").ToString())
            Me.Set_GEN_IdUsuariosCritico(filas.Item("GEN_IdUsuarios").ToString())
            Me.Set_GEN_IdUsuarios_NotificadoBiopsias_Critico(filas.Item("GEN_IdUsuarios_NotificadoBiopsias_Critico").ToString())
            Me.Set_Ana_ObservacionCritico(filas.Item("ANA_ObservacionBiopsias_Critico").ToString())
            Me.Set_ANA_RespuestaBiopsias_Critico(filas.Item("ANA_RespuestaBiopsias_Critico").ToString())
            Me.Set_ANA_CerradoBiopsias_Critico(filas.Item("ANA_CerradoBiopsias_Critico").ToString())
            Me.Set_ANA_LeidoBiopsias_Critico(filas.Item("ANA_LeidoBiopsias_Critico").ToString())
            Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())
            Me.Set_ANA_IdBiopsias_Critico(filas.Item("ANA_IdBiopsias_Critico").ToString())
            datos = True

        End If

        Return datos

    End Function

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Function BiopsiaCritico_Id_Notificacion(ByVal id_notificacion As Integer)

        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Dim datos As Boolean = False
        consulta = "SELECT * FROM ANA_Biopsias_Critico WHERE ANA_IdBiopsias_Critico=" & id_notificacion
        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        If tablas.Rows.Count > 0 Then
            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.Set_ANA_IdBiopsias_Critico(filas.Item("ANA_IdBiopsias_Critico").ToString())
            Me.Set_ANA_FechaBiopsias_Critico(filas.Item("ANA_FechaBiopsias_Critico").ToString())
            Me.Set_GEN_IdUsuariosCritico(filas.Item("GEN_IdUsuarios").ToString())
            Me.Set_GEN_IdUsuarios_NotificadoBiopsias_Critico(filas.Item("GEN_IdUsuarios_NotificadoBiopsias_Critico").ToString())
            Me.Set_Ana_ObservacionCritico(filas.Item("ANA_ObservacionBiopsias_Critico").ToString())
            Me.Set_ANA_RespuestaBiopsias_Critico(filas.Item("ANA_RespuestaBiopsias_Critico").ToString())
            Me.Set_ANA_CerradoBiopsias_Critico(filas.Item("ANA_CerradoBiopsias_Critico").ToString())
            Me.Set_ANA_LeidoBiopsias_Critico(filas.Item("ANA_LeidoBiopsias_Critico").ToString())
            Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())

            Me.Set_ANA_FechaLeidoBiopsias_Critico(filas.Item("ANA_FechaLeidoBiopsias_Critico").ToString())

            datos = True

        End If

        Return datos

    End Function

#End Region


#Region "METODO CRUD"

#End Region

#Region "METODO GET"
    Public Function Get_Ana_IdBiopsiaCritico()
        Return Me.ANA_IdBiopsias_Critico
    End Function

    Public Function Get_ANA_FechaBiopsias_Critico()
        Return Me.ANA_FechaBiopsias_Critico
    End Function

    Public Function Get_GEN_IdUsuariosCritico()
        Return Me.GEN_IdUsuariosCritico
    End Function

    Public Function Get_GEN_IdUsuarios_NotificadoBiopsias_Critico()
        Return Me.GEN_IdUsuarios_NotificadoBiopsias_Critico
    End Function

    Public Function Get_Ana_ObservacionCritico()
        Return Me.ANA_ObservacionCritico
    End Function

    Public Function Get_ANA_RespuestaBiopsias_Critico()
        Return Me.ANA_RespuestaBiopsias_Critico
    End Function

    Public Function Get_ANA_CerradoBiopsias_Critico()
        Return Me.ANA_CerradoBiopsias_Critico
    End Function

    Public Function Get_ANA_LeidoBiopsias_Critico()
        Return Me.ANA_LeidoBiopsias_Critico
    End Function

    Public Function Get_ANA_FechaLeidoBiopsias_Critico()
        Return Me.ANA_FechaLeidoBiopsias_Critico
    End Function
#End Region

#Region "METODO SET"
    Public Sub Set_ANA_IdBiopsias_Critico(ByVal ANA_IdBiopsias_Critico)
        Me.ANA_IdBiopsias_Critico = ANA_IdBiopsias_Critico
    End Sub

    Public Sub Set_ANA_FechaBiopsias_Critico(ByVal ANA_FechaBiopsias_Critico)
        Me.ANA_FechaBiopsias_Critico = ANA_FechaBiopsias_Critico
    End Sub

    Public Sub Set_GEN_IdUsuariosCritico(ByVal GEN_IdUsuariosCritico)
        Me.GEN_IdUsuariosCritico = GEN_IdUsuariosCritico
    End Sub

    Public Sub Set_GEN_IdUsuarios_NotificadoBiopsias_Critico(ByVal GEN_IdUsuarios_NotificadoBiopsias_Critico)
        Me.GEN_IdUsuarios_NotificadoBiopsias_Critico = GEN_IdUsuarios_NotificadoBiopsias_Critico
    End Sub

    Public Sub Set_Ana_ObservacionCritico(ByVal ANA_observacionCritico)
        Me.ANA_ObservacionCritico = ANA_observacionCritico
    End Sub

    Public Sub Set_ANA_RespuestaBiopsias_Critico(ByVal ANA_RespuestaBiopsias_Critico)
        Me.ANA_RespuestaBiopsias_Critico = ANA_RespuestaBiopsias_Critico
    End Sub

    Public Sub Set_ANA_CerradoBiopsias_Critico(ByVal ANA_CerradoBiopsias_Critico)
        Me.ANA_CerradoBiopsias_Critico = ANA_CerradoBiopsias_Critico
    End Sub

    Public Sub Set_ANA_LeidoBiopsias_Critico(ByVal ANA_LeidoBiopsias_Critico)
        Me.ANA_LeidoBiopsias_Critico = ANA_LeidoBiopsias_Critico
    End Sub

    Public Sub Set_ANA_FechaLeidoBiopsias_Critico(ByVal ANA_FechaLeidoBiopsias_Critico)
        Me.ANA_FechaLeidoBiopsias_Critico = ANA_FechaLeidoBiopsias_Critico
    End Sub

    Public Function get_CuentaNotificaciones(ANA_idBiopsia As Integer) As String
        Dim sql As New StringBuilder()
        sql.AppendLine("select")
        sql.AppendLine("count(ana_idbiopsias_critico) AS Cantidad")
        sql.AppendLine("from")
        sql.AppendLine("ANA_Biopsias_Critico")
        sql.AppendLine("where ANA_IdBiopsia=" & ANA_idBiopsia)
        Return consulta_sql_devuelve_string(sql.ToString())
    End Function

    Public Sub Set_update_Critico_Biopsia_db()
        consulta = "UPDATE ANA_Biopsias_Critico SET " _
        & "ANA_RespuestaBiopsias_Critico ='" & Me.Get_ANA_RespuestaBiopsias_Critico() & "', " _
        & "ANA_CerradoBiopsias_Critico='" & Me.Get_ANA_CerradoBiopsias_Critico() & "', " _
        & "ANA_LeidoBiopsias_Critico='" & Me.Get_ANA_LeidoBiopsias_Critico() & "' " _
        & "WHERE  ANA_IdBiopsias_Critico=" & Me.Get_Ana_IdBiopsiaCritico()
        ejecuta_sql(consulta)
    End Sub

    Public Sub Set_CerrarNoficicaciondeCritico(id As Integer, resp As String, GEN_idUsuario As Integer)

        Dim bc As New BiopsiaCritico
        bc.BiopsiaCritico(id)
        bc.Set_ANA_CerradoBiopsias_Critico("SI")
        bc.Set_ANA_LeidoBiopsias_Critico("SI")
        bc.Set_ANA_RespuestaBiopsias_Critico(resp)
        bc.Set_update_Critico_Biopsia_db()

        Dim mov As New movimientos
        mov.Set_GEN_IdUsuarios(GEN_idUsuario)
        mov.Set_ANA_IdBiopsia(bc.Get_ANA_IdBiopsia)
        mov.Set_GEN_idTipo_Movimientos_Sistemas(189)
        mov.Set_ANA_DetalleMovimiento("Se cierra notificación de critico por usuario.")
        mov.Set_CrearNuevoMoviento()

    End Sub

#End Region

    ' *** FIN DE SET
    '==========================================================

End Class
