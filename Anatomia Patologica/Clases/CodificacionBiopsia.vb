﻿Public Class CodificacionBiopsia : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private ANA_Cod001Codificacion_Biopsia, ANA_Cod002Codificacion_Biopsia, ANA_Cod003Codificacion_Biopsia, ANA_Cod004Codificacion_Biopsia As Short
    Private ANA_Cod005Codificacion_Biopsia, ANA_Cod006Codificacion_Biopsia, ANA_Cod007Codificacion_Biopsia, ANA_Cod008Codificacion_Biopsia As Short
    Private ANA_FecCodificacion_Biopsia As DateTime
    Private ANA_IdCodificacion_Biopsia As Integer
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub CodificacionBiopsia(ByVal IdBiopsia As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Dim datos As Boolean = False
        Dim consulta As String = "SELECT * FROM  ANA_Codificacion_Biopsia WHERE ANA_IdBiopsia=" & IdBiopsia
        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        If tablas.Rows.Count > 0 Then
            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.ANA_FecCodificacion_Biopsia = filas.Item("ANA_FecCodificacion_Biopsia").ToString
            Me.ANA_IdCodificacion_Biopsia = filas.Item("ANA_IdCodificacion_Biopsia").ToString
            Me.Set_GEN_IdUsuarios(filas.Item("GEN_IdUsuarios").ToString())
            Me.ANA_Cod001Codificacion_Biopsia = filas.Item("ANA_Cod001Codificacion_Biopsia").ToString
            Me.ANA_Cod002Codificacion_Biopsia = filas.Item("ANA_Cod002Codificacion_Biopsia").ToString
            Me.ANA_Cod003Codificacion_Biopsia = filas.Item("ANA_Cod003Codificacion_Biopsia").ToString
            Me.ANA_Cod004Codificacion_Biopsia = filas.Item("ANA_Cod004Codificacion_Biopsia").ToString
            Me.ANA_Cod005Codificacion_Biopsia = filas.Item("ANA_Cod005Codificacion_Biopsia").ToString
            Me.ANA_Cod006Codificacion_Biopsia = filas.Item("ANA_Cod006Codificacion_Biopsia").ToString
            Me.ANA_Cod007Codificacion_Biopsia = filas.Item("ANA_Cod007Codificacion_Biopsia").ToString
            Me.ANA_Cod008Codificacion_Biopsia = filas.Item("ANA_Cod008Codificacion_Biopsia").ToString

        End If
    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region


#Region "METODOS GET"

    Public Function Get_ANA_FecCodificacion_Biopsia()
        Return Me.ANA_FecCodificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod001Codificacion_Biopsia()
        Return Me.ANA_Cod001Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod002Codificacion_Biopsia()
        Return Me.ANA_Cod002Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod003Codificacion_Biopsia()
        Return Me.ANA_Cod003Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod004Codificacion_Biopsia()
        Return Me.ANA_Cod004Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod005Codificacion_Biopsia()
        Return Me.ANA_Cod005Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod006Codificacion_Biopsia()
        Return Me.ANA_Cod006Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod007Codificacion_Biopsia()
        Return Me.ANA_Cod007Codificacion_Biopsia
    End Function

    Public Function Get_ANA_Cod008Codificacion_Biopsia()
        Return Me.ANA_Cod008Codificacion_Biopsia
    End Function

    Public Function Get_ANA_IdCodificacion_Biopsia()
        Return Me.ANA_IdCodificacion_Biopsia
    End Function


#End Region


#Region "METODO SET"

    Public Sub Set_ANA_FecCodificacion_Biopsia(ByVal ANA_FecCodificacion_Biopsia)
        Me.ANA_FecCodificacion_Biopsia = ANA_FecCodificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod001Codificacion_Biopsia(ByVal ANA_Cod001Codificacion_Biopsia)
        Me.ANA_Cod001Codificacion_Biopsia = ANA_Cod001Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod002Codificacion_Biopsia(ByVal ANA_Cod002Codificacion_Biopsia)
        Me.ANA_Cod002Codificacion_Biopsia = ANA_Cod002Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod003Codificacion_Biopsia(ByVal ANA_Cod003)
        Me.ANA_Cod003Codificacion_Biopsia = ANA_Cod003Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod004Codificacion_Biopsia(ByVal ANA_Cod004Codificacion_Biopsia)
        Me.ANA_Cod004Codificacion_Biopsia = ANA_Cod004Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod005Codificacion_Biopsia(ByVal ANA_Cod005Codificacion_Biopsia)
        Me.ANA_Cod005Codificacion_Biopsia = ANA_Cod005Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod006Codificacion_Biopsia(ByVal ANA_Cod006Codificacion_Biopsia)
        Me.ANA_Cod006Codificacion_Biopsia = ANA_Cod006Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod007Codificacion_Biopsia(ByVal ANA_Cod007Codificacion_Biopsia)
        Me.ANA_Cod007Codificacion_Biopsia = ANA_Cod007Codificacion_Biopsia
    End Sub

    Public Sub Set_ANA_Cod008Codificacion_Biopsia(ByVal ANA_Cod008Codificacion_Biopsia)
        Me.ANA_Cod008Codificacion_Biopsia = ANA_Cod008Codificacion_Biopsia
    End Sub

    Public Sub Set_Ana_ANA_IdCodificacion_Biopsia(ByVal ANA_IdCodificacion_Biopsia)
        Me.ANA_IdCodificacion_Biopsia = ANA_IdCodificacion_Biopsia
    End Sub


#End Region

#Region "NEGOCIOS"
    Public Sub Set_CrearCodificacion()
        Dim consulta As String = ""
        consulta = "INSERT INTO ANA_Codificacion_Biopsia (" _
        & "ANA_FecCodificacion_Biopsia, " _
        & "ANA_IdBiopsia, " _
        & "ANA_Cod001Codificacion_Biopsia, " _
        & "ANA_Cod002Codificacion_Biopsia, " _
        & "ANA_Cod003Codificacion_Biopsia, " _
        & "ANA_Cod004Codificacion_Biopsia, " _
        & "ANA_Cod005Codificacion_Biopsia, " _
        & "ANA_Cod006Codificacion_Biopsia, " _
        & "ANA_Cod007Codificacion_Biopsia, " _
        & "ANA_Cod008Codificacion_Biopsia, " _
        & "GEN_IdUsuarios) " _
        & "VALUES(" _
        & "'" & Me.ANA_FecCodificacion_Biopsia & "', " _
        & Me.Get_ANA_IdBiopsia() & ", " _
        & Me.ANA_Cod001Codificacion_Biopsia & ", " _
        & Me.ANA_Cod002Codificacion_Biopsia & ", " _
        & Me.ANA_Cod003Codificacion_Biopsia & ", " _
        & Me.ANA_Cod004Codificacion_Biopsia & ", " _
        & Me.ANA_Cod005Codificacion_Biopsia & ", " _
        & Me.ANA_Cod006Codificacion_Biopsia & ", " _
        & Me.ANA_Cod007Codificacion_Biopsia & ", " _
        & Me.ANA_Cod008Codificacion_Biopsia & ", " _
        & Me.Get_GEN_IdUsuarios() & ")"
        ejecuta_sql(consulta)

        consulta = "UPDATE ANA_Registro_Biopsias SET " _
        & "ANA_CodificadaEstadoRegistro_Biopsias = 'SI' " _
        & "WHERE (" _
        & "ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ")"
        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_UpdateCodificacion()
        Dim consulta As String = ""
        consulta = "UPDATE ANA_Codificacion_Biopsia " _
        & "SET ANA_FecCodificacion_Biopsia = '" & Me.ANA_FecCodificacion_Biopsia & "', " _
        & "ANA_Cod001Codificacion_Biopsia =" & Me.ANA_Cod001Codificacion_Biopsia & ", " _
        & "ANA_Cod002Codificacion_Biopsia =" & Me.ANA_Cod002Codificacion_Biopsia & ", " _
        & "ANA_Cod003Codificacion_Biopsia =" & Me.ANA_Cod003Codificacion_Biopsia & ", " _
        & "ANA_Cod004Codificacion_Biopsia =" & Me.ANA_Cod004Codificacion_Biopsia & ", " _
        & "ANA_Cod005Codificacion_Biopsia =" & Me.ANA_Cod005Codificacion_Biopsia & ", " _
        & "ANA_Cod006Codificacion_Biopsia =" & Me.ANA_Cod006Codificacion_Biopsia & ", " _
        & "ANA_Cod007Codificacion_Biopsia =" & Me.ANA_Cod007Codificacion_Biopsia & ", " _
        & "ANA_Cod008Codificacion_Biopsia =" & Me.ANA_Cod008Codificacion_Biopsia & ", " _
        & "GEN_IdUsuarios =" & Me.Get_GEN_IdUsuarios() & " " _
        & "WHERE " _
        & "ANA_IdCodificacion_Biopsia=" & Me.ANA_IdCodificacion_Biopsia
        ejecuta_sql(consulta)

        consulta = "UPDATE ANA_Registro_Biopsias SET " _
        & "ANA_CodificadaEstadoRegistro_Biopsias = 'SI' " _
        & "WHERE(" _
        & "ANA_IdBiopsia= " & Me.Get_ANA_IdBiopsia() & ")"
        ejecuta_sql(consulta)

    End Sub

#End Region

End Class
