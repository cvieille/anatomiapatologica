﻿Public Class ComboBoxAnatomia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    '*** DETALLE DE CATALOGO DE MUESTRAS DE BIOPSIAS. SE USA POR EJ. EN RECEPCION
    '==========================================================
    Public Sub DetalleCatalogoBiopsias_combobox(ByVal combo As DropDownList, ByVal CodMuestra As String)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable

        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)
        Dim consulta As String = "SELECT " _
        & "ANA_IdDetalleCatalogo_Muestras, ANA_DetalleCatalogo_Muestras " _
        & "FROM ANA_Detalle_Catalogo_Muestras " _
        & "WHERE ANA_IdCatalogo_Muestras = " & CodMuestra & " " _
        & "ORDER BY ANA_DetalleCatalogo_Muestras"
        tb = fun_sql.consulta_sql_datatable(consulta)
        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DetalleCatalogo_Muestras").ToString(), tb.Rows(i).Item("ANA_IdDetalleCatalogo_Muestras").ToString())
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub NEO_Lateralidad_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        lista = New ListItem("NO CORRESPONDE", "NO CORRESPONDE")
        combo.Items.Add(lista)

        lista = New ListItem("BILATERAL", "BILATERAL")
        combo.Items.Add(lista)

        lista = New ListItem("DERECHA", "DERECHA")
        combo.Items.Add(lista)

        lista = New ListItem("DESCONOCIDO", "DESCONOCIDO")
        combo.Items.Add(lista)

        lista = New ListItem("IZQUIERDA", "IZQUIERDA")
        combo.Items.Add(lista)

        combo.DataBind()

    End Sub

    Public Sub NEO_Morfologia_combobox(ByVal combo As DropDownList, ByVal CodComportamiento As String)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_DesMorfologia, ANA_IdMorfologia " _
        & "FROM ANA_morfologia " _
        & "WHERE (ANA_CodMorfologia LIKE '%" & CodComportamiento & "%')"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DesMorfologia").ToString(), tb.Rows(i).Item("ANA_IdMorfologia").ToString())
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Usuarios_notificaciones_combobox(ByVal combo As DropDownList)
        Try
            Dim lista As New ListItem
            combo.Items.Clear()

            Dim tb As New DataTable
            Dim consulta As String = "SELECT u.GEN_IdUsuarios, " _
            & "ISNULL(RTRIM(GEN_nombreUsuarios) + ' ','') + " _
            & "ISNULL(rtrim(GEN_apellido_paternoUsuarios) + ' ','') + " _
            & "ISNULL(rtrim(GEN_apellido_maternoUsuarios),'') as nombre " _
            & "FROM GEN_Usuarios AS u INNER JOIN " _
            & "GEN_Sist_Usuario AS su ON " _
            & "u.GEN_IdUsuarios = su.GEN_IdUsuarios INNER JOIN " _
            & "GEN_Perfil AS gp ON " _
            & "su.GEN_idPerfil = gp.GEN_idPerfil " _
            & "WHERE (su.GEN_estadoSist_Usuario = 'Activo') AND " _
            & "u.GEN_recibe_notificacionesUsuarios='SI' AND " _
            & "gp.GEN_IdSistemas=1"
            tb = fun_sql.consulta_sql_datatable(consulta)

            For i = 0 To (tb.Rows.Count - 1)
                lista = New ListItem(tb.Rows(i).Item("nombre").ToString(), tb.Rows(i).Item("GEN_IdUsuarios").ToString())
                combo.Items.Add(lista)
            Next
            combo.DataBind()

        Catch ex As Exception

        End Try

    End Sub

End Class