﻿Public Class Consultas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private consulta As String = ""
    Private ReadOnly ANA_UrlProtocolo As String = ""
    Private cant_registros As Integer = 0
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Public Sub Set_Cant_Registros(ByVal cant_registros)
        Me.cant_registros = cant_registros
    End Sub

    Public Function Get_Cant_Registros()
        Return Me.cant_registros
    End Function

    ' *** consultas sobre registro biopsia pra perfil 
    '==========================================================
    Public Function Ds_con_clinicos(ByVal RutPaciente As String, ByVal NUI As String, ByVal NomPaciente As String,
                                    ByVal ApePac1 As String, ByVal ApePac2 As String, ByVal idprofesional As String,
                                    ByVal fec_recepcionInicio As String, ByVal fec_recepcionFinal As String)
        ' consulta sql para modulo de consultas clinicos
        '====================================================================================================
        Dim sql As New StringBuilder()

        sql.AppendLine("select")
        sql.AppendLine("ANA_Registro_Biopsias.ANA_IdBiopsia,")
        sql.AppendLine("RTRIM(CONVERT(char(10), ANA_Registro_Biopsias.ANA_NumeroRegistro_Biopsias)) + '-' +")
        sql.AppendLine("RTRIM(CONVERT(char(10), ANA_Registro_Biopsias.ANA_AñoRegistro_Biopsias)) AS n_biopsia,")
        sql.AppendLine("ANA_Registro_Biopsias.ANA_fec_RecepcionRegistro_Biopsias,")
        sql.AppendLine("ISNULL(GEN_Paciente.GEN_numero_documentoPaciente + '-' + GEN_Paciente.GEN_digitoPaciente,")
        sql.AppendLine("GEN_Paciente.GEN_numero_documentoPaciente) As NumDocumentoPaciente,")
        sql.AppendLine("GEN_Paciente.GEN_nombrePaciente + ' ' + ISNULL(GEN_Paciente.GEN_ape_paternoPaciente + ' ', '') +")
        sql.AppendLine("ISNULL(GEN_Paciente.GEN_ape_maternoPaciente + ' ','') AS GEN_nombrePaciente,")
        sql.AppendLine("RTRIM(GEN_PersonasPatologo.GEN_nombrePersonas) + ' ' + ")
        sql.AppendLine("RTRIM(GEN_PersonasPatologo.GEN_apellido_paternoPersonas) +")
        sql.AppendLine("ISNULL(' ' + GEN_PersonasPatologo.GEN_apellido_maternoPersonas,'') AS Gen_nombrePatologo,")


        sql.AppendLine("RTRIM(GEN_PersonasTecnologo.GEN_nombrePersonas) + ' ' + ")
        sql.AppendLine("RTRIM(GEN_PersonasTecnologo.GEN_apellido_paternoPersonas) +")
        sql.AppendLine("ISNULL(' ' + GEN_PersonasTecnologo.GEN_apellido_maternoPersonas,'') AS Gen_nombreTecnologo,")

        sql.AppendLine("ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas,")
        sql.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas,")
        sql.AppendLine("GEN_Servicio.GEN_nombreServicio,")
        sql.AppendLine("ANA_Registro_Biopsias.ANA_fecValidaBiopsia")
        sql.AppendLine("FROM")
        sql.AppendLine("ANA_Registro_Biopsias INNER JOIN")
        sql.AppendLine("GEN_Paciente ON ANA_Registro_Biopsias.GEN_idPaciente = GEN_Paciente.GEN_idPaciente INNER JOIN")
        sql.AppendLine("GEN_Tipo_Estados_Sistemas ON")
        sql.AppendLine("ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas = GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas INNER JOIN")
        sql.AppendLine("GEN_Servicio ON GEN_Servicio.GEN_idServicio = ANA_Registro_Biopsias.GEN_IdServicioDestino LEFT OUTER JOIN")
        sql.AppendLine("GEN_Personas AS GEN_PersonasTecnologo INNER JOIN")
        sql.AppendLine("GEN_Usuarios AS GEN_UsuariosTecnologo ON GEN_PersonasTecnologo.GEN_idPersonas = GEN_UsuariosTecnologo.GEN_idPersonas ON")
        sql.AppendLine("GEN_UsuariosTecnologo.GEN_idUsuarios = ANA_Registro_Biopsias.GEN_IdUsuarioTecnologo LEFT OUTER JOIN")
        sql.AppendLine("GEN_Personas AS GEN_PersonasPatologo INNER JOIN")
        sql.AppendLine("GEN_Usuarios AS GEN_UsuariosPatologo ON GEN_PersonasPatologo.GEN_idPersonas = GEN_UsuariosPatologo.GEN_idPersonas ON")
        sql.AppendLine("GEN_UsuariosPatologo.GEN_idUsuarios = ANA_Registro_Biopsias.GEN_IdUsuarioPatologo")
        sql.AppendLine("WHERE (ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas != 48) ")

        If idprofesional <> "" Then sql.AppendLine("and ANA_Registro_Biopsias.ANA_IdMedSolicitaBiopsia = " & idprofesional)

        If (fec_recepcionInicio <> "") Then
            sql.AppendLine("and ANA_Registro_Biopsias.ANA_fec_RecepcionRegistro_Biopsias BETWEEN '" & fec_recepcionInicio & "' and '" & fec_recepcionFinal & "'")
        End If


        If RutPaciente <> "" Then
            sql.AppendLine("AND GEN_Paciente.GEN_numero_documentoPaciente LIKE '" & RutPaciente & "%'")
        End If
        If Not (String.IsNullOrEmpty(NUI) Or NUI = "0") Then
            sql.AppendLine("AND GEN_nuiPaciente=" & NUI)
        End If

        If NomPaciente <> "" Then
            sql.AppendLine("AND GEN_Paciente.GEN_NombrePaciente LIKE '" & NomPaciente & "%'")
        End If

        If ApePac1 <> "" Then
            sql.AppendLine("AND GEN_Paciente.GEN_ape_paternoPaciente like '" & ApePac1 & "%' ")
        End If

        If ApePac2 <> "" Then
            sql.AppendLine("AND GEN_Paciente.GEN_ape_maternoPaciente Like '" & ApePac2 & "%'")
        End If

        Return sql.ToString()

    End Function


    ' *** consultas sobre registro biopsia
    '==========================================================
    Public Function Ds_con_Notificacionclinicos(ByVal id_notificado As Integer, ByVal RutPaciente As String, ByVal NUI As String, ByVal NomPaciente As String, ByVal ApePac1 As String, ByVal ApePac2 As String)
        ' *** consulta sql para modulo de consultas clinicos
        '==========================================================
        consulta = "SELECT " _
        & "arb.ANA_IdBiopsia, " _
        & "RTRIM(CONVERT(char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia, " _
        & "vbp.nombre_patologo, " _
        & "ANA_EstadoRegistro_Biopsias AS estado_biopsia, " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias, " _
        & "abc.ANA_IdBiopsias_Critico, " _
        & "ISNULL(RTRIM(p.GEN_NombrePaciente),'') + ' ' + ISNULL(RTRIM(p.GEN_ape_paternoPaciente), '') + ' ' + ISNULL(RTRIM(p.GEN_ape_maternoPaciente),'') AS Paciente, " _
        & "arb.ANA_IdBiopsia AS id_biopsia, " _
        & "'Ver' AS reporte " _
        & "FROM " _
        & "ANA_Registro_Biopsias as arb INNER JOIN " _
        & "ANA_vi_BiopsiasPatologo AS vbp ON " _
        & "arb.ANA_IdBiopsia = vbp.ANA_IdBiopsia INNER JOIN " _
        & "ANA_Biopsias_Critico AS abc ON " _
        & "arb.ANA_IdBiopsia = abc.ANA_IdBiopsia INNER JOIN " _
        & "GEN_Paciente AS p ON " _
        & "arb.GEN_idPaciente = p.GEN_IdPaciente And " _
        & "abc.GEN_IdUsuarios_NotificadoBiopsias_Critico = " & id_notificado & " " _
        & "WHERE " _
        & "abc.ANA_CerradoBiopsias_Critico='NO' "

        If RutPaciente <> "" Then
            consulta &= " AND p.GEN_numero_documentoPaciente LIKE '" & RutPaciente & "%' "
        End If
        If NUI <> "" Then
            consulta &= " AND GEN_nuiPaciente=" & NUI
        End If

        If NomPaciente <> "" Or ApePac1 <> "" Or ApePac2 <> "" Then
            consulta &= " AND GEN_NombrePaciente LIKE '" & NomPaciente & "%' AND " _
            & "GEN_ape_paternoPaciente like '" & ApePac1 & "%' " _
            & "and GEN_ape_maternoPaciente like '" & ApePac2 & "%' "
        End If

        Return consulta

    End Function



    ' *** CONSULTA QUE TRAE LAS INMUNO PENDIENTES. SI GEN_IdUsuarios ES 0, TRAE TODO LO PENDIENTE, SI NO FILTRA POR ID
    ' *** SE USA EN PRINCIPAL.ASPX
    '==========================================================
    Public Function Ds_con_InmunosPendientes(ByVal GEN_IdUsuarios As Integer)
        Me.consulta = "SELECT        AIHB.ANA_FecInmuno_Histoquimica_Biopsia, c.ANA_NomCortes_Muestras, AIH.ANA_NomInmunoHistoquimica, u.GEN_loginUsuarios, AIHB.ANA_IdInmuno_Histoquimica_Biopsia, AIHB.ANA_IdBiopsia, 
                         GEN_UsuarioTecnologo.GEN_idUsuarios AS GEN_ANA_IdTecnologo, RTRIM(GEN_UsuarioTecnologo.GEN_nombreUsuarios) + ' ' + RTRIM(GEN_UsuarioTecnologo.GEN_apellido_paternoUsuarios) 
                         + ' ' + RTRIM(GEN_UsuarioTecnologo.GEN_apellido_maternoUsuarios) AS ANA_NombreTecnologo, c.ANA_SolCortes_Muestras, c.GEN_idTipo_Estados_Sistemas, 
                         GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas
FROM            GEN_Usuarios AS u INNER JOIN
                         ANA_Inmuno_Histoquimica_Biopsia AS AIHB ON u.GEN_idUsuarios = AIHB.GEN_IdUsuarios INNER JOIN
                         ANA_Cortes_Muestras AS c ON AIHB.ANA_IdCortes_Muestras = c.ANA_IdCortes_Muestras AND AIHB.ANA_IdBiopsia = c.ANA_IdBiopsia INNER JOIN
                         ANA_Inmuno_Histoquimica AS AIH ON AIHB.ANA_idInmunoHistoquimica = AIH.ANA_idInmunoHistoquimica INNER JOIN
                         ANA_Registro_Biopsias ON AIHB.ANA_IdBiopsia = ANA_Registro_Biopsias.ANA_IdBiopsia INNER JOIN
                         GEN_Tipo_Estados_Sistemas ON c.GEN_idTipo_Estados_Sistemas = GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas LEFT OUTER JOIN
                         GEN_Usuarios AS GEN_UsuarioTecnologo ON ANA_Registro_Biopsias.GEN_IdUsuarioTecnologo = GEN_UsuarioTecnologo.GEN_idUsuarios " _
        & "WHERE " _
        & "(AIHB.ANA_EstInmuno_Histoquimica_Biopsia = 'Solicitada') "

        If GEN_IdUsuarios > 0 Then Me.consulta = Me.consulta & " AND GEN_UsuarioTecnologo.GEN_IdUsuarios=" & GEN_IdUsuarios

        Return Me.consulta

    End Function

    ' *** DEVUELVE REGISTROS DE NOTIFICACIONES DE CRITICO EN INFORME MEDICO.
    '==========================================================
    Public Function Ds_con_Notificacion_informe(ByVal id_biopsia As Integer)
        consulta = "SELECT " _
        & "abc.ANA_IdBiopsias_Critico, " _
        & "abc.ANA_FechaBiopsias_Critico as Fecha, " _
        & "abc.ANA_ObservacionBiopsias_Critico as Observacion, " _
        & "abc.ANA_LeidoBiopsias_Critico, " _
        & "ISNULL(rtrim(u1.GEN_nombreUsuarios),'') + ' ' + ISNULL(rtrim(u1.GEN_apellido_paternoUsuarios), '')  + ' ' + ISNULL(rtrim(u1.GEN_apellido_maternoUsuarios),'') AS Patologo, " _
        & "ISNULL(rtrim(u2.GEN_nombreUsuarios),'') + ' ' + ISNULL(rtrim(u2.GEN_apellido_paternoUsuarios),'')  + ' ' + ISNULL(rtrim(u2.GEN_apellido_maternoUsuarios),'') AS Notificado, " _
        & "abc.ANA_RespuestaBiopsias_Critico " _
        & "FROM ANA_Biopsias_Critico AS abc INNER JOIN " _
        & "GEN_Usuarios AS u1 ON " _
        & "abc.GEN_idUsuarios = u1.GEN_idUsuarios INNER JOIN " _
        & "GEN_Usuarios AS u2 ON " _
        & "abc.GEN_IdUsuarios_NotificadoBiopsias_Critico = u2.GEN_idUsuarios " _
        & "WHERE abc.ANA_Idbiopsia= " & id_biopsia & " " _
        & "ORDER BY abc.ANA_FechaBiopsias_Critico"

        Return consulta

    End Function

    ' *** DEVUELVE REGISTROS DE ANA_Cortes_Muestras POR BIOPSIA
    ' *** SE USA EN MODULO DE CORTES Y RESUMEN BIOPSIA.
    '==========================================================
    Public Function Ds_con_Cortes_Muestras_biopsia(ByVal id_biopsia As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("select ANA_IdCortes_Muestras,")
        sql.AppendLine("ANA_NomCortes_Muestras,")
        sql.AppendLine("ANA_FecCortes_Muestras,")
        sql.AppendLine("'DR(A) ' + GEN_Personas_Patologo.GEN_nombrePersonas + ' ' + GEN_Personas_Patologo.GEN_apellido_paternoPersonas  + ' ' + isnull(GEN_Personas_Patologo.GEN_apellido_maternoPersonas,'') as nombre_patologo,")
        sql.AppendLine("GEN_Usuarios_CreaCasete.GEN_loginUsuarios,")
        sql.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas")
        sql.AppendLine("from ANA_Cortes_Muestras")
        sql.AppendLine("inner join GEN_Usuarios as gen_GEN_Usuarios_patologo on gen_GEN_Usuarios_patologo.GEN_idUsuarios = ANA_Cortes_Muestras.GEN_IdUsuarioPatologo")
        sql.AppendLine("inner join GEN_Personas as GEN_Personas_Patologo on GEN_Personas_Patologo.GEN_idPersonas=gen_GEN_Usuarios_patologo.GEN_idPersonas inner join GEN_Usuarios AS GEN_Usuarios_CreaCasete on GEN_Usuarios_CreaCasete.GEN_idUsuarios = ANA_Cortes_Muestras.GEN_IdUsuarios")
        sql.AppendLine("inner join GEN_Tipo_Estados_Sistemas on GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas = ANA_Cortes_Muestras.GEN_idTipo_Estados_Sistemas")
        sql.AppendLine("where ANA_IdBiopsia=" & id_biopsia)
        sql.AppendLine("order by ANA_Cortes_Muestras.ANA_IdCortes_Muestras")

        Return sql.ToString()
    End Function


    Public Function GetCortesSolicitadosaBodega()
        consulta = "select * from ANA_Cortes_Muestras where ANA_Cortes_Muestras.ANA_SolCortes_Muestras = 'SI' AND ANA_Cortes_Muestras.ANA_estadoCortes_Muestras='Activo'"
        Return consulta

    End Function
    ' *** MUESTRA EN GRILLA LOS CASETE QUE EL AUXILIAR DEBE ALMACENAR
    '==========================================================
    Public Function GetCaseteparaAlmacenar()
        'busca "para almacenar" y "Despachado para interconsulta"
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("ANA_Cortes_Muestras.ANA_NomCortes_Muestras, 
                         ANA_Cortes_Muestras.ANA_FecCortes_Muestras, GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas, ANA_Cortes_Muestras.ANA_SolporCortes_Muestras, ANA_Cortes_Muestras.ANA_IdBiopsia, 
                         ANA_Cortes_Muestras.ANA_IdCortes_Muestras
FROM            ANA_Cortes_Muestras INNER JOIN
                         ANA_Registro_Biopsias ON ANA_Registro_Biopsias.ANA_IdBiopsia = ANA_Cortes_Muestras.ANA_IdBiopsia INNER JOIN
                         GEN_Tipo_Estados_Sistemas ON ANA_Cortes_Muestras.GEN_idTipo_Estados_Sistemas = GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas
WHERE        (ANA_Cortes_Muestras.GEN_idTipo_Estados_Sistemas in ( 55, 57)) AND (ANA_Cortes_Muestras.ANA_estadoCortes_Muestras = 'Activo') AND (ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas <> 48)
ORDER BY ANA_Cortes_Muestras.ANA_IdBiopsia")
        Return sql.ToString()

    End Function

    ' *** MUESTRA TODAS LAS LAMINAS POR UN ID DE BIOPSIA
    '==========================================================
    Public Function Ds_con_Laminas_Biopsia(ByVal idBiopsia As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("ANA_Laminas.ANA_IdLamina,")
        sql.AppendLine("ANA_Laminas.ANA_NomLamina,")
        sql.AppendLine("ANA_Laminas.ANA_IdBiopsia,")
        sql.AppendLine("ANA_Laminas.ANA_FecLamina,")
        sql.AppendLine("ANA_Laminas.GEN_idTipo_Estados_Sistemas,")
        sql.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas")
        sql.AppendLine("FROM")
        sql.AppendLine("ANA_Laminas INNER JOIN")
        sql.AppendLine("GEN_Tipo_Estados_Sistemas ON")
        sql.AppendLine("ANA_Laminas.GEN_idTipo_Estados_Sistemas = GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas")
        sql.AppendLine("WHERE")
        sql.AppendLine("ANA_IdBiopsia = " & idBiopsia)
        sql.AppendLine("ORDER BY ANA_FecLamina")

        Return sql.ToString()

    End Function

    ' *** CONSULTA MUESTRAS QUE TENGAN CORTES O LAMINAS PARA INTERCONSULTA Y LAS MUESTRA EN MOVIMIENTOS DE SECRETARIA
    '==========================================================
    Public Function Ds_con_Biopsias_para_Interconsulta()
        Dim sql As New StringBuilder()
        sql.AppendLine("Select")
        sql.AppendLine("RTRIM(CONVERT(Char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia,")
        sql.AppendLine("arb.ANA_OrganoBiopsia, arb.ANA_GesRegistro_Biopsias,")
        sql.AppendLine("arb.ANA_IdBiopsia,")
        sql.AppendLine("GEN_Personas.GEN_nombrePersonas + ' ' + ISNULL(GEN_Personas.GEN_apellido_paternoPersonas, '') + ' ' + ISNULL(GEN_Personas.GEN_apellido_maternoPersonas, '') AS nombre_tecnologo")
        sql.AppendLine("FROM")
        sql.AppendLine("ANA_Registro_Biopsias AS arb INNER JOIN")
        sql.AppendLine("GEN_Usuarios ON arb.GEN_IdUsuarioTecnologo = GEN_Usuarios.GEN_idUsuarios INNER JOIN")
        sql.AppendLine("GEN_Personas ON GEN_Personas.GEN_idPersonas = GEN_Usuarios.GEN_idPersonas")
        sql.AppendLine("WHERE")
        sql.AppendLine("(arb.ANA_IdBiopsia IN
                             (SELECT        ANA_IdBiopsia
                               FROM            ANA_Cortes_Muestras AS c
                               WHERE        (GEN_idTipo_Estados_Sistemas  = 59))) OR
                         (arb.ANA_IdBiopsia IN
                             (SELECT        ANA_IdBiopsia
                               FROM            ANA_Laminas
                               WHERE        (GEN_idTipo_Estados_Sistemas  = 59)))")
        sql.AppendLine("ORDER BY arb.ANA_IdBiopsia, arb.ANA_NumeroRegistro_Biopsias")

        Return sql.ToString()

    End Function

    ' *** CONSULTA BIOPSIAS PARA DESPACHAR EN MOVIMIENTOS DE SECRETARIA
    '==========================================================
    Public Function Ds_con_Biopsias_para_Despachar()
        consulta = "SELECT " _
        & "arb.ANA_IdBiopsia, " _
        & "RTRIM(CONVERT(char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia, " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias, " _
        & "arb.ANA_OrganoBiopsia, " _
        & "arb.ANA_GesRegistro_Biopsias, " _
        & "GSD.GEN_nombreServicio AS serv_destino, " _
        & "GSO.GEN_nombreServicio AS serv_origen " _
        & "FROM " _
        & "ANA_Registro_Biopsias as arb INNER JOIN " _
        & "GEN_Servicio AS GSD ON " _
        & "arb.GEN_IdServicioDestino = GSD.GEN_idServicio INNER JOIN " _
        & "GEN_Servicio AS GSO ON " _
        & "arb.GEN_IdServicioOrigen = GSO.GEN_idServicio " _
        & "WHERE " _
        & "(arb.ANA_DespachadaBiopsia = 'NO') AND " _
        & "(arb.ANA_AnuladaBiopsia = 'NO') AND " _
        & "(arb.ANA_ValidadaBiopsia = 'SI') " _
        & "ORDER BY arb.ANA_NumeroRegistro_Biopsias"

        Return consulta

    End Function

    Public Function GetLaminasSolicitasaBodega()
        Dim sql As New StringBuilder()
        sql.AppendLine("select * from ANA_Laminas where ANA_Laminas.ANA_SolLamina='SI'")
        Return sql.ToString()
    End Function

    '*** CONSULTA PARA IMPRIMIR LISTA DE NEOPLACIA
    Public Function Ds_con_Biopsias_Neoplasia(ByVal FecInicio As String, ByVal FecFinal As String)
        consulta = "SELECT " _
        & "arb.ANA_IdBiopsia, " _
        & "RTRIM(arb.ANA_NumeroRegistro_Biopsias) + '-' + RTRIM(arb.ANA_AñoRegistro_Biopsias) AS ANA_NumeroRegistro_Biopsias, " _
        & "arb.ANA_EstadoRegistro_Biopsias, " _
        & "arb.ANA_CriticoBiopsia, " _
        & "nb.ANA_IdNeoplasia_Biopsia, " _
        & "nb.ANA_IdBiopsia, " _
        & "nb.Lateralidad, " _
        & "nb.Her2, " _
        & "arb.ANA_fec_IngresoRegistro_Biopsias,  p.GEN_nuiPaciente, " _
        & "ISNULL(RTRIM(p.GEN_nombrePaciente), '') + ' ' " _
        & "+ ISNULL(RTRIM(p.GEN_ape_paternoPaciente),'') + ' ' " _
        & "+ ISNULL(RTRIM(p.GEN_ape_maternoPaciente), '') AS NomPaciente, " _
        & "ISNULL(RTRIM(p.GEN_numero_documentoPaciente), '') + '-' + ISNULL(p.GEN_digitoPaciente, '') AS RutPaciente, " _
        & "arb.ANA_OrganoBiopsia, " _
        & "am.ANA_CodMorfologia, " _
        & "am.ANA_DesMorfologia, " _
        & "ac.ANA_CodigoComportamiento, " _
        & "ac.ANA_DescripcionComportamiento, " _
        & "ag.ANA_DescGrado_Diferenciacion, " _
        & "at.ANA_CodTopografia, " _
        & "at.ANA_DesTopografia, " _
        & "oc.ANA_IdOrganoCie, " _
        & "oc.ANA_DesOrganoCie " _
        & "FROM " _
        & "ANA_Registro_Biopsias as arb INNER JOIN " _
        & "ANA_Neoplasia_Biopsia AS nb ON " _
        & "arb.ANA_IdBiopsia = nb.ANA_IdBiopsia INNER JOIN " _
        & "GEN_Paciente AS p ON " _
        & "arb.gen_idpaciente = p.gen_idpaciente LEFT OUTER JOIN " _
        & "ANA_Topografia AS at ON " _
        & "nb.ANA_IdTopografia = at.ANA_IdTopografia LEFT OUTER JOIN " _
        & "ANA_Comportamiento AS ac ON " _
        & "nb.ANA_IdComportamiento = ac.ANA_IdComportamiento LEFT OUTER JOIN " _
        & "ANA_Organo_Cie AS oc ON " _
        & "nb.ANA_IdOrganoCie = oc.ANA_IdOrganoCie LEFT OUTER JOIN " _
        & "ANA_morfologia AS am ON " _
        & "nb.ANA_IdMorfologia = am.ANA_IdMorfologia LEFT OUTER JOIN " _
        & "ANA_Grado_Diferenciacion AS ag ON " _
        & "nb.ANA_IdGrado_Diferenciacion = ag.ANA_IdGrado_Diferenciacion " _
        & "WHERE " _
        & "(arb.ANA_ValidadaBiopsia = 'SI') " _
        & "and ANA_fecValidaBiopsia>='" & FecInicio & "' AND " _
        & "ANA_fecValidaBiopsia<='" & FecFinal & "'"

        Return consulta

    End Function


    '*** CONSULTA PARA MOSTRAR BIOPSIAS CON INTERCONSULTAS ABIERTAS
    Public Function Ds_con_Biopsia_Interconsulta(ByVal IdTecnologo As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT DISTINCT")
        sql.AppendLine("RTRIM(arb.ANA_NumeroRegistro_Biopsias) + '-' + RTRIM(arb.ANA_AñoRegistro_Biopsias) AS ANA_NumBiopsia,")
        sql.AppendLine("arb.ANA_fec_RecepcionRegistro_Biopsias, arb.ANA_OrganoBiopsia, arb.ANA_IdBiopsia,")
        sql.AppendLine("ISNULL(GEN_Personas.GEN_nombrePersonas, '') + ' ' + ISNULL(GEN_Personas.GEN_apellido_paternoPersonas, '') + ' ' + ISNULL(GEN_Personas.GEN_apellido_maternoPersonas, '') AS nombre_tecnologo")
        sql.AppendLine(", arb.ANA_EstadoRegistro_Biopsias")
        sql.AppendLine("FROM")
        sql.AppendLine("ANA_Registro_Biopsias AS arb INNER JOIN")
        sql.AppendLine("GEN_Usuarios AS u ON u.GEN_idUsuarios = arb.GEN_IdUsuarioTecnologo INNER JOIN")
        sql.AppendLine("GEN_Personas ON u.GEN_idPersonas = GEN_Personas.GEN_idPersonas")
        sql.AppendLine("WHERE")
        sql.AppendLine("(arb.ANA_IdBiopsia IN")
        sql.AppendLine("(SELECT DISTINCT ANA_IdBiopsia")
        sql.AppendLine("FROM")
        sql.AppendLine("ANA_Cortes_Muestras AS c")
        sql.AppendLine("WHERE 
                                                         (GEN_idTipo_Estados_Sistemas = 59) 
                                                         )) OR
                         (arb.ANA_IdBiopsia IN
                             (SELECT DISTINCT ANA_IdBiopsia
                               FROM            ANA_Laminas AS l
                               WHERE        (GEN_idTipo_Estados_Sistemas = 59)))")

        If IdTecnologo > 0 Then sql.AppendLine("AND arb.GEN_IdUsuarioTecnologo=" & IdTecnologo)

        Return sql.ToString()

    End Function




    Public Function Ds_con_InfTipoMuestra(ByVal FechaInicio As String, ByVal FechaFinal As String, ByVal IdCatalogo As Long, ByVal IdDetalleCatalogo As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("Select ANA_Registro_Biopsias.ANA_IdBiopsia,")
        sql.AppendLine("RTrim(Convert(Char(10), ANA_Registro_Biopsias.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), ANA_Registro_Biopsias.ANA_AñoRegistro_Biopsias)) AS n_biopsia,")
        sql.AppendLine("ANA_Catalogo_Muestras.ANA_DetalleCatalogo_Muestras as ANA_Catalogo_Muestras,")
        sql.AppendLine("ANA_Detalle_Catalogo_Muestras.ANA_DetalleCatalogo_Muestras,")
        sql.AppendLine("(select count(ANA_Cortes_Muestras.ANA_IdCortes_Muestras) from ANA_Cortes_Muestras where ANA_Cortes_Muestras.ANA_IdBiopsia = ANA_Registro_Biopsias.ANA_IdBiopsia) as cortes,")
        sql.AppendLine("(select count( ANA_Laminas.ANA_IdLamina) from ANA_Laminas where ANA_Laminas.ANA_IdBiopsia = ANA_Registro_Biopsias.ANA_IdBiopsia) as laminas,")
        sql.AppendLine("(select count( ANA_Tecnica_Biopsia.ANA_IdTecnica) from ANA_Tecnica_Biopsia where ANA_Tecnica_Biopsia.ANA_IdBiopsia = ANA_Registro_Biopsias.ANA_IdBiopsia) as tecnicas,")
        sql.AppendLine("(select count( ANA_Inmuno_Histoquimica_Biopsia.ANA_IdInmuno_Histoquimica_Biopsia) from ANA_Inmuno_Histoquimica_Biopsia where ANA_Inmuno_Histoquimica_Biopsia.ANA_IdBiopsia = ANA_Registro_Biopsias.ANA_IdBiopsia) as inmuno")
        sql.AppendLine("From ANA_Registro_Biopsias inner Join ANA_Catalogo_Muestras")
        sql.AppendLine("On ANA_Registro_Biopsias.ANA_IdCatalogo_Muestras = ANA_Catalogo_Muestras.ANA_IdCatalogo_Muestras")
        sql.AppendLine("inner Join ANA_Detalle_Catalogo_Muestras")
        sql.AppendLine("on ANA_Catalogo_Muestras.ANA_IdCatalogo_Muestras = ANA_Detalle_Catalogo_Muestras.ANA_IdCatalogo_Muestras")
        sql.AppendLine("And ANA_Registro_Biopsias.ANA_IdDetalleCatalogo_Muestras = ANA_Detalle_Catalogo_Muestras.ANA_IdDetalleCatalogo_Muestras")
        sql.AppendLine("WHERE")
        sql.AppendLine("(ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas != 48) AND")
        sql.AppendLine("ANA_Registro_Biopsias.ANA_fec_RecepcionRegistro_Biopsias>='" & FechaInicio & "' AND ")
        sql.AppendLine("ANA_Registro_Biopsias.ANA_fec_RecepcionRegistro_Biopsias<='" & FechaFinal & "' ")

        If IdCatalogo > 0 Then
            sql.AppendLine("AND (ANA_Registro_Biopsias.ANA_IdCatalogo_Muestras = " & IdCatalogo & ")")
        End If

        If IdDetalleCatalogo > 0 Then
            sql.AppendLine("AND (ANA_Registro_Biopsias.ANA_IdDetalleCatalogo_Muestras = " & IdDetalleCatalogo & ")")
        End If

        Return sql.ToString()

    End Function

    Public Function Ds_con_InfWinsig(ByVal FechaInicio As String, ByVal FechaFinal As String)
        Dim consulta As String = ""

        consulta = "SELECT " _
        & "COUNT(arb.ANA_IdBiopsia) AS tot_biopsia, " _
        & "s.GEN_nombreServicio AS serv_origen, " _
        & "s.GEN_dependenciaServicio AS origen, " _
        & "arb.GEN_IdServicioOrigen AS cod_origen, " _
        & "s.GEN_codwinsigServicio " _
        & "FROM ANA_Registro_Biopsias as arb LEFT OUTER JOIN " _
        & "GEN_Servicio AS s ON " _
        & "arb.GEN_IdServicioOrigen = s.GEN_idServicio " _
        & "WHERE " _
        & "(arb.ANA_fec_RecepcionRegistro_Biopsias >='" & FechaInicio & "') AND " _
        & "(arb.ANA_fec_RecepcionRegistro_Biopsias <= '" & FechaFinal & "') AND " _
        & "(arb.ANA_AnuladaBiopsia = 'NO') " _
        & "GROUP BY " _
        & "s.GEN_nombreServicio, " _
        & "s.GEN_dependenciaServicio, " _
        & "arb.GEN_IdServicioOrigen, " _
        & "s.GEN_codwinsigServicio " _
        & "ORDER BY " _
        & "origen, " _
        & "serv_origen"

        Return consulta

    End Function

    Public Function Ds_con_DetalleWinsig(ByVal IdServicio As Short, ByVal FechaInicio As String, ByVal FechaFinal As String)
        Dim consulta = "SELECT " _
        & "RTRIM(CONVERT(char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), " _
        & "arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia, " _
        & "arb.ANA_fec_IngresoRegistro_Biopsias as Ingreso, " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias as Recepcion, " _
        & "arb.ANA_fecDespBiopsia as Despacho, " _
        & "arb.ANA_fecValidaBiopsia as Valida, " _
        & "arb.ANA_EstadoRegistro_Biopsias AS Estado, " _
        & "s.GEN_nombreServicio AS serv_origen, " _
        & "FROM ANA_Registro_Biopsias as arb LEFT OUTER JOIN " _
        & "gen_Servicio AS s ON " _
        & "AND arb.GEN_IdServicioOrigen = s.GEN_idServicio " _
        & "WHERE ANA_AnuladaBiopsia='NO' AND " _
        & "s.GEN_idServicio=" & IdServicio & " AND " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias>='" & FechaInicio & "' AND " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias<='" & FechaFinal & "' " _
        & "GROUP BY s.GEN_nombreServicio, " _
        & "arb.ANA_IdBiopsia, " _
        & "arb.ANA_fecDespBiopsia, " _
        & "arb.ANA_fec_IngresoRegistro_Biopsias, " _
        & "arb.ANA_fec_RecepcionRegistro_Biopsias, " _
        & "arb.ANA_fecValidaBiopsia, " _
        & "arb.ANA_NumeroRegistro_Biopsias, " _
        & "arb.ANA_AñoRegistro_Biopsias, " _
        & "ANA_EstadoRegistro_Biopsias " _
        & "ORDER BY  S.GEN_nombreServicio ASC "

        Return consulta

    End Function

    Public Function Ds_con_InfConsolidado(ByVal FechaInicio As String, ByVal FechaFinal As String)
        Dim sql As New StringBuilder()

        sql.AppendLine("SELECT")
        sql.AppendLine("SUM(acb.ANA_Cod001Codificacion_Biopsia) AS '001',")
        sql.AppendLine("SUM(acb.ANA_Cod002Codificacion_Biopsia) AS '002',")
        sql.AppendLine("SUM(acb.ANA_Cod003Codificacion_Biopsia) AS '003',")
        sql.AppendLine("SUM(acb.ANA_Cod004Codificacion_Biopsia) AS '004',")
        sql.AppendLine("SUM(acb.ANA_Cod005Codificacion_Biopsia) AS '005',")
        sql.AppendLine("SUM(acb.ANA_Cod006Codificacion_Biopsia) AS '006',")
        sql.AppendLine("SUM(acb.ANA_Cod007Codificacion_Biopsia) AS '007',")
        sql.AppendLine("SUM(acb.ANA_Cod008Codificacion_Biopsia) AS '008',")
        sql.AppendLine("PAB_Modalidad.PAB_nombreModalidad")
        sql.AppendLine("FROM")
        sql.AppendLine("PAB_Modalidad INNER JOIN")
        sql.AppendLine("ANA_Registro_Biopsias AS arb ON")
        sql.AppendLine("PAB_Modalidad.PAB_idModalidad = arb.PAB_idModalidad LEFT OUTER JOIN")
        sql.AppendLine("ANA_Codificacion_Biopsia AS acb ON arb.ANA_IdBiopsia = acb.ANA_IdBiopsia")
        sql.AppendLine("WHERE")
        sql.AppendLine("(arb.ANA_AnuladaBiopsia = 'NO') AND")
        sql.AppendLine("(arb.ANA_fec_RecepcionRegistro_Biopsias >= '" & FechaInicio & "') AND (arb.ANA_fec_RecepcionRegistro_Biopsias <= '" & FechaFinal & "')")
        sql.AppendLine("GROUP BY PAB_Modalidad.PAB_nombreModalidad")

        Return consulta
    End Function

    Public Function Ds_con_InfDetalladoConsolidado(ByVal FechaInicio As String, ByVal FechaFinal As String)
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("RTRIM(CONVERT(char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia,")
        sql.AppendLine("PAB_Modalidad.PAB_nombreModalidad, acb.ANA_IdBiopsia, acb.ANA_Cod001Codificacion_Biopsia,")
        sql.AppendLine("acb.ANA_Cod002Codificacion_Biopsia, acb.ANA_Cod003Codificacion_Biopsia,")
        sql.AppendLine("acb.ANA_Cod004Codificacion_Biopsia, acb.ANA_Cod005Codificacion_Biopsia,")
        sql.AppendLine("acb.ANA_Cod006Codificacion_Biopsia, acb.ANA_Cod007Codificacion_Biopsia,")
        sql.AppendLine("acb.ANA_Cod008Codificacion_Biopsia, arb.ANA_fec_RecepcionRegistro_Biopsias,")
        sql.AppendLine("ANA_EstadoRegistro_Biopsias, arb.ANA_TipoBiopsia,")
        sql.AppendLine("(select count(ANA_IdCortes_Muestras)  from ANA_Cortes_Muestras where ANA_Cortes_Muestras.ANA_IdBiopsia = arb.ANA_IdBiopsia group by ANA_Cortes_Muestras.ANA_IdBiopsia ) cant_cortes")
        sql.AppendLine("FROM ANA_Codificacion_Biopsia AS acb  RIGHT OUTER JOIN ANA_Registro_Biopsias as arb ON")
        sql.AppendLine("acb.ANA_IdBiopsia = arb.ANA_IdBiopsia inner join PAB_Modalidad on PAB_Modalidad.PAB_idModalidad = arb.PAB_idModalidad")
        sql.AppendLine("WHERE (arb.ANA_AnuladaBiopsia = 'NO') AND")
        sql.AppendLine("(arb.ANA_fec_RecepcionRegistro_Biopsias BETWEEN '" & FechaInicio & "' AND '" & FechaFinal & "') ")

        Return sql.ToString()

    End Function

    Public Function Ds_con_ListaUsuariosdelSistema()
        Dim consulta As String = "SELECT " _
        & "gp.GEN_descripcionPerfil, " _
        & "gsu.GEN_estadoSist_Usuario, " _
        & "RTRIM(gu.GEN_rutUsuarios) + '-' + RTRIM(gu.GEN_digitoUsuarios) AS rut, " _
        & "RTRIM(gu.GEN_nombreUsuarios) + ' ' + RTRIM(gu.GEN_apellido_paternoUsuarios) + ' ' + RTRIM(gu.GEN_apellido_maternoUsuarios) AS nombre, " _
        & "gu.GEN_loginUsuarios, " _
        & "gu.GEN_IdUsuarios " _
        & "FROM Gen_Perfil AS gp INNER JOIN GEN_Sist_Usuario AS gsu ON " _
        & "gp.GEN_idPerfil = gsu.GEN_idPerfil INNER JOIN " _
        & "GEN_Sistemas AS gs ON " _
        & "gsu.GEN_idSistemas = gs.GEN_idSistemas AND gp.GEN_IdSistemas = gs.GEN_idSistemas INNER JOIN " _
        & "GEN_Usuarios AS gu ON " _
        & "gsu.GEN_IdUsuarios = gu.GEN_IdUsuarios " _
        & "WHERE(gp.GEN_IdSistemas = 1)"

        Return consulta

    End Function

    Public Function Get_ANA_UrlProtocolo()
        Return Me.ANA_UrlProtocolo
    End Function
End Class
