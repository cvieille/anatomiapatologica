﻿Public Class Cortes : Inherits DatoBiopsia


#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private ANA_NomCortes_Muestras As String
    Private ANA_FecCortes_Muestras As Date
    Private ANA_SolporCortes_Muestras As String = Nothing
    Private ANA_FecInterCortes_Muestras As Date = Nothing
    Private ANA_InterconsCortes_Muestras As String
    Private GEN_IdUsuarioPatologo, GEN_IdUsuarioTecnologo, GEN_IdUsuarioDespCortes_Muestras As Integer
    Private ANA_DescalcificadoCortes_Muestras As String
    Private ANA_estadoCortes_Muestras As String
    Private consulta As String
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Cortes(ByVal id_c As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Try
            Dim sql As New StringBuilder
            sql.AppendLine("SELECT *")
            sql.AppendLine("FROM ANA_Cortes_Muestras")
            sql.AppendLine("WHERE")
            sql.AppendLine("ANA_IdCortes_Muestras = " & id_c)
            Dim tablas As Data.DataTable = consulta_sql_datatable(sql.ToString())
            Dim filas As Data.DataRow = tablas.Rows(0)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then
                Me.Set_ANA_IdCortes_Muestras(filas.Item("ANA_IdCortes_Muestras").ToString())
                Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())
                Me.ANA_NomCortes_Muestras = filas.Item("ANA_NomCortes_Muestras").ToString()
                Me.ANA_FecCortes_Muestras = filas.Item("ANA_FecCortes_Muestras").ToString()
                Me.Set_GEN_IdUsuarios(CInt(filas.Item("GEN_IdUsuarios").ToString()))
                If IsNumeric(filas.Item("GEN_IdUsuarioPatologo").ToString()) Then
                    Me.GEN_IdUsuarioPatologo = filas.Item("GEN_IdUsuarioPatologo").ToString()
                Else
                    Me.GEN_IdUsuarioPatologo = 0
                End If

                If IsNumeric(filas.Item("GEN_IdUsuarioTecnologo").ToString()) Then
                    Me.GEN_IdUsuarioTecnologo = filas.Item("GEN_IdUsuarioTecnologo").ToString()
                Else
                    Me.GEN_IdUsuarioTecnologo = 0
                End If

                Me.Set_GEN_idTipo_Estados_Sistemas(filas.Item("GEN_idTipo_Estados_Sistemas").ToString())
                Me.Set_ANA_Solicitada(filas.Item("ANA_SolCortes_Muestras").ToString())
                Me.Set_ANA_Almacenada(filas.Item("ANA_AlmCortes_Muestras").ToString())

                Me.Set_ANA_Desechada(filas.Item("ANA_DesCortes_Muestras").ToString())

                If Not IsDBNull(filas.Item("ANA_SolporCortes_Muestras").ToString()) Then
                    Me.ANA_SolporCortes_Muestras = filas.Item("ANA_SolporCortes_Muestras").ToString()
                End If

                If IsDate(filas.Item("ANA_FecInterCortes_Muestras").ToString()) Then
                    Me.ANA_FecInterCortes_Muestras = filas.Item("ANA_FecInterCortes_Muestras").ToString()
                End If

                Me.ANA_InterconsCortes_Muestras = filas.Item("ANA_InterconsCortes_Muestras").ToString()

                If IsNumeric(filas.Item("GEN_IdUsuarioDespCortes_Muestras").ToString()) Then
                    Me.GEN_IdUsuarioDespCortes_Muestras = filas.Item("GEN_IdUsuarioDespCortes_Muestras").ToString()
                End If

                Me.ANA_DescalcificadoCortes_Muestras = filas.Item("ANA_DescalcificadoCortes_Muestras").ToString()
                ANA_estadoCortes_Muestras = filas.Item("ANA_estadoCortes_Muestras").ToString()
            End If

        Catch ex As Exception
            Dim a = ex.Message
        End Try

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "METODO CRUD"




    Public Sub Set_Update_Cortes()

        consulta = "UPDATE ANA_Cortes_Muestras SET " _
        & "ANA_IdBiopsia =" & Me.Get_ANA_IdBiopsia() & ", " _
        & "ANA_NomCortes_Muestras ='" & Me.Get_ANA_NomCortes_Muestras() & "', " _
        & "ANA_FecCortes_Muestras ='" & Me.ANA_FecCortes_Muestras & "', " _
        & "GEN_IdUsuarioPatologo =" & Me.GEN_IdUsuarioPatologo & ", " _
        & "GEN_IdUsuarioTecnologo =" & Me.GEN_IdUsuarioTecnologo & ", " _
        & "GEN_idTipo_Estados_Sistemas =" & Me.Get_GEN_idTipo_Estados_Sistemas & ", " _
        & "ANA_AlmCortes_Muestras ='" & Me.Get_ANA_Almacenada() & "', " _
        & "ANA_DesCortes_Muestras ='" & Me.Get_ANA_Desechada & "', "


        If Me.Get_ANA_SolporCortes_Muestras <> Nothing Then
            consulta &= "ANA_SolCortes_Muestras='" & Me.Get_ANA_Solicitada & "', " _
            & "ANA_SolporCortes_Muestras ='" & Me.ANA_SolporCortes_Muestras & "', "
        End If

        If Me.ANA_FecInterCortes_Muestras <> Nothing Then
            consulta &= "ANA_FecInterCortes_Muestras ='" & Me.ANA_FecInterCortes_Muestras & "', "
        End If

        consulta &= "ANA_DescalcificadoCortes_Muestras='" & Me.ANA_DescalcificadoCortes_Muestras & "', " _
        & "ANA_InterconsCortes_Muestras ='" & Me.ANA_InterconsCortes_Muestras & "', " _
        & "GEN_IdUsuarioDespCortes_Muestras =" & Me.GEN_IdUsuarioDespCortes_Muestras & " " _
        & "WHERE ANA_IdCortes_Muestras=" & Me.Get_ANA_IdCortes_Muestras()

        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_Update_Cortes_Inter()
        consulta = "UPDATE ANA_Cortes_Muestras SET " _
        & "ANA_FecInterCortes_Muestras='" & Me.ANA_FecInterCortes_Muestras & "', " _
        & "GEN_IdUsuarioDespCortes_Muestras=" & Me.GEN_IdUsuarioDespCortes_Muestras & ", " _
        & "GEN_idTipo_Estados_Sistemas=57, " _
        & "ANA_InterconsCortes_Muestras='" & Me.ANA_InterconsCortes_Muestras & "' " _
        & "WHERE GEN_idTipo_Estados_Sistemas in (59) " _
        & "and ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia()
        ejecuta_sql(consulta)
    End Sub

#End Region

    Public Function Devuelve_max_corte()
        Dim consulta As String = "SELECT max(ANA_IdCortes_Muestras) as maximo FROM ANA_Cortes_Muestras"
        Devuelve_max_corte = consulta_sql_devuelve_string(consulta)
    End Function

#Region "METODO SET"

    Public Sub Set_ANA_DescalcificadoCortes_Muestras(ByVal ANA_DescalcificadoCortes_Muestras)
        Me.ANA_DescalcificadoCortes_Muestras = ANA_DescalcificadoCortes_Muestras
    End Sub
    Public Sub Set_ANA_NomCortes_Muestras(ByVal ANA_NomCortes_Muestras)
        Me.ANA_NomCortes_Muestras = ANA_NomCortes_Muestras
    End Sub

    Public Sub Set_ANA_FecCortes_Muestras(ByVal ANA_FecCortes_Muestras)
        Me.ANA_FecCortes_Muestras = ANA_FecCortes_Muestras
    End Sub

    Public Sub Set_ANA_SolporCortes_Muestras(ByVal ANA_SolporCortes_Muestras)
        Me.ANA_SolporCortes_Muestras = ANA_SolporCortes_Muestras
    End Sub

    Public Sub Set_ANA_FecInterCortes_Muestras(ByVal ANA_FecInterCortes_Muestras)
        Me.ANA_FecInterCortes_Muestras = ANA_FecInterCortes_Muestras
    End Sub

    Public Sub Set_ANA_InterconsCortes_Muestras(ByVal ANA_InterconsCortes_Muestras)
        Me.ANA_InterconsCortes_Muestras = ANA_InterconsCortes_Muestras
    End Sub

    Public Sub Set_GEN_IdUsuarioDespCortes_Muestras(ByVal GEN_IdUsuarioDespCortes_Muestras)
        Me.GEN_IdUsuarioDespCortes_Muestras = GEN_IdUsuarioDespCortes_Muestras
    End Sub

    Public Sub Set_ANA_estadoCortes_Muestras(ByVal ANA_estadoCortes_Muestras)
        Me.ANA_estadoCortes_Muestras = ANA_estadoCortes_Muestras
    End Sub
#End Region

#Region "METODO GET"

    Public Function Get_ANA_DescalcificadoCortes_Muestras()
        Return Me.ANA_DescalcificadoCortes_Muestras
    End Function

    Public Function Get_ANA_NomCortes_Muestras()
        Return Me.ANA_NomCortes_Muestras
    End Function

    Public Function Get_ANA_FecCortes_Muestras()
        Return Me.ANA_FecCortes_Muestras
    End Function

    Public Function Get_ANA_SolporCortes_Muestras()
        Return Me.ANA_SolporCortes_Muestras
    End Function

    Public Function Get_ANA_FecInterCortes_Muestras()
        Return Me.ANA_FecInterCortes_Muestras
    End Function

    Public Function Get_ANA_InterconsCortes_Muestras()
        Return Me.ANA_InterconsCortes_Muestras
    End Function

    Public Function Get_GEN_IdUsuarioDespCortes_Muestras()
        Return Me.GEN_IdUsuarioDespCortes_Muestras
    End Function

    Public Function Get_ANA_estadoCortes_Muestras()
        Return Me.ANA_estadoCortes_Muestras
    End Function
#End Region

End Class
