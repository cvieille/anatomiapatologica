﻿Public Class DatoBiopsia : Inherits DatoMuestra

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** DATOBIOPSIA TIENE METODOS RELACIONADOS CON:
    ' *** DATOS GENERALES DE LA BIOPSIA DESDE QUE ES TOMADA HASTA ANTES DE QUE LLEGUE A ANATOMIA PATOLOGICA

    Private ANA_IdMedSolicitaBiopsia As Integer
    Private GEN_IdServicioDestino, GEN_IdServicioOrigen As Integer
    Private ANA_OrganoBiopsia, ANA_NMuestrasFrasco As String
    Private ANA_GesRegistro_Biopsias, GEN_idPaciente As String
    Private GEN_IdPrograma As Integer

    Private ANA_IdCatalogoBiopsia, ANA_IdDetalleCatalogoBiopsia As Long
    Private ANA_fec_RecepcionRegistro_Biopsias, ANA_fec_IngresoRegistro_Biopsias, ANA_EstadoRegistro_Biopsias As String

    Private GEN_IdUsuarioPatologo As Integer = Nothing
    Private GEN_idTipo_Estados_Sistemas As Integer = Nothing
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region
    Public Function Get_GEN_idTipo_Estados_Sistemas()
        Return GEN_idTipo_Estados_Sistemas
    End Function

    Public Function Get_GEN_IdUsuarioPatologo()
        Return Me.GEN_IdUsuarioPatologo
    End Function

    Public Function Get_ANA_IdMedSolicitaBiopsia()
        Return Me.ANA_IdMedSolicitaBiopsia
    End Function

    Public Function Get_GEN_IdServicioDestino()
        Return Me.GEN_IdServicioDestino
    End Function

    Public Function Get_GEN_IdServicioOrigen()
        Return GEN_IdServicioOrigen
    End Function

    Public Sub Set_ANA_EstadoRegistro_Biopsias(ByVal ANA_EstadoRegistro_Biopsias)
        Me.ANA_EstadoRegistro_Biopsias = ANA_EstadoRegistro_Biopsias
    End Sub

    Public Sub Set_ANA_NMuestrasFrasco(ByVal ANA_NMuestrasFrasco)
        Me.ANA_NMuestrasFrasco = ANA_NMuestrasFrasco
    End Sub


    Public Sub Set_GEN_IdUsuarioPatologo(ByVal GEN_IdUsuarioPatologo)
        If IsNumeric(GEN_IdUsuarioPatologo) Then
            If GEN_IdUsuarioPatologo > 0 Then
                Me.GEN_IdUsuarioPatologo = GEN_IdUsuarioPatologo
            End If
        End If
    End Sub
    Public Sub Set_GEN_idTipo_Estados_Sistemas(GEN_idTipo_Estados_Sistemas As Integer)
        Me.GEN_idTipo_Estados_Sistemas = GEN_idTipo_Estados_Sistemas
    End Sub

    Public Sub Set_ANA_fec_IngresoRegistro_Biopsias(ByVal ANA_fec_IngresoRegistro_Biopsias)
        Me.ANA_fec_IngresoRegistro_Biopsias = ANA_fec_IngresoRegistro_Biopsias
    End Sub

    Public Sub Set_ANA_fec_RecepcionRegistro_Biopsias(ByVal ANA_fec_RecepcionRegistro_Biopsias)
        Me.ANA_fec_RecepcionRegistro_Biopsias = ANA_fec_RecepcionRegistro_Biopsias
    End Sub

    Public Sub Set_GEN_IdServicioDestino(ByVal GEN_IdServicioDestino)
        Me.GEN_IdServicioDestino = GEN_IdServicioDestino
    End Sub

    Public Sub Set_ANA_IdMedSolicitaBiopsia(ByVal ANA_IdMedSolicitaBiopsia)
        Me.ANA_IdMedSolicitaBiopsia = ANA_IdMedSolicitaBiopsia
    End Sub

    Public Sub Set_GEN_idPaciente(ByVal GEN_idPaciente)
        Me.GEN_idPaciente = GEN_idPaciente
    End Sub

    Public Sub Set_ANA_OrganoBiopsia(ByVal ANA_OrganoBiopsia)
        Me.ANA_OrganoBiopsia = ANA_OrganoBiopsia
    End Sub

    Public Sub Set_ANA_IdCatalogoBiopsia(ByVal ANA_IdCatalogoBiopsia)
        Me.ANA_IdCatalogoBiopsia = ANA_IdCatalogoBiopsia
    End Sub

    Public Sub Set_ANA_IdDetalleCatalogoBiopsia(ByVal ANA_IdDetalleCatalogoBiopsia)
        Me.ANA_IdDetalleCatalogoBiopsia = ANA_IdDetalleCatalogoBiopsia
    End Sub

    Public Sub Set_GEN_IdServicioOrigen(ByVal GEN_IdServicioOrigen)
        Me.GEN_IdServicioOrigen = GEN_IdServicioOrigen
    End Sub

    Public Sub Set_GEN_IdPrograma(ByVal GEN_IdPrograma)
        Me.GEN_IdPrograma = GEN_IdPrograma
    End Sub

    Public Sub Set_ANA_GesRegistro_Biopsias(ByVal ANA_GesRegistro_Biopsias)
        Me.ANA_GesRegistro_Biopsias = ANA_GesRegistro_Biopsias
    End Sub

    Public Function Get_ANA_fec_RecepcionRegistro_Biopsias()
        Return Me.ANA_fec_RecepcionRegistro_Biopsias
    End Function

    Public Function Get_GEN_Id_Paciente()
        If Not IsNumeric(GEN_idPaciente) Then GEN_idPaciente = 0
        Return GEN_idPaciente
    End Function

    Public Function Get_ANA_GesRegistro_Biopsias()
        Return ANA_GesRegistro_Biopsias
    End Function

    Public Function Get_ANA_OrganoBiopsia()
        Return Me.ANA_OrganoBiopsia
    End Function

    Public Function Get_Nom_Medico_Solicita(ByVal id_p As Integer)
        Dim dato As String = consulta_sql_devuelve_string("SELECT rtrim(GEN_apellidoProfesional) + ' ' + rtrim(GEN_nombreProfesional)  AS dato FROM GEN_Profesional WHERE GEN_idProfesional = " & id_p)
        Return dato
    End Function

    Public Function Get_Nom_Servicio_Solicita(ByVal id_s As Integer)
        Dim dato As String = consulta_sql_devuelve_string("SELECT rtrim(GEN_dependenciaServicio) + '-' + GEN_nombreServicio AS dato FROM GEN_Servicio WHERE  GEN_idServicio=" & id_s)
        Return dato
    End Function

    Public Function Get_Nom_Servicio_Destino(ByVal id_s As Integer)
        Dim dato As String = consulta_sql_devuelve_string("SELECT rtrim(GEN_dependenciaServicio) + '-' + GEN_nombreServicio AS dato FROM GEN_Servicio WHERE  GEN_idServicio=" & id_s)
        Return dato
    End Function

    Public Function Get_ANA_fec_IngresoRegistro_Biopsias()
        Return Me.ANA_fec_IngresoRegistro_Biopsias
    End Function

    Public Function Get_ANA_IdCatalogoBiopsia()
        Return Me.ANA_IdCatalogoBiopsia
    End Function

    Public Function Get_ANA_IdDetalleCatalogoBiopsia()
        Return Me.ANA_IdDetalleCatalogoBiopsia
    End Function

    Public Function Get_GEN_IdPrograma()
        Return Me.GEN_IdPrograma
    End Function

    Public Function Get_ANA_EstadoRegistro_Biopsias()
        Return Me.ANA_EstadoRegistro_Biopsias
    End Function

    Public Function Get_ANA_NMuestrasFrasco()
        Return Me.ANA_NMuestrasFrasco
    End Function
    Public Function Get_GEN_IdPaciente()
        Return Me.GEN_idPaciente
    End Function
End Class
