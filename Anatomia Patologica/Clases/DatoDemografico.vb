﻿Public Class DatoDemografico : Inherits Personas

    Private consulta As String
#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Public Function Get_IdCiudad(ByVal codCiudad)
        consulta = "SELECT GEN_idCiudad " _
        & "FROM " _
        & "GEN_Ciudad " _
        & "WHERE " _
        & "GEN_codigoCiudad = " & Trim(codCiudad)
        Return consulta_sql_devuelve_string(consulta)
    End Function

    Public Function Get_IdComuna(ByVal codComuna)
        consulta = "SELECT GEN_idcomuna " _
        & "FROM " _
        & "GEN_Comuna " _
        & "WHERE " _
        & "GEN_codigoComuna = " & Trim(codComuna)
        Return consulta_sql_devuelve_string(consulta)
    End Function

    Public Function Get_IdRegion(ByVal codRegion)
        consulta = "SELECT " _
        & "GEN_idregion " _
        & "FROM " _
        & "GEN_region " _
        & "WHERE " _
        & "GEN_codRegionOrden = '" & Trim(codRegion) & "'"
        Return consulta_sql_devuelve_string(consulta)
    End Function

End Class
