﻿Public Class DatoMuestra

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** DATOMUESTRA TIENE METODOS RELACIONADOS CON:
    ' *** LA MUESTRA LLEGA A ANATOMIA PATOLOGICA Y HASTA QUE ES DESPACHADA.
    Private ANA_IdBiopsia, ANA_IdCortes_Muestras, ANA_idLamina, GEN_IdUsuarios As Integer
    Private ANA_Almacenada, ANA_Desechada, ANA_Solicitada As String
    Private ANA_NumBiopsia, ANA_AñoBiopsia As Integer
    Private PAB_idModalidad As Byte
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region
    Public Function Get_PAB_idModalidad()
        Return Me.PAB_idModalidad
    End Function
    Public Sub Set_ANA_IdBiopsia(ByVal ANA_IdBiopsia)
        Me.ANA_IdBiopsia = ANA_IdBiopsia
    End Sub

    Public Sub Set_PAB_idModalidad(ByVal PAB_idModalidad)
        Me.PAB_idModalidad = PAB_idModalidad
    End Sub

    Public Function Get_ANA_IdBiopsia()
        Return Me.ANA_IdBiopsia
    End Function

    Public Function Get_ANA_NumBiopsia()
        Return Me.ANA_NumBiopsia
    End Function

    Public Function Get_ANA_AñoBiopsia()
        Return Me.ANA_AñoBiopsia
    End Function

    Public Sub Set_ANA_IdCortes_Muestras(ByVal ANA_IdCortes_Muestras)
        Me.ANA_IdCortes_Muestras = ANA_IdCortes_Muestras
    End Sub

    Public Function Get_ANA_IdCortes_Muestras()
        Return Me.ANA_IdCortes_Muestras
    End Function

    Public Sub Set_ANA_idLamina(ByVal ANA_idLamina)
        Me.ANA_idLamina = ANA_idLamina
    End Sub

    Public Function Get_ANA_idLamina()
        Return Me.ANA_idLamina
    End Function

    Public Sub Set_GEN_IdUsuarios(ByVal GEN_IdUsuarios)
        Me.GEN_IdUsuarios = GEN_IdUsuarios
    End Sub

    Public Function Get_GEN_IdUsuarios()
        Return Me.GEN_IdUsuarios
    End Function

    Public Sub Set_ANA_Almacenada(ByVal ANA_almacenada)
        Me.ANA_Almacenada = ANA_almacenada
    End Sub

    Public Function Get_ANA_Almacenada()
        Return Me.ANA_Almacenada
    End Function

    Public Function Get_NumBiopsiaCompleto()
        Return Me.ANA_NumBiopsia & "-" & Me.ANA_AñoBiopsia
    End Function

    Public Sub Set_ANA_Desechada(ByVal ANA_Desechada)
        Me.ANA_Desechada = ANA_Desechada
    End Sub

    Public Function Get_ANA_Desechada()
        Return Me.ANA_Desechada
    End Function

    Public Sub Set_ANA_Solicitada(ByVal ANA_Solicitada)
        Me.ANA_Solicitada = ANA_Solicitada
    End Sub

    Public Function Get_ANA_Solicitada()
        Return Me.ANA_Solicitada
    End Function

    Public Sub Set_ANA_AñoBiopsia(ByVal ANA_AñoBiopsia)
        Me.ANA_AñoBiopsia = ANA_AñoBiopsia
    End Sub

    Public Sub Set_ANA_NumBiopsia(ByVal ANA_NumBiopsia)
        Me.ANA_NumBiopsia = ANA_NumBiopsia
    End Sub

End Class
