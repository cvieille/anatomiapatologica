﻿Imports System.Runtime.InteropServices
Imports System.Security.Principal

Public Class Impersonator

    Public Const LOGON32_LOGON_INTERACTIVE As Integer = 2

    Public Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    <DllImport("advapi32.dll", SetLastError:=True)>
    Public Shared Function LogonUser(ByVal lpszUsername As String, ByVal lpszDomain As String, ByVal lpszPassword As String, ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll", SetLastError:=True)>
    Public Shared Function RevertToSelf() As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)>
    Public Shared Function CloseHandle(ByVal hObject As IntPtr) As Integer
    End Function

    Public Sub PerformImpersonatedTask(ByVal username As String, ByVal domain As String, ByVal password As String, ByVal logonType As Integer, ByVal logonProvider As Integer, ByVal methodToPerform As Action)
        Dim token As IntPtr = IntPtr.Zero
        If RevertToSelf() Then
            If LogonUser(username, domain, password, logonType, logonProvider, token) <> 0 Then
                Dim identity = New WindowsIdentity(token)
                Dim impersonationContext = identity.Impersonate()
                If impersonationContext IsNot Nothing Then
                    methodToPerform.Invoke()
                    impersonationContext.Undo()
                End If
            Else
            End If
        End If

        If token <> IntPtr.Zero Then
            CloseHandle(token)
        End If
    End Sub
End Class

Public Class UserImpersonation
    Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2

    Private Const LOGON32_LOGON_NETWORK As Integer = 3

    Private Const LOGON32_LOGON_BATCH As Integer = 4

    Private Const LOGON32_LOGON_SERVICE As Integer = 5

    Private Const LOGON32_LOGON_UNLOCK As Integer = 7

    Private Const LOGON32_LOGON_NETWORK_CLEARTEXT As Integer = 8

    Private Const LOGON32_LOGON_NEW_CREDENTIALS As Integer = 9

    Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    Private Const LOGON32_PROVIDER_WINNT35 As Integer = 1

    Private Const LOGON32_PROVIDER_WINNT40 As Integer = 2

    Private Const LOGON32_PROVIDER_WINNT50 As Integer = 3

    Private impersonationContext As WindowsImpersonationContext

    <DllImport("advapi32.dll", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)>
    Public Shared Function LogonUserA(ByVal lpszUsername As String, ByVal lpszDomain As String, ByVal lpszPassword As String, ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True, ExactSpelling:=True)>
    Public Shared Function DuplicateToken(ByVal ExistingTokenHandle As IntPtr, ByVal ImpersonationLevel As Integer, ByRef DuplicateTokenHandle As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True, ExactSpelling:=True)>
    Public Shared Function RevertToSelf() As Long
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True, ExactSpelling:=True)>
    Public Shared Function CloseHandle(ByVal handle As IntPtr) As Long
    End Function

    Public Function impersonateUser(ByVal userName As String, ByVal domain As String, ByVal password As String) As Boolean
        Return impersonateValidUser(userName, domain, password)
    End Function

    Public Sub undoimpersonateUser()
        undoImpersonation()
    End Sub

    Private Function impersonateValidUser(ByVal userName As String, ByVal domain As String, ByVal password As String) As Boolean
        Dim functionReturnValue As Boolean = False
        Dim tempWindowsIdentity As WindowsIdentity = Nothing
        Dim token As IntPtr = IntPtr.Zero
        Dim tokenDuplicate As IntPtr = IntPtr.Zero
        functionReturnValue = False
        If LogonUserA(userName, domain, password, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_WINNT50, token) <> 0 Then
            If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                impersonationContext = tempWindowsIdentity.Impersonate()
                If (impersonationContext IsNot Nothing) Then
                    functionReturnValue = True
                End If
            End If
        End If

        If Not tokenDuplicate.Equals(IntPtr.Zero) Then
            CloseHandle(tokenDuplicate)
        End If

        If Not token.Equals(IntPtr.Zero) Then
            CloseHandle(token)
        End If

        Return functionReturnValue
    End Function

    Private Sub undoImpersonation()
        impersonationContext.Undo()
    End Sub

End Class
