﻿Public Class InformesPDF
    Inherits DatoMuestra

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Private Sub imprime_campos_bold(ByVal texto As String, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase
        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.Border = 0
        tabla.AddCell(celda)
    End Sub

    Private Sub imprime_campos_bold_no(ByVal texto As Object, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase

        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.Border = 0
        tabla.AddCell(celda)

    End Sub

    Private Sub imprime_campos_bold_noCentrado(ByVal texto As Object, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase

        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.Border = 0
        tabla.AddCell(celda)

    End Sub

    Private Sub imprime_campos_Cabecera(ByVal texto As Object, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase

        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 11, iTextSharp.text.BaseColor.WHITE))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY

        celda.Border = 1
        tabla.AddCell(celda)

    End Sub

    Public Sub imprime_espacios_en_blanco(ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase
        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.Colspan = 2
        celda.Border = 0
        celda.Border = 0
        tabla.AddCell(celda)
    End Sub

End Class