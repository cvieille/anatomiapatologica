﻿Public Class InmunoHistoquimica : Inherits Cortes

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private ANA_EstadoInmuno As String, ANA_IdInmuno As Integer, ANA_NombreInmuno As String
    Private consulta As String = ""

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Inmuno_Histoquimica(ByVal id_inmuno As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Try
            consulta = "SELECT * " _
            & "FROM " _
            & "ANA_Inmuno_Histoquimica " _
            & "WHERE " _
            & "(ANA_idInmunoHistoquimica = " & id_inmuno & ")"
            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)

                Me.ANA_IdInmuno = filas.Item("ANA_idInmunoHistoquimica").ToString()
                Me.ANA_NombreInmuno = filas.Item("ANA_NomInmunoHistoquimica").ToString()
                Me.ANA_EstadoInmuno = Trim(filas.Item("ANA_EstInmunoHistoquimica").ToString())

            End If

        Catch ex As Exception

        End Try
    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    Public Sub Set_Crea_Inmuno_Histoquimica()
        consulta = "insert into ANA_Inmuno_Histoquimica (" _
        & "ANA_NomInmunoHistoquimica, " _
        & "ANA_EstInmunoHistoquimica) " _
        & "values(" _
        & "'" & Me.ANA_NombreInmuno & "', " _
        & "'" & Me.ANA_EstadoInmuno & "')"
        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_update_Inmuno_Histoquimica()
        consulta = "UPDATE ANA_Inmuno_Histoquimica SET " _
        & "ANA_NomInmunoHistoquimica='" & Me.ANA_NombreInmuno & "', " _
        & "ANA_EstInmunoHistoquimica='" & Me.ANA_EstadoInmuno & "' " _
        & "WHERE(ANA_idInmunoHistoquimica = " & Me.ANA_IdInmuno & ")"
        ejecuta_sql(consulta)

    End Sub

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================

    Public Sub Set_ANA_IdInmuno(ByVal ANA_IdInmuno)
        Me.ANA_IdInmuno = ANA_IdInmuno
    End Sub

    Public Sub Set_ANA_NombreInmuno(ByVal ANA_NombreInmuno)
        Me.ANA_NombreInmuno = ANA_NombreInmuno
    End Sub

    Public Sub Set_ANA_EstadoInmuno(ByVal ANA_EstadoInmuno)
        Me.ANA_EstadoInmuno = ANA_EstadoInmuno
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================

    Public Function Get_ANA_IdInmuno()
        Return Me.ANA_IdInmuno
    End Function

    Public Function Get_ANA_NombreInmuno()
        Return Me.ANA_NombreInmuno
    End Function

    Public Function Get_ANA_EstadoInmuno()
        Return Me.ANA_EstadoInmuno
    End Function

    ' *** FIN DE GET
    '==========================================================
#End Region

End Class
