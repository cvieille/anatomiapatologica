﻿Public Class InmunoHistoquimicaBiopsia
    Inherits InmunoHistoquimica

    Private consulta As String

    Private ANA_FecInmuno As String
    Private ANA_IdInmunoBiopsia As Long

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================
    Public Sub InmunoHistoquimicaBiopsia(ByVal ANA_IdInmunoBiopsia As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Try
            consulta = "SELECT " _
            & "aih.ANA_IdInmuno_Histoquimica_Biopsia, " _
            & "aih.ANA_IdBiopsia, " _
            & "aih.ANA_IdCortes_Muestras, " _
            & "aih.ANA_idInmunoHistoquimica, " _
            & "ih.ANA_NomInmunoHistoquimica, " _
            & "aih.ANA_FecInmuno_Histoquimica_Biopsia, " _
            & "aih.GEN_IdUsuarios, " _
            & "aih.ANA_EstInmuno_Histoquimica_Biopsia, GEN_idTipo_Estados_Sistemas " _
            & "FROM ANA_Inmuno_Histoquimica_Biopsia AS aih INNER JOIN " _
            & "ANA_Inmuno_Histoquimica AS ih ON " _
            & "aih.ANA_idInmunoHistoquimica = ih.ANA_idInmunoHistoquimica " _
            & "WHERE (aih.ANA_IdInmuno_Histoquimica_Biopsia = " & ANA_IdInmunoBiopsia & ")"

            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)
                Me.Set_ANA_IdInmunoBiopsia(filas.Item("ANA_IdInmuno_Histoquimica_Biopsia").ToString())
                Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())
                Me.Set_ANA_IdCortes_Muestras(filas.Item("ANA_IdCortes_Muestras").ToString())
                Me.Set_ANA_IdInmuno(filas.Item("ANA_idInmunoHistoquimica").ToString())
                Me.Set_ANA_NombreInmuno(filas.Item("ANA_NomInmunoHistoquimica").ToString())
                Me.Set_ANA_FecInmuno(filas.Item("ANA_FecInmuno_Histoquimica_Biopsia").ToString())
                Me.Set_ANA_EstadoInmuno(Trim(filas.Item("ANA_EstInmuno_Histoquimica_Biopsia").ToString()))
                Me.Set_GEN_idTipo_Estados_Sistemas(filas.Item("GEN_idTipo_Estados_Sistemas").ToString())

                Me.Set_GEN_IdUsuarios(Trim(filas.Item("GEN_IdUsuarios").ToString()))

            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region



    Public Sub Set_CambiaEstadoInmuno()
        consulta = "UPDATE ANA_Inmuno_Histoquimica_Biopsia " _
        & "SET ANA_EstInmuno_Histoquimica_Biopsia = '" & Me.Get_ANA_EstadoInmuno & "', " _
        & "GEN_idTipo_Estados_Sistemas=58 " _
        & "WHERE (ANA_IdInmuno_Histoquimica_Biopsia = " & Me.Get_ANA_IdInmunoBiopsia() & ")"
        ejecuta_sql(consulta)
    End Sub


    Public Sub Set_ANA_FecInmuno(ByVal ANA_FecInmuno)
        Me.ANA_FecInmuno = ANA_FecInmuno
    End Sub

    Public Sub Set_ANA_IdInmunoBiopsia(ByVal ANA_IdInmunoBiopsia)
        Me.ANA_IdInmunoBiopsia = ANA_IdInmunoBiopsia
    End Sub

    Public Function Get_ANA_FecInmuno()
        Return Me.ANA_FecInmuno
    End Function

    Public Function Get_ANA_IdInmunoBiopsia()
        Return Me.ANA_IdInmunoBiopsia
    End Function

End Class