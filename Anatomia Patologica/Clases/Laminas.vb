﻿Public Class Laminas : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private consulta As String
    Private ANA_nomLamina, ANA_FecLamina, interconsultista As String
    Private ANA_FecInterLamina As Date
    Private ANA_IdDespLamina As Integer
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Laminas(ByVal ANA_idLamina As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Try
            consulta = "SELECT * FROM ANA_Laminas WHERE ANA_idLamina=" & ANA_idLamina
            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)
            Dim filas As Data.DataRow = tablas.Rows(0)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then
                Me.Set_ANA_idLamina(filas.Item("ANA_idLamina").ToString())
                Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())
                Me.ANA_nomLamina = filas.Item("ANA_nomLamina").ToString()
                Me.Set_ANA_IdCortes_Muestras(filas.Item("ANA_IdCortes_Muestras").ToString())
                Me.ANA_FecLamina = filas.Item("ANA_FecLamina").ToString()
                Me.Set_GEN_IdUsuarios(filas.Item("GEN_IdUsuarios").ToString())
                Me.Set_ANA_Almacenada(filas.Item("ANA_AlmLamina").ToString())
                Me.Set_ANA_Desechada(filas.Item("ANA_DesLamina").ToString())
                Me.Set_ANA_Solicitada(filas.Item("ANA_SolLamina").ToString())
                If IsDate(filas.Item("ANA_FecInterLamina").ToString()) Then Me.Set_ANA_FecInterLamina(filas.Item("ANA_FecInterLamina").ToString())
                Me.interconsultista = filas.Item("interconsultista").ToString()
                If IsNumeric(filas.Item("ANA_IdDespLamina").ToString()) Then Me.ANA_IdDespLamina = filas.Item("ANA_IdDespLamina").ToString()
                Me.Set_GEN_idTipo_Estados_Sistemas(filas.Item("GEN_idTipo_Estados_Sistemas").ToString())

            End If

        Catch ex As Exception

        End Try
    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region


    ' *** COMIENZA LOS SET
    Public Sub Set_ANA_nomLamina(ByVal ANA_nomLamina)
        Me.ANA_nomLamina = ANA_nomLamina
    End Sub

    Public Sub Set_ANA_FecLamina(ByVal ANA_FecLamina)
        Me.ANA_FecLamina = ANA_FecLamina
    End Sub

    Public Sub Set_ANA_FecInterLamina(ByVal ANA_FecInterLamina)
        Me.ANA_FecInterLamina = ANA_FecInterLamina
    End Sub

    Public Sub Set_ANA_IdDespLamina(ByVal ANA_IdDespLamina)
        Me.ANA_IdDespLamina = ANA_IdDespLamina
    End Sub

    Public Sub Set_Interconsultista(ByVal interconsultista)
        Me.interconsultista = interconsultista
    End Sub

    ' *** FIN DE SET
    '==========================================================

    ' *** COMIENZA LOS GET
    Public Function Get_Interconsultista()
        Return Me.interconsultista
    End Function

    Public Function Get_ANA_IdDespLamina()
        Return Me.ANA_IdDespLamina
    End Function

    Public Function Get_ANA_NomLamina()
        Return Me.ANA_nomLamina
    End Function

    Public Function Get_ANA_FecLamina()
        Return Me.ANA_FecLamina
    End Function

    Public Function Get_ANA_FecInterLamina()
        Return Me.ANA_FecInterLamina
    End Function

    ' *** FIN DE GET
    '==========================================================

    ' *** COMIENZA EL CRUD
    '==========================================================
    Public Sub Set_Update_Lamina()
        Dim consulta As String = "UPDATE ANA_Laminas SET " _
        & "ANA_nomLamina ='" & Me.ANA_nomLamina & "', " _
        & "ANA_IdBiopsia =" & Me.Get_ANA_IdBiopsia() & ", " _
        & "ANA_IdCortes_Muestras =" & Me.Get_ANA_IdCortes_Muestras() & ", " _
        & "ANA_FecLamina ='" & Me.ANA_FecLamina & "', " _
        & "GEN_idTipo_Estados_Sistemas =" & Me.Get_GEN_idTipo_Estados_Sistemas() & ", " _
        & "GEN_IdUsuarios =" & Me.Get_GEN_IdUsuarios() & ", " _
        & "ANA_AlmLamina = '" & Me.Get_ANA_Almacenada() & "', " _
        & "ANA_DesLamina ='" & Me.Get_ANA_Desechada & "', " _
        & "ANA_SolLamina ='" & Me.Get_ANA_Solicitada & "', " _
        & "ANA_FecInterLamina ='" & Me.ANA_FecInterLamina & "', " _
        & "ANA_InterconLamina ='" & Me.interconsultista & "', " _
        & "ANA_IdDespLamina = " & Me.ANA_IdDespLamina & " " _
        & "WHERE ANA_idLamina=" & Me.Get_ANA_idLamina()

        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_Update_Laminas_Inter()
        consulta = "UPDATE ANA_Laminas SET " _
        & "ANA_FecInterLamina='" & Me.ANA_FecInterLamina & "', " _
        & "GEN_idTipo_Estados_Sistemas = 57, " _
        & "ANA_IdDespLamina=" & Me.ANA_IdDespLamina & ", " _
        & "ANA_InterconLamina='" & Me.interconsultista & "' " _
        & "WHERE ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia()
        ejecuta_sql(consulta)
    End Sub

    Public Sub Set_Crea_lamina()
        Dim consulta As String = "INSERT INTO ANA_Laminas (" _
        & "ANA_IdBiopsia, " _
        & "ANA_nomLamina, " _
        & "ANA_FecLamina, " _
        & "GEN_IdUsuarios, " _
        & "GEN_idTipo_Estados_Sistemas, ANA_IdCortes_Muestras, ANA_estadoLaminas) " _
        & "VALUES (" _
        & Me.Get_ANA_IdBiopsia() & ", " _
        & "'" & Me.ANA_nomLamina & "', " _
        & "'" & Get_FechaconHora() & "', " _
        & Me.Get_GEN_IdUsuarios() & ", " _
        & "58, " & Me.Get_ANA_IdCortes_Muestras() & ", 'Activo')"
        ejecuta_sql(consulta)

    End Sub

    ' *** FIN DE CRUD
    '==========================================================
End Class