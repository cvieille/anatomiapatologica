﻿Imports System.IO
Imports System.Security.Cryptography
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports Settings.Configuracion



Public Class MetodosGenerales
    Implements System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Select Case context.Request("method")
            Case "DeleteSession"
                DeleteSession(context.Request("key").ToString())
            Case "GetSession"
                GetSession()
            Case "SetSession"
                SetSession(context.Request("key").ToString(), context.Request("value").ToString())
            Case "GuardarArchivo"
                GuardarArchivo(context.Request("idBiopsia").ToString())
            Case "EliminarArchivo"
                DeleteArchivo(context.Request("nombreArchivo").ToString(), context.Request("idBiopsia").ToString())
            Case "MoverArchivo"
                MoverArchivo(context.Request("nombreArchivo").ToString(), context.Request("idBiopsia").ToString())
            Case "Encriptar"
                aesEncrypt(context.Request("texto").ToString())
            Case "GeturlWebApi"
                GeturlWebApi()
            Case "GeturlWebApiFunciones"
                GeturlWebApiFunciones()
            Case "RutaRedirecciona"
                RutaRedirecciona(context.Request("idPerfil").ToString())
            Case "GuardarConfiguracion"
                GuardarConfiguracion(context)
            Case "LeerConfiguracion"
                LeerConfiguracion()
            Case "GetRutaAdjuntoFront"
                GetRutaAdjuntoParaFront()
            Case Else
                Console.WriteLine("")
        End Select
    End Sub

    Public Shared Function LeerConfiguracion() As String

        ' Obtener la configuración actual
        Dim conf As ConfiguracionAnaDto = Correlativo.GetNumeracionActual()

        ' Serializar el objeto a JSON
        Dim jsonResponse As String = JsonConvert.SerializeObject(conf, Formatting.Indented)

        ' Escribir el JSON en la respuesta HTTP
        Dim contexto As HttpContext = HttpContext.Current
        contexto.Response.ContentType = "application/json"
        contexto.Response.Write(jsonResponse)
        contexto.Response.End()

        ' Retornar el JSON como String (útil si se usa en otras partes del código)
        Return jsonResponse
    End Function


    Private Sub GuardarConfiguracion(ByVal context As HttpContext)
        Try
            ' Leer el JSON del body de la petición
            Dim requestBody As String
            Using reader As New StreamReader(context.Request.InputStream)
                requestBody = reader.ReadToEnd()
            End Using

            ' Deserializar JSON a ConfiguracionAnaDto
            Dim config As ConfiguracionAnaDto = JsonConvert.DeserializeObject(Of ConfiguracionAnaDto)(requestBody)

            ' Llamar al método para guardar la configuración en JSON
            Correlativo.GuardarNumeracion(config)

            ' Responder con éxito
            context.Response.StatusCode = 200
            context.Response.Write("{""success"": ""Configuración guardada correctamente""}")

        Catch ex As Exception
            context.Response.StatusCode = 500
            context.Response.Write("{""error"": """ & ex.Message & """}")
        End Try
    End Sub

    Private Sub aesEncrypt(txt As String)
        Try
            Dim contexto As HttpContext = HttpContext.Current
            Dim pass As String = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6"
            Dim sha As SHA256 = SHA256.Create()

            Dim key As Byte() = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass))
            Dim iv As Byte() = New Byte() {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Dim txtEncriptado As String = encryptString(txt, key, iv)

            contexto.Response.Write(txtEncriptado)
            contexto.Response.End()
        Catch ex As Exception
            'String fail = ex.Message
        End Try

    End Sub

    Private Function encryptString(txt As String, key As Byte(), iv As Byte())
        Dim Aes As Aes = Aes.Create()
        Aes.Mode = CipherMode.CBC

        Aes.Key = key
        Aes.IV = iv

        Dim MS As MemoryStream = New MemoryStream()
        Dim aesEncryptor As ICryptoTransform = Aes.CreateEncryptor()
        Dim CryptoStream As CryptoStream = New CryptoStream(MS, aesEncryptor, CryptoStreamMode.Write)
        Dim plainBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(txt)
        CryptoStream.Write(plainBytes, 0, plainBytes.Length)
        CryptoStream.FlushFinalBlock()
        Dim cypherBytes As Byte() = MS.ToArray()
        MS.Close()
        CryptoStream.Close()
        Dim cypherText As String = Convert.ToBase64String(cypherBytes, 0, cypherBytes.Length)

        Return cypherText
    End Function
    Public Sub Credenciales(usuario, contraseña)
        Dim impersonator As New UserImpersonation()
        impersonator.impersonateUser(usuario, "", contraseña)
    End Sub

    Public Function ObtenerJsonFile(ByVal path As String)
        Dim r As StreamReader = New StreamReader(path, Encoding.GetEncoding("iso-8859-15"), True)
        Using (r)
            Dim stringFile As String = r.ReadToEnd()
            Dim json As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(stringFile)
            Return json
        End Using
    End Function



    Private Function GetAppSetting()
        Dim context As HttpContext = HttpContext.Current
        Dim path As String = HttpContext.Current.Server.MapPath("~/AppSettings.json")
        Dim json As String = File.ReadAllText(path)
        Return json
    End Function

    Private Function GetAmbienteSistema(json As String)
        Dim jsonObject As JObject = JObject.Parse(json)
        Dim ambiente As New AmbienteDto()

        ambiente.Actual = jsonObject("ambiente")("actual").Value(Of String)()
        Dim dataAmbiente As JObject = jsonObject("ambiente")(ambiente.Actual)
        Return dataAmbiente
    End Function

    Public Function GetRutaAdjunto()
        Dim context As HttpContext = HttpContext.Current
        Dim jsonObject As JObject = GetAmbienteSistema(GetAppSetting())
        Return jsonObject("rutaAdjuntos")
    End Function
    Public Function GetRutaAdjuntoParaFront()
        Dim context As HttpContext = HttpContext.Current
        Dim jsonObject As JObject = GetAmbienteSistema(GetAppSetting())
        context.Response.Write(jsonObject("rutaAdjuntos"))
        context.Response.End()
    End Function

    Public Sub GeturlWebApi()
        Dim context As HttpContext = HttpContext.Current
        Dim jsonObject As JObject = GetAmbienteSistema(GetAppSetting())
        context.Response.Write(jsonObject("urlWebApi"))
        context.Response.End()

    End Sub

    Public Sub GeturlWebApiFunciones()
        Dim context As HttpContext = HttpContext.Current
        Dim jsonObject As JObject = GetAmbienteSistema(GetAppSetting())
        context.Response.Write(jsonObject("urlWebApifunciones"))
        context.Response.End()
    End Sub
    Public Function GeturlArchivoConfiguracion()
        Dim context As HttpContext = HttpContext.Current
        Dim jsonObject As JObject = GetAmbienteSistema(GetAppSetting())

        context.Response.Write(jsonObject("archivoConfiguracion"))
        context.Response.End()
        Return jsonObject("archivoConfiguracion")

    End Function

    Public Sub RutaRedirecciona(idPerfil)
        Dim context As HttpContext = HttpContext.Current
        Dim path As String = HttpContext.Current.Server.MapPath("~/AppSettings.json")
        Dim appConfig As Dictionary(Of String, Object) = ObtenerJsonFile(path)
        Dim rutasRedirecciona As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(appConfig.Item("RutasRedirecciona").ToString())
        Dim re As String = rutasRedirecciona.Item(idPerfil)
        context.Response.Write(re)
        context.Response.End()
    End Sub

    Public Sub GuardarArchivo(idBiopsia As Integer)
        Credenciales("uftp", "Minsal2023")
        Dim contexto As HttpContext = HttpContext.Current
        Dim ColeccionArchivos As HttpFileCollection = contexto.Request.Files
        Dim nombreArchivo As String = ""
        For i = 0 To ColeccionArchivos.Count - 1
            nombreArchivo = ColeccionArchivos(i).FileName
            Dim fi As New IO.FileInfo(ColeccionArchivos(i).FileName)
            Dim DatosArchivo As String = fi.Name
            DatosArchivo = DatosArchivo.Replace("+", " ")
            DatosArchivo = DatosArchivo.Replace("°", " ")
            DatosArchivo = DatosArchivo.Replace("  ", " ")
            DatosArchivo = DatosArchivo.Replace("   ", " ")

            If (DatosArchivo.Length > 100) Then
                DatosArchivo = DatosArchivo.Substring(0, 95)
                DatosArchivo &= ".pdf"
            End If
            ''Dim p As String = ConfigurationManager.AppSettings("RutaAdjunto").ToString()
            Dim p As String = GetRutaAdjunto()
            Dim CarpetaParaGuardar As String = p + idBiopsia.ToString() + "\"
            Directory.CreateDirectory(CarpetaParaGuardar)
            ColeccionArchivos(i).SaveAs(CarpetaParaGuardar + DatosArchivo)
        Next
    End Sub

    Public Sub DeleteSession(ByVal key As String)
        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session.Remove(key)
        contexto.Response.End()
    End Sub

    Public Sub GetSession()
        Dim contexto As HttpContext = HttpContext.Current
        Dim json As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

        For index = 0 To contexto.Session.Count - 1
            If contexto.Session(index) IsNot Nothing Then
                Dim key As String = contexto.Session.Keys(index).ToString()
                Dim value As String = contexto.Session(index).ToString()
                json.Add(key, value)
            End If
        Next

        json.Add("FECHA_ACTUAL", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"))
        contexto.Response.Write(JsonConvert.SerializeObject(json))
        contexto.Response.End()
    End Sub

    Public Sub DeleteArchivo(ByVal nombreArchivo As String, idBiopsia As Integer)

        Try

            Dim imp As New UserImpersonation()
            imp.impersonateUser("Desarrollo", "", "Hcm2016")

            Dim p As String = GetRutaAdjunto()
            Dim sPath As String = p + idBiopsia.ToString() + "\" + nombreArchivo

            If File.Exists(sPath) Then
                File.Delete(sPath)
            End If

        Catch ex As Exception
            Log.WriteLog("Error al eliminar archivo: " + ex.Message + " " + ex.StackTrace)
        End Try

    End Sub
    Public Sub MoverArchivo(ByVal nombreArchivo As String, idBiopsia As Integer)
        Try
            Dim p As String = GetRutaAdjunto()
            Dim sPathAnterior As String = p + nombreArchivo
            Dim sPathNueva As String = p + idBiopsia.ToString + "\" + nombreArchivo

            Dim imp As New UserImpersonation()
            imp.impersonateUser("Desarrollo", "", "Hcm2016")

            If File.Exists(sPathAnterior) Then
                Directory.CreateDirectory(p + idBiopsia.ToString + "\")
                File.Move(sPathAnterior, sPathNueva)
            End If

        Catch ex As Exception
            Log.WriteLog("Error al mover archivo: " + ex.Message + " " + ex.StackTrace)
        End Try

    End Sub


    Public Sub SetSession(ByVal key As String, ByVal value As String)

        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session(key) = value
        contexto.Response.Write("{'" + key + "':'" + value.ToString() + "'}")
        contexto.Response.End()
    End Sub


#Region "WEB API PDF"

    Private Function GetTokenApiPdf() As String

        Dim contexto As HttpContext = HttpContext.Current

        'Try
        '    'If funciones.getPropiedad(contexto, "tokenApiPdf").ToString() = "" Then
        '    If CargarConf("tokenApiPdf", True) = "" Then
        '        Dim dominio As String = CargarConf("urlApiPdf", True)
        '        'Dim dominio As String = funciones.getPropiedad(contexto, "urlApiPdf").ToString()
        '        Dim url As String = dominio + "oauth/token"
        '        Dim webClient As New System.Net.WebClient()
        '        Dim parametros As New System.Collections.Specialized.NameValueCollection()
        '        parametros.Add("grant_type", "client_credentials")
        '        parametros.Add("client_id", "1")
        '        parametros.Add("client_secret", "Xp2zY4rwPoBZQ3SyBADFD2QO1YmNJaWtTICPff1M")

        '        Dim responsebytes As Byte() = webClient.UploadValues(url, "POST", parametros)
        '        Dim responsebody As String = (New System.Text.UTF8Encoding()).GetString(responsebytes)

        '        Dim json As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(responsebody)
        '        'funciones.setPropiedad(contexto, "tokenApiPdf", json("access_token").ToString())
        '        GuardarConf("tokenApiPdf", json("access_token").ToString())

        '        'Return funciones.getPropiedad(contexto, "tokenApiPdf").ToString()
        '        Return CargarConf("tokenApiPdf", True)
        '    End If

        '    'Return funciones.getPropiedad(contexto, "tokenApiPdf").ToString()
        '    Return CargarConf("tokenApiPdf", True)
        'Catch ex As System.Net.WebException
        '    Dim fail As String = ex.Message & ex.StackTrace
        'End Try

        Return Nothing
    End Function

#End Region
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
'Clase para guardar la información del ambiente
Public Class AmbienteDto
    Public Property Actual As String
    Public Property Produccion As DetalleAmbienteDto
    Public Property Pruebas As DetalleAmbienteDto
    Public Property Desarrollo As DetalleAmbienteDto
End Class
'Clase para guardar la información del detalle del ambiente
Public Class DetalleAmbienteDto
    Public Property UrlWebApi As String
    Public Property UrlAdjuntos As String
End Class
