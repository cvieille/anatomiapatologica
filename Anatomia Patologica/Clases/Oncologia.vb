﻿Public Class Oncologia : Inherits Registro_Biopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private ANA_IdNeoplasia_Biopsia As Long = 0
    Private ANA_IdComportamiento As Long
    Private ANA_IdMorfologia As Integer
    Private ANA_IdGrado_Diferenciacion As Long
    Private ANA_IdOrganoCie As Long
    Private ANA_IdTopografia As Long
    Private Lateralidad As String
    Private Her2 As String

    Private consulta As String

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Neoplasia(ByVal id_biopsia As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        consulta = "SELECT * " _
        & "FROM " _
        & "ANA_Neoplasia_biopsia " _
        & "WHERE " _
        & "ANA_IdBiopsia = " & id_biopsia
        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        If tablas.Rows.Count > 0 Then
            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.ANA_IdNeoplasia_Biopsia = filas.Item("ANA_IdNeoplasia_Biopsia").ToString()
            Me.ANA_IdComportamiento = filas.Item("ANA_IdComportamiento").ToString()
            Me.ANA_IdOrganoCie = filas.Item("ANA_IdOrganoCie").ToString()
            Me.ANA_IdMorfologia = Trim(filas.Item("ANA_IdMorfologia").ToString())
            Me.ANA_IdGrado_Diferenciacion = filas.Item("ANA_IdGrado_Diferenciacion").ToString()
            Me.ANA_IdTopografia = filas.Item("ANA_IdTopografia").ToString()
            Me.Lateralidad = filas.Item("Lateralidad").ToString()
            Me.Her2 = filas.Item("Her2").ToString()
        End If

        If Not IsNumeric(Me.ANA_IdNeoplasia_Biopsia) Then Me.ANA_IdNeoplasia_Biopsia = 0

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    Public Sub Set_Crea_Neoplasia()
        Try
            consulta = "INSERT INTO ANA_Neoplasia_biopsia(" _
            & "ANA_IdBiopsia, " _
            & "ANA_IdComportamiento, " _
            & "ANA_IdMorfologia, " _
            & "ANA_IdGrado_Diferenciacion, " _
            & "ANA_IdOrganoCie, " _
            & "ANA_IdTopografia, " _
            & "Lateralidad, " _
            & "Her2) " _
            & "VALUES(" _
            & Me.Get_ANA_IdBiopsia() & ", " _
            & "'" & Me.ANA_IdComportamiento & "', " _
            & Me.ANA_IdMorfologia & ", " _
            & Me.ANA_IdGrado_Diferenciacion & ", " _
            & Me.ANA_IdOrganoCie & ", " _
            & Me.ANA_IdTopografia & ", " _
            & "'" & Me.Lateralidad & "', " _
            & "'" & Me.Her2 & "')"

            ejecuta_sql(consulta)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Set_Update_Neoplasia()
        consulta = "UPDATE ANA_Neoplasia_biopsia SET " _
        & "ANA_IdBiopsia =" & Me.Get_ANA_IdBiopsia() & ", " _
        & "ANA_IdComportamiento = '" & Me.ANA_IdComportamiento & "', " _
        & "ANA_IdMorfologia = " & Me.ANA_IdMorfologia & ", " _
        & "ANA_IdGrado_Diferenciacion =" & Me.ANA_IdGrado_Diferenciacion & ", " _
        & "ANA_IdOrganoCie =" & Me.ANA_IdOrganoCie & ", " _
        & "ANA_IdTopografia =" & Me.ANA_IdTopografia & ", " _
        & "Lateralidad ='" & Me.Lateralidad & "', " _
        & "Her2 ='" & Me.Her2 & "' " _
        & "WHERE " _
        & "ANA_IdNeoplasia_Biopsia = " & Me.ANA_IdNeoplasia_Biopsia

        ejecuta_sql(consulta)

    End Sub

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================

    Public Sub Set_ANA_IdNeoplasia_Biopsia(ByVal ANA_IdNeoplasia_Biopsia As Integer)
        Me.ANA_IdNeoplasia_Biopsia = ANA_IdNeoplasia_Biopsia
    End Sub

    Public Sub Set_ANA_IdOrganoCie(ByVal ANA_IdOrganoCie As Integer)
        Me.ANA_IdOrganoCie = ANA_IdOrganoCie
    End Sub

    Public Sub Set_ANA_IdComportamiento(ByVal ANA_IdComportamiento As Integer)
        Me.ANA_IdComportamiento = ANA_IdComportamiento
    End Sub

    Public Sub Set_ANA_IdMorfologia(ByVal ANA_IdMorfologia As Integer)
        Me.ANA_IdMorfologia = ANA_IdMorfologia
    End Sub

    Public Sub Set_ANA_IdGrado_Diferenciacion(ByVal ANA_IdGrado_Diferenciacion As Integer)
        Me.ANA_IdGrado_Diferenciacion = ANA_IdGrado_Diferenciacion
    End Sub

    Public Sub Set_Lateralidad(ByVal Lateralidad As String)
        Me.Lateralidad = Lateralidad
    End Sub

    Public Sub Set_ANA_IdTopografia(ByVal ANA_IdTopografia As Integer)
        Me.ANA_IdTopografia = ANA_IdTopografia
    End Sub

    Public Sub Set_Her2(ByVal Her2 As String)
        Me.Her2 = Her2
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================

    Public Function Get_ANA_IdNeoplasia_Biopsia()
        If Not IsNumeric(ANA_IdNeoplasia_Biopsia) Then ANA_IdNeoplasia_Biopsia = 0

        Return ANA_IdNeoplasia_Biopsia
    End Function

    Public Function Get_ANA_IdOrganoCie()
        Return ANA_IdOrganoCie
    End Function


    Public Function Get_ANA_IdComportamiento()
        Return ANA_IdComportamiento
    End Function

    Public Function Get_ANA_IdMorfologia()
        Return ANA_IdMorfologia
    End Function

    Public Function Get_ANA_IdGrado_Diferenciacion()
        Return ANA_IdGrado_Diferenciacion
    End Function

    Public Function Get_ANA_IdTopografia()
        Return ANA_IdTopografia
    End Function

    Public Function Get_Lateralidad()
        Return Lateralidad
    End Function

    Public Function Get_Her2()
        Return Her2
    End Function

    ' *** FIN DE GET
    '==========================================================
#End Region

End Class
