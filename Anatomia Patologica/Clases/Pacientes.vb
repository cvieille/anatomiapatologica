﻿Public Class Pacientes : Inherits Personas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private GEN_FecNacimientoPaciente As String
    Private GEN_IdPaciente As Long, GEN_NacionalidadPaciente As String = "Chileno (a)"

    Private GEN_idIdentificacion As Byte

    Private GEN_NuiPaciente As String = "0"
    Private GEN_otros_fonosPaciente As String

    Private GEN_FecActualizacionPaciente As String
    Private GEN_IdPrevision As Byte = Nothing
    Private GEN_IdPrevision_Tramo As Byte = Nothing
    Private GEN_numero_documentoPaciente As String
    Private GEN_digitoPaciente As String
    Private GEN_pac_pac_numero = 0

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================


    Public Sub Pacientes(ByVal RutPaciente As String, ByVal TipoDocumento As Byte)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Dim consulta As String = "SELECT " _
        & "GEN_idPaciente " _
        & "FROM GEN_Paciente " _
        & "WHERE " _
        & "GEN_numero_documentoPaciente='" & RutPaciente & "' AND " _
        & "GEN_idIdentificacion = " & TipoDocumento
        Dim IdPaciente As String = consulta_sql_devuelve_string(consulta)
        If IsNumeric(IdPaciente) Then
            Pacientes(CLng(IdPaciente))
        End If


    End Sub

    Public Sub Pacientes(ByVal GEN_idPaciente As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Dim consulta As String = "SELECT * " _
        & "FROM " _
        & "GEN_Paciente " _
        & "WHERE " _
        & "(GEN_idPaciente = " & GEN_idPaciente & ")"

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        If tablas.Rows.Count > 0 Then

            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.Set_GEN_idPaciente(Trim(filas.Item("GEN_idPaciente").ToString()))
            Me.Set_GEN_numero_documentoPaciente(Trim(filas.Item("GEN_numero_documentoPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_digitoPaciente").ToString()) Then Me.Set_GEN_digitoPaciente(Trim(filas.Item("GEN_digitoPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_nombrePaciente").ToString()) Then Me.Set_GEN_Nombre(Trim(filas.Item("GEN_nombrePaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_ape_paternoPaciente").ToString()) Then Me.Set_GEN_ApePaterno(Trim(filas.Item("GEN_ape_paternoPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_ape_maternoPaciente").ToString()) Then Me.Set_GEN_ApeMaterno(Trim(filas.Item("GEN_ape_maternoPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_dir_callePaciente").ToString()) Then Me.Set_GEN_dir_calle(Trim(filas.Item("GEN_dir_callePaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_dir_numeroPaciente").ToString()) Then Me.Set_Gen_Dir_Numero(Trim(filas.Item("GEN_dir_numeroPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_telefonoPaciente").ToString()) Then Me.Set_GEN_telefono(Trim(filas.Item("GEN_telefonoPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_otros_fonosPaciente").ToString()) Then Me.Set_GEN_otros_fonosPaciente(Trim(filas.Item("GEN_otros_fonosPaciente").ToString()))
            If Not IsDBNull(filas.Item("GEN_fec_nacimientoPaciente").ToString()) Then Me.Set_GEN_FecNacimientoPaciente(filas.Item("GEN_fec_nacimientoPaciente").ToString())
            If Not IsDBNull(filas.Item("GEN_estadoPaciente").ToString()) Then Me.Set_GEN_EstadoP(Trim(filas.Item("GEN_estadoPaciente").ToString()))

            If IsNumeric(filas.Item("GEN_IdPais").ToString()) Then Me.Set_Gen_IdPais(filas.Item("GEN_IdPais").ToString())
            If IsNumeric(filas.Item("GEN_IdRegion").ToString()) Then Me.Set_Gen_IdRegion(filas.Item("GEN_IdRegion").ToString())
            If IsNumeric(filas.Item("GEN_IdComuna").ToString()) Then Me.Set_Gen_IdComuna(filas.Item("GEN_IdComuna").ToString())
            If IsNumeric(filas.Item("GEN_IdCiudad").ToString()) Then Me.Set_GEN_idCiudad(filas.Item("GEN_IdCiudad").ToString())

            If IsNumeric(filas.Item("GEN_idPrevision").ToString()) Then Me.Set_Gen_Idprevision(filas.Item("GEN_idPrevision").ToString())
            If IsNumeric(filas.Item("GEN_idPrevision_Tramo").ToString()) Then Me.Set_Gen_Idprevision_Tramo(filas.Item("GEN_idPrevision_Tramo").ToString())

            Me.Set_GEN_nuiPaciente(filas.Item("GEN_nuiPaciente").ToString())
            Me.Set_GEN_idIdentificacion(filas.Item("GEN_idIdentificacion").ToString())
            Me.Set_GEN_FecActualizacionPaciente(filas.Item("GEN_fec_actualizacionPaciente").ToString())
            Me.Set_GEN_IdSexo(filas.Item("GEN_idsexo").ToString())

            If IsNumeric(filas.Item("GEN_pac_pac_numeroPaciente").ToString()) Then
                Me.GEN_pac_pac_numero = filas.Item("GEN_pac_pac_numeroPaciente").ToString()
            End If

        End If

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

    ' *** COMIENZA EL CRUD    

    Public Sub Set_Crea_Pacientes()

        Dim consulta As String = "INSERT INTO GEN_Paciente (" _
        & "GEN_numero_documentoPaciente, " _
        & "GEN_digitoPaciente, " _
        & "GEN_nombrePaciente, " _
        & "GEN_Ape_paternoPaciente, " _
        & "GEN_Ape_maternoPaciente, " _
        & "GEN_dir_callePaciente, " _
        & "GEN_dir_numeroPaciente, " _
        & "GEN_telefonoPaciente, " _
        & "GEN_otros_fonosPaciente, " _
        & "GEN_Idsexo, " _
        & "GEN_fec_nacimientoPaciente, " _
        & "GEN_IdCiudad, " _
        & "GEN_NuiPaciente, " _
        & "GEN_estadoPaciente, " _
        & "GEN_IdComuna, " _
        & "GEN_IdPais, " _
        & "GEN_IdRegion, " _
        & "Gen_Fec_Actualizacionpaciente, " _
        & "GEN_IdIdentificacion, " _
        & "GEN_IdPrevision, " _
        & "GEN_IdPrevision_Tramo, " _
        & "GEN_pac_pac_numeroPaciente) " _
        & "values(" _
        & "'" & Me.Get_GEN_numero_documentoPaciente() & "', "

        If Me.Get_GEN_digitoPaciente() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_digitoPaciente() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_GEN_Nombre() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_Nombre() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_GEN_ApePaterno() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_ApePaterno() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_GEN_ApeMaterno() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_ApeMaterno() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_GEN_dir_calle() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_dir_calle() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_Gen_Dir_Numero() <> Nothing Then
            consulta &= "'" & Me.Get_Gen_Dir_Numero() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_GEN_telefono() <> Nothing Then
            consulta &= "'" & Me.Get_GEN_telefono() & "', "
        Else
            consulta &= "null, "
        End If

        consulta &= "" _
        & "'" & GEN_otros_fonosPaciente & "', " _
        & Me.Get_GEN_Sexo() & ", " _
        & "'" & GEN_FecNacimientoPaciente & "', " _
        & "'" & Me.Get_GEN_IdCiudad() & "', " _
        & Me.Get_GEN_NuiPaciente() & ", " _
        & "'" & Me.Get_GEN_EstadoP() & "', " _
        & Me.Get_Gen_Idcomuna() & ", " _
        & Me.Get_Gen_Idpais() & ", " _
        & Me.Get_Gen_Idregion() & ", " _
        & "'" & Get_FechaconHora() & "', " _
        & Me.Get_GEN_idIdentificacion() & ", "

        If Me.Get_Gen_IdPrevision() <> Nothing Then
            consulta &= "'" & Me.Get_Gen_IdPrevision() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.Get_Gen_Idprevision_Tramo() <> Nothing Then
            consulta &= "'" & Me.Get_Gen_Idprevision_Tramo() & "', "
        Else
            consulta &= "null, "
        End If

        If Me.GEN_pac_pac_numero <> Nothing Then
            consulta &= "'" & Me.GEN_pac_pac_numero & "'"
        Else
            consulta &= "null"
        End If

        consulta &= ")"
        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_update_Pacientes()
        Dim consulta As String = "UPDATE GEN_Paciente SET " _
        & "GEN_numero_documentoPaciente ='" & Me.Get_GEN_numero_documentoPaciente() & "', " _
        & "GEN_digitoPaciente ='" & Me.Get_GEN_digitoPaciente() & "', " _
        & "GEN_nombrePaciente ='" & Me.Get_GEN_Nombre() & "', " _
        & "GEN_ape_paternoPaciente ='" & Me.Get_GEN_ApePaterno() & "', " _
        & "GEN_ape_maternoPaciente ='" & Me.Get_GEN_ApeMaterno() & "', " _
        & "GEN_dir_callePaciente ='" & Me.Get_GEN_dir_calle() & "', " _
        & "GEN_dir_numeroPaciente='" & Me.Get_Gen_Dir_Numero() & "', " _
        & "GEN_telefonoPaciente ='" & Me.Get_GEN_telefono() & "', " _
        & "GEN_otros_fonosPaciente ='" & Me.GEN_otros_fonosPaciente & "', " _
        & "GEN_Idsexo =" & Me.Get_GEN_Sexo() & ", " _
        & "GEN_fec_nacimientoPaciente ='" & Me.Get_GEN_FecNacimientoPaciente() & "', " _
        & "GEN_nuiPaciente =" & Me.Get_GEN_NuiPaciente() & ", " _
        & "GEN_IdIdentificacion=" & Me.Get_GEN_idIdentificacion() & ", " _
        & "GEN_pac_pac_numeroPaciente=" & Me.GEN_pac_pac_numero & ", " _
        & "GEN_estadoPaciente ='" & Me.Get_GEN_EstadoP() & "', " _
        & "GEN_IdCiudad=" & Me.Get_GEN_IdCiudad() & ", " _
        & "GEN_IdComuna=" & Me.Get_Gen_Idcomuna() & ", " _
        & "GEN_IdPais=" & Me.Get_Gen_Idpais() & ", " _
        & "GEN_IdRegion=" & Me.Get_Gen_Idregion() & ", " _
        & "GEN_Fec_Actualizacionpaciente='" & Get_FechaconHora() & "' " _
        & "WHERE GEN_idPaciente=" & Me.Get_GEN_IdPaciente()
        ejecuta_sql(consulta)

    End Sub



    ' *** FIN DE LOS CRUD
    '==========================================================

    ' *** COMIENZA LOS SET

    Public Sub Set_GEN_numero_documentoPaciente(ByVal GEN_numero_documentoPaciente)
        Me.GEN_numero_documentoPaciente = GEN_numero_documentoPaciente
    End Sub

    Public Sub Set_GEN_digitoPaciente(ByVal GEN_digitoPaciente)
        Me.GEN_digitoPaciente = GEN_digitoPaciente
    End Sub
    Public Sub Set_GEN_FecActualizacionPaciente(ByVal GEN_FecActualizacionPaciente)
        Me.GEN_FecActualizacionPaciente = GEN_FecActualizacionPaciente
    End Sub

    Public Sub Set_GEN_nuiPaciente(ByVal GEN_nuiPaciente)
        Me.GEN_NuiPaciente = GEN_nuiPaciente
    End Sub

    Public Sub Set_GEN_idPaciente(ByVal GEN_idPaciente)
        Me.GEN_IdPaciente = GEN_idPaciente
    End Sub

    Public Sub Set_GEN_NacionalidadPaciente(ByVal GEN_NacionalidadPaciente)
        Me.GEN_NacionalidadPaciente = GEN_NacionalidadPaciente
    End Sub

    Public Sub Set_GEN_FecNacimientoPaciente(ByVal GEN_FecNacimientoPaciente)
        Me.GEN_FecNacimientoPaciente = GEN_FecNacimientoPaciente
    End Sub

    Public Sub Set_GEN_otros_fonosPaciente(ByVal GEN_otros_fonosPaciente)
        Me.GEN_otros_fonosPaciente = GEN_otros_fonosPaciente
    End Sub

    Public Sub Set_Gen_Idprevision(ByVal GEN_idPrevision)
        Me.GEN_IdPrevision = GEN_idPrevision
    End Sub

    Public Sub Set_Gen_Idprevision_Tramo(ByVal GEN_idPrevision_Tramo)
        Me.GEN_IdPrevision_Tramo = GEN_idPrevision_Tramo
    End Sub

    Public Sub Set_GEN_idIdentificacion(ByVal GEN_idIdentificacion)
        Me.GEN_idIdentificacion = GEN_idIdentificacion
    End Sub

    ' *** FIN DE SET
    '==========================================================

    ' *** COMIENZA LOS GET
    Public Function Get_GEN_numero_documentoPaciente()
        Return Me.GEN_numero_documentoPaciente
    End Function

    Public Function Get_GEN_digitoPaciente()
        Return Me.GEN_digitoPaciente
    End Function
    Public Function Get_GEN_NuiPaciente()
        Return Me.GEN_NuiPaciente
    End Function

    Public Function Get_GEN_idIdentificacion()
        Return Me.GEN_idIdentificacion
    End Function

    Public Function Get_GEN_IdPaciente()
        Return Me.GEN_IdPaciente
    End Function

    Public Function Get_GEN_NacionalidadPaciente()
        Return Me.GEN_NacionalidadPaciente
    End Function

    Public Function Get_GEN_FecNacimientoPaciente()
        Return Me.GEN_FecNacimientoPaciente
    End Function

    Public Function Get_GEN_OtrosFonosPaciente()
        Return Me.GEN_otros_fonosPaciente
    End Function

    Public Function Get_GEN_FecActualizacionPaciente()
        Return Me.GEN_FecActualizacionPaciente
    End Function

    Public Function Get_Gen_IdPrevision()
        Return Me.GEN_IdPrevision
    End Function

    Public Function Get_Gen_Idprevision_Tramo()
        Return Me.GEN_IdPrevision_Tramo
    End Function

    Public Function Get_Edad()

        Dim Meses_Totales = DateDiff("m", Me.GEN_FecNacimientoPaciente, Date.Today)
        Dim Years = Int(Meses_Totales / 12)
        Dim edad As String = Years & " años " & Meses_Totales - (Years * 12) & " meses"
        Return edad

    End Function



    ' *** FIN DE GET
    '==========================================================
End Class