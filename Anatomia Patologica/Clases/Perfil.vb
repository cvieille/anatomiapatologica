﻿Public Class Perfil : Inherits Usuarios

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private GEN_codigoPerfil As Integer, GEN_descripcionPerfil As String, GEN_IdSistemas As Integer

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region


    '*** METODOS VARIOS
    Public Function devuelve_perfil_por_tipo(ByVal descripcion As String)
        Dim consulta As String
        consulta = "SELECT GEN_codigoPerfil FROM GEN_Perfil WHERE  GEN_descripcionPerfil='" & descripcion & "'"
        devuelve_perfil_por_tipo = consulta_sql_devuelve_string(consulta)
    End Function

    '*** FIN DE METODOS VARIOS
    '==========================================================

    ' *** COMIENZA LOS GET
    Public Function Get_Gen_CodigoPerfil()
        Return Me.GEN_codigoPerfil
    End Function

    Public Function Get_Gen_DescripcionPerfil()
        Return Me.GEN_descripcionPerfil
    End Function

    Public Function Get_Gen_IdSistemas()
        Return Me.GEN_IdSistemas
    End Function

    ' *** FIN DE GET
    '==========================================================


    ' *** COMIENZA LOS SET
    Public Sub Set_Gen_CodigoPerfil(ByVal GEN_codigoPerfil)
        Me.GEN_codigoPerfil = GEN_codigoPerfil
    End Sub

    Public Sub Set_Gen_DescripcionPerfil(ByVal GEN_descripcionPerfil)
        Me.GEN_descripcionPerfil = GEN_descripcionPerfil
    End Sub

    Public Sub Set_Gen_IdSistemas(ByVal GEN_IdSistemas)
        Me.GEN_IdSistemas = GEN_IdSistemas
    End Sub

    ' *** FIN DE SET
    '==========================================================
End Class
