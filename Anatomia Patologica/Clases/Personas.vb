﻿Public Class Personas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private GEN_Nombre As String
    Private GEN_ApePaterno As String = Nothing
    Private GEN_ApeMaterno As String = Nothing
    Private GEN_dir_calle As String = Nothing
    Private GEN_dir_numero = Nothing
    Private GEN_telefono As String = Nothing
    Private GEN_idCiudad As Integer = 284, GEN_idComuna As Integer = 45
    Private GEN_idPais As Integer = 1, GEN_IdRegion As Integer = 14
    Private GEN_IdSexo As Byte = 1, GEN_EstadoP As String = "Activo"



    Private GEN_numero_documentoPersonas As String = Nothing
    Private GEN_digitoPersonas As String = Nothing



    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    ' *** COMIENZA LOS GET
    Public Function Get_GEN_numero_documentoPersonas()
        Return Me.GEN_numero_documentoPersonas
    End Function

    Public Function Get_GEN_digitoPersonas()
        Return Me.GEN_digitoPersonas
    End Function
    Public Function Get_GEN_Nombre()
        Return Me.GEN_Nombre
    End Function

    Public Function Get_GEN_ApePaterno()
        Return Me.GEN_ApePaterno
    End Function

    Public Function Get_GEN_ApeMaterno()
        Return Me.GEN_ApeMaterno
    End Function

    Public Function Get_GEN_NombreCompleto()
        Dim NomCompleto As String = Me.GEN_Nombre & " " & Me.GEN_ApePaterno & " " & Me.GEN_ApeMaterno
        Return NomCompleto
    End Function

    Public Function Get_GEN_Sexo()
        Return Me.GEN_IdSexo
    End Function

    Public Function Get_GEN_dir_calle()
        Return Me.GEN_dir_calle
    End Function

    Public Function Get_GEN_telefono()
        Return Me.GEN_telefono
    End Function

    Public Function Get_Gen_Idcomuna()
        Return Me.GEN_idComuna
    End Function

    Public Function Get_Gen_Idpais()
        Return Me.GEN_idPais
    End Function

    Public Function Get_Gen_Idregion()
        Return Me.GEN_IdRegion
    End Function

    Public Function Get_Id_ciudad_por_Codigo(ByVal codigoCiudad)
        Dim consulta As String = "SELECT " _
        & "GEN_idCiudad " _
        & "FROM " _
        & "GEN_Ciudad " _
        & "WHERE " _
        & "GEN_codigoCiudad = " & codigoCiudad
        Return CInt(consulta_sql_devuelve_string(consulta))
    End Function

    Public Function Get_Gen_Dir_Numero()
        Return Me.GEN_dir_numero
    End Function

    Public Sub Set_GEN_IdSexo(ByVal GEN_IdSexo)
        Me.GEN_IdSexo = GEN_IdSexo
    End Sub

    Public Function Get_GEN_EstadoP()
        Return Me.GEN_EstadoP
    End Function

    ' *** FIN DE LOS GET
    Public Sub Set_GEN_numero_documentoPersonas(ByVal GEN_numero_documentoPersonas)
        Me.GEN_numero_documentoPersonas = GEN_numero_documentoPersonas
    End Sub

    Public Sub Set_GEN_digitoPersonas(ByVal GEN_digitoPersonas)
        Me.GEN_digitoPersonas = GEN_digitoPersonas
    End Sub
    Public Sub Set_GEN_Nombre(ByVal GEN_Nombre)
        Me.GEN_Nombre = GEN_Nombre
    End Sub

    Public Sub Set_GEN_ApePaterno(ByVal GEN_ApePaterno)
        Me.GEN_ApePaterno = GEN_ApePaterno
    End Sub

    Public Sub Set_GEN_ApeMaterno(ByVal GEN_ApeMaterno)
        Me.GEN_ApeMaterno = GEN_ApeMaterno
    End Sub

    Public Sub Set_GEN_dir_calle(ByVal GEN_dir_calle)
        Me.GEN_dir_calle = GEN_dir_calle
    End Sub

    Public Sub Set_GEN_telefono(ByVal GEN_telefono)
        Me.GEN_telefono = GEN_telefono
    End Sub

    Public Sub Set_GEN_idCiudad(ByVal GEN_idCiudad)
        Me.GEN_idCiudad = GEN_idCiudad
    End Sub

    Public Sub Set_Gen_IdComuna(ByVal GEN_idComuna)
        Me.GEN_idComuna = GEN_idComuna
    End Sub

    Public Sub Set_Gen_IdPais(ByVal GEN_idPais)
        Me.GEN_idPais = GEN_idPais
    End Sub

    Public Sub Set_Gen_IdRegion(ByVal GEN_idRegion)
        Me.GEN_IdRegion = GEN_idRegion
    End Sub

    Public Sub Set_Gen_Dir_Numero(ByVal GEN_dir_numero)
        Me.GEN_dir_numero = GEN_dir_numero
    End Sub

    Public Sub Set_GEN_EstadoP(ByVal GEN_estadoP)
        Me.GEN_EstadoP = GEN_estadoP
    End Sub

    Public Function Get_GEN_IdCiudad()
        Return Me.GEN_idCiudad
    End Function

End Class
