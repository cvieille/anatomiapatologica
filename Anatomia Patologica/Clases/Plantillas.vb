﻿Public Class Plantillas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private ANA_NombrePlantilla, ANA_DescAntClinicosPlantilla As String
    Private ANA_DescMacroscopicaPlantilla, ANA_DescMicroscopicaPlantilla, ANA_DescDiagnosticoPlantilla, ANA_DescNotaPlantilla, ANA_EstuInmunoPlantilla As String
    Private GEN_IdUsuarios As Integer = 0, ANA_IdPlantilla As Long = 0
    Private ANA_estadoPlantillas As String = "Activo"

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Plantillas(ByVal id_plantilla As Integer)

        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        If id_plantilla > 0 Then
            Dim consulta As String = "SELECT * FROM ANA_Plantillas " _
            & "WHERE(ANA_IdPlantilla = " & id_plantilla & ")"

            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            If tablas.Rows.Count > 0 Then
                Dim filas As Data.DataRow = tablas.Rows(0)
                Me.ANA_NombrePlantilla = Trim(filas.Item("ANA_NombrePlantilla").ToString())
                Me.ANA_DescAntClinicosPlantilla = filas.Item("ANA_desc_ant_clinicosPlantilla").ToString()
                Me.ANA_DescMacroscopicaPlantilla = Trim(filas.Item("ANA_desc_macroscopicaPlantilla")).ToString()
                Me.ANA_DescMicroscopicaPlantilla = Trim(filas.Item("ANA_desc_microscopicaPlantilla")).ToString()
                Me.ANA_DescDiagnosticoPlantilla = filas.Item("ANA_desc_diagnosticoPlantilla").ToString()
                Me.ANA_DescNotaPlantilla = filas.Item("ANA_desc_notaPlantilla").ToString()
                Me.ANA_EstuInmunoPlantilla = filas.Item("ANA_estu_inmunoPlantilla").ToString()
                Me.ANA_estadoPlantillas = filas.Item("ANA_estadoPlantillas").ToString()
            End If
        End If
    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

    ' *** COMIENZA EL CRUD
    '==========================================================
    Public Sub Set_Elimina_Plantilla(ByVal id_plantilla As Integer)
        Dim consulta As String = "DELETE ANA_Plantillas " _
        & "WHERE (ANA_idPlantilla = " & id_plantilla & ")"
        ejecuta_sql(consulta)
    End Sub

    Public Sub Set_Crea_plantilla()
        Dim consulta As String = ""

        consulta = "INSERT INTO ANA_Plantillas ( " _
        & "ANA_nombreplantilla, " _
        & "ANA_desc_ant_clinicosPlantilla, " _
        & "ANA_desc_macroscopicaPlantilla, " _
        & "ANA_desc_microscopicaPlantilla, " _
        & "ANA_desc_diagnosticoPlantilla, " _
        & "ANA_desc_notaPlantilla, " _
        & "GEN_IdUsuarios, " _
        & "ANA_fec_creacionPlantilla, " _
        & "ANA_estu_inmunoPlantilla, ANA_estadoPlantillas) " _
        & "VALUES('" _
        & Me.Get_ANA_NombrePlantilla() & "', " _
        & "'" & Me.Get_ANA_DescAntClinicosPlantilla() & "', " _
        & "'" & Me.Get_ANA_DescMacroscopicaPlantilla() & "', " _
        & "'" & Me.Get_ANA_DescMicroscopicaPlantilla() & "', " _
        & "'" & Me.Get_ANA_DescDiagnosticoPlantilla() & "', " _
        & "'" & Me.Get_ANA_DescNotaPlantilla() & "', " _
        & "" & Me.Get_GEN_IdUsuarios() & ", " _
        & "'" & Get_FechaconHora() & "', " _
        & "'" & Me.Get_ANA_EstuInmunoPlantilla() & "', 'Activo')"

        ejecuta_sql(consulta)

    End Sub

    Public Sub Set_Update_plantilla()
        Dim consulta As String = ""

        consulta = "UPDATE ANA_Plantillas SET " _
        & "ANA_nombreplantilla ='" & Me.Get_ANA_NombrePlantilla() & "', " _
        & "ANA_desc_ant_clinicosPlantilla ='" & Me.Get_ANA_DescAntClinicosPlantilla() & "', " _
        & "ANA_desc_macroscopicaPlantilla ='" & Me.Get_ANA_DescMacroscopicaPlantilla() & "', " _
        & "ANA_desc_microscopicaPlantilla ='" & Me.Get_ANA_DescMicroscopicaPlantilla() & "', " _
        & "ANA_desc_diagnosticoPlantilla ='" & Me.Get_ANA_DescDiagnosticoPlantilla() & "', " _
        & "ANA_desc_notaPlantilla ='" & Me.Get_ANA_DescNotaPlantilla() & "', " _
        & "ANA_estadoPlantillas ='" & Me.Get_ANA_estadoPlantillas() & "', " _
        & "GEN_IdUsuarios =" & Me.Get_GEN_IdUsuarios() & ", " _
        & "ANA_estu_inmunoPlantilla = '" & Me.Get_ANA_EstuInmunoPlantilla() & "' " _
        & "WHERE (ANA_idPlantilla = " & Me.Get_ANA_IdPlantilla() & ")"

        ejecuta_sql(consulta)

    End Sub

    Public Function consulta_si_existe_plantilla(ByVal nom_p As String)

        Dim consulta As String = "SELECT " _
        & "count(ANA_nombreplantilla) " _
        & "FROM  ANA_Plantillas " _
        & "WHERE " _
        & "ANA_nombreplantilla = '" & nom_p & "'"

        Dim cantidad As Byte
        If IsNumeric(consulta_sql_devuelve_string(consulta)) Then
            cantidad = consulta_sql_devuelve_string(consulta)
        Else
            cantidad = 0
        End If

        Return cantidad

    End Function

    Public Sub Set_ANA_NombrePlantilla(ByVal ANA_NombrePlantilla)
        Me.ANA_NombrePlantilla = ANA_NombrePlantilla
    End Sub

    Public Sub Set_ANA_DescAntClinicosPlantilla(ByVal ANA_DescAntClinicosPlantilla)
        Me.ANA_DescAntClinicosPlantilla = ANA_DescAntClinicosPlantilla
    End Sub

    Public Sub Set_ANA_DescMacroscopicaPlantilla(ByVal ANA_DescMacroscopicaPlantilla)
        Me.ANA_DescMacroscopicaPlantilla = ANA_DescMacroscopicaPlantilla
    End Sub

    Public Sub Set_ANA_DescMicroscopicaPlantilla(ByVal ANA_DescMicroscopicaPlantilla)
        Me.ANA_DescMicroscopicaPlantilla = ANA_DescMicroscopicaPlantilla
    End Sub

    Public Sub Set_ANA_DescDiagnosticoPlantilla(ByVal ANA_DescDiagnosticoPlantilla)
        Me.ANA_DescDiagnosticoPlantilla = ANA_DescDiagnosticoPlantilla
    End Sub

    Public Sub Set_ANA_DescNotaPlantilla(ByVal ANA_DescNotaPlantilla)
        Me.ANA_DescNotaPlantilla = ANA_DescNotaPlantilla
    End Sub

    Public Sub Set_ANA_EstuInmunoPlantilla(ByVal ANA_EstuInmunoPlantilla)
        Me.ANA_EstuInmunoPlantilla = ANA_EstuInmunoPlantilla
    End Sub

    Public Sub Set_GEN_IdUsuarios(ByVal GEN_IdUsuarios)
        Me.GEN_IdUsuarios = GEN_IdUsuarios
    End Sub

    Public Sub Set_ANA_IdPlantilla(ByVal ANA_IdPlantilla)
        Me.ANA_IdPlantilla = ANA_IdPlantilla
    End Sub

    Public Sub Set_ANA_estadoPlantillas(ByVal ANA_estadoPlantillas)
        Me.ANA_estadoPlantillas = ANA_estadoPlantillas
    End Sub
    Public Function Get_ANA_NombrePlantilla()
        Return Me.ANA_NombrePlantilla
    End Function

    Public Function Get_ANA_DescAntClinicosPlantilla()
        Return Me.ANA_DescAntClinicosPlantilla
    End Function

    Public Function Get_ANA_DescMacroscopicaPlantilla()
        Return Me.ANA_DescMacroscopicaPlantilla
    End Function

    Public Function Get_ANA_DescMicroscopicaPlantilla()
        Return Me.ANA_DescMicroscopicaPlantilla
    End Function

    Public Function Get_ANA_DescDiagnosticoPlantilla()
        Return Me.ANA_DescDiagnosticoPlantilla
    End Function

    Public Function Get_ANA_DescNotaPlantilla()
        Return Me.ANA_DescNotaPlantilla
    End Function

    Public Function Get_ANA_EstuInmunoPlantilla()
        Return Me.ANA_EstuInmunoPlantilla
    End Function

    Public Function Get_GEN_IdUsuarios()
        Return Me.GEN_IdUsuarios
    End Function

    Public Function Get_ANA_IdPlantilla()
        Return Me.ANA_IdPlantilla
    End Function

    Public Function Get_ANA_estadoPlantillas()
        Return ANA_estadoPlantillas
    End Function
End Class
