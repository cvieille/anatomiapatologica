﻿Public Class Profesionales
    Inherits Personas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private GEN_idProfesional As Long
    Private GEN_dependenciaProfesional As String, GEN_estadoProfesional As String
    Private consulta As String

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Profesionales(ByVal id_profesional As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        ' *** HACE SOBRE CARGA DE METODO, UN COSTRUCTOR BUSCA POR RUT Y EL OTRO POR ID
        Try
            consulta = "SELECT * " _
            & "FROM  GEN_Profesional " _
            & "WHERE " _
            & "(GEN_idProfesional = " & id_profesional & ")"
            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)
                Me.Set_GEN_Nombre(Trim(filas.Item("GEN_nombreProfesional").ToString()))
                Me.Set_GEN_ApePaterno(Trim(filas.Item("GEN_apellidoProfesional").ToString()))
                Me.Set_GEN_ApeMaterno(Trim(filas.Item("GEN_sapellidoProfesional").ToString()))
                Me.Set_GEN_numero_documentoPersonas(Trim(filas.Item("GEN_rutProfesional").ToString()))
                Me.Set_GEN_digitoPersonas(Trim(filas.Item("GEN_digitoProfesional").ToString()))


                Me.GEN_dependenciaProfesional = Trim(filas.Item("GEN_dependenciaProfesional").ToString())

                Me.GEN_estadoProfesional = Trim(filas.Item("GEN_estadoProfesional").ToString())
                Me.GEN_idProfesional = filas.Item("GEN_idProfesional").ToString
                Me.Set_GEN_IdSexo(filas.Item("GEN_idSexo").ToString())

            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub Profesionales(ByVal rut_profesional As String)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        ' *** HACE SOBRE CARGA DE METODO, UN COSTRUCTOR BUSCA POR RUT Y EL OTRO POR ID
        Try
            consulta = "SELECT * " _
            & "FROM  GEN_Profesional " _
            & "WHERE " _
            & "GEN_rutProfesional='" & rut_profesional & "'"
            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)

                Me.Set_GEN_ApePaterno(Trim(filas.Item("GEN_apellidoProfesional").ToString()))
                Me.GEN_dependenciaProfesional = Trim(filas.Item("GEN_dependenciaProfesional").ToString())
                Me.Set_GEN_digitoPersonas(filas.Item("GEN_digitoProfesional").ToString())
                Me.GEN_estadoProfesional = Trim(filas.Item("GEN_estadoProfesional").ToString())
                Me.GEN_idProfesional = filas.Item("GEN_idProfesional").ToString
                Me.Set_GEN_Nombre(Trim(filas.Item("GEN_nombreProfesional").ToString()))
                Me.Set_GEN_numero_documentoPersonas(filas.Item("GEN_rutProfesional").ToString())
                Me.Set_GEN_IdSexo(filas.Item("GEN_idSexo").ToString())

            End If

        Catch ex As Exception

        End Try

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region


    ' *** COMIENZA EL CRUD    
    '==========================================================
    Public Sub Set_crear_profesional()
        consulta = "INSERT INTO GEN_Profesional(" _
        & "GEN_rutProfesional, " _
        & "GEN_digitoProfesional, " _
        & "GEN_nombreProfesional, " _
        & "GEN_apellidoProfesional, " _
        & "GEN_sapellidoProfesional, " _
        & "GEN_idsexo, " _
        & "GEN_dependenciaProfesional, " _
        & "GEN_estadoProfesional) " _
        & "VALUES('" & UCase(Me.Get_GEN_numero_documentoPersonas) & "', " _
        & "'" & UCase(Me.Get_GEN_digitoPersonas()) & "', " _
        & "'" & UCase(Me.Get_GEN_Nombre()) & "', " _
        & "'" & UCase(Me.Get_GEN_ApePaterno()) & "', " _
        & "'" & UCase(Me.Get_GEN_ApeMaterno()) & "', " _
        & Me.Get_GEN_Sexo & ", '" _
        & Me.GEN_dependenciaProfesional & "', " _
        & "'" & Me.GEN_estadoProfesional & "')"

        ejecuta_sql(consulta)

        consulta = "SELECT " _
        & "GEN_idProfesional " _
        & "FROM " _
        & "GEN_Profesional " _
        & "WHERE " _
        & "GEN_rutProfesional ='" & Me.Get_GEN_numero_documentoPersonas & "'"

        Set_Gen_Idprofesional(CLng(consulta_sql_devuelve_string(consulta)))

    End Sub

    Public Sub Set_update_profesional()
        consulta = "UPDATE GEN_Profesional " _
        & "SET GEN_rutProfesional ='" & Me.Get_GEN_numero_documentoPersonas & "', " _
        & "GEN_digitoProfesional ='" & UCase(Me.Get_GEN_digitoPersonas()) & "', " _
        & "GEN_nombreProfesional ='" & UCase(Me.Get_GEN_Nombre()) & "', " _
        & "GEN_apellidoProfesional ='" & UCase(Me.Get_GEN_ApePaterno()) & "', " _
        & "GEN_sapellidoProfesional ='" & UCase(Me.Get_GEN_ApeMaterno()) & "', " _
        & "GEN_idsexo = " & Me.Get_GEN_Sexo & ", " _
        & "GEN_dependenciaProfesional ='" & Me.GEN_dependenciaProfesional & "', " _
        & "GEN_estadoProfesional = '" & Me.GEN_estadoProfesional & "' " _
        & "WHERE(GEN_idProfesional = " & Me.GEN_idProfesional & ")"

        ejecuta_sql(consulta)
    End Sub

    ' *** FIN DE CRUD
    '==========================================================


    ' *** COMIENZA LOS GET
    Public Function Get_Gen_Idprofesional()
        Return Me.GEN_idProfesional
    End Function

    Public Function Get_Gen_Dependenciaprofesional()
        Return Me.GEN_dependenciaProfesional
    End Function

    Public Function Get_Gen_Estadoprofesional()
        Return Me.GEN_estadoProfesional
    End Function

    ' *** FIN DE GET
    '==========================================================


    ' *** COMIENZA LOS SET
    Public Sub Set_Gen_Idprofesional(ByVal GEN_idProfesional)
        Me.GEN_idProfesional = GEN_idProfesional
    End Sub

    Public Sub Set_Gen_Dependenciaprofesional(ByVal GEN_dependenciaProfesional)
        Me.GEN_dependenciaProfesional = GEN_dependenciaProfesional
    End Sub

    Public Sub Set_Gen_Estadoprofesional(ByVal GEN_estadoProfesional)
        Me.GEN_estadoProfesional = GEN_estadoProfesional
    End Sub

    ' *** FIN DE SET
    '==========================================================

End Class
