﻿
Public Class Registro_Biopsia : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private consulta As String
    Private ANA_SolicitadaBiopsia As String = Nothing
    Private ANA_ValidadaBiopsia As String = Nothing
    Private ANA_DespachadaBiopsia As String = Nothing

    Private ANA_DictadaBiopsia As String = Nothing
    Private ANA_TumoralBiopsia As String = Nothing

    Private ANA_MotivoAnulaBiopsia As String
    Private GEN_IdUsuarioTecnologo As Integer = Nothing


    Private ANA_CriticoBiopsia As String
    Private ANA_fecDespBiopsia As Date = Nothing
    Private ANA_fecValidaBiopsia As Date
    Private GEN_idUsuarioValida As Integer = Nothing, GEN_IdUsuarioDespacha As Integer = Nothing
    Private ANA_NomRecibeBiopsia As String


    Private ANA_InterconsultaBiopsia As String = "NO"
    Private ANA_CodificadaEstadoRegistro_Biopsias As String
    Private ANA_UltActualizacionBiopsia As String
    Private ANA_RevalidadaBiopsia As String

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region



#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    'Public Function Set_Consulta_CrearRegistroBiopsia()
    '    'Dim v As New Valores_Sistema

    '    'v.Valores_Sistema()

    '    'Me.Set_ANA_NumBiopsia(v.Get_NumBiopsia)
    '    'Me.Set_ANA_AñoBiopsia(v.Get_AñoBiopsia)
    '    Dim m As New MetodosGenerales

    '    Me.Set_ANA_NumBiopsia(m.GetPropiedad("n_biopsia", True))
    '    Me.Set_ANA_AñoBiopsia(m.GetPropiedad("a_biopsia", True))

    '    consulta = "INSERT INTO ANA_Registro_Biopsias (" _
    '    & "ANA_NumeroRegistro_Biopsias, " _
    '    & "ANA_AñoRegistro_Biopsias, " _
    '    & "ANA_fec_IngresoRegistro_Biopsias, " _
    '    & "ANA_fec_RecepcionRegistro_Biopsias, " _
    '    & "GEN_idPaciente, " _
    '    & "ANA_GesRegistro_Biopsias, " _
    '    & "GEN_IdServicioOrigen, " _
    '    & "GEN_IdServicioDestino, " _
    '    & "GEN_IdPrograma, " _
    '    & "ANA_IdMedSolicitaBiopsia, " _
    '    & "ANA_EstadoRegistro_Biopsias, " _
    '    & "ANA_IdCatalogo_Muestras, " _
    '    & "ANA_IdDetalleCatalogo_Muestras, " _
    '    & "PAB_idModalidad, " _
    '    & "ANA_OrganoBiopsia, " _
    '    & "GEN_idTipo_Estados_Sistemas, ANA_UltActualizacionBiopsia ) " _
    '    & "VALUES(" & Me.Get_ANA_NumBiopsia() & ", " _
    '    & Me.Get_ANA_AñoBiopsia() & ", " _
    '    & "'" & Me.Get_ANA_fec_IngresoRegistro_Biopsias() & "', " _
    '    & "'" & Me.Get_ANA_fec_RecepcionRegistro_Biopsias() & "', " _
    '    & Me.Get_GEN_Id_Paciente() & ", " _
    '    & "'" & Me.Get_ANA_GesRegistro_Biopsias() & "', " _
    '    & Me.Get_GEN_IdServicioOrigen() & ", " _
    '    & Me.Get_GEN_IdServicioDestino() & ", " _
    '    & Me.Get_GEN_IdPrograma() & ", " _
    '    & Me.Get_ANA_IdMedSolicitaBiopsia() & ", " _
    '    & "'" & Me.Get_ANA_EstadoRegistro_Biopsias() & "', " _
    '    & Me.Get_ANA_IdCatalogoBiopsia() & ", " _
    '    & Me.Get_ANA_IdDetalleCatalogoBiopsia() & ", '" _
    '    & Me.Get_PAB_idModalidad() & "', " _
    '    & "'" & Me.Get_ANA_OrganoBiopsia() & "', " _
    '    & "42, '" & Now() & "')"

    '    ejecuta_sql(consulta)

    '    consulta = "Select max(ANA_IdBiopsia) as ANA_IdBiopsia FROM  ANA_Registro_Biopsias " _
    '    & "WHERE(ANA_NumeroRegistro_Biopsias = " & Me.Get_ANA_NumBiopsia() & " " _
    '    & "And ANA_AñoRegistro_Biopsias = " & Me.Get_ANA_AñoBiopsia() & ")"

    '    'v.Set_ANA_Num_BiopsiaConfiguracion(v.Get_NumBiopsia() + 1)
    '    m.SetPropiedad("n_biopsia", m.GetPropiedad("n_biopsia", True) + 1)


    '    Return consulta_sql_devuelve_string(consulta)

    'End Function


    Public Function Get_Registro_biopsia_n_biopsia(ByVal n_biopsia As String, ByVal ANA_AñoRegistro_Biopsias As String)
        consulta = "SELECT ANA_IdBiopsia " _
        & "FROM " _
        & "ANA_Registro_Biopsias " _
        & "WHERE " _
        & "(ANA_NumeroRegistro_Biopsias = " & n_biopsia & " And " _
        & "ANA_AñoRegistro_Biopsias = " & ANA_AñoRegistro_Biopsias & ")"
        Return CLng(consulta_sql_devuelve_string(consulta))

    End Function

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================
    Public Sub Set_ANA_CodificadaEstadoRegistro_Biopsias(ByVal ANA_CodificadaEstadoRegistro_Biopsias)
        Me.ANA_CodificadaEstadoRegistro_Biopsias = ANA_CodificadaEstadoRegistro_Biopsias
    End Sub

    Public Sub Set_ANA_CriticoBiopsia(ByVal ANA_CriticoBiopsia)
        Me.ANA_CriticoBiopsia = ANA_CriticoBiopsia
    End Sub

    Public Sub Set_ANA_DespachadaBiopsia(ByVal ANA_DespachadaBiopsia)
        Me.ANA_DespachadaBiopsia = ANA_DespachadaBiopsia
    End Sub

    Public Sub Set_ANA_DictadaBiopsia(ByVal ANA_DictadaBiopsia)
        Me.ANA_DictadaBiopsia = ANA_DictadaBiopsia
    End Sub

    Public Sub Set_ANA_TumoralBiopsia(ByVal ANA_TumoralBiopsia)
        Me.ANA_TumoralBiopsia = ANA_TumoralBiopsia
    End Sub


    Public Sub Set_ANA_SolicitadaBiopsia(ByVal ANA_SolicitadaBiopsia)
        Me.ANA_SolicitadaBiopsia = ANA_SolicitadaBiopsia
    End Sub

    Public Sub Set_ANA_ValidadaBiopsia(ByVal ANA_ValidadaBiopsia)
        Me.ANA_ValidadaBiopsia = ANA_ValidadaBiopsia
    End Sub

    Public Sub Set_ANA_fecValidaBiopsia(ByVal ANA_fecValidaBiopsia)
        Me.ANA_fecValidaBiopsia = ANA_fecValidaBiopsia
    End Sub

    Public Sub Set_ANA_InterconsultaBiopsia(ByVal ANA_InterconsultaBiopsia)
        Me.ANA_InterconsultaBiopsia = ANA_InterconsultaBiopsia
    End Sub

    Public Sub Set_GEN_idUsuarioValida(ByVal GEN_idUsuarioValida)
        Me.GEN_idUsuarioValida = GEN_idUsuarioValida
    End Sub

    Public Sub Set_GEN_IdUsuarioTecnologo(ByVal GEN_IdUsuarioTecnologo)
        If IsNumeric(GEN_IdUsuarioTecnologo) Then
            If GEN_IdUsuarioTecnologo > 0 Then
                Me.GEN_IdUsuarioTecnologo = GEN_IdUsuarioTecnologo
            End If
        End If
    End Sub

    Public Sub Set_ANA_fecDespBiopsia(ByVal ANA_fecDespBiopsia)
        Me.ANA_fecDespBiopsia = ANA_fecDespBiopsia
    End Sub

    Public Sub Set_ANA_NomRecibeBiopsia(ByVal ANA_NomRecibeBiopsia)
        Me.ANA_NomRecibeBiopsia = ANA_NomRecibeBiopsia
    End Sub

    Public Sub Set_GEN_IdUsuarioDespacha(ByVal GEN_IdUsuarioDespacha)
        Me.GEN_IdUsuarioDespacha = GEN_IdUsuarioDespacha
    End Sub

    Public Sub Set_ANA_UltActualizacionBiopsia(ByVal ANA_UltActualizacionBiopsia)
        Me.ANA_UltActualizacionBiopsia = ANA_UltActualizacionBiopsia
    End Sub

    Public Sub Set_ANA_RevalidadaBiopsia(ByVal ANA_RevalidadaBiopsia)
        Me.ANA_RevalidadaBiopsia = ANA_RevalidadaBiopsia
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================
    Public Function Get_ANA_CodificadaEstadoRegistro_Biopsias()
        Return Me.ANA_CodificadaEstadoRegistro_Biopsias
    End Function

    Public Function Get_ANA_CriticoBiopsia()
        Return Me.ANA_CriticoBiopsia
    End Function

    Public Function Get_ANA_DespachadaBiopsia()
        Return Me.ANA_DespachadaBiopsia
    End Function

    Public Function Get_ANA_DictadaBiopsia()
        Return Me.ANA_DictadaBiopsia
    End Function

    Public Function Get_ANA_fecDespBiopsia()
        Return Me.ANA_fecDespBiopsia
    End Function

    Public Function Get_ANA_fecValidaBiopsia()
        Dim retorno As String = DBNull.Value.ToString
        ' *** SON LOS DOS FORMATOS DE FECHA QUE SE DEBEN EVALUAR PARA DECIR QUE TODAVIA NO SE DEFINE
        '==========================================================
        If Me.ANA_fecValidaBiopsia <> "01-01-1900 0:00:00" And Me.ANA_fecValidaBiopsia <> "#12:00:00 AM#" Then
            retorno = Me.ANA_fecValidaBiopsia
        End If
        Return retorno
    End Function

    Public Function Get_ANA_InterconsultaBiopsia()
        Return Me.ANA_InterconsultaBiopsia
    End Function

    Public Function Get_ANA_NomRecibeBiopsia()
        Return Me.ANA_NomRecibeBiopsia
    End Function

    Public Function Get_ANA_RevalidadaBiopsia()
        Return Me.ANA_RevalidadaBiopsia
    End Function

    Public Function Get_ANA_SolicitadaBiopsia()
        Return Me.ANA_SolicitadaBiopsia
    End Function

    Public Function Get_ANA_TumoralBiopsia()
        Return Me.ANA_TumoralBiopsia
    End Function

    Public Function Get_ANA_UltActualizacionBiopsia()
        Return Me.ANA_UltActualizacionBiopsia
    End Function

    Public Function Get_ANA_ValidadaBiopsia()
        Return Me.ANA_ValidadaBiopsia
    End Function

    Public Function Get_GEN_IdUsuarioDespacha()
        Return Me.GEN_IdUsuarioDespacha
    End Function

    Public Function Get_GEN_IdUsuarioTecnologo()
        Return Me.GEN_IdUsuarioTecnologo
    End Function

    Public Function Get_GEN_idUsuarioValida()
        Return Me.GEN_idUsuarioValida
    End Function

    Public Function Getnombre_estado_sistema(id As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("select GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas from GEN_Tipo_Estados_Sistemas where GEN_idTipo_Estados_Sistemas=" & id)
        Return consulta_sql_devuelve_string(sql.ToString())
    End Function


    ' *** FIN DE GET
    '==========================================================
#End Region

End Class