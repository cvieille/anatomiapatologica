﻿Public Class Usuarios : Inherits Personas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private GEN_LoginUsuarios As String, GEN_ClaveUsuarios As String, GEN_IdPerfil As Integer, estado As String, idServicio As String, GEN_IdUsuarios As Integer
    Private GEN_recibe_notificacionesUsuarios As String
    Private ReadOnly consulta_SELECT As String = "SELECT " _
    & "u.GEN_IdUsuarios, " _
    & "u.GEN_loginUsuarios, " _
    & "u.GEN_claveUsuarios, " _
    & "u.GEN_nombreUsuarios, " _
    & "ISNULL(GEN_apellido_paternoUsuarios,'') AS GEN_apellido_paternoUsuarios, " _
    & "ISNULL(GEN_apellido_maternoUsuarios,'') AS GEN_apellido_maternoUsuarios, " _
    & "u.GEN_rutUsuarios, " _
    & "u.GEN_digitoUsuarios, " _
    & "u.GEN_telefonoUsuarios, " _
    & "u.GEN_recibe_notificacionesUsuarios, " _
    & "gsu.GEN_idPerfil, " _
    & "gsu.GEN_estadoSist_Usuario " _
    & "FROM " _
    & "GEN_Usuarios AS u INNER JOIN " _
    & "GEN_Sist_Usuario AS gsu ON " _
    & "u.GEN_IdUsuarios = gsu.GEN_IdUsuarios "

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Usuarios(ByVal id_usuario As Integer)
        ' *** HACE SOBRE CARGA DE METODO, UN COSTRUCTOR BUSCA POR RUT, LOGIN Y EL OTRO POR ID
        '==========================================================
        Dim consulta As String = consulta_SELECT & "WHERE u.GEN_IdUsuarios=" & id_usuario

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        Try
            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)

                Me.GEN_IdUsuarios = Trim(filas.Item("GEN_IdUsuarios")).ToString
                Me.GEN_LoginUsuarios = Trim(filas.Item("GEN_loginUsuarios")).ToString
                Me.GEN_ClaveUsuarios = filas.Item("GEN_claveUsuarios").ToString
                Me.GEN_IdPerfil = filas.Item("GEN_IdPerfil").ToString

                If Not IsDBNull(filas.Item("GEN_nombreUsuarios").ToString()) Then
                    Me.Set_GEN_Nombre(Trim(filas.Item("GEN_nombreUsuarios")).ToString())
                End If

                If filas.Item("GEN_apellido_paternoUsuarios").ToString() <> "" Then
                    Me.Set_GEN_ApePaterno(Trim(filas.Item("GEN_apellido_paternoUsuarios")).ToString())
                End If

                If filas.Item("GEN_apellido_maternoUsuarios").ToString() <> "" Then
                    Me.Set_GEN_ApeMaterno(Trim(filas.Item("GEN_apellido_maternoUsuarios")).ToString())
                End If

                Me.Set_GEN_numero_documentoPersonas(filas.Item("GEN_rutUsuarios").ToString())
                Me.Set_GEN_digitoPersonas(filas.Item("GEN_digitoUsuarios").ToString())
                Me.estado = filas.Item("GEN_estadoSist_Usuario").ToString()
                Me.Set_GEN_telefono(filas.Item("GEN_telefonoUsuarios").ToString())
                Me.GEN_recibe_notificacionesUsuarios = filas.Item("GEN_recibe_notificacionesUsuarios")

            End If
        Catch ex As Exception

        End Try


    End Sub

    Public Sub Usuarios(ByVal rut As String)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        ' *** HACE SOBRE CARGA DE METODO, UN COSTRUCTOR BUSCA POR RUT, LOGIN Y EL OTRO POR ID
        Dim consulta As String = consulta_SELECT & "WHERE u.GEN_rutUsuarios='" & rut & "'"

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        '==========================================================
        If tablas.Rows.Count > 0 Then

            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.GEN_IdUsuarios = Trim(filas.Item("GEN_IdUsuarios")).ToString

            ' *** LLAMO AL CONSTRUCTOR QUE TRAE TODOS LOS DATOS DEL USUARIO
            Usuarios(Me.GEN_IdUsuarios)

        End If

    End Sub

    Public Sub Usuarios(ByVal login As Object)

        '==========================================================
        ' *** HACE SOBRE CARGA DE METODO, UN COSTRUCTOR BUSCA POR RUT, LOGIN Y EL OTRO POR ID
        Dim consulta As String = consulta_SELECT & "WHERE  u.GEN_loginUsuarios='" & login & "'"

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        '==========================================================
        If tablas.Rows.Count > 0 Then

            Dim filas As Data.DataRow = tablas.Rows(0)

            Me.GEN_IdUsuarios = Trim(filas.Item("GEN_IdUsuarios")).ToString

            ' *** LLAMO AL CONSTRUCTOR QUE TRAE TODOS LOS DATOS DEL USUARIO
            '==========================================================
            Usuarios(Me.GEN_IdUsuarios)
        End If

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    Public Function Get_Existe_Usuario_Validador()
        Dim consulta As String = "SELECT COUNT(GEN_loginUsuarios) as cantidad " _
        & "FROM  GEN_Usuarios " _
        & "WHERE GEN_loginUsuarios='" & Me.GEN_LoginUsuarios & "' AND " _
        & "GEN_claveUsuarios=CONVERT(VARCHAR(32),HashBytes('MD5', '" & Me.GEN_ClaveUsuarios & "'),2)"
        Dim cuenta As Byte = consulta_sql_devuelve_string(consulta)

        Return cuenta

    End Function

    Public Sub Set_update_usuario()
        Dim consulta As String = "UPDATE GEN_Usuarios SET " _
        & "GEN_loginUsuarios ='" & Me.GEN_LoginUsuarios & "', " _
        & "GEN_claveUsuarios =CONVERT(VARCHAR(32),HashBytes('MD5', '" & Me.GEN_ClaveUsuarios & "'),2), " _
        & "GEN_nombreUsuarios ='" + Me.Get_GEN_Nombre() + "', " _
        & "GEN_apellido_paternoUsuarios ='" + Me.Get_GEN_ApePaterno() + "', " _
        & "GEN_apellido_maternoUsuarios ='" + Me.Get_GEN_ApeMaterno() + "', " _
        & "GEN_rutUsuarios ='" + Me.Get_GEN_numero_documentoPersonas() + "', " _
        & "GEN_digitoUsuarios ='" + Me.Get_GEN_digitoPersonas() + "', " _
        & "GEN_telefonoUsuarios='" & Get_GEN_telefono() & "', " _
        & "GEN_recibe_notificacionesUsuarios='" & Me.GEN_recibe_notificacionesUsuarios & "' " _
        & "WHERE(GEN_IdUsuarios = " & Me.GEN_IdUsuarios & ")"
        ejecuta_sql(consulta)

        '*** actualiza sistema usuario.
        Update_Usuario_Sistema()

    End Sub

    Public Sub Set_crea_usuario()

        Dim consulta As String = "insert into GEN_Usuarios (GEN_loginUsuarios, " _
        & "GEN_claveUsuarios, " _
        & "GEN_nombreUsuarios, " _
        & "GEN_apellido_paternoUsuarios, " _
        & "GEN_apellido_maternoUsuarios, " _
        & "GEN_rutUsuarios, " _
        & "GEN_digitoUsuarios, " _
        & "GEN_telefonoUsuarios, " _
        & "GEN_recibe_notificacionesUsuarios) " _
        & " values(" _
        & "'" & Me.GEN_LoginUsuarios + "', " _
        & "CONVERT(VARCHAR(32),HashBytes('MD5', '" & Me.GEN_ClaveUsuarios & "'),2), " _
        & "'" & Me.Get_GEN_Nombre() + "', " _
        & "'" + Me.Get_GEN_ApePaterno() + "'," _
        & "'" + Me.Get_GEN_ApeMaterno() + "', " _
        & "'" & Me.Get_GEN_numero_documentoPersonas() + "', " _
        & "'" & Me.Get_GEN_digitoPersonas() + "', " _
        & "'" & Me.Get_GEN_telefono() & "', " _
        & "'" & Me.GEN_recibe_notificacionesUsuarios & "')"
        ejecuta_sql(consulta)

        Dim cons As String = "SELECT " _
        & "GEN_IdUsuarios " _
        & "FROM GEN_Usuarios " _
        & "WHERE  GEN_loginUsuarios='" & Me.GEN_LoginUsuarios & "'"
        Me.GEN_IdUsuarios = CInt(consulta_sql_devuelve_string(consulta))

        Crea_Usuario_Sistema()

    End Sub

    Private Sub Crea_Usuario_Sistema()
        Dim consulta As String = "DELETE FROM GEN_Sist_Usuario " _
        & "WHERE " _
        & "GEN_IdUsuarios=" & Me.GEN_IdUsuarios & " " _
        & " AND GEN_idSistemas=1"
        ejecuta_sql(consulta)

        consulta = "INSERT INTO GEN_Sist_Usuario(" _
        & "GEN_IdUsuarios, " _
        & "GEN_idSistemas, " _
        & "GEN_IdPerfil, " _
        & "GEN_estadoSist_Usuario) " _
        & "VALUES(" _
        & Me.GEN_IdUsuarios & ", " _
        & "1, " _
        & Me.GEN_IdPerfil & ", " _
        & "'" & Me.estado & "') "
        ejecuta_sql(consulta)

    End Sub

    Private Sub Update_Usuario_Sistema()
        Dim consulta As String = "UPDATE GEN_Sist_Usuario SET " _
        & "GEN_idSistemas =1, " _
        & "GEN_IdPerfil =" & Me.Get_GEN_IdPerfil & ", " _
        & "GEN_estadoSist_Usuario ='" & Me.estado & "' " _
        & "WHERE(GEN_IdUsuarios = " & Me.GEN_IdUsuarios & ")"
        ejecuta_sql(consulta)
    End Sub

    Public Function Get_Ingreso_Usuario_a_Sistema()
        Dim consulta = "SELECT        GEN_Usuarios.GEN_loginUsuarios, GEN_Usuarios.GEN_idUsuarios, GEN_Usuarios.GEN_nombreUsuarios, GEN_Usuarios.GEN_apellido_paternoUsuarios, 
GEN_Usuarios.GEN_apellido_maternoUsuarios, GEN_Usuarios.GEN_claveUsuarios, GEN_Usuarios.GEN_recibe_notificacionesUsuarios, 
                         GEN_Usuarios.GEN_rutUsuarios, GEN_Usuarios.GEN_digitoUsuarios, GEN_Usuarios.GEN_telefonoUsuarios, gp.GEN_codigoPerfil, GEN_Profesional.GEN_idProfesional
FROM            dbo.GEN_Perfil AS gp INNER JOIN
                         dbo.GEN_Sist_Usuario AS gsu ON gp.GEN_idSistemas = gsu.GEN_idSistemas AND gp.GEN_idPerfil = gsu.GEN_idPerfil INNER JOIN
                         dbo.GEN_Usuarios  ON gsu.GEN_idUsuarios = GEN_Usuarios.GEN_idUsuarios 
left join GEN_Profesional on GEN_Usuarios.GEN_idPersonas = GEN_Profesional.GEN_idPersonas

WHERE        (gsu.GEN_estadoSist_Usuario = 'Activo') and gp.GEN_idSistemas =1 AND " _
        & "GEN_loginUsuarios='" & Me.GEN_LoginUsuarios & "' AND " _
        & "GEN_claveUsuarios = '" & Me.GEN_ClaveUsuarios & "' "
        Return consulta_sql_datatable(consulta)
    End Function

    Public Function Get_telefono_por_nombre(ByVal GEN_nombreUsuarios As String)
        Dim consulta As String = "SELECT GEN_telefonoUsuarios " _
        & "FROM GEN_Usuarios " _
        & "WHERE GEN_nombreUsuarios = '" & GEN_nombreUsuarios & "'"
        Dim telefono As String = consulta_sql_devuelve_string(consulta)
        Return telefono
    End Function

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================

    Public Sub Set_GEN_LoginUsuarios(ByVal GEN_loginUsuarios)
        Me.GEN_LoginUsuarios = GEN_loginUsuarios
    End Sub

    Public Sub Set_GEN_ClaveUsuarios(ByVal GEN_claveUsuarios)
        Me.GEN_ClaveUsuarios = GEN_claveUsuarios
    End Sub

    Public Sub Set_GEN_IdPerfil(ByVal GEN_IdPerfil)
        Me.GEN_IdPerfil = GEN_IdPerfil
    End Sub

    Public Sub Set_Estado(ByVal estado)
        Me.estado = estado
    End Sub

    Public Sub Set_Idservicio(ByVal idServicio)
        Me.idServicio = idServicio
    End Sub

    Public Sub Set_GEN_IdUsuarios(ByVal GEN_IdUsuarios)
        Me.GEN_IdUsuarios = GEN_IdUsuarios
    End Sub

    Public Sub Set_GEN_recibe_notificacionesUsuarios(ByVal GEN_recibe_notificacionesUsuarios)
        Me.GEN_recibe_notificacionesUsuarios = GEN_recibe_notificacionesUsuarios
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================

    Public Function Get_GEN_recibe_notificacionesUsuarios()
        Return Me.GEN_recibe_notificacionesUsuarios
    End Function

    Public Function Get_GEN_loginUsuarios()
        Return Me.GEN_LoginUsuarios
    End Function

    Public Function Get_GEN_claveUsuarios()
        Return Me.GEN_ClaveUsuarios
    End Function

    Public Function Get_GEN_IdPerfil()
        Return Me.GEN_IdPerfil
    End Function

    Public Function Get_Estado()
        Return Me.estado
    End Function

    Public Function Get_Idservicio()
        Return Me.idServicio
    End Function

    Public Function Get_GEN_idUsuarios()
        Return Me.GEN_IdUsuarios
    End Function

    ' *** FIN DE GET
    '==========================================================
#End Region

End Class