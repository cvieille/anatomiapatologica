﻿Public Class movimientos : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Private ANA_DetalleMovimiento As String
    Private GEN_idTipo_Movimientos_Sistemas As Integer = Nothing
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Public Sub Set_CrearNuevoMoviento()

        Dim consulta As String
        Try
            consulta =
                "INSERT INTO ANA_movimientos (" _
                    & "ANA_IdBiopsia, " _
                    & "ANA_FechaMovimientos, " _
                    & "ANA_DescMovimientos, " _
                    & "GEN_idTipo_Movimientos_Sistemas, " _
                    & "GEN_IdUsuarios) " _
                & "VALUES(" _
                    & Me.Get_ANA_IdBiopsia() & ", " _
                    & "'" & Now & "', "

            If Me.ANA_DetalleMovimiento <> Nothing Then
                consulta &= "'" & Me.ANA_DetalleMovimiento & "', "
            Else
                consulta &= "NULL, "
            End If

            If GEN_idTipo_Movimientos_Sistemas <> Nothing Then
                consulta &= Me.GEN_idTipo_Movimientos_Sistemas & ", "
            Else
                consulta &= "NULL, "

            End If

            consulta &= Me.Get_GEN_IdUsuarios() & " )"

            ejecuta_sql(consulta)
        Catch ex As Exception
            Dim fail As String = ex.Message & ex.StackTrace
        End Try

    End Sub

    Public Sub Set_ANA_DetalleMovimiento(ByVal ANA_DetalleMovimiento As String)
        Me.ANA_DetalleMovimiento = ANA_DetalleMovimiento
    End Sub
    Public Sub Set_GEN_idTipo_Movimientos_Sistemas(ByVal GEN_idTipo_Movimientos_Sistemas As String)
        Me.GEN_idTipo_Movimientos_Sistemas = GEN_idTipo_Movimientos_Sistemas
    End Sub
End Class
