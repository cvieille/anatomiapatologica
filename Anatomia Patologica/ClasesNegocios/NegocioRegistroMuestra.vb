﻿Public Class NegocioRegistroMuestra
    Inherits Registro_Biopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Private ReadOnly GEN_idTipo_Estados_Sistemas As Integer
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Public Function Get_NombrePatologoValida()
        Dim consulta As String = "SELECT " _
        & "ISNULL(rtrim(GEN_nombreUsuarios),'') + ' ' + ISNULL(rtrim(GEN_apellido_paternoUsuarios),'') + ' ' + ISNULL(rtrim(GEN_apellido_maternoUsuarios),'') as Patologo " _
        & "FROM " _
        & "GEN_Usuarios " _
        & "WHERE(GEN_IdUsuarios = " & Me.Get_GEN_idUsuarioValida() & ")"
        Dim dato As String = consulta_sql_devuelve_string(consulta)
        Return dato
    End Function

End Class
