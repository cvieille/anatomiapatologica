﻿Public Class NegocioTecnica
    Inherits TecnicaBiopsia

    Public Sub Set_EntregarTecnicayCreaLamina(ByVal IdUsuario As Long, ByVal ANA_IdTecnicaBiopsia As Integer)
        Dim tb As New TecnicaBiopsia
        tb.TecnicaBiopsia(ANA_IdTecnicaBiopsia)
        tb.Set_Ana_Idtecnicabiopsia(ANA_IdTecnicaBiopsia)
        tb.Set_GEN_idTipo_Estados_Sistemas(46)
        tb.Set_ANA_EstadoTecnica("Entregada a Patologo")
        tb.Set_CambiaEstadoTecnica()

        Dim c As New Cortes
        c.Cortes(tb.Get_ANA_IdCortes_Muestras())
        Dim NombreTecnica As String = Mid(tb.Get_ANA_NombreTecnica(), 1, 3)
        Dim NombreLamina As String = Trim(c.Get_ANA_NomCortes_Muestras()) & "-(" & NombreTecnica & ")"

        Dim l As New Laminas
        l.Set_ANA_IdBiopsia(tb.Get_ANA_IdBiopsia())
        l.Set_ANA_nomLamina(NombreLamina)
        l.Set_GEN_IdUsuarios(IdUsuario)
        l.Set_ANA_IdCortes_Muestras(0)
        l.Set_Crea_lamina()

        Dim mov1 As New movimientos

        mov1.Set_GEN_IdUsuarios(IdUsuario)
        mov1.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov1.Set_GEN_idTipo_Movimientos_Sistemas(96)
        mov1.Set_ANA_DetalleMovimiento("Genera Laminas:" & NombreLamina)
        mov1.Set_CrearNuevoMoviento()
    End Sub

End Class