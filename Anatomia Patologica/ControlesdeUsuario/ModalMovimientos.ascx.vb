﻿Imports Newtonsoft.Json

Public Class ModalMovimientos
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnExportarMovimiento_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id Movimiento") = des(i)(0).ToString()
            d("Fecha") = des(i)(1).ToString()
            d("Tipo Movimiento") = des(i)(2).ToString()
            '   Console.WriteLine(des(i)(3).ToString())

            d("Descripcion") = Convert.ToString(des(i)(3))

            d("Login") = des(i)(4).ToString()

            dic.Add(d)
        Next

        '  Dim workbook As New XLWorkbook
        ' funciones.CrearHojaExcel(workbook, "Movimientos de Biopsia", dic)
        '  funciones.ExportarTablaClosedXml(Me.Page, "Movimientos de Biopsia", workbook)
    End Sub
End Class