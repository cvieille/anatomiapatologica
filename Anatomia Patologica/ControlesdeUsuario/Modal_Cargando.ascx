﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Modal_Cargando.ascx.vb" Inherits="Anatomia_Patologica.Modal_Cargando" %>
<!-- Modal de Cargando -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <div class="loader-container">
          <div class="loader"></div>
        </div>
        <p class="loading-text">Cargando...pE</p>
      </div>
    </div>
  </div>
</div>
