﻿Public Class Interconsultas
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub gdv_BiopsiasInterconsulta_DataBound(sender As Object, e As EventArgs)
        CType(Me.Parent.FindControl("lbl_totalInterconsulta"), Label).Text = gdv_BiopsiasInterconsulta.Rows.Count
        If gdv_BiopsiasInterconsulta.Rows.Count > 0 Then
            CType(Me.Parent.FindControl("lbl_totalInterconsulta"), Label).BackColor = System.Drawing.ColorTranslator.FromHtml("#5cb85c")
        End If
    End Sub

    Protected Sub gdv_BiopsiasInterconsulta_PreRender(sender As Object, e As EventArgs)
        If gdv_BiopsiasInterconsulta.Rows.Count > 0 Then
            If (Not IsNothing(gdv_BiopsiasInterconsulta.HeaderRow)) Then
                gdv_BiopsiasInterconsulta.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If (Not IsNothing(gdv_BiopsiasInterconsulta.FooterRow)) Then
                gdv_BiopsiasInterconsulta.FooterRow.TableSection = TableRowSection.TableFooter
            End If
        End If
    End Sub


    Protected Sub cmd_VerCaso_Click(sender As Object, e As EventArgs)
        Dim idcaso = CType(sender, Button).CommandArgument
        Response.Redirect("../FormAnatomia/frm_Cortes.aspx?id=" & idcaso)
    End Sub

    Protected Sub cmd_ExcelInterconsulta_Click(sender As Object, e As EventArgs) Handles cmd_ExcelInterconsulta.Click
        gdv_BiopsiasInterconsulta.Columns(3).Visible = False
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        Me.gdv_BiopsiasInterconsulta.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.gdv_BiopsiasInterconsulta)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Interconsultas Pendientes</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=InterconsultasPendientes.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class