﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TecnicasPendientes.ascx.vb" Inherits="Anatomia_Patologica.TecnicasPendientes" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:UpdatePanel ID="upd_TecnicasPendientes" runat="server">
    <ContentTemplate>

        <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />

        <!-------------------- Panel Principal de Tecnicas-->
        <div class="text-right" style="margin-bottom: 10px;">
            <asp:Button ID="cmd_ExcelTecnica" runat="server" Text="Exportar" CssClass="btn btn-success" />
            <br />
        </div>

        <asp:GridView ID="gdv_tecnicas" runat="server" AutoGenerateColumns="False"
            DataSourceID="dts_TecBiopsia"
            OnRowDataBound="gdv_tecnicas_DataBound"
            CssClass="table table-bordered table-hover table-responsive">
            <Columns>
                <asp:BoundField DataField="ANA_FecTecnica" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha"
                    SortExpression="ANA_FecTecnica" />
                <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" SortExpression="Nº Corte" />
                <asp:BoundField DataField="ANA_NomTecnica" HeaderText="Técnica" SortExpression="ANA_NomTecnica" />
                <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Sol. Por:" ReadOnly="True" SortExpression="GEN_loginUsuarios" />
                <asp:BoundField DataField="ANA_NombreTecnologo" HeaderText="Tecnólogo" SortExpression="ANA_NombreTecnologo" />
                <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado Casete" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                <asp:BoundField DataField="ANA_SolCortes_Muestras" HeaderText="Solicitado" SortExpression="ANA_SolCortes_Muestras" />
                <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="Estado Corte" SortExpression="GEN_idTipo_Estados_Sistemas" />
                <asp:BoundField DataField="ANA_AlmCortes_Muestras" HeaderText="Almacenado" SortExpression="ANA_AlmCortes_Muestras" />


                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="cmd_SolicitarCasete" runat="server" Text="Solicitar Casete" CssClass="btn btn-warning"
                            OnClientClick="return confirm('¿Esta seguro que desea solicitar este casete ?');"
                            CommandArgument='<%# Eval("ANA_IdTecnicaBiopsia") %>' OnClick="cmd_SolicitarCasete_Click" />
                        <asp:Button ID="cmd_EntregarTecnica" runat="server" Text="Entregar" CssClass="btn btn-success"
                            CommandArgument='<%# Eval("ANA_IdTecnicaBiopsia") %>' OnClick="cmd_EntregarTecnica_Click" />

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="cmd_VerCaso" runat="server" Text="Ver" CssClass="btn btn-info"
                            CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_VerCaso_Click" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="cmd_ExcelTecnica" />
    </Triggers>
</asp:UpdatePanel>

<wuc:Alert ID="wuc_alert" runat="server" />
