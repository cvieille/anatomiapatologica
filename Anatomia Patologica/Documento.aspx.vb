﻿Imports System.IO

Public Class Documento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim nombreArchivo As String = Request.QueryString("nombre")
        Dim idBiopsia As String = Request.QueryString("idBiopsia")
        Page.Title = nombreArchivo
        Me.Title = nombreArchivo

        Dim m As New MetodosGenerales
        '' provisorio mientras los archivos se ordenan en carpetas por id de biopsia
        Dim p As String = m.GetRutaAdjunto()

        Dim imp As New UserImpersonation()
        imp.impersonateUser("uftp", "", "Minsal2023")

        'Dim sPathAnterior As String = p + nombreArchivo
        Dim sPathNueva As String = p + idBiopsia + "\" + nombreArchivo

        'If File.Exists(sPathAnterior) Then
        '    Directory.CreateDirectory(p + idBiopsia + "/")
        '    File.Move(sPathAnterior, sPathNueva)
        'End If
        If IO.File.Exists(sPathNueva) Then
            Dim file As New FileStream(sPathNueva, FileMode.Open)
            Dim ext As String = Path.GetExtension(sPathNueva)
            Select Case ext.ToLower()
                Case ".doc"
                    Response.ContentType = "application/msword"
                Case ".docx"
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                Case ".pdf"
                    Response.ContentType = "application/pdf"
                Case ".xls"
                    Response.ContentType = "application/vnd.ms-excel"
                Case ".xlsx"
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                Case ".gif"
                    Response.ContentType = "image/gif"
                Case ".jpg"
                    Response.ContentType = "image/jpeg"
                Case ".png"
                    Response.ContentType = "image/png"
                Case ".bmp"
                    Response.ContentType = "image/bmp"
                Case ".ppt"
                    Response.ContentType = "application/vnd.ms-powerpoint"
                Case ".pptx"
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
                Case Else
                    Response.ContentType = "application/unknown"
            End Select

            Dim MS As New MemoryStream()

            file.CopyTo(MS)
            MS.CopyTo(file)

            MS.Close()
            file.Close()

            Response.BufferOutput = True
            Response.AddHeader("Content-Disposition", "inline;filename=" + nombreArchivo)
            Response.BinaryWrite(MS.ToArray())
            Response.Flush()
        End If
    End Sub

End Class