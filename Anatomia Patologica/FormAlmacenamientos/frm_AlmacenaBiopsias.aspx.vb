﻿Partial Public Class frm_AlmacenaBiopsias
    Inherits System.Web.UI.Page


    Private Sub frm_biopsias_almacenar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If

        If IsPostBack = False Then



            ' *** INSTANCIA EL CLASE PERFIL
            '==========================================================
            Dim per As New Perfil
            If Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Administrador del Sistema") Then
                '  Me.cmd_EliminarSolicitud.Enabled = True
            End If

            If Not (Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Supervisor") Or Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Administrador del Sistema")) Then
                Me.cmd_desechar.Enabled = False
            End If
        End If
    End Sub


    Protected Sub cmd_VerBiopsia_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' *** CAPTURA ID DE MUESTRA
        '==========================================================
        Dim ANA_IdBiopsia As Long = TryCast(sender, Button).CommandArgument
        Response.Redirect("../FormAnatomia/frm_Cortes.aspx?id=" & ANA_IdBiopsia)
    End Sub
    Protected Sub cmd_exportarBiopsia_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_exportarBiopsia.Click
        gdv_biopsias.Columns(4).Visible = False

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        Me.gdv_biopsias.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.gdv_biopsias)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Biopsias por Almacenar</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=Biopsiasporalmacenar.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class