﻿<%@ Page Title="Almacena Laminas" Language="vb" AutoEventWireup="true" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_AlmacenaLaminas.aspx.vb" Inherits="Anatomia_Patologica.frm_AlmacenaLaminas" Buffer="true" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Laminas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Laminas").find("tr:first"))).dataTable({
                "aaSorting": []
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server">
                <ProgressTemplate>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Laminas para Almacenar</h2>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gdv_Laminas" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dts_laminas"
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccion" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia"
                                DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                                DataTextField="ANA_nomLamina" HeaderText="Lamina" Text="Lamina" Target="_blank" />
                            <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha" SortExpression="ANA_FecLamina" />
                            <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" SortExpression="ANA_EstLamina" />
                            <asp:BoundField DataField="ANA_idLamina" HeaderText="Id. Lamina" InsertVisible="False" ReadOnly="True" SortExpression="ANA_idLamina" />
                        </Columns>
                    </asp:GridView>
                    <div class="DivFullCentrado">
                        <asp:Button ID="cmd_seleccionar_todo" runat="server" Text="Seleccionar Todo" CssClass="btn btn-default" />
                        <asp:Button ID="cmd_seleccionar_todo0" runat="server" Text="Quitar Selección" CssClass="btn btn-default" />
                    </div>
                </div>
            </div>
            
            <wuc:Alert ID="wuc_alert" runat="server" />
            <br />
            <table class="DivFullCentrado">
                <tr>
                    <td>
                        <asp:Button ID="cmd_almacenar" runat="server" Text="Almacenar" CssClass="btn btn-primary" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_desechar" runat="server" Text="Desechar" CssClass="btn btn-warning" />
                    </td>

                    <td>
                        <asp:Button ID="cmd_imprimir" runat="server" Text="Imprimir Selección" CssClass="btn btn-info" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-warning" role="alert">
    </asp:Label>
    <br />

    <asp:SqlDataSource ID="dts_laminas" runat="server"
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
