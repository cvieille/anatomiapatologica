<%@ Page Title="Almacenamiento" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Almacenamiento.aspx.vb" Inherits="Anatomia_Patologica.frm_almacenamiento" %>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAlmacenamientos/frm_Almacenamiento.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Gesti�n de almacenamiento</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabBiopsia" data-toggle="tab">
                                    <label style="color: black">Solicitudes de biopsia&nbsp;<span id="bdgBiopsia" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabCasete" data-toggle="tab">
                                    <label style="color: black">Solicitud de casete&nbsp;<span id="bdgCasete" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabLamina" data-toggle="tab">
                                    <label style="color: black">Solicitud de l�mina&nbsp;<span id="bdgLamina" class="badge">0</span></label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="divTabBiopsia">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table id="tblSolBiopsia" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabCasete">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table id="tblSolCasete" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabLamina">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table id="tblSolLamina" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabMuestraAlmacenar" data-toggle="tab">
                                    <label style="color: black">Muestras para almacenar&nbsp;<span id="bdgMuestraAlmacenar" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabCaseteAlmacenar" data-toggle="tab">
                                    <label style="color: black">Casetes para almacenar&nbsp;<span id="bdgCaseteAlmacenar" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabCaseteDespachado" data-toggle="tab">
                                    <label style="color: black">Casetes despachados&nbsp;<span id="bdgCaseteDespachado" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabLaminaAlmacenar" data-toggle="tab">
                                    <label style="color: black">L�minas para almacenar&nbsp;<span id="bdgLaminaAlmacenar" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabLaminaDespachado" data-toggle="tab">
                                    <label style="color: black">Laminas despachadas&nbsp;<span id="bdgLaminasDespachadas" class="badge">0</span></label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="divTabMuestraAlmacenar">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <hr />
                                        <table id="tblMuestrasAlmacenar" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabCaseteAlmacenar">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table id="tblCaseteAlmacenar" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabCaseteDespachado">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Button runat="server" ID="Button1" OnClick="btnExportarCaseteAlmH_Click" Style="display: none;" />
                                            </div>
                                        </div>
                                        <hr />
                                        <table id="tblCasetedespachadoparaAlmacenar" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="divTabLaminaAlmacenar">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table id="tblLaminaAlmacenar" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabLaminaDespachado">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <hr />
                                        <table id="tblLaminaDesechadaporAlmacenar" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlConfirmacion">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmaci�n</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        &nbsp
                        <label id="lblConfirmacion"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="btnConfirmarModal" class="btn btn-danger">Confirmar</a>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mensaje</h4>
                </div>
                <div class="modal-body">
                    <div id="divMensaje" style="display: none;">
                        <p>
                            <label id="lblModalMensaje"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalC"></label>
                            </b>
                        </p>
                    </div>
                    <hr id="hrSeparador" style="display: none;" />
                    <div id="divMensajeExtra" style="display: none;">
                        <p>
                            <label id="lblModalMensajeE"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalCE"></label>
                            </b>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />

</asp:Content>
