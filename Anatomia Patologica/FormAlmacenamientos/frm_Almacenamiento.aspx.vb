﻿Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_almacenamiento
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    Protected Sub btnExportarMuestrasAlmH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id biopsia") = des(i)(0).ToString()
            d("N°") = des(i)(2).ToString()
            d("Órgano") = des(i)(3).ToString()
            d("Tumoral") = des(i)(4).ToString()
            d("Rápida") = des(i)(5).ToString()
            d("Fecha recepción") = des(i)(6).ToString()
            d("Estado sistemas") = des(i)(7).ToString()
            d("Usuario patólogo") = des(i)(8).ToString()
            d("Usuario tecnólogo") = Convert.ToString(des(i)(9))
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Biopsias por almacenar", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Biopsias por almacenar", workbook)
    End Sub

    Protected Sub btnExportarCaseteAlmH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id corte") = des(i)(1).ToString()
            d("N° corte") = des(i)(5).ToString()
            d("Fecha") = des(i)(6).ToString()
            d("Estado") = des(i)(7).ToString()
            d("Solicitado por") = des(i)(8).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Casetes por almacenar", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Casetes por almacenar", workbook)
    End Sub

    Protected Sub btnExportarLaminaAlmH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id lámina") = des(i)(1).ToString()
            d("Lámina") = des(i)(4).ToString()
            d("Fecha") = des(i)(5).ToString()
            d("Estado") = des(i)(6).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Láminas por almacenar", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Laminas por almacenar", workbook)
    End Sub

    Protected Sub btnExportarBiopsiaH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id biopsia") = des(i)(0).ToString()
            d("N°") = des(i)(1).ToString()
            d("Órgano") = des(i)(2).ToString()
            d("Almacenada") = des(i)(3).ToString()
            d("Tumoral") = des(i)(4).ToString()
            d("Desechada") = des(i)(5).ToString()
            d("Rápida") = des(i)(6).ToString()
            d("Usuario patólogo") = Convert.ToString(des(i)(7))
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Solicitudes de biopsia", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Solicitudes de biopsia", workbook)
    End Sub

    Protected Sub btnExportarCaseteH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id casete") = des(i)(0).ToString()
            d("N°") = des(i)(1).ToString()
            d("Fecha") = des(i)(2).ToString()
            d("Estado") = des(i)(3).ToString()
            d("Solicitado por") = des(i)(4).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Solicitudes de casete", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Solicitudes de casete", workbook)
    End Sub

    Protected Sub btnExportarLaminaH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Id lámina") = des(i)(0).ToString()
            d("N°") = des(i)(1).ToString()
            d("Fecha") = des(i)(2).ToString()
            d("Estado") = des(i)(3).ToString()
            d("Almacenamiento") = des(i)(4).ToString()
            d("Solicitada") = des(i)(5).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Solicitudes de lámina", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Solicitudes de lámina", workbook)
    End Sub
End Class