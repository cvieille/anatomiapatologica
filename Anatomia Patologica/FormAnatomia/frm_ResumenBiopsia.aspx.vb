﻿Partial Public Class frm_ResumenBiopsia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txt_id.Text = Request.QueryString("id")
        If IsPostBack = False Then
            Try
                ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
                '==========================================================
                Dim com As New Combobox
                com.Tipo_Tecnicas_especiales_combobox(Me.cmb_tecnicas_especiales)

                '*** SE REFRESCAN LAS GRILLAS
                '==========================================================
                CargaGrillas()

                Dim cb As New CodificacionBiopsia
                cb.CodificacionBiopsia(Me.txt_id.Text)
                Me.txt_0801001.Text = cb.Get_ANA_Cod001Codificacion_Biopsia()
                Me.txt_0801002.Text = cb.Get_ANA_Cod002Codificacion_Biopsia()
                Me.txt_0801003.Text = cb.Get_ANA_Cod003Codificacion_Biopsia()
                Me.txt_0801004.Text = cb.Get_ANA_Cod004Codificacion_Biopsia()
                Me.txt_0801005.Text = cb.Get_ANA_Cod005Codificacion_Biopsia()
                Me.txt_0801006.Text = cb.Get_ANA_Cod006Codificacion_Biopsia()
                Me.txt_0801007.Text = cb.Get_ANA_Cod007Codificacion_Biopsia()
                Me.txt_0801008.Text = cb.Get_ANA_Cod008Codificacion_Biopsia()

            Catch ex As Exception
                Me.lbl_mensaje.Text = "Se ha producido un error al cargar la pagina. Debido a :" & ex.Message
                Me.lbl_mensaje.Visible = True
            End Try

        End If
    End Sub

    Protected Sub cmd_especiales_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_especiales.Click
        ' *** MUESTRA PANEL CON LISTADO DE TECNICAS 
        '==========================================================
        Panel1.Visible = True
        cmb_tecnicas_especiales.Visible = True
        Me.cmd_agregar_especiales.Visible = True
    End Sub

    Protected Sub cmd_imprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_imprimir.Click
        Try
            Session("dt_Impr_MovCasete") = Nothing

            Dim dt As New DataTable()

            dt.Columns.Add("Nº Corte", GetType(String))
            dt.Columns.Add("Chequeo Nº1", GetType(String))
            dt.Columns.Add("Chequeo Nº2", GetType(String))
            dt.Columns.Add("Fecha", GetType(String))
            dt.Columns.Add("Patologo", GetType(String))
            dt.Columns.Add("Creado Por", GetType(String))
            dt.Columns.Add("Estado", GetType(String))

            Dim Row1 As DataRow

            For Each row As GridViewRow In gdv_cortes.Rows
                Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
                Dim c As New Cortes
                c.Cortes(row.Cells(7).Text)
                If elcheckbox.Checked Then
                    Row1 = dt.NewRow()
                    Row1("Nº Corte") = c.Get_ANA_NomCortes_Muestras()
                    Row1("Chequeo Nº1") = ""
                    Row1("Chequeo Nº2") = ""
                    Row1("Fecha") = c.Get_ANA_FecCortes_Muestras()
                    Row1("Patologo") = row.Cells(4).Text

                    ' *** INSTANCIA EL CLASE USUARIOS
                    '==========================================================
                    Dim u As New Usuarios
                    u.Usuarios(CInt(c.Get_GEN_IdUsuarios()))

                    Row1("Creado Por") = u.Get_GEN_loginUsuarios()
                    Row1("Estado") = c.Get_ANA_EstCortes_Muestras()

                    dt.Rows.Add(Row1)

                End If
            Next

            Session("dt_Impr_MovCasete") = dt

            Dim url As String = "../FormImpresion/frm_SeleccionCasete.aspx"
            Dim s As String = "window.open('" & url + "', 'popup_window', 'width=600,height=500,left=200,top=100,resizable=yes');"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", s, True)


        Catch ex As Exception
            Me.lbl_mensaje.Text = ex.Message
            Me.lbl_mensaje.Visible = True
        End Try
    End Sub

    Protected Sub cmd_solicitar_macro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_solicitar_macro.Click

        Dim Nrm As New NegocioRegistroMuestra
        wuc_alert.showModal(Alert.INFORMACION, Nrm.Set_SolicitaMacroscopiaAlmacenada(Get_IdBiopsiadeNavegador(), Session("id_usuario")))

    End Sub

    Protected Sub cmd_solicitar_casete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_solicitar_casete.Click

        For Each row As GridViewRow In gdv_cortes.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

            If elcheckbox.Checked Then
                '*** INSTANCIA CLASE DE NEGOCIOS REGISTRO MUESTRA
                '==========================================================
                Dim Nc As New NegocioCortes
                Nc.Set_ANA_IdBiopsia(Get_IdBiopsiadeNavegador())
                Nc.Set_ANA_IdCortes_Muestras(row.Cells(7).Text)
                Nc.Set_GEN_IdUsuarios(Session("id_usuario"))

                Me.lbl_mensaje.Visible = True
                Me.lbl_mensaje.Text = Nc.Set_SolicitaCaseteAlmacenado(Session("GEN_CodigoPerfil"))

            End If
        Next
    End Sub

    Protected Sub cmd_para_interconsulta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_para_interconsulta.Click
        ' *** MARCA TACOS PARA INTERCONSULTA. 
        ' *** LOS ALMACENADOS DEBEN SALIR DE BODEGA Y LLEGAR A TECNOLOGO. LOS NO ALMACENADOS SOLO SE MARCAN Y EL TECNOLOGO PREPARA LA CAJA PARA DESPACHO
        '==========================================================
        Dim id_biopsia As String = Request.QueryString("id")
        Dim glosa As String = ""

        For Each row As GridViewRow In gdv_cortes.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

            If elcheckbox.Checked Then

                Try
                    '*** INSTANCIA EL CONSTRUCTOR DE CORTES Y CARGA ID DE CORTE
                    '==========================================================
                    Dim nc As New NegocioCortes
                    nc.Cortes(row.Cells(7).Text)

                    If elcheckbox.Checked Then
                        Try
                            nc.Set_GEN_IdUsuarios(Session("id_usuario"))
                            row.Cells(6).Text = nc.SolicitaCaseteparaInterconsulta()

                        Catch ex As Exception
                            Me.lbl_mensaje.Visible = True
                            Me.lbl_mensaje.Text = ex.Message
                        End Try

                    End If

                Catch ex As Exception

                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message

                End Try

            End If

        Next

        '*** SE REFRESCAN LAS GRILLAS
        '==========================================================
        CargaGrillas()

    End Sub

    Protected Sub cmd_agregar_especiales_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_agregar_especiales.Click

        For Each row As GridViewRow In gdv_cortes.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

            If elcheckbox.Checked Then
                Try
                    '*** INSTANCIA EL CLASE DE NEGOCIOS DE REGISTRO MUESTRA
                    '==========================================================
                    Dim Nrm As New NegocioRegistroMuestra
                    Nrm.Set_GEN_IdUsuarios(Session("id_usuario"))
                    Nrm.Set_ANA_IdCortes_Muestras(row.Cells(7).Text)
                    Nrm.Set_ANA_IdBiopsia(Get_IdBiopsiadeNavegador())
                    Nrm.Set_CreaTecnicaEspecialaMuestra(Me.cmb_tecnicas_especiales.SelectedValue, Me.cmb_tecnicas_especiales.SelectedItem.Text)

                Catch ex As Exception

                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message

                End Try

            End If

        Next
        ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
        '==========================================================
        Dim con As New Consultas
        dts_tecnicas.SelectCommand = con.Ds_con_Tecnicas_Biopsia(Me.txt_id.Text)
        dts_tecnicas.DataBind()
        gdv_tecnicas.DataBind()

    End Sub

    Protected Sub cmd_para_interconsulta1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_para_interconsulta1.Click
        Estado_Laminas("Para Interconsulta")
    End Sub

    Protected Sub cmd_imprime_laminas_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_imprime_laminas.Click
        Try
            Session("dt_Impr_MovLamina") = Nothing

            Dim dt As New DataTable()

            dt.Columns.Add("Lamina", GetType(String))
            dt.Columns.Add("Fecha", GetType(String))
            dt.Columns.Add("Estado", GetType(String))

            Dim Row1 As DataRow

            For Each row As GridViewRow In gdv_laminas.Rows
                Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

                If elcheckbox.Checked Then
                    Row1 = dt.NewRow()
                    Row1("Lamina") = row.Cells(1).Text
                    Row1("Fecha") = row.Cells(2).Text
                    Row1("Estado") = row.Cells(3).Text

                    dt.Rows.Add(Row1)

                End If
            Next
            Session("dt_Impr_MovLamina") = dt
            Dim url As String = "../FormImpresion/frm_SeleccionLaminas.aspx"
            Dim s As String = "window.open('" & url + "', 'popup_window', 'width=600,height=500,left=200,top=100,resizable=yes');"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", s, True)

        Catch ex As Exception
            Me.lbl_mensaje.Text = ex.Message
            Me.lbl_mensaje.Visible = True
        End Try

    End Sub

    Protected Sub cmd_solicitar_casete0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_solicitar_casete0.Click
        Estado_Laminas("Solicitar")
    End Sub

    Private Sub Estado_Laminas(ByVal estado As String)

        For Each row As GridViewRow In gdv_laminas.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion0"), CheckBox)

            Dim m As New movimientos

            If elcheckbox.Checked Then

                Try
                    Dim NL As New NegocioLaminas
                    NL.Set_ANA_idLamina(row.Cells(4).Text)

                    Select Case estado

                        Case "Eliminar"
                            NL.Set_EliminaLaminaPorId(Session("id_usuario"))
                            Exit Select

                        Case "Para Interconsulta"
                            NL.Set_EnviarLaminaaInterconsulta(Session("id_usuario"))
                            Exit Select

                        Case "Solicitar"
                            NL.Set_SolicitarLamina(Session("id_usuario"))
                            Dim Realizado As Boolean = NL.Set_SolicitarLamina(Session("id_usuario"))
                            If Realizado = False Then
                                Me.lbl_mensaje.Visible = True
                                Me.lbl_mensaje.Text = "¡Selecciono laminas que no estaban almacenadas. Se han solicitado solo las almacenadas!"
                            End If
                            Exit Select

                        Case "Almacenar"
                            NL.Get_ANA_EstLamina()
                            NL.Set_EnviarLaminaaAlmacenar(Session("id_usuario"))
                            Exit Select

                    End Select
                    ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
                    '==========================================================
                    Dim con As New Consultas
                    dts_laminas.SelectCommand = con.Ds_con_Laminas_Biopsia(Request.QueryString("id"))
                    dts_laminas.DataBind()

                Catch ex As Exception
                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message
                End Try

            End If

        Next

        '*** SE REFRESCAN LAS GRILLAS
        '==========================================================
        CargaGrillas()

    End Sub

    Protected Sub cmd_seleccionar_todo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_cortes, "chkSeleccion", True)
    End Sub

    Protected Sub cmd_seleccionar_todo0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo0.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_cortes, "chkSeleccion", False)
    End Sub

    Protected Sub cmd_solicitar_casete1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_solicitar_casete1.Click
        Estado_Laminas("Almacenar")
    End Sub

    Protected Sub cmd_enviar_almacena_casete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_enviar_almacena_casete.Click
        Dim id_biopsia As String = Request.QueryString("id")

        For Each row As GridViewRow In gdv_cortes.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

            If elcheckbox.Checked Then
                '*** INSTANCIA EL CONSTRUCTOR DE CORTES Y CARGA ID DE CORTE
                '==========================================================
                Dim c As New Cortes
                c.Cortes(row.Cells(7).Text)
                Try
                    If Trim(row.Cells(6).Text) = "Entregado a Patologo" Then
                        row.Cells(6).Text = "Para Almacenar"
                        '*** CAMBIA ESTADO DE CORTE
                        '==========================================================
                        Dim Nc As New NegocioCortes
                        Nc.Set_ANA_IdCortes_Muestras(row.Cells(7).Text)
                        Nc.Set_ANA_EstCortes_Muestras(row.Cells(6).Text)
                        Nc.Set_ANA_IdBiopsia(row.Cells(8).Text)
                        Nc.Set_GEN_IdUsuarios(Session("id_usuario"))
                        Nc.Set_AAlmacenarCasete()
                    Else
                        Me.lbl_mensaje.Visible = True
                        Me.lbl_mensaje.Text = "Solo se puede enviar a almacenamiento las muestras entregadas a patologo"
                    End If

                Catch ex As Exception
                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message
                End Try
            End If
        Next
        '*** SE REFRESCAN LAS GRILLAS
        '==========================================================
        CargaGrillas()
    End Sub

    Private Function Get_IdBiopsiadeNavegador()
        Dim id_biopsia As String = Request.QueryString("id")
        Return id_biopsia
    End Function

    Private Sub CargaGrillas()
        Try
            ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
            '==========================================================
            Dim con As New Consultas
            dts_cortes.SelectCommand = con.Ds_con_Cortes_Muestras_biopsia(CLng(Get_IdBiopsiadeNavegador()))
            dts_cortes.DataBind()
            gdv_cortes.DataBind()

            dts_laminas.SelectCommand = con.Ds_con_Laminas_Biopsia(Me.txt_id.Text)
            dts_laminas.DataBind()
            gdv_laminas.DataBind()

            dts_tecnicas.SelectCommand = con.Ds_con_Tecnicas_Biopsia(Me.txt_id.Text)
            dts_tecnicas.DataBind()
            gdv_tecnicas.DataBind()

            dts_InmunoHistoquimica.SelectCommand = con.Ds_con_Inmuno_Biopsia(Me.txt_id.Text)
            dts_InmunoHistoquimica.DataBind()
            gdv_InmunoHistoquimica.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmd_select_all3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_select_all3.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_laminas, "chkSeleccion0", True)
    End Sub

    Protected Sub cmd_select_all4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_select_all4.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_laminas, "chkSeleccion0", False)
    End Sub

    Protected Sub cmd_RecalcularCodificacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_RecalcularCodificacion.Click
        Me.txt_0801001.Text = 0
        Me.txt_0801002.Text = 0
        Me.txt_0801003.Text = 0
        Me.txt_0801004.Text = 0
        Me.txt_0801005.Text = 0
        Me.txt_0801006.Text = 0
        Me.txt_0801007.Text = 0
        Me.txt_0801008.Text = 0

        'CONTAR TECNICAS ESPECIALES ES CODIGO 08-01-005 - 
        'TECNICAS ESPECIALES MENOS NIVEL Y DESGASTE Y SUMAR UNA TECNICA CUANDO LA BIOPSIA ESTUVO EN ESTADO EN DESCALCIFICACION
        Dim neg As New Negocio
        Dim suma_tecnica As Integer = neg.Cantidad_Tecnicas(Me.txt_id.Text)
        suma_tecnica = suma_tecnica + neg.Cantidad_Descalcificaciones(Me.txt_id.Text)

        Me.txt_0801005.Text = suma_tecnica

        'CONTAR RAPIDAS ES CODIGO 08-01-006
        Me.txt_0801006.Text = neg.Cantidad_Rapidas(Me.txt_id.Text)

        'CONTAR TACOS
        Dim CantidadTacos As Integer = neg.Cantidad_Cortes(Me.txt_id.Text)
        Dim CantidadTacosDecenas As Double

        CantidadTacosDecenas = CantidadTacos / 20
        Me.txt_0801007.Text = Int(CantidadTacosDecenas)
        If CantidadTacosDecenas - CInt(Me.txt_0801007.Text) > 0 Then
            Me.txt_0801008.Text = 1
        End If

    End Sub

    Protected Sub cmd_GuardarCodificacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_GuardarCodificacion.Click
        If IsNumeric(Me.txt_0801001.Text) And IsNumeric(Me.txt_0801002.Text) And IsNumeric(Me.txt_0801002.Text) And IsNumeric(Me.txt_0801003.Text) And IsNumeric(Me.txt_0801004.Text) And IsNumeric(Me.txt_0801005.Text) And IsNumeric(Me.txt_0801006.Text) And IsNumeric(Me.txt_0801007.Text) And IsNumeric(Me.txt_0801008.Text) Then
            Dim cb As New CodificacionBiopsia
            Dim glosa As String = ""

            cb.CodificacionBiopsia(Me.txt_id.Text)

            cb.Set_ANA_FecCodificacion_Biopsia(Date.Now)
            cb.Set_ANA_IdBiopsia(Me.txt_id.Text)
            cb.Set_ANA_Cod001Codificacion_Biopsia(Me.txt_0801001.Text)
            cb.Set_ANA_Cod002Codificacion_Biopsia(Me.txt_0801002.Text)
            cb.Set_ANA_Cod003Codificacion_Biopsia(Me.txt_0801003.Text)
            cb.Set_ANA_Cod004Codificacion_Biopsia(Me.txt_0801004.Text)
            cb.Set_ANA_Cod005Codificacion_Biopsia(Me.txt_0801005.Text)
            cb.Set_ANA_Cod006Codificacion_Biopsia(Me.txt_0801006.Text)
            cb.Set_ANA_Cod007Codificacion_Biopsia(Me.txt_0801007.Text)
            cb.Set_ANA_Cod008Codificacion_Biopsia(Me.txt_0801008.Text)
            cb.Set_GEN_IdUsuarios(Session("id_usuario"))

            If cb.Get_ANA_IdCodificacion_Biopsia() > 0 Then
                cb.Set_UpdateCodificacion()
                glosa = "Se crea codificación de biopsia"

            Else
                cb.Set_CrearCodificacion()
                glosa = "Se modifica codificación de biopsia"
            End If

            Me.lbl_MensajeCodificacion.Text = "Se ha guardado la codificación con fecha " & Date.Now()
            Me.lbl_MensajeCodificacion.Visible = True

            '*** CREA MOVIMIENTO REALIZADO
            '==========================================================
            Dim mov As New movimientos
            mov.Set_GEN_IdUsuarios(Session("id_usuario"))
            mov.Set_GEN_idTipo_Movimientos_Sistemas(206)
            mov.Set_ANA_IdBiopsia(Me.txt_id.Text)
            mov.Set_ANA_DetalleMovimiento(glosa)
            mov.Set_CrearNuevoMoviento()

        Else
            Me.lbl_MensajeCodificacion.Text = "Revise los valores ingresados"
            Me.lbl_MensajeCodificacion.Visible = True
        End If

    End Sub

    Protected Sub cmd_agregar_inmuno_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_agregar_inmuno.Click

        For Each row As GridViewRow In gdv_cortes.Rows
            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
            If elcheckbox.Checked Then
                Try
                    '*** INSTANCIA EL CLASE DE NEGOCIOS DE REGISTRO MUESTRA
                    '==========================================================
                    Dim Nrm As New NegocioRegistroMuestra
                    Nrm.Set_GEN_IdUsuarios(Session("id_usuario"))
                    Nrm.Set_ANA_IdCortes_Muestras(row.Cells(7).Text)
                    Nrm.Set_ANA_IdBiopsia(txt_id.Text)
                    Nrm.Set_CreaHinmunoaMuestra(Me.cmb_inmuno_histoquimica.SelectedValue, Me.cmb_inmuno_histoquimica.SelectedItem.Text)

                Catch ex As Exception
                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message
                End Try
            End If
        Next

        '*** ACTUALIZA GRILLA DE INMUNO
        '==========================================================
        Dim con As New Consultas
        dts_InmunoHistoquimica.SelectCommand = con.Ds_con_Inmuno_Biopsia(Me.txt_id.Text)
        dts_InmunoHistoquimica.DataBind()
        gdv_InmunoHistoquimica.DataBind()
    End Sub

    Protected Sub cmd_inmuno_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_inmuno.Click
        ' *** MUESTRA PANEL CON LISTADO DE INMUNO 
        '==========================================================
        Me.pnl_inmuno.Visible = True

        ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
        '==========================================================
        Dim com As New Combobox
        com.Tipo_Inmuno_Histoquimicas_combobox(Me.cmb_inmuno_histoquimica)
    End Sub
End Class