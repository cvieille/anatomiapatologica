﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm_SeleccionBiopsias.aspx.vb" Inherits="Anatomia_Patologica.frm_SeleccionBiopsias" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Titulo" >
            Macroscopia en Almacenamiento<asp:Button ID="cmd_impr_tec_pendientes" runat="server" 
            CssClass="BotonNegro" Text="Exportar" TabIndex="10" />
        </div>
    <div style="text-align: center">
    
    <asp:GridView ID="gdv_Biopsias" runat="server" AutoGenerateColumns="True" 
        CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
            Height="100%">
        <RowStyle BackColor="#EFF3FB" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    
    </div>
    </form>
</body>
</html>
