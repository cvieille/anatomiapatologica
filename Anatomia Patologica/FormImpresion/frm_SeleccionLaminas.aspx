﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm_SeleccionLaminas.aspx.vb" Inherits="Anatomia_Patologica.frm_SeleccionLaminas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   
    <div class="Titulo">
        Listado de Lamina<asp:Button ID="cmd_exportar_excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
    </div>

    <asp:GridView ID="gdv_lamina" runat="server" CellPadding="4" 
        HorizontalAlign="Center" ForeColor="#333333" GridLines="None" Width="910px">
        <RowStyle BackColor="#EFF3FB" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    
    </form>
</body>
</html>