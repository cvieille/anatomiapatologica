﻿<%@ Page Title="Inf. Biopsias Recibidas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasRecepcionadas.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasRecepcionadas" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasRecepcionadas.js") %>'}?${new Date().getTime()}` });
    </script>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Biopsias recepcionadas</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control" />
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control" />
                    </div>
                    <div class="col-md-1">
                        <br />
                        <button id="btnBuscar" type="button" class="btn btn-success" style="width:100%;">Buscar</button>
                    </div>
                </div>
            </div>
            <hr />
            <div>
                <table id="tblRecepcionadas" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>
</asp:Content>