﻿Partial Public Class frm_BiopsiasRecepcionadas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    'Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
    '    Dim sCommand As String = Request.Form("__EVENTARGUMENT")

    '    Dim settings As New JsonSerializerSettings()
    '    settings.NullValueHandling = NullValueHandling.Ignore
    '    settings.MissingMemberHandling = MissingMemberHandling.Ignore

    '    Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
    '    Dim dic As New List(Of Dictionary(Of String, String))

    '    For i = 0 To des.Count - 1
    '        Dim d As New Dictionary(Of String, String)
    '        d("N° biopsia") = des(i)(1).ToString()
    '        d("Nombre paciente") = des(i)(2).ToString()
    '        d("Fecha recepción") = des(i)(3).ToString()
    '        d("Dependencia") = des(i)(4).ToString()
    '        d("Origen") = des(i)(5).ToString()

    '        Dim a As Linq.JArray = des(i)(6)
    '        Dim b As New StringBuilder
    '        For index = 0 To a.Count - 1
    '            If (a.Count - 1) = index Then
    '                b.Append(a(index).ToString())
    '            Else
    '                b.AppendLine(a(index).ToString())
    '            End If
    '        Next

    '        d("Órgano") = b.ToString()
    '        d("Fecha despacho") = des(i)(7).ToString()
    '        d("Recibe") = des(i)(8).ToString()
    '        dic.Add(d)
    '    Next

    '    Dim workbook As New XLWorkbook
    '    funciones.CrearHojaExcel(workbook, "Biopsias recepcionadas", dic)
    '    funciones.ExportarTablaClosedXml(Me.Page, "Biopsias recepcionadas", workbook)
    'End Sub
End Class