<%@ Page Title="Biopsias con Neoplasia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasconNeoplasia.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasconNeoplasia" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasconNeoplasia.js") %>'}?${new Date().getTime()}` });
    </script>
    <style>
        ._row{
            border-bottom: solid 1px #00000010;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Biopsias con neoplasia</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control"/>
                    </div>
                    <div class="col-md-2" style="margin-top: 4px;">
                        <br />
                        <button id="btnBuscar" type="button" class="btn btn-success" style="width:100%;">Exportar</button>
                    </div>
                </div>
            </div>
            <div id="divResultadoReporte" style="margin-top: 10px;"></div>
        </div>
    </div>

</asp:Content>
