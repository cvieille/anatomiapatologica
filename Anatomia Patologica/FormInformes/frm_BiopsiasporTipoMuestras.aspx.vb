﻿Partial Public Class frm_BiopsiasporTipoMuestras
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsNothing(Session("ID_USUARIO")) Then
                Response.Redirect("../frm_login.aspx")
            End If
            'If IsPostBack = False Then
            '    ' *** DEFINE RANGO DE FECHAS 
            '    '==========================================================
            '    'Me.txt_fec_final.Text = Today.Date.ToString("yyyy-MM-dd")
            '    'Me.txt_fec_inicio.Text = Today.Date.ToString("yyyy-MM-dd")

            '    'Me.cmb_catalogo.Enabled = False
            '    'Me.cmb_detalle_catalogo.Enabled = False



            '    ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
            '    '==========================================================
            '    Dim con As New Consultas
            '    'dts_cuenta.SelectCommand = con.Ds_con_InfTipoMuestra(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text, 0, 0)
            '    'gdv_Detalle.DataBind()

            'End If

        Catch ex As Exception
        End Try

    End Sub
    '-------------------------------------------------------
    'FUNCION QUE EXPORTA LOS DATOS A UN EXCEL  
    '-------------------------------------------------------
    'Protected Sub cmd_exportar_excel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Excel.Click

    '    If gdv_Detalle.Rows.Count = 0 Then Exit Sub
    '    On Error Resume Next
    '    ' *** DEFINE RANGO DE FECHAS 
    '    '==========================================================
    '    'Dim fec_inicio As String = Me.txt_fec_inicio.Text
    '    'Dim fec_final As String = Me.txt_fec_final.Text

    '    Dim sb As StringBuilder = New StringBuilder()
    '    Dim sw As IO.StringWriter = New IO.StringWriter(sb)
    '    Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '    Dim pagina As Page = New Page
    '    Dim form = New HtmlForm
    '    gdv_Detalle.EnableViewState = False
    '    pagina.EnableEventValidation = False
    '    pagina.DesignerInitialize()
    '    pagina.Controls.Add(form)
    '    form.Controls.Add(gdv_Detalle)
    '    pagina.RenderControl(htw)
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.Write("<h1>Informe por tipo de Muestra</h1>")
    '    Response.Write(Date.Today)
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=BiopsiasporTipo" + Today.Date() + ".xls")
    '    Response.Charset = "UTF-8"
    '    Response.ContentEncoding = Encoding.Default
    '    Response.Write(sb.ToString())
    '    Response.End()

    'End Sub
    '-------------------------------------------------------
    'FUNCION QUE OCULTA UNA COLUMNA DE LA GRILLA 
    '-------------------------------------------------------
    'Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdv_Detalle.RowDataBound
    '    On Error Resume Next
    '    'EN CASO QUE NO SEA LA PRIMERA FILA <HEADER>
    '    If (e.Row.Cells(3).Text <> "Cortes") Then
    '        If Not IsNumeric(e.Row.Cells(3).Text) Then
    '            e.Row.Cells(3).Text = "0"
    '        End If
    '        If Not IsNumeric(e.Row.Cells(4).Text) Then
    '            e.Row.Cells(4).Text = "0"
    '        End If
    '        If Not IsNumeric(e.Row.Cells(5).Text) Then
    '            e.Row.Cells(5).Text = "0"
    '        End If
    '        If Not IsNumeric(e.Row.Cells(6).Text) Then
    '            e.Row.Cells(6).Text = "0"
    '        End If
    '    End If
    'End Sub
    '--------------------------------------------------------------------------------
    'FUNCION QUE BUSCA EL TOTAL DE BIOPSIAS EN EL PERIODO BUSCADO
    '--------------------------------------------------------------------------------
    'Protected Sub Btn_Buscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Buscar.Click

    '    ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
    '    '==========================================================
    '    Dim con As New Consultas

    '    'Dim selectcomand As String = ""
    '    'If Me.CheckBoxTodos.Checked = True Then
    '    '    selectcomand = con.Ds_con_InfTipoMuestra(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text) ', Me.cmb_catalogo.SelectedValue, 0)

    '    'ElseIf Me.CheckBoxTodos.Checked = False Then
    '    '    selectcomand = con.Ds_con_InfTipoMuestra(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text) ', Me.cmb_catalogo.SelectedValue, Me.cmb_detalle_catalogo.SelectedValue)
    '    'End If

    '    'dts_cuenta.SelectCommand = selectcomand
    '    'gdv_Detalle.DataBind()


    'End Sub



    'Private Sub CheckBoxTodos_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxTodos.CheckedChanged
    '    If CheckBoxTodos.Checked Then
    '        cmb_catalogo.Enabled = True
    '        cmb_detalle_catalogo.Enabled = True
    '    Else
    '        cmb_catalogo.Enabled = False
    '        cmb_detalle_catalogo.Enabled = False
    '        cmb_catalogo.Text = "0"
    '        cmb_detalle_catalogo.Text = "0"
    '    End If
    'End Sub
End Class