﻿<%@ Page Title="Inf. Biopsias Consolidada" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ConsolidadoBiopsias.aspx.vb" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_ConsolidadoBiopsias.js") %>'}?${new Date().getTime()}` });
    </script>   
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe consolidado</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control" />
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control" />
                    </div>
                    <div class="col-md-1">
                        <br />
                        <button id="btnBuscar" class="btn btn-success" style="width:100%;">Buscar</button>
                    </div>
                </div>
            </div>
            <hr />
            <div>
                <table id="tblConsolidado" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>
</asp:Content>
