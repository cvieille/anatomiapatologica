﻿Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_impr_consolidado
    Inherits System.Web.UI.Page
    '-------------------------------------------------------
    'VARIABLES GLOBALES  
    '-------------------------------------------------------
    Private ReadOnly tot_tipopac, tot_tipopaci, tot_biopsia, tot_codcinco, tot_codseis, tot_codsiete, tot_codocho As Integer

    Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("N° biopsia") = des(i)(1).ToString()
            d("Tipo paciente") = des(i)(2).ToString()
            d("08-01-001") = des(i)(3).ToString()
            d("08-01-002") = des(i)(4).ToString()
            d("08-01-003") = des(i)(5).ToString()
            d("08-01-004") = des(i)(6).ToString()
            d("08-01-005") = des(i)(7).ToString()
            d("08-01-006") = des(i)(8).ToString()
            d("08-01-007") = des(i)(9).ToString()
            d("08-01-008") = des(i)(10).ToString()
            d("Cortes") = des(i)(11).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Informe consolidado", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Informe consolidado", workbook)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub
End Class