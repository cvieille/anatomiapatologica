﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_DetalleWinsigporServicio.aspx.vb" Inherits="Anatomia_Patologica.frm_DetalleWinsigporServicio" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Cnt_Principal" runat="server">

	<!-- Panel Principal -->
	<div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Detalle de Biopsias por Servicio</div>
        <div class="panel-body">
            
            <br />
        <asp:Label ID="LabelPeriodo" runat="server" Font-Size="Large"></asp:Label>
        <br />        
        <asp:GridView ID="gdv_Detalle" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" 
            CellPadding="4" DataSourceID="dts_DetalleWinsig" ForeColor="#333333" 
            PageSize="50">
            <RowStyle BackColor="#EFF3FB" BorderStyle="None" />
             <Columns>      
                 <asp:BoundField DataField="serv_origen" HeaderText="Servicio" SortExpression="serv_origen" />
                 <asp:BoundField DataField="n_biopsia" HeaderText="Numero de Biopsia" InsertVisible="False" SortExpression="n_biopsia" />
                 <asp:BoundField DataField="Ingreso" HeaderText="Fecha de Ingreso" 
                     InsertVisible="False" SortExpression="Ingreso" DataFormatString="{0:dd-MM-yyyy}" />
                 <asp:BoundField DataField="Recepcion" HeaderText="Fecha de Recepción" 
                     InsertVisible="False" SortExpression="Recepcion" DataFormatString="{0:dd-MM-yyyy}" />
                 <asp:BoundField DataField="Despacho" HeaderText="Fecha de Despacho" 
                     InsertVisible="False" SortExpression="Despacho" DataFormatString="{0:dd-MM-yyyy}" />
                 <asp:BoundField DataField="Valida" HeaderText="Fecha de Validación" 
                     InsertVisible="False" SortExpression="Valida" DataFormatString="{0:dd-MM-yyyy}" />
                 <asp:BoundField DataField="Estado" HeaderText="Estado" InsertVisible="False" SortExpression="Estado" />
            </Columns>
             <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        
        <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false">
        </asp:Label>
            
	    </div>
	</div>
	
    <div style="text-align:center"> 
      
        
    </div>
</asp:Content>