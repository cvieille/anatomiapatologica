﻿<%@ Page Title="Muestras en Macroscopia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MuestrasenMacroscopia.aspx.vb" Inherits="Anatomia_Patologica.frm_MuestrasenMacroscopia" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_MuestrasenMacroscopia.js") %>'}?${new Date().getTime()}` });
    </script>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de muestras en macroscopía</h2>
        </div>
        <div class="panel-body">            
            <table id="tblMuestrasenMacroscopia" class="table table-bordered table-hover table-responsive"></table>
        </div>
    </div>
</asp:Content>
