﻿Partial Public Class frm_MuestrasenMacroscopia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    'Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
    '    Dim sCommand As String = Request.Form("__EVENTARGUMENT")

    '    Dim settings As New JsonSerializerSettings()
    '    settings.NullValueHandling = NullValueHandling.Ignore
    '    settings.MissingMemberHandling = MissingMemberHandling.Ignore

    '    Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
    '    Dim dic As New List(Of Dictionary(Of String, String))

    '    For i = 0 To des.Count - 1
    '        Dim d As New Dictionary(Of String, String)
    '        d("N° biopsia") = des(i)(0).ToString()
    '        d("Tipo biopsia") = des(i)(1).ToString()

    '        Dim a As Linq.JArray = des(i)(2)
    '        Dim b As New StringBuilder
    '        For index = 0 To a.Count - 1
    '            If (a.Count - 1) = index Then
    '                b.Append(a(index).ToString())
    '            Else
    '                b.AppendLine(a(index).ToString())
    '            End If
    '        Next
    '        d("Órgano") = b.ToString()

    '        d("Cortes") = des(i)(3).ToString()
    '        d("Tipo macroscopía") = des(i)(4).ToString()
    '        d("Patólogo") = des(i)(5).ToString()
    '        d("Técnicas") = des(i)(6).ToString()
    '        d("Descalc") = des(i)(7).ToString()
    '        dic.Add(d)
    '    Next

    '    Dim workbook As New XLWorkbook
    '    funciones.CrearHojaExcel(workbook, "Muestras en macroscopia", dic)
    '    funciones.ExportarTablaClosedXml(Me.Page, "Muestras en macroscopia", workbook)
    'End Sub
End Class