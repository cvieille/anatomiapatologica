﻿<%@ Page Title="Informe Winsig" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_WinsigporServicios.aspx.vb" Inherits="Anatomia_Patologica.frm_WinsigporServicios" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_WinsigporServicios.js") %>'}?${new Date().getTime()}` });
    </script>   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe Winsig - Por Servicio</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                <label>Desde:</label>     
                <input type="date" id="txtDesde" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Hasta:</label>
                    <input type="date" id="txtHasta" class="form-control" />
                </div>
                <div class="col-md-3">
                    <br />
                    <div class="form-inline">
                        <button type="button" onclick="buscarInformesWinsig()" class="btn btn-success">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:2em;">
                <div class="col-md-12 ">
                    <table id="tblInformWing" class="table table-striped table-hover table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
