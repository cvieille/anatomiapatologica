﻿Partial Public Class frm_WinsigporServicios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Session("ID_USUARIO")) Then
        '    Response.Redirect("../frm_login.aspx")
        'End If
        'Try
        '    If IsPostBack = False Then
        '        ' *** DEFINE RANGO DE FECHAS 
        '        '==========================================================
        '        Me.txt_fec_final.Text = Today.Date.ToString("yyyy-MM-dd")
        '        Me.txt_fec_inicio.Text = Today.Date.ToString("yyyy-MM-dd")

        '        ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
        '        '==========================================================
        '        Dim con As New Consultas
        '        sqldata_cuenta_biopsias.SelectCommand = con.Ds_con_InfWinsig(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
        '        gdv_datos.DataBind()

        '    End If
        'Catch ex As Exception
        '    LabelMensaje.Visible = True
        '    LabelMensaje.Text = ex.Message
        'End Try

    End Sub
    '-------------------------------------------------------
    'FUNCION QUE EXPORTA LOS DATOS A UN EXCEL  
    '-------------------------------------------------------
    'Protected Sub cmd_exportar_excel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Excel.Click

    '    If gdv_datos.Rows.Count = 0 Then Exit Sub
    '    ' *** DEFINE RANGO DE FECHAS 
    '    '==========================================================
    '    Dim fec_inicio As String = Me.txt_fec_inicio.Text
    '    Dim fec_final As String = Me.txt_fec_final.Text

    '    Dim sb As StringBuilder = New StringBuilder()
    '    Dim sw As IO.StringWriter = New IO.StringWriter(sb)
    '    Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '    Dim pagina As Page = New Page
    '    Dim form = New HtmlForm
    '    gdv_datos.EnableViewState = False
    '    pagina.EnableEventValidation = False
    '    pagina.DesignerInitialize()
    '    pagina.Controls.Add(form)


    '    form.Controls.Add(gdv_datos)
    '    pagina.RenderControl(htw)
    '    Response.Clear()
    '    Response.Buffer = True

    '    Response.Write("<h1>Informe Winsig</h1>")
    '    Response.Write("Detalle de Biopsias por Servicio")
    '    Response.Write("Desde:" & fec_inicio & " - Hasta:" & fec_final)
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=informewinsig-" & fec_inicio & "-" & fec_final & ".xls")
    '    Response.Charset = "UTF-8"
    '    Response.ContentEncoding = Encoding.Default
    '    Response.Write(sb.ToString())
    '    Response.End()



    'End Sub

    '--------------------------------------------------------------------------------
    'FUNCION QUE BUSCA EL TOTAL DE BIOPSIAS EN EL PERIODO BUSCADO
    '--------------------------------------------------------------------------------
    'Protected Sub Btn_Buscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Buscar.Click
    '    If Not (IsDate(Me.txt_fec_inicio.Text) And IsDate(Me.txt_fec_final.Text)) Then
    '        Lbl_error.Visible = True
    '        Exit Sub
    '    Else
    '        Lbl_error.Visible = False
    '    End If

    '    ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
    '    '==========================================================
    '    Dim con As New Consultas
    '    sqldata_cuenta_biopsias.SelectCommand = con.Ds_con_InfWinsig(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
    '    gdv_datos.DataBind()
    '    UpdatePanel1.Update()

    '    Response.Cookies("Fecha_ini").Value = Me.txt_fec_inicio.Text
    '    Response.Cookies("Fecha_fin").Value = Me.txt_fec_final.Text

    'End Sub
End Class