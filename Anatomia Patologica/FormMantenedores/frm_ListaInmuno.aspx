﻿<%@ Page Title="Listado de Inmunohistoquimica" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaInmuno.aspx.vb" Inherits="Anatomia_Patologica.frm_ListaInmuno" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_ListaInmunos.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Listado de Inmuno Histoquimica</h2>
        </div>
        <div class="panel-body">
            <div class="pull-right" style="margin-bottom: 10px;">
                <button id="btnNuevoRegistro" class="btn btn-success">Nuevo registro</button>
            </div>
            <hr />
            <div>
                <table id="tblInmunos" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>

    <div id="mdlInmuno" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de Inmunohistoquímica</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Id:</label>
                            <input id="txtIdInmuno" type="text" disabled class="form-control" value="0" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre:</label>
                            <input id="txtNombreInmuno" type="text" maxlength="50" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Estado:</label>
                            <select id="selEstadoInmuno" class="form-control">
                                <option value="0">-Seleccione-</option>
                                <option value="true">Activo</option>
                                <option value="false">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnGuardarTecnica" class="btn btn-primary">Guardar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>