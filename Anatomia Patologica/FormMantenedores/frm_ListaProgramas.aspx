﻿<%@ Page Title="Listado de programas" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaProgramas.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_programas" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_ListaProgramas.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Listado de programas</h2>
                </div>
                <div class="panel-body">
                    <div class="pull-right" style="margin-bottom: 10px;">
                        <button id="btnNuevoRegistro" class="btn btn-success">Nuevo registro</button>
                    </div>
                    <hr />
                    <div>
                        <table id="tblProgramas" class="table table-bordered table-hover table-responsive"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlEditarPrograma" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de programas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Id:</label>
                            <input id="txtIdPrograma" type="text" disabled class="form-control" value="0"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre:</label>
                            <input id="txtNombrePrograma" type="text" maxlength="50" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnGuardarPrograma" class="btn btn-primary">Guardar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>   
</asp:Content>