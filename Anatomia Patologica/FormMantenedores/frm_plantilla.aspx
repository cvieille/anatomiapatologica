﻿<%@ Page Title="Lista de Plantillas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_plantilla.aspx.vb" Inherits="Anatomia_Patologica.WebForm3" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent"> 
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_plantilla.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Gestión de plantilla</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a id="aAnteClinicos" href="#divTabAnteClinicos" data-toggle="tab">
                                    <label style="color: black">Antecedentes clínicos</label>
                                </a>
                            </li>
                            <li>
                                <a id="aDescMacro" href="#divTabDescMacro" data-toggle="tab">
                                    <label style="color: black">Descripción macroscópica</label>
                                </a>
                            </li>
                            <li>
                                <a id="aExamenMicro" href="#divTabExamenMicro" data-toggle="tab">
                                    <label style="color: black">Exámen microscópico</label>
                                </a>
                            </li>
                            <li>
                                <a id="aDiag" href="#divTabDiag" data-toggle="tab">
                                    <label style="color: black">Diagnóstico</label>
                                </a>
                            </li>
                            <li>
                                <a id="aInmuno" href="#divTabInmuno" data-toggle="tab">
                                    <label style="color: black">Estudio inmunohistoquímico</label>
                                </a>
                            </li>
                            <li>
                                <a id="aNotaAdicional" href="#divTabNotaAdicional" data-toggle="tab">
                                    <label style="color: black">Nota adicional(opcional)</label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nombre de plantilla:</label>
                                    <input id="txtNombrePlantilla" maxlength="100" class="form-control"/>
                                </div>
                            </div>
                            <hr />
                            <div class="row tab-pane fade in active" id="divTabAnteClinicos">
                                <div class="col-md-12">
                                    <label>Antecedentes clínicos:</label>
                                    <textarea id="txtAntecedentesClinicos" maxlength="300" rows="10" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row tab-pane fade" id="divTabDescMacro">
                                <div class="col-md-12">
                                    <label>Descripción macroscópica:</label>
                                    <textarea id="txtDescMacro" rows="10"  maxlength="3000"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row tab-pane fade" id="divTabExamenMicro">
                                <div class="col-md-12">
                                    <label>Exámen microscópico:</label>
                                    <textarea id="txtExamenMicro" rows="10" maxlength="3000" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row tab-pane fade" id="divTabDiag">
                                <div class="col-md-12">
                                    <label>Diagnóstico:</label>
                                    <textarea id="txtDiag" rows="10" maxlength="3000" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row tab-pane fade" id="divTabInmuno">
                                <div class="col-md-12">
                                    <label>Estudio inmunohistoquímico:</label>
                                    <textarea id="txtInmuno" rows="10" maxlength="3000" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row tab-pane fade" id="divTabNotaAdicional">
                                <div class="col-md-12">
                                    <label>Nota adicional(opcional):</label>
                                    <textarea id="txtNotaAdicional" rows="10" maxlength="3000" class="form-control"></textarea>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="btnGuardar" type="button" class="btn btn-success">Guardar plantilla</button>
                                    <button id="btnCancelar" class="btn btn-primary">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
