﻿Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_MovimientosCasetes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub
    Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("ID corte") = des(i)(1).ToString()
            d("N° corte") = des(i)(2).ToString()
            d("Fecha") = des(i)(3).ToString()
            d("Patólogo") = des(i)(4).ToString()
            d("Creado por") = des(i)(5).ToString()
            d("Estado") = des(i)(7).ToString()
            d("Chequeo N°1") = " "
            d("Chequeo N°2") = " "
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Movimiento de Casete", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Movimiento de Casete", workbook)
    End Sub
End Class