﻿<%@ Page Title="Movimientos Laminas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MovimientosLaminas.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosLaminas" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMovimientos/frm_MovimientosLaminas.js") %>'}?${new Date().getTime()}` });
    </script>    
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Movimiento de láminas</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3 col-md-offset-3">
                    <label>Patólogo:</label>
                    <select id="selPatologo" class="form-control">
                        <option value="0">-Todos-</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <br />
                    <a id="btnBuscarMovLaminas" class="btn btn-success" >Buscar</a>
                </div>
            </div>
            <hr />
            <table id="tblMovimientosLaminas" class="table table-bordered table-hover table-responsive"></table>
            <hr />
            <div class="text-center">
                <button id="btnEntregarMedico" class="btn btn-primary">Entregar a patólogo</button>                  
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMensaje" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mensaje</h4>
                </div>
                <div class="modal-body">
                    <div id="divMensaje" style="display:none;">
                        <p><label id="lblModalMensaje"></label></p>
                        <p>No se modificaron los siguientes registros: <b><label id="lblModalC"></label></b></p>
                    </div>
                    <hr id="hrSeparador" style="display:none;" />
                    <div id="divMensajeExtra" style="display:none;">
                        <p><label id="lblModalMensajeE"></label></p>
                        <p>No se modificaron los siguientes registros: <b><label id="lblModalCE"></label></b></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>