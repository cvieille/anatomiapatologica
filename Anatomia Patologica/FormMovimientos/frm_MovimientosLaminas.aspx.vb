﻿Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_MovimientosLaminas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    '<WebMethod>
    'Public Shared Function entregarPatologo(s, idUsuario)
    '    Dim l = JsonConvert.DeserializeObject(Of String()())(s)
    '    Dim lReturn As New List(Of String)
    '    '0 = id lámina
    '    '1 = estado lámina(string)
    '    '2 = patólogo?

    '    For i = 0 To l.Count() - 1
    '        If Trim(l(i)(1)) = "Solicitada" Then
    '            Dim lam As New Laminas
    '            lam.Laminas(l(i)(0))
    '            lam.Set_GEN_idTipo_Estados_Sistemas(46)
    '            lam.Set_Update_Lamina()

    '            ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
    '            '==========================================================
    '            Dim rm As New Registro_Biopsia

    '            rm.Registro_Biopsia(lam.Get_ANA_IdBiopsia())
    '            rm.Set_GEN_idTipo_Estados_Sistemas(46)
    '            rm.Set_ANA_EstadoRegistro_Biopsias("Entregada a Patologo")
    '            rm.Set_ModificaRegistroBiopsia()

    '            '*** CREA MOVIMIENTO REALIZADO
    '            '==========================================================
    '            Dim mov As New movimientos
    '            mov.Set_ANA_IdBiopsia(lam.Get_ANA_IdBiopsia())
    '            mov.Set_GEN_idTipo_Movimientos_Sistemas(87)
    '            mov.Set_ANA_DetalleMovimiento("Se Entrega lamina a patologo:" & l(i)(2))
    '            mov.Set_GEN_IdUsuarios(idUsuario)
    '            mov.Set_CrearNuevoMoviento()
    '        Else
    '            lReturn.Add(l(i)(0))
    '            'Image1.Visible = True
    '            'Me.lbl_debe_guardar.Visible = True
    '            'Me.lbl_debe_guardar.Text = "¡Existen muestras que ya estaban entregadas a patologo!"
    '        End If
    '    Next
    '    Return JsonConvert.SerializeObject(lReturn)
    'End Function

    Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Lámina") = des(i)(2).ToString()
            d("Fecha") = des(i)(3).ToString()
            d("Estado") = des(i)(5).ToString()
            d("Descripción") = des(i)(6).ToString()
            d("Usuario patólogo") = des(i)(7).ToString()
            d("Usuario ingreso") = des(i)(8).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Movimiento de Lámina", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Movimiento de Lámina", workbook)
    End Sub
End Class