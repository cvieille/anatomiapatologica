<%@ Page Title="Consulta Clinicos" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_PrincipalClinico.aspx.vb" Inherits="Anatomia_Patologica.frm_PrincipalClinico" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormPrincipales/frm_PrincipalClinico.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/DataApi/ProfesionalApi.js") %>'}?${new Date().getTime()}` });
    </script>

    <style>
        ._row {
            border-bottom: solid 1px #00000010;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default" id="pnlBiopsiasSolicitadas" style="display: none;">
        <div class="alert alert-info" role="alert" style="font-size: 16px;">
            En esta pantalla se presenta un resumen de la actividad solicita por usted como clinico.<br />
            Si usted ha solicitado biopsias, se mostrar� un listado con el estado de las biopsias para hacer siguimiento.<br />
            Para buscar resultados de pacientes, dirigase al menu superior <b>"Informes -> Resultados por paciente</b>, o haga clic <a href="../frm_consulta_clinicos.aspx"><b>aqu�</b></a>
        </div>
        <div class="panel-heading">
            <h2 class="text-center">Biopsias solicitadas</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 col-md-offset-3">
                    <label>Desde:</label>
                    <input id="txtFechaDesde" required type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Hasta:</label>
                    <input id="txtFechaHasta" required type="date" class="form-control" />
                </div>
                <div class="col-md-2">
                    <br />
                    <button id="btnBuscar" class="btn btn-success" style="width: 100%; margin-top: 4px;">Buscar</button>
                </div>
            </div>
            <hr />
            <span id="spanNotaGes"></span>
            <table id="tblGES" class="table table-bordered  table-hover table-responsive"></table>
            <table id="tblBiopsias" class="table table-bordered  table-hover table-responsive"></table>
        </div>
    </div>
    <br />
    <div class="panel panel-default" id="pnlNotificaciones">
        <div class="panel-heading">
            <h2 class="text-center">Bandeja de notificaciones</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 col-md-offset-1">
                    <label>N� de documento:</label>
                    <div class="input-group">
                        <input id="txtRut" type="text" class="form-control">
                        <span id="txtDigito" class="input-group-addon">-</span>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Nombre:</label>
                    <input id="txtNombre" maxlength="100" type="text" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Apellidos:</label>
                    <input id="txtApellidoP" type="text" maxlength="100" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input id="txtApellidoM" type="text" maxlength="100" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Ficha:</label>
                    <input id="txtFicha" max="6" type="number" class="form-control" />
                </div>

            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-7">
                    <br />
                    <button id="btnLimpiarFiltro" class="btn btn-warning" style="width: 100%; margin-top: 4px;">Limpiar filtros</button>
                </div>
                <div class="col-md-2">
                    <br />
                    <button id="btnBuscarNotificacion" class="btn btn-success" style="width: 100%; margin-top: 4px;">Buscar</button>
                </div>
            </div>
            <hr />
            <table id="tblNotificaciones" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
        </div>
    </div>

    <div class="modal fade" id="mdlInformacionB" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Informaci�n de la biopsia</h4>
                </div>
                <div class="modal-body">
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Fecha de recepci�n:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanFechaRecepcion"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Rut paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanRutPaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Nombre paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanNombrePaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Pat�logo:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanPatologo"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>M�dico solicitante:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanMedico"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio origen:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanOrigen"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio destino:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanDestino"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <strong>Estado:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanEstado"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlNotificacion" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Notificaci�n paciente cr�tico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="containerCheck">
                                Visto
                            <input id="chkVisto" type="checkbox" disabled>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Id�:</label>
                            <input id="txtIdBiopsia" disabled class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>N�:</label>
                            <input id="txtNumero" disabled class="form-control" />
                        </div>
                        <div class="col-md-6">
                            <label>Ubicaci�n interna:</label>
                            <input id="txtUbInterna" disabled class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Documento:</label>
                            <input id="txtDocumento" disabled class="form-control" />
                        </div>
                        <div class="col-md-8">
                            <label>Nombre:</label>
                            <input id="txtNombrePaciente" disabled class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Servicio origen:</label>
                            <input id="txtServicioOrigen" disabled class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <label>Solicitado por:</label>
                            <input id="txtSolicitadoPor" disabled class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <label>Validado por:</label>
                            <input id="txtValidadoPor" disabled class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Estado informe:</label>
                            <input id="txtEstadoInforme" disabled class="form-control" />
                        </div>
                        <div class="col-md-8">
                            <label>�rgano:</label>
                            <input id="txtOrgano" disabled class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nota:</label>
                            <textarea id="txtNota" rows="3" class="form-control" disabled style="resize: none;"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Respuesta notificaci�n:</label>
                            <textarea id="txtRespuestaNotificacion" rows="3" class="form-control" style="resize: none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
                    <button id="btnCerrar" type="button" class="btn btn-warning">Cerrar notificaci�n</button>
                    <button id="btnImprimir" type="button" class="btn btn-info" onclick="imprimirPdf()">Imprimir</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
