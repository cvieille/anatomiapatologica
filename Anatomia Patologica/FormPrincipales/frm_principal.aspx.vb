﻿Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_principal
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    Protected Sub btnExportarTecH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Fecha") = des(i)(3).ToString()
            d("Nº corte") = des(i)(4).ToString()
            d("Técnica") = des(i)(5).ToString()
            d("Solicitado por") = des(i)(6).ToString()
            d("Tecnólogo") = des(i)(7).ToString()
            d("Estado casete") = des(i)(8).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Listado tecnicas especiales", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Listado tecnicas especiales", workbook)
    End Sub

    Protected Sub btnExportarInmunoH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Fecha") = des(i)(4).ToString()
            d("Nº corte") = des(i)(5).ToString()
            d("Inmunohistoquímicas") = des(i)(6).ToString()
            d("Solicitado por") = des(i)(7).ToString()
            d("Tecnólogo") = des(i)(8).ToString()
            d("Estado casete") = des(i)(9).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Listado inmunohistoquimicas", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Listado inmunohistoquimicas", workbook)
    End Sub

    Protected Sub btnExportarInterconsultasH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("N° de biopsia") = des(i)(1).ToString()
            d("Órgano") = des(i)(2).ToString()
            d("Fecha recepción") = des(i)(3).ToString()
            d("Tecnólogo") = des(i)(4).ToString()
            d("Estado") = des(i)(5).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Listado interconsultas", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Listado interconsultas", workbook)
    End Sub
End Class