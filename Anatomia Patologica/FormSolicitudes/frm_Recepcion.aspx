﻿<%@ Page Language="vb" Title="Registro de Muestras" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Recepcion.aspx.vb" Inherits="Anatomia_Patologica.WebForm7" %>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
<script>
    const scripts = [
        '<%= ResolveClientUrl("~/js/FuncionesEntidades/Profesional.js") %>',
        '<%= ResolveClientUrl("~/js/FormSolicitudes/frm_recepcion.combo.js") %>',
        '<%= ResolveClientUrl("~/js/FormSolicitudes/frm_Recepcion.js") %>'
    ];


    scripts.forEach(script => {
        const scriptElement = document.createElement("script");
        scriptElement.src = `${script}?v=${versionSistema}`;
        document.head.appendChild(scriptElement);
    });
</script>

</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Registro de muestras</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row" id="divPestañas">
                    <div class="panel with-nav-tabs panel-info">
                        <div class="panel-heading clearfix">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#divTabTecnicasEspeciales" data-toggle="tab">
                                        <label style="color: black">Recepción</label>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="divTabTecnicasEspeciales">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Id:</label>
                                                    <input id="txtId" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de registro:</label>
                                                    <input id="txtNRegistro" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Fecha de recepción:</label>
                                                    <input type="date" id="txtRecepcion" required class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>&nbsp;</label>
                                                    <button id="btnMovimientos" class="btn btn-info" disabled="disabled" style="width: 100%;">Ver movimientos</button>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <input id="txtIdPaciente" type="hidden" class="form-control" disabled />

                                                <div class="col-md-2">
                                                    <label>Tipo de documento:</label>
                                                    <select id="selDocumento" class="form-control identificacion-Paciente">
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Nº de documento:</label>
                                                    <div class="input-group">
                                                        <input id="txtRut" type="text" maxlength="8" class="form-control identificacion-Paciente">
                                                        <span id="txtguion" class="input-group-addon">-</span>
                                                        <input id="txtDigito" type="text" maxlength="1" class="form-control identificacion-Paciente" style="width: 40%;">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Nombre:</label>
                                                    <input id="txtNombre" type="text" class="form-control datos-paciente" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Apellido paterno:</label>
                                                    <input id="txtApellidoP" type="text" class="form-control datos-paciente" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Apellido materno:</label>
                                                    <input id="txtApellidoM" type="text" class="form-control datos-paciente" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Sexo:</label>
                                                    <select id="selSexo" class="form-control datos-paciente">
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Genero:</label>
                                                    <select id="selGenero" class="form-control datos-paciente">
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Nombre Social:</label>
                                                    <input id="txtNombreSocial" type="text" class="form-control datos-paciente" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Fecha de nacimiento:</label>
                                                    <input type="date" id="txtNacimiento" class="form-control datos-paciente" />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Edad:</label>
                                                    <input id="txtEdad" type="text" class="form-control datos-paciente" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Nacionalidad:</label>
                                                    <select id="selNacionalidad" class="form-control datos-paciente">
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de ubicación interna:</label>
                                                    <input id="txtNUI" type="text" class="form-control datos-paciente" />
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row ">
                                                <div class="col-md-2">
                                                    <label>GES:</label>
                                                    <select id="selGES" class="form-control datos-paciente-adicional">
                                                        <option value="SI">SI</option>
                                                        <option selected value="NO">NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Tipo de paciente:</label>
                                                    <select id="selPaciente" class="form-control datos-paciente-adicional">
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Estado Muestra:</label>
                                                    <input id="txtEstado" type="text" class="form-control" disabled />
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Tipo de muestra:</label>
                                                    <select id="selTipoMuestra" name="selTipoMuestra" class="form-control datos-ingreso-muestra">
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Detalle de muestra:</label>
                                                    <select id="selDetalleMuestra" name="selDetalleMuestra" class="form-control datos-ingreso-muestra">
                                                        <option value="0">-Seleccione-</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Órgano:</label>
                                                    <input id="txtOrgano" name="txtOrgano" type="text" maxlength="200" class="form-control datos-ingreso-muestra">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de muestras por frasco:</label>
                                                    <select id="selMuestrasPorFrasco" name="selMuestrasPorFrasco" class="form-control datos-ingreso-muestra">
                                                        <option value="0">-Seleccione-</option>
                                                        <script>
                                                            for (let i = 1; i <= 15; i++) {
                                                                let option = document.createElement("option");
                                                                option.value = i;
                                                                option.text = i;
                                                                document.getElementById("selMuestrasPorFrasco").add(option);
                                                            }
                                                            let option = document.createElement("option");
                                                            option.value = ">15";
                                                            option.text = ">15";
                                                            document.getElementById("selMuestrasPorFrasco").add(option);
                                                        </script>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <br />
                                                    <a id="btnAgregar" disabled="disabled" class="btn btn-success" style="margin-top: 5px;"><span class="glyphicon glyphicon-plus"></span></a>
                                                </div>
                                            </div>
                                            <hr />
                                            <table id="tblDetalle" class="table table-bordered  table-hover table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Detalle muestra</th>
                                                        <th>Organo</th>
                                                        <th>Muestras por Frasco</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Derivado de:</label>
                                                    <div class="form-inline">
                                                        <select id="selTipoOrigen" class="form-control" disabled>
                                                            <option value="HCM">HCM</option>
                                                            <option value="Externo">Externo</option>
                                                        </select>
                                                        <select id="selServicioOrigen" class="form-control" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Lugar de destino:</label>
                                                    <div class="form-inline">
                                                        <select id="selTipoDestino" class="form-control" disabled>
                                                            <option value="HCM">HCM</option>
                                                            <option value="Externo">Externo</option>
                                                        </select>
                                                        <select id="selServicioDestino" class="form-control" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Programa:</label>
                                                    <select id="selPrograma" class="form-control" disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Solicitado por:</label>
                                                    <select id="selSolicitado" class="form-control" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Antecedentes Clinicos:</label>
                                                    <textarea id="txtAntecedentesClinicos" rows="4" maxlength="300" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button id="btnGuardar" type="button" class="btn btn-primary" style="display: none;">Guardar</button>
                                            <button id="btnCancelar" class="btn btn-warning">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />
</asp:Content>
