﻿Public Class ImprimirInforme
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()

        Response.ContentType = "application/pdf"
        ' la variable nombre es el nombre del archivo .pdf
        Response.AddHeader("Content-disposition", "attachment; filename=" & Request.QueryString("nombre") & ".pdf")
        ' aqui va la ruta del archivo
        '    Response.WriteFile("../pdf/nombre.pdf")
        Response.WriteFile("C:/GIAP/GIAP/dcto/inf/" & Request.QueryString("archivo"))
        Response.Flush()
        Response.Clear()
        Response.End()

    End Sub

End Class