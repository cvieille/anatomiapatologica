﻿<%@ Page Title="Busqueda por Paciente" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_consulta_clinicos.aspx.vb" Inherits="Anatomia_Patologica.frm_consulta_clinicos" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        const scripts = [
            '<%= ResolveClientUrl("~/js/frm_consulta_clinicos.js") %>',
            '<%= ResolveClientUrl("~/js/FuncionesEntidades/Profesional.js") %>'
        ];

        scripts.forEach(script => {
            const scriptElement = document.createElement("script");
            scriptElement.src = `${script}?v=${versionSistema}`;
            document.head.appendChild(scriptElement);
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Búsqueda de resultados por paciente</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 col-md-offset-1">
                    <label>Rut:</label>
                    <div class="input-group">
                        <input id="txtRut" type="text" maxlength="8" class="form-control">
                        <span id="txtDigito" class="input-group-addon">-</span>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>N° ubicación interna:</label>
                    <input id="txtNUI" maxlength="100" type="text" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Nombre:</label>
                    <input id="txtNombre" maxlength="100" type="text" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Apellidos:</label>
                    <input id="txtApellidoP" type="text" maxlength="100" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input id="txtApellidoM" type="text" maxlength="100" class="form-control" />
                </div>

            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-7">
                    <br />
                    <button type="button" id="btnBuscar" class="btn btn-success" style="margin-top: 5px; width: 100%;">Buscar exámenes</button>
                </div>
                <div class="col-md-2">
                    <br />
                    <button id="btnLimpiar" class="btn btn-warning" disabled style="margin-top: 5px; width: 100%;">Limpiar campos</button>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <table id="tblPaciente" class="table table-bordered table-hover table-responsive"></table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="mdlBiopsia">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de biopsia</h4>
                </div>
                <div class="modal-body">
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Id Biopsia:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanIdBiopsia"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Fecha de recepción:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanFechaRecepcion"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Rut paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanRutPaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Nombre paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanNombrePaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Órgano:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanOrgano"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Patólogo:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanPatologo"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Tecnólogo:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanTecnologo"></span>
                        </div>
                    </div>                   
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio origen:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanServOrigen"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio destino:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanServDestino"></span>
                        </div>
                    </div>
                    <hr />

                    <div id="dvCodificacion" class="row">
                        <div class="col-md-12">
                            <h4>Codificación del Caso</h4>
                            <table class="table table-bordered table-hover table-responsive">
                                <tr>
                                    <td>
                                        <label>Total 08-01-001</label>
                                        <input id="txt0801001" class="form-control" type="number" title="Citodiagnóstico corriente (PAP)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-002</label>
                                        <input id="txt0801002" class="form-control" type="number" title="Citología aspirativa (por punción)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-003</label>
                                        <input id="txt0801003" class="form-control" type="number" title="Estudio histopatológico con microscopía electrónica (por cada órgano)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-004</label>
                                        <input id="txt0801004" class="form-control" type="number" title="Estudio histopatológico c/técnicas de inmunohistoquímica o inmunofluorescencia (por cada órgano)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-005</label>
                                        <input id="txt0801005" class="form-control" type="number" title="Estudio histopatológico con técnicas histoquímicas especiales (incluye descalcificación) (por cada órgano)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-006</label>
                                        <input id="txt0801006" class="form-control" type="number" title="Estudio histopatol. de biopsia contemporánea (ráp.) a intervenc. quirúrgicas (X cada órgano, no incl. biopsia diferida)" max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-007</label>
                                        <input id="txt0801007" class="form-control" type="number" title="Estudio histopatológico con tinción corriente de biopsia 
                                    diferida con estudio seriado (mínimo 10 muestras) de un órgano o parte de él
                                    (no incluye estudio con técnica habitual de otros órganos incluidos en la muestra)"
                                            max="999" value="0" disabled />
                                    </td>
                                    <td>
                                        <label>Total 08-01-008</label>
                                        <input id="txt0801008" class="form-control" type="number" title="Estudio histopatológico corriente de biopsia diferida (por cada órgano)" max="999" value="0" disabled />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="dvAdjuntos">
                        <h4>Documentos adjuntos:</h4>
                        <table id="tblArchivo" class="table table-bordered table-hover table-responsive" style="width: 100%; font-size: small;"></table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnImprimirInforme" type="button" class="btn btn-info" data-dismiss="modal">Ver informe</button>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
