﻿Partial Public Class WebForm11
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    Protected Sub cmd_tecnicas_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_tecnicas.Click
        Response.Redirect("frm_lista_tecnicas.aspx")
    End Sub

    Protected Sub cmd_programas_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_programas.Click
        Response.Redirect("frm_lista_programas.aspx")
    End Sub

    Protected Sub cmd_programas0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_programas0.Click
        Response.Redirect("frm_lista_servicios.aspx")
    End Sub

    Protected Sub cmd_medicos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_medicos.Click
        Response.Redirect("frm_lista_medicos.aspx")
    End Sub
End Class