﻿Partial Public Class frm_pdf_solicitud
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim reporte As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER) 'tipo carta 
        Dim Frase As iTextSharp.text.Phrase
        Dim Parrafo As iTextSharp.text.Paragraph
        Dim archivo As String

        'RESCATO EL PARAMETRO CON EL ID DE LA SOLICITUD
        Dim id_solicitud As String
        Me.txt_id.Text = Request.QueryString("id")
        id_solicitud = Request.QueryString("id")

        'SE DECLARAN LAS VARIABLES DE CONEXION
        Dim cnnBaseDat As SqlClient.SqlConnection
        Dim comBaseDat As SqlClient.SqlCommand
        Dim adpBaseDat As SqlClient.SqlDataAdapter

        Dim dstTablas As DataSet
        cnnBaseDat = New SqlClient.SqlConnection
        cnnBaseDat.ConnectionString = cadena_conexion
        cnnBaseDat.Open()

        Dim consulta As String
        Try
            consulta = "SELECT * FROM vi_reporte_solicitud WHERE id=" & id_solicitud
            comBaseDat = New SqlClient.SqlCommand(consulta, cnnBaseDat)
            adpBaseDat = New SqlClient.SqlDataAdapter(comBaseDat)
            dstTablas = New DataSet
            adpBaseDat.Fill(dstTablas, "MiTabla")

            Dim tablas As Data.DataTable = dstTablas.Tables("MiTabla")
            Dim filas As Data.DataRow = tablas.Rows(0)

            'SE COMPLETA LOS DATOS DE LA PRIMERA TABLA
            Me.txt_ot.Text = Trim(filas.Item("OT").ToString() + "-" + filas.Item("AÑO").ToString())
            Me.txt_ges.Text = Trim(filas.Item("ANA_GesRegistro_Biopsias").ToString())
            Me.txt_nombre.Text = Trim(filas.Item("NOMBRE_PACIENTE").ToString())
            Me.txt_apellidos.Text = Trim(filas.Item("APELLIDO_PACIENTE").ToString())
            Me.txt_rut.Text = Trim(filas.Item("RUT_PACIENTE").ToString() + "-" + filas.Item("DIGITO").ToString())
            Me.txt_ficha.Text = Trim(filas.Item("FICHA").ToString())
            Me.txt_fnac.Text = FormatDateTime(Trim(filas.Item("FNACIMIENTO").ToString()), 2)
            Dim Meses_Totales = DateDiff("m", Me.txt_fnac.Text, Date.Today)
            Dim Years = Int(Meses_Totales / 12)
            Me.txt_edad.Text = Years & " años " & Meses_Totales - (Years * 12) & " meses"
            Me.txt_catalogo.Text = (Trim(filas.Item("MUESTRA").ToString())).ToUpper()

            'SE COMPLETA LOS DATOS DE LA TABLA FINAL
            Me.txt_derivado.Text = Trim(filas.Item("DERIVADO").ToString()) + " - " + Trim(filas.Item("S_DERIVADO").ToString())
            Me.txt_hipo.Text = Trim(filas.Item("HIPOTESIS").ToString())
            Me.txt_ante.Text = Trim(filas.Item("ANTECEDENTE").ToString())
            Me.txt_fmuestra.Text = FormatDateTime(Trim(filas.Item("INGRESO").ToString()), 2)
            If filas.Item("RECEPCION").ToString() <> Nothing Then
                Me.txt_frecibe.Text = FormatDateTime(Trim(filas.Item("RECEPCION").ToString()), 2)
            End If
            Me.txt_destino.Text = Trim(filas.Item("DESTINO").ToString()) + " - " + Trim(filas.Item("S_DESTINO").ToString())
            Me.txt_medico.Text = Trim(filas.Item("MEDICO").ToString())

        Catch ex As Exception
            Me.lbl_mensaje.Visible = True
            Me.lbl_mensaje.Text = ex.Message
        End Try

        'SE DECLARA LA RUTA EN SERVIDOR DONDE QUEDA UNA COPIA DEL PDF
        'archivo = "C://GIAP/GIAP/dcto/sol/sol" + DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf"
        archivo = "C://GIAP/GIAP/dcto/sol/sol" + DateTime.Now.ToString("yyyyMMdd") + ".pdf"
        iTextSharp.text.pdf.PdfWriter.GetInstance(reporte, New IO.FileStream(archivo, IO.FileMode.Create))

        Dim Tabla As iTextSharp.text.pdf.PdfPTable
        Dim Celda As iTextSharp.text.pdf.PdfPCell

        Parrafo = New iTextSharp.text.Paragraph()
        Parrafo.Alignment = iTextSharp.text.Element.ALIGN_CENTER
        Parrafo.Leading = 10.0!

        'ABRO EL DOCUMENTO
        reporte.Open()

        'AGREGO EL LOGO EN EL BORDE SUPERIOR IZQUIERDO
        Dim jpga As iTextSharp.text.Image 'Declaracion de una imagen
        jpga = iTextSharp.text.Image.GetInstance("C://GIAP/GIAP/imagenes/logo.jpg") 'Dirreccion a la imagen que se hace referencia
        jpga.ScaleAbsoluteWidth(180) 'Ancho de la imagen
        jpga.ScaleAbsoluteHeight(80) 'Altura de la imagen
        'reporte.Add(jpga)

        'AGREGO LA PRIMERA TABLA 
        'Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
        Tabla = New iTextSharp.text.pdf.PdfPTable(4) With {
            .WidthPercentage = "100"
        }
        Celda = New iTextSharp.text.pdf.PdfPCell(jpga)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Tabla.SetWidths(New Single() {35.0!, 30.0!, 20.0!, 15.0!})
        Celda.Rowspan = 8
        Celda.Colspan = 2
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("N° OT:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_ot.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("GES:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_ges.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
        }
        Tabla.AddCell(Celda)


        Frase = New iTextSharp.text.Phrase("Fecha Toma de Muestra:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_fmuestra.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("Fecha Recepción:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_frecibe.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .Border = iTextSharp.text.Rectangle.BOTTOM_BORDER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Border = 0,
            .Colspan = 4
        }
        Tabla.AddCell(Celda)
        Tabla.AddCell(Celda)

        reporte.Add(Tabla)

        'AGREGO EL TITULO DEL REPORTE
        Tabla = New iTextSharp.text.pdf.PdfPTable(1)
        Tabla.WidthPercentage = "100"
        Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Frase = New iTextSharp.text.Phrase(" SOLICITUD DE ESTUDIO HISTOPATOLOGICO Y CITOLOGICO ")
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .Colspan = 4,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ")
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 4,
            .Border = 0
        }
        Tabla.AddCell(Celda)
        reporte.Add(Tabla)

        'ESTA ES LA PRIMERA FILA DEL DOCUMENTO
        Tabla = New iTextSharp.text.pdf.PdfPTable(4)
        Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Tabla.WidthPercentage = "100"
        Tabla.SetWidths(New Single() {20.0!, 30.0!, 20.0!, 30.0!})

        'FILA 1 DE LA TABLA
        Frase = New iTextSharp.text.Phrase("Nombre del Paciente:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_nombre.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("Apellidos:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_apellidos.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 4,
            .Border = 0
        }
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'TERCERA FILA DE LA TABLA
        Frase = New iTextSharp.text.Phrase("Rut :", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_rut.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("Ficha Clínica:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_ficha.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 4,
            .Border = 0
        }
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'CUARTA FILA DE LA TABLA
        Frase = New iTextSharp.text.Phrase("Edad:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_edad.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("Fecha Nacimiento:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_fnac.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 4,
            .Border = 0
        }
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'AGREGO LA TABLA EN EL REPORTE
        reporte.Add(Tabla)

        'SE CREA LA TABLA CON EL DETALLE DE LAS MUESTRAS
        Tabla = New iTextSharp.text.pdf.PdfPTable(4)
        Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Tabla.WidthPercentage = 100.0!
        Tabla.SetWidths(New Single() {10.0!, 40.0!, 30.0!, 20.0!})

        Frase = New iTextSharp.text.Phrase(Me.txt_catalogo.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Colspan = 4
        }
        Tabla.AddCell(Celda)

        'FILA UNO CON EL ENCABEZADO
        Frase = New iTextSharp.text.Phrase("FRASCOS", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        }

        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("DETALLE MUESTRA", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        }

        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("DESCRIPCION", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        }

        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("N° DE MUESTRAS", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        }

        Tabla.AddCell(Celda)

        For Each row As GridViewRow In GridView1.Rows

            'SEGUNDA FILA CON EL DETALLE DE LAS MUESTRAS
            Frase = New iTextSharp.text.Phrase(row.Cells(0).Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE

            Tabla.AddCell(Celda)

            Dim str As String
            str = HttpUtility.HtmlDecode(row.Cells(1).Text)

            Frase = New iTextSharp.text.Phrase(str.ToUpper(), iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE

            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(row.Cells(2).Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE

            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(row.Cells(3).Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE

            Tabla.AddCell(Celda)
        Next

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 3,
            .Border = 0
        }
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'SE AGREGA LA SEGUNDA TABLA
        reporte.Add(Tabla)

        'COMIENZO A CREAR LA SEGUNDA TABLA
        Tabla = New iTextSharp.text.pdf.PdfPTable(2)
        Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Tabla.WidthPercentage = 100.0!
        Tabla.SetWidths(New Single() {40.0!, 60.0!})

        'FILA 1
        Frase = New iTextSharp.text.Phrase("Derivado de:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_derivado.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED,
            .VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE,
            .Border = 0
        }
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase) With {
            .Colspan = 2,
            .Border = 0
        }
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'FILA 2
        Frase = New iTextSharp.text.Phrase("Hipótesis Clínica:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_hipo.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Colspan = 2
        Celda.Border = 0
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'FILA 3
        Frase = New iTextSharp.text.Phrase("Antecedentes Clínicos:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_ante.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Colspan = 2
        Celda.Border = 0
        Celda.Border = 0
        Tabla.AddCell(Celda)

        'FILA 4
        Frase = New iTextSharp.text.Phrase("Lugar de Destino:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_destino.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Colspan = 2
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Tabla.AddCell(Celda)

        'FILA 5 - MEDICO Y FIRMA
        Frase = New iTextSharp.text.Phrase("Médico Solicitante:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER

        Tabla.AddCell(Celda)
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(Me.txt_medico.Text, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Colspan = 2
        Celda.Border = 0
        Tabla.AddCell(Celda)
        Tabla.AddCell(Celda)

        'SE AGREGA LA TERCERA TABLA
        reporte.Add(Tabla)

        'SE CREA LA TABLA CON EL MENSAJE
        Tabla = New iTextSharp.text.pdf.PdfPTable(1)
        Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Tabla.WidthPercentage = 100.0!
        Tabla.SetWidths(New Single() {100.0!})

        Frase = New iTextSharp.text.Phrase("CONDICION DE LA MUESTRA", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = iTextSharp.text.Rectangle.TOP_BORDER
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("LA MUESTRA DEBE VENIR EN FRASCO HERMÉTICO, RESISTENTE Y ROTULADO.", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("EL FIJADOR (FORMALINA NEUTRA AL 10%) DEBE CUBRIR TOTALMENTE LA MUESTRA", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 8, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase("Fecha de Impresión:" + Now, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 6, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
        Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        Celda.Border = 0
        Tabla.AddCell(Celda)

        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Border = 0
        Tabla.AddCell(Celda)
        Tabla.AddCell(Celda)

        'SE AGREGA LA CUARTA TABLA
        reporte.Add(Tabla)

        'AGREGO EL LOGO DEL PIE DE PAGINA
        'jpga = iTextSharp.text.Image.GetInstance("C://GIAP/GIAP/imagenes/logo_azul.jpg") 'Dirreccion a la imagen que se hace referencia
        'jpga.SetAbsolutePosition(250, 75) 'Posicion en el eje cartesiano
        'jpga.ScaleAbsoluteWidth(100) 'Ancho de la imagen
        'jpga.ScaleAbsoluteHeight(50) 'Altura de la imagen
        'reporte.Add(jpga)

        'AGREGO EL PIE DE PAGINA
        jpga = iTextSharp.text.Image.GetInstance("C://GIAP/GIAP/imagenes/pie.jpg") 'Dirreccion a la imagen que se hace referencia
        jpga.SetAbsolutePosition(40, 35) 'Posicion en el eje cartesiano
        jpga.ScaleAbsoluteWidth(525) 'Ancho de la imagen
        jpga.ScaleAbsoluteHeight(30) 'Altura de la imagen
        reporte.Add(jpga)

        'SE CIERRA EL DOCUMENTO
        reporte.Close()

        System.Diagnostics.Process.Start(archivo)

    End Sub

End Class