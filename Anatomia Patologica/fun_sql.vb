﻿Friend Module fun_sql
    '------------------------------------------------------------------------------
    ' FUNCION QUE EJECUTA LAS CONSULTAS A LA BASE DE DATOS Y DEVUELVE UN DATATABLE 
    '------------------------------------------------------------------------------
    Public Function consulta_sql_datatable(ByVal consulta_sql3 As String)
        Dim cnnBaseDat As SqlClient.SqlConnection
        Dim comBaseDat As SqlClient.SqlCommand
        Dim adpBaseDat As SqlClient.SqlDataAdapter

        Dim dstTablas As DataSet
        cnnBaseDat = New SqlClient.SqlConnection
        cnnBaseDat.ConnectionString = cadena_conexion
        cnnBaseDat.Open()

        comBaseDat = New SqlClient.SqlCommand(consulta_sql3, cnnBaseDat)
        adpBaseDat = New SqlClient.SqlDataAdapter(comBaseDat)
        dstTablas = New DataSet
        adpBaseDat.Fill(dstTablas, "MiTabla")

        Dim tablas As Data.DataTable = dstTablas.Tables("MiTabla")

        cnnBaseDat.Close()
        comBaseDat.Connection.Close()

        Return tablas

    End Function

    '------------------------------------------------------------------------------
    ' FUNCION QUE EJECUTA LAS CONSULTAS A LA BASE DE DATOS Y DEVUELVE UN STRING 
    '------------------------------------------------------------------------------
    Public Function consulta_sql_devuelve_string(ByVal consulta_sql2 As String)
        Dim cnnBaseDat As SqlClient.SqlConnection
        Dim comBaseDat As SqlClient.SqlCommand

        cnnBaseDat = New SqlClient.SqlConnection
        cnnBaseDat.ConnectionString = cadena_conexion
        cnnBaseDat.Open()

        comBaseDat = New SqlClient.SqlCommand(consulta_sql2, cnnBaseDat)

        Dim n As String
        n = CStr(comBaseDat.ExecuteScalar())

        cnnBaseDat.Close()
        comBaseDat.Connection.Close()

        Return n

    End Function

    '------------------------------------------------------------------------------
    ' FUNCION QUE EJECUTA LAS CONSULTAS A LA BASE DE DATOS
    '------------------------------------------------------------------------------
    Public Function ejecuta_sql(ByVal consulta_sql As String, Optional ByVal bID As Boolean = False)
        Dim cnnBaseDat As SqlClient.SqlConnection
        Dim comBaseDat As SqlClient.SqlCommand

        cnnBaseDat = New SqlClient.SqlConnection
        cnnBaseDat.ConnectionString = cadena_conexion
        cnnBaseDat.Open()

        comBaseDat = New SqlClient.SqlCommand(consulta_sql, cnnBaseDat)

        Dim iID As Integer = 0
        If bID Then
            iID = comBaseDat.ExecuteScalar()
        Else
            comBaseDat.ExecuteNonQuery()
        End If

        cnnBaseDat.Close()
        comBaseDat.Connection.Close()

        Return iID
    End Function

    Public Sub paciente_orden()

        Dim myConString As String = "DRIVER={MySQL ODBC 3.51 Driver};" + "SERVER=10.6.180.142:15000;" + "DATABASE=BD_ENTI_CORPORATIVA;" + "UID=cvieille;" + "PASSWORD=cv2014;" + "OPTION=3"
        Dim cnnBaseDat As Odbc.OdbcConnection

        cnnBaseDat = New Odbc.OdbcConnection
        cnnBaseDat.ConnectionString = myConString

    End Sub
End Module
