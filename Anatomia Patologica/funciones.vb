﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Web.Configuration

Imports ClosedXML.Excel

Friend Module funciones

    Public cadena_conexion As String = ConfigurationManager.ConnectionStrings("anatomia_patologicaConnectionString").ConnectionString


    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
    Public Function RutDigito(ByVal Rut As Integer) As String
        Dim Digito As Integer
        Dim Contador As Integer
        Dim Multiplo As Integer
        Dim Acumulador As Integer

        Contador = 2
        Acumulador = 0
        While Rut <> 0
            Multiplo = (Rut Mod 10) * Contador
            Acumulador = Acumulador + Multiplo
            Rut = Rut \ 10
            Contador = Contador + 1
            If Contador = 8 Then
                Contador = 2
            End If
        End While
        Digito = 11 - (Acumulador Mod 11)
        RutDigito = CStr(Digito)
        If Digito = 10 Then RutDigito = "K"
        If Digito = 11 Then RutDigito = "0"
    End Function

    Public Function Get_FechaconHora()
        Get_FechaconHora = Now
    End Function

    Public Function reemplaza_comillas(ByRef texto)
        reemplaza_comillas = Replace(texto, "'", "#")
    End Function




    '---------------------------------------------------
    ' MUESTRA U OCULTA UNA COLUMNA DE UN GRIDVIEW
    '---------------------------------------------------
    Public Sub OcultarColumna(ByVal gdv As GridView, ByVal bool As Boolean, ByVal columna As Byte)
        On Error Resume Next
        gdv.HeaderRow.Cells(columna).Visible = bool
        For Each row As GridViewRow In gdv.Rows
            row.Cells(columna).Visible = bool
        Next
    End Sub


    Public Function getUrlAbsolutaHost(ByRef page As Page)
        Return page.Request.Url.Scheme & "://" & page.Request.Url.Authority _
                & page.Request.ApplicationPath.TrimEnd("/") & "/"
    End Function

    '----------------------------------------------------------------------------------------------------
    ' DEVUELVE EL HASH MD5 DE LA CLAVE PASADA POR PARAMETRO
    '----------------------------------------------------------------------------------------------------
    Public Function get_HashClaveMD5(clave() As Char) As String

        ' SE ESPECIFICA EL OBJ QUE ENCRIPTA LA CLAVE A UN HASH MD5
        '====================================================================================================
        Dim md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()

        ' SE ESPECIFICA EL ARREGLO DE BYTES QUE GUARDA LOS CARACTERE DE LA CLAVE ENCRIPTADA
        '====================================================================================================
        Dim array() As Byte = System.Text.Encoding.UTF8.GetBytes(clave)
        array = md5.ComputeHash(array)
        Dim hashDevuelto As New System.Text.StringBuilder

        ' SE ARMA EL STRING PARA ENTREGAR LA CLAVE ENCRIPTADA
        '====================================================================================================
        For Each b As Byte In array
            hashDevuelto.Append(b.ToString("x2").ToLower())
        Next

        Return hashDevuelto.ToString()
    End Function
#Region "PDF"

    Public Sub ExportarPDF(ByVal baseURL As String, ByVal page As Page, ByRef nombreArchivo As String)

        Dim pdf_page_size As String = SelectPdf.PdfPageSize.Letter
        Dim pageSize As SelectPdf.PdfPageSize = [Enum].Parse(GetType(SelectPdf.PdfPageSize),
                pdf_page_size, True)
        Dim converter As New SelectPdf.HtmlToPdf()

        Dim sw As New System.IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        page.RenderControl(hw)

        Dim htmlString As String = sw.ToString()

        converter.Options.PdfPageSize = pageSize
        converter.Options.WebPageWidth = 765
        converter.Options.WebPageHeight = 990
        'converter.Options.WebPageWidth = 612
        'converter.Options.WebPageHeight = 792

        converter.Options.MarginBottom = 14
        converter.Options.MarginLeft = 14
        converter.Options.MarginRight = 14
        converter.Options.MarginTop = 14

        Dim doc As SelectPdf.PdfDocument = converter.ConvertHtmlString(htmlString, baseURL)

        'doc.Save(page.Response, False, nombreArchivo & ".pdf")
        'doc.Close()

        Dim ms As New System.IO.MemoryStream()
        doc.Save(ms)
        doc.Close()

        page.Response.ContentType = "application/pdf"
        page.Response.BufferOutput = True
        page.Response.AddHeader("Content-Disposition", "inline;filename=" + nombreArchivo + ".pdf")
        page.Response.BinaryWrite(ms.ToArray())
        page.Response.Flush()

    End Sub

#End Region


    Public Function CrearHojaExcel(ByRef workbook As XLWorkbook, ByVal nombreHoja As String, ByVal list As List(Of Dictionary(Of String, String)))
        If list.Count = 0 Then
            workbook.Worksheets.Add(nombreHoja)
            Return 0
        End If

        'EXCEL NO PERMITE HOJAS CON NOMBRES MAYOR A 30 CARACTERES
        If nombreHoja.Length > 25 Then
            nombreHoja = nombreHoja.Substring(0, 25)
        End If

        Dim worksheet As IXLWorksheet
        worksheet = workbook.Worksheets.Add((workbook.Worksheets.Count + 1).ToString() + "-" + nombreHoja)

        Dim iRow As Integer = 1
        Dim keys As New List(Of String)(list(0).Keys)
        Dim iColumnCount As Integer = keys.Count

        Dim iC As Integer = 0
        For c = 0 To iColumnCount - 1
            worksheet.Cell(iRow, iC + 2).Value = keys(c)
            iC = iC + 1
        Next

        For i = 0 To list.Count - 1
            worksheet.Cell(i + 1 + iRow, 1).Value = (i + 1)
            iC = 0

            For c = 0 To iColumnCount - 1
                Dim sText As String = list(i)(keys(c)).ToString()
                If String.IsNullOrEmpty(sText) Then
                    worksheet.Cell(i + 1 + iRow, iC + 2).Style.Fill.BackgroundColor = XLColor.FromArgb(230, 230, 230)
                Else
                    worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(sText)
                End If

                worksheet.Cell(i + 1 + iRow, iC + 2).Value = HttpUtility.HtmlDecode(list(i)(keys(c)).ToString())
                iC = iC + 1
            Next
        Next

        worksheet.CellsUsed().Style.Border.OutsideBorder = XLBorderStyleValues.Thin
        worksheet.Column(1).Cells().Style.Fill.BackgroundColor = XLColor.FromArgb(188, 218, 255)
        worksheet.ColumnsUsed().AdjustToContents()

        Dim rangeTable As IXLRange = worksheet.Range(1, 2, worksheet.LastCellUsed().Address.RowNumber, worksheet.LastCellUsed().Address.ColumnNumber)
        Dim table As IXLTable = rangeTable.CreateTable

        Return 0
    End Function

    Public Function ExportarTablaClosedXml(page As Page, nombreArchivo As String, workbook As XLWorkbook)
        Dim stream As MemoryStream = GetStream(workbook)

        page.Response.Clear()
        page.Response.Buffer = True
        page.Response.AddHeader("content-disposition", "attachment; filename=" + nombreArchivo + ".xlsx")
        page.Response.ContentType = "application/vnd.ms-excel"
        page.Response.ContentEncoding = System.Text.Encoding.Unicode
        page.Response.BinaryWrite(stream.ToArray())
        page.Response.End()

        Return 0
    End Function

    Public Function GetStream(workbook As XLWorkbook)
        Dim fs As New MemoryStream
        workbook.SaveAs(fs)
        fs.Position = 0
        Return fs
    End Function

    Public Function AESDec(ByVal encrypted As String) As String

        Dim password As String = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6"

        Dim mySHA256 As SHA256 = SHA256Managed.Create
        Dim key() As Byte = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(password))

        Dim iv() As Byte = New Byte() {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

        Dim decrypted As String = funciones.DecryptString(encrypted, key, iv)

        Return decrypted
    End Function

    Private Function EncryptString(ByVal plainText As String, ByVal key() As Byte, ByVal iv() As Byte) As String

        Dim encryptor As Aes = Aes.Create
        encryptor.Mode = CipherMode.CBC

        encryptor.Key = key
        encryptor.IV = iv

        Dim memoryStream As MemoryStream = New MemoryStream

        Dim aesEncryptor As ICryptoTransform = encryptor.CreateEncryptor

        Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write)

        Dim plainBytes() As Byte = Encoding.ASCII.GetBytes(plainText)

        cryptoStream.Write(plainBytes, 0, plainBytes.Length)

        cryptoStream.FlushFinalBlock()

        Dim cipherBytes() As Byte = memoryStream.ToArray

        memoryStream.Close()
        cryptoStream.Close()

        Dim cipherText As String = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length)

        Return cipherText
    End Function

    Private Function DecryptString(ByVal cipherText As String, ByVal key() As Byte, ByVal iv() As Byte) As String
        Dim encryptor As Aes = Aes.Create
        encryptor.Mode = CipherMode.CBC

        encryptor.Key = key
        encryptor.IV = iv

        Dim memoryStream As MemoryStream = New MemoryStream

        Dim aesDecryptor As ICryptoTransform = encryptor.CreateDecryptor

        Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write)

        Dim plainText As String = String.Empty

        Try
            Dim cipherBytes() As Byte = Convert.FromBase64String(cipherText)
            cryptoStream.Write(cipherBytes, 0, cipherBytes.Length)
            cryptoStream.FlushFinalBlock()
            Dim plainBytes() As Byte = memoryStream.ToArray
            plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length)
        Finally
            memoryStream.Close()
            cryptoStream.Close()
        End Try

        Return plainText
    End Function

    '--------------------------------------------------------------------------------------------------
    ' SE OBTIENEN LAS PROPIEDADES DEL WEB.CONFIG
    '--------------------------------------------------------------------------------------------------
    Public Function getConfiguracion(ByRef page As Object)
        Return WebConfigurationManager.OpenWebConfiguration(page.Request.ApplicationPath.TrimEnd("/") & "/")
    End Function

    '--------------------------------------------------------------------------------------------------
    ' SE SETEA LA PROPIEDAD EN EL WEBCONFIG
    '--------------------------------------------------------------------------------------------------
    Public Sub setPropiedad(ByRef page As Object, ByVal key As String, ByVal value As String)
        Dim config = getConfiguracion(page)
        config.AppSettings.Settings(key).Value = value
        config.Save(ConfigurationSaveMode.Modified)
    End Sub

    '--------------------------------------------------------------------------------------------------
    ' SE OBTIENE LA PROPIEDAD ESPECIFICADA
    '--------------------------------------------------------------------------------------------------
    Public Function getPropiedad(ByRef page As Object, ByVal key As String)
        Return getConfiguracion(page).AppSettings.Settings(key).Value
    End Function

End Module