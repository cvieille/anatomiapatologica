﻿var vIdUsuario;
$(document).ready(function () {

    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;
    cargarGrillas()


    $('body').on('change', '#chkTodoCasete', function () {
        var bValidar = true;
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++) {
            $(d[i]).find('.check')[0].checked = $(this).prop('checked');

            if ($(d[i]).find('.check').attr('s') != 52 && $(d[i]).find('.check').attr('s') != 55)
                bValidar = false;
        }

        if (bValidar || !$(this).prop('checked'))
            $('#btnAlmacenarCasete').removeAttr('disabled');
        else
            $('#btnAlmacenarCasete').attr('disabled', true);
    });
    $('body').on('change', '#chkTodoLamina', function () {
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.check')[0].checked = $(this).prop('checked');
    });

    $('#btnAlmacenarCasete').click(function (e) {
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        var cantCasete = 0;
        let listadoCortes = [];
        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.check')[0].checked) {
                cantCasete = cantCasete + 1;
                idCorte = $(d[i]).find('.check')[0].id;
                listadoCortes.push(idCorte)

            }
        }
        AlmacenarCaseteporID(listadoCortes);
        if (cantCasete == 0)
            toastr.info('Debe seleccionar al menos un casete');
        else
            grillaCaseteAlmacenar();

        e.preventDefault();
    });

    $('#btnDesecharCasete').click(function (e) {
        var a = [];
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.check')[0].checked)
                a.push($(d[i]).find('.check')[0].id);
        if (a.length > 0) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormAlmacenamientos/frm_Almacenamiento.aspx/desecharCasete',
                data: JSON.stringify({ s: JSON.stringify(a), idUsuario: vIdUsuario }),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    var d = JSON.parse(data.d);
                    if (d.length > 0) {
                        var v = '';
                        for (var i = 0; i < d.length; i++)
                            v += ', ' + d[i];
                        $('#lblModalC').text(v.substr(1));
                        $('#lblModalMensaje').text('Solo se pueden desechar las muestras que se encuentren almacenadas');
                        $('#divMensaje').show();
                        $('#mdlDetalle').modal('show');
                    }
                    else
                        toastr.success('Se han desechado las muestras seleccionadas');
                    grillaCaseteAlmacenar();
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        else
            toastr.info('Debe seleccionar al menos un casete');
        e.preventDefault();
    });

    $('#btnAlmacenarLamina').click(function (e) {
        //trabajando
        var cantLaminas = 0;
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.check')[0].checked) {
                cantLaminas = cantLaminas + 1;
                idLamina = $(d[i]).find('.check')[0].id;
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}ANA_Laminas/almacenar/${idLamina}`,
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function () {
                        toastr.success('Se han almacenado la lamina');
                    },
                    error: function (jqXHR, status) {
                        console.log(json.stringify(jqXHR));
                    }
                });
            }
        }
        if (cantLaminas == 0)
            toastr.info('Debe seleccionar al menos una lamina');
        else
            grillaLaminaAlmacenar();

        e.preventDefault();
    });
    $('#btnDesecharLamina').click(function (e) {
        var a = [];
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.check')[0].checked)
                a.push($(d[i]).find('.check')[0].id);
        if (a.length > 0) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormAlmacenamientos/frm_Almacenamiento.aspx/desecharLamina',
                data: JSON.stringify({ s: JSON.stringify(a), idUsuario: vIdUsuario }),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    var d = JSON.parse(data.d);
                    if (d.length > 0) {
                        var v = '';
                        for (var i = 0; i < d.length; i++)
                            v += ', ' + d[i];
                        $('#lblModalC').text(v.substr(1));
                        $('#lblModalMensaje').text('Solamente se pueden desechar las muestras que se encuentren almacenadas');
                        $('#divMensaje').show();
                        $('#mdlDetalle').modal('show');
                    }
                    else
                        toastr.success('Se han desechado las muestras seleccionadas');
                    grillaLaminaAlmacenar();
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        else
            toastr.info('Debe seleccionar al menos un casete');
        e.preventDefault();
    });


});

function mostrarMuestra(e) {
    let id = $(e).data('id');
    let url = `../FormAnatomia/frm_Cortes.aspx?id=${id}`;
    window.open(url, '_blank');
}

function grillaMuestraAlmacenar() {
    ShowModalCargando(true);
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/buscar?idTipoEstadoSistema=55`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                let stringOrganos = ""
                val.OrganoBiopsia.map((a, index) => {
                    stringOrganos += a.Valor + "  "
                })

                adataset.push([
                    val.Id,
                    val.Estado.Id,
                    val.Numero,
                    stringOrganos,
                    val.Tumoral,
                    val.TipoBiopsia.Valor,
                    moment(val.FechaRecepcion).format("DD-MM-YYYY"),
                    val.Estado.Valor,
                    '',
                    ''
                ]);
            });
            $('#bdgMuestraAlmacenar').html(adataset.length);

            $('#tblMuestrasAlmacenar').DataTable({
                dom: 'Bfrtipl',
                buttons: [{
                    extend: 'excel',// Usa la extensión de Excel
                    text: 'Exportar a excel',// mostrar texto
                    title: " Muestras a almacenar",
                    className: 'btn btn-info',
                    exportOptions: {
                        // Personalizar las opciones de exportación
                        // Como por ejemplo: exportación personalizada de esas columnas y filas
                        //TODO...
                        fileName: "Muestras a almacenar",
                        columns: [0, 2, 3, 4, 5, 6, 7]
                    }
                }],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[0]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 6], visible: false, searchable: false },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-success" data-id="' + adataset[fila][0] + '" data-name="almacenarmuestra" onclick="modalConfirmacion(this);">Almacenar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="mostrarMuestra(this);">Ver</a>';
                                botones += '&nbsp; <a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][0] + ')">Movimientos</a>'
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id biopsia' },
                    { title: 'Id Estado' },
                    { title: 'N°' },
                    { title: 'Órgano' },
                    { title: 'Tumoral' },
                    { title: 'Tipo Biopsia' },
                    { title: 'Fecha recepcion registro' },
                    { title: 'Estado' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }, error: function (err) {
            console.log("ha ocurrido un error ")
        }
    });
    ShowModalCargando(false);
}
function grillaCaseteAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=55`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.Id,
                    val.IdBiopsia,
                    val.TipoEstadoSistema.Id,
                    val.Usuario.IdUsuario,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY HH:mm:ss'),
                    val.TipoEstadoSistema.Valor,
                    ''
                ]);
            });
            $('#bdgCaseteAlmacenar').html(adataset.length);

            $('#tblCaseteAlmacenar').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        title: "Casetes para almacenar",
                        className: 'btn btn-info',
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "Casetes para almacenar",
                            columns: [1, 2, 4, 5, 6, 7]
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[1]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 2, 3, 4], visible: false, searchable: false },
                        { targets: 6, sType: 'date-ukLong' },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones = '';
                                //if (adataset[fila][3] == 55 || adataset[fila][3] == 52) {
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" s="' + adataset[fila][3] + '" type="checkbox" class="check">';
                                botones += '<span class="checkmark" style="margin-top:-8px; margin-left:6px;"></span>';
                                botones += '</label>';
                                //}
                                return botones;
                            }
                        },
                        {
                            targets: 8,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return '<a  href="#/" class="btn btn-success" onclick="AlmacenarCaseteporID(' + adataset[fila][1] + ', 52)">Almacenar</a>';
                            }
                        },
                        {
                            targets: 9,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][2] + '" onclick="mostrarMuestra(this);">Ver</a>';
                                botones += '&nbsp; <a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][2] + ')">Movimientos</a>'
                                return botones
                            }
                        }
                    ],
                columns: [
                    { title: '' },
                    { title: 'Id corte' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'GEN_idUsuarios' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: 'Almacenar' },
                    { title: 'Movimientos' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaCaseteDespachadoparaAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=57`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {

                adataset.push([
                    val.Id,
                    val.IdBiopsia,
                    val.TipoEstadoSistema.Id,
                    val.Usuario.IdUsuario,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY HH:mm:ss'),
                    val.TipoEstadoSistema.Valor,
                    ''
                ]);
            });
            $('#bdgCaseteDespachado').html(adataset.length);

            $('#tblCasetedespachadoparaAlmacenar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2, 3], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 7,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="almacenarcasete" onclick="AlmacenarCaseteporID(' + adataset[fila][0] + ', 57)" class="btn btn-success">Almacenar Interconsulta</a>';
                                botones += '&nbsp; <a class="btn btn-info" data-id="' + adataset[fila][1] + '" onclick="mostrarMuestra(this);">Ver</a>';
                                botones += '&nbsp; <a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][1] + ')">Movimientos</a>'
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id corte' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'GEN_idUsuarios' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaLaminaAlmacenar() {
    $.ajax({
        type: 'GET',
        //url: `${GetWebApiUrl()}ANA_Laminas/Grilla/ParaAlmacenar/55`,
        url: `${GetWebApiUrl()}ANA_Laminas/buscar?idTipoEstadoSistema=55`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.Id,
                    val.IdBiopsia,
                    val.Estado.Id,
                    val.Nombre,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Estado.Valor
                ]);
            });
            $('#bdgLaminaAlmacenar').html(adataset.length);
            $('#tblLaminaAlmacenar').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        title: " Laminas a almacenar",
                        className: 'btn btn-info',
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "LaminaAlmacenar",
                            columns: [1, 2, 4, 5, 6]
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[1]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 2, 3], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;
                                var botones;
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="check">';
                                botones += '<span class="checkmark" style="margin-top:-8px; margin-left:6px;"></span>';
                                botones += '</label>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;
                                let botones = `<button type="button" class="btn btn-success" onclick="AlmacenarLaminaporID(${adataset[fila][1]})">Almacenar</button>`
                                botones += '&nbsp; <a class="btn btn-info" data-id="' + adataset[fila][2] + '" onclick="mostrarMuestra(this);">Ver</a>'
                                botones += '&nbsp; <a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][2] + ')">Movimientos</a>'
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: '' },
                    { title: 'Id lámina' },
                    { title: 'Id Biopsia' },
                    { title: 'IdEstado' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaLaminaDesechadasporAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/buscar?idTipoEstadoSistema=57`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdBiopsia,
                    val.Estado.Id,
                    val.Nombre,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Estado.Valor
                ]);
            });
            $('#bdgLaminasDespachadas').html(adataset.length);

            $('#tblLaminaDesechadaporAlmacenar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 6,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '"  onclick="AlmacenarLaminaporID(' + adataset[fila][0] + ', 57)" class="btn btn-success">Almacenar Interconsulta</a>';
                                botones += '&nbsp; <a class="btn btn-info" data-id="' + adataset[fila][1] + '" onclick="mostrarMuestra(this);">Ver</a>'
                                botones += '&nbsp; <a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][1] + ')">Movimientos</a>'
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Id lámina' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                ],
                bDestroy: true
            });
        }
    });
}
function modalConfirmacion(e) {
    let id = $(e).data('id');
    var n = $(e).data('name');
    var mensaje;
    if (n == "retirarbiopsia")
        mensaje = '¿Está seguro (a) que desea retirar esta biopsia?';
    else if (n == "eliminarbiopsia")
        mensaje = '¿Está seguro (a) que desea eliminar esta biopsia?'
    else if (n == "retirarcasete")
        mensaje = '¿Está seguro (a) que desea retirar este casete?'
    else if (n == "eliminarcasete")
        mensaje = '¿Está seguro (a) que desea eliminar este casete?'
    else if (n == "almacenarcasete")
        mensaje = '¿Está seguro (a) que desea almacenar este casete?'
    else if (n == "retirarlamina")
        mensaje = '¿Está seguro (a) que desea retirar esta lámina?'
    else if (n == "eliminarlamina")
        mensaje = '¿Está seguro (a) que desea eliminar esta lámina?'
    else if (n == "almacenarmuestra")
        mensaje = '¿Está seguro (a) que desea almacenar esta muestra?';

    $("#lblConfirmacion").text(mensaje);
    $("#btnConfirmarModal").attr('data-id', id);
    $("#btnConfirmarModal").attr('data-name', n);
    $("#btnConfirmarModal").attr("onclick", "btnConfirmarModal('" + n + "', " + id + ")");
    $("#btnConfirmarModal").removeClass("disabled")
    $("#mdlConfirmacion").modal('show');
}

function btnConfirmarModal(vModal, id) {
    $("#btnConfirmarModal").addClass("disabled")

    if (vModal == 'retirarbiopsia') {
        retirarBiopsiaSolicitada();
    }
    else if (vModal == 'eliminarbiopsia') {
        eliminarSolicitudBiopsia();
    }
    else if (vModal == 'retirarcasete') {
        retirarCorteSolicitado();
    }
    else if (vModal == 'eliminarcasete') {
        eliminarSolicitudCorte();
    }
    else if (vModal == 'retirarlamina') {
        retirarLaminaAlmacenada();
    }
    else if (vModal == 'eliminarlamina') {
        eliminarSolicitudLamina();
    }
    if (vModal == 'almacenarmuestra') {
        almacenarBiopsia();
    }
    function almacenarBiopsia() {
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Almacenar`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha almacenado la muestra');
                eliminarFilaDatatable("tblMuestrasAlmacenar", id);
                $('#mdlConfirmacion').modal('hide');
            }
        });
    }

    function eliminarSolicitudLamina() {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Laminas/EliminarSolicituddeLamina/${id}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado la lámina.');
                eliminarFilaSolicitudLamina(id);
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    function retirarLaminaAlmacenada() {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Laminas/RetirarLaminaAlmacenada/${id}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado la lámina.');
                eliminarFilaSolicitudLamina(id);
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    function eliminarSolicitudCorte() {
        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${id}/EliminarSolicitud`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado el casete.');
                eliminarFilaSolicitudCasete(id);
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    function retirarCorteSolicitado() {
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${id}/Retirar`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado el casete.');
                eliminarFilaSolicitudCasete(id);
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    function eliminarSolicitudBiopsia() {
        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/EliminarSolicituddeMacro`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado la biopsia.');
                eliminarFilaSolicitudBiopsia(id);
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    function retirarBiopsiaSolicitada() {
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/RetirarMacro`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado la biopsia.');
                //Se elimina solo la fila y no se recarga la tabla completa
                eliminarFilaSolicitudBiopsia(id);

                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
}
function eliminarFilaActualizarBadge(idTable, id, idBadge) {
    eliminarFilaDatatable(idTable, id);
    let filas = $(idBadge).html();
    $(idBadge).html(filas - 1);
}

function eliminarFilaSolicitudBiopsia(id) {
    eliminarFilaDatatable("tblSolBiopsia", id);
    let filas = $('#bdgBiopsia').html();
    $('#bdgBiopsia').html(filas - 1);
}
function eliminarFilaSolicitudCasete(id) {
    eliminarFilaDatatable("tblSolCasete", id);
    let filas = $('#bdgCasete').html();
    $('#bdgCasete').html(filas - 1);
}
function eliminarFilaCaseteAlmacenar(id) {
    eliminarFilaDatatable("tblCaseteAlmacenar", id);
    let filas = $('#bdgCaseteAlmacenar').html();
    $('#bdgCaseteAlmacenar').html(filas - 1);
}
function eliminarFilaSolicitudLamina(id) {
    eliminarFilaDatatable("tblSolLamina", id);
    let filas = $('#bdgLamina').html();
    $('#bdgLamina').html(filas - 1);
}

function grillaSolBiopsia() {
    ShowModalCargando(true);
    let adataset = [];
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?idTipoEstadosistemasMuestra=98`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Numero,
                    val.OrganoBiopsia.length > 0 ? val.OrganoBiopsia[0].Valor : "",
                    val.Almacenada,
                    val.Tumoral,
                    val.Desechada,
                    val.TipoBiopsia == null ? null : val.TipoBiopsia.Valor,
                    val.Patologo == null ? null : val.Patologo.Nombre + " " + val.Patologo.ApellidoPaterno + " " + val.Patologo.ApellidoMaterno,
                    '',
                    ''
                ]);
            });
            $('#bdgBiopsia').html(adataset.length);

            $('#tblSolBiopsia').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        title: "Solicitud biopsia",
                        className: 'btn btn-info',
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "Solicitud biopsia",
                            columns: [0, 1, 2, 3, 4, 5, 6, 7]
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[0]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarbiopsia" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarbiopsia" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Id Biopsia' },
                    { title: 'Biopsia' },
                    { title: 'Órgano' },
                    { title: 'Alm.' },
                    { title: 'Tum.' },
                    { title: 'Des.' },
                    { title: 'Descripción.' },
                    { title: 'Patólogo' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });


        }, error: function (err) {
            console.log("Ha ocurrido un errrrooor")
        }
    });


}

function grillaSolCasete() {
    ShowModalCargando(false);
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=98&idTipoEstadoSistema=61`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY HH:mm:ss'),
                    val.TipoEstadoSistema.Valor,
                    val.TipoEstadoSistema.Id,
                    '',
                    ''

                ]);
            });
            $('#bdgCasete').html(adataset.length);

            $('#tblSolCasete').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        title: "Solicitud cassete",
                        className: 'btn btn-info',
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "Solicitud cassete",
                            columns: [0, 1, 2, 3]
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[0]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: [4], visible: false, searchable: false },

                        { targets: 2, sType: 'date-ukLong' },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;

                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarcasete" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarcasete" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdCortes_Muestras' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: '' },
                    { title: '' }

                ],
                bDestroy: true
            });
        }
    });
}

function grillaSolLamina() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/Buscar?idTipoEstadoSistema=98`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Nombre,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Estado.Valor,
                    '',
                    ''
                ]);
            });
            $('#bdgLamina').html(adataset.length);

            $('#tblSolLamina').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        title: "Solicitud lamina",
                        className: 'btn btn-info',
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "Solicitud lamina",
                            columns: [0, 1, 2, 3]
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[0]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: 2, sType: 'date-ukLong' },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarlamina" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarlamina" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Id Lamina' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function VerMovimientos(id) {
    modalMovimiento(id);
    $('#mdlMovimientos').modal('show');
}
function AlmacenarCaseteporID(id, idEstado) {

    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${id}/almacenar`,
        success: function () {
            toastr.success('Se ha almacenado el casete');
            if (idEstado == 57)// 'Interconsulta'
                grillaCaseteDespachadoparaAlmacenar();
            else
                eliminarFilaCaseteAlmacenar(id)
        },
        error: function (jqXHR, status) {
            console.error("error al intentar almacenar el cassete");
            console.log(jqXHR)
            console.log(status)
        }
    });
}
function AlmacenarLaminaporID(id, idEstado) {
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Laminas/almacenar/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            toastr.success('Se ha almacenado la Lamina', '', { timeOut: 6000 });
            if (idEstado == 57) {// 'Interconsulta'
                grillaLaminaDesechadasporAlmacenar();
            } else {
                eliminarFilaActualizarBadge('tblLaminaAlmacenar', id, '#bdgLaminaAlmacenar')
            }
        },
        error: function (jqXHR, status) {
            // console.log(json.stringify(jqXHR));
        }
    });
}
function cargarGrillas() {
    grillaSolBiopsia();
    grillaSolCasete();
    grillaSolLamina();
    grillaMuestraAlmacenar();
    grillaCaseteAlmacenar();
    grillaCaseteDespachadoparaAlmacenar();
    grillaLaminaAlmacenar();
    grillaLaminaDesechadasporAlmacenar()
}