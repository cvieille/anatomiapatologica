﻿const vIdBiopsia = getUrlParameter('id');
var isEmptyTable = false

$(document).ready(function () {
    cargarInformacion(vIdBiopsia);


    $('#modalLaminas').on('hide.bs.modal', function (e) {
        $('#divMensaje').hide();
        $('#divMensajeExtra').hide();
        $('#hrSeparador').hide();
    });

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:checkbox', function () {
        $(this).parent().children('span').removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#btnVerMovimientos').click(function (e) {
        modalMovimiento(vIdBiopsia);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });

    $('#selTipoMuestra').change(function (e) {
        permiteCrearlamina();
    });

    $('#btnGuardarMacro').click(function (e) {
        var bValido = true;
        if ($('#selPatologo').prop('value') == 0) {
            resaltaElemento($('#selPatologo'));
            bValido = false;
        }

        if ($('#selTipoMuestra').prop('value') == 0) {
            resaltaElemento($('#selTipoMuestra'));
            bValido = false;
        }

        if ($('#selTipoMacroscopia').prop('value') == 0) {
            resaltaElemento($('#selTipoMacroscopia'));
            bValido = false;
        }
        if (bValido) {
            $('#lblConfirmacion').val('¿Esta seguro (a) de guardar esta macroscopia?');
            $("#btnConfirmarModal").attr("onclick", "btnConfirmarModal('guardarMacro', " + vIdBiopsia + ")");

            $('#mdlConfirmacion').modal('show');

        }

        e.preventDefault();
    });

    $('#btnPlantillaAc').click(function (e) {
        var sel = $('#selNomPlantilla').prop('value');
        if (sel == 0)
            resaltaElemento($('#selNomPlantilla'));
        else {
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}/ANA_Plantillas/${sel}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if (data != null)
                        $('#txtDescMacro').val($('#txtDescMacro').val() + ' ' + data.Macroscopia);
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnSecuenciaCortes').click(function (e) {
        if ($(this).attr('disabled') == 'disabled')
            return false;
        var bValido = true;
        if ($('#selPatologo').val() == '0' || $('#selPatologo').val() == null || $('#selPatologo').val() == undefined) {
            bValido = false;
            resaltaElemento($('#selPatologo'));
            toastr.error('Solo se puede generar casete si tiene patologo definido');
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
            toastr.error('Solo se puede generar casete si la biopsia se encuentra dictada');
        }

        if ($('#selCantCortes').prop('value') == 0) {
            bValido = false;
            resaltaElemento($('#selCantCortes'));
        }
        if (bValido) {

            if ($('#tblListadoCasete').DataTable().rows().count() == 0) {
                const secuenciaCorte = {
                    IdBiopsia: vIdBiopsia,
                    Rotulo: $('#txtSufijoCorte').val(),
                    Cantidad: $('#selCantCortes').prop('value')
                }
                $.ajax({
                    type: 'POST',
                    url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdBiopsia}`,
                    data: JSON.stringify(secuenciaCorte),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function () {
                        getDataCortesApi(vIdBiopsia)
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            }
            else {
                toastr.error('Solo se pueden crear secuencias nuevas mientras no existan cortes');
            }
        }

        e.preventDefault();
    });
    $('#btnRotuloCorte').click(function (e) {

        var bValido = true;
        if ($('#selPatologo').val() == '0' || $('#selPatologo').val() == null || $('#selPatologo').val() == undefined) {
            bValido = false;
            resaltaElemento($('#selPatologo'));
            toastr.error('Solo se puede generar casete si tiene patologo definido');
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
            toastr.error('Solo se puede generar casete si la biopsia se encuentra dictada');
        }

        if ($('#txtSufijoCorte').val() == '' || $('#txtSufijoCorte').val() == undefined || $('#txtSufijoCorte').val() == null) {
            bValido = false;
            resaltaElemento($('#txtSufijoCorte'));
        }

        if (bValido) {
            const secuenciaCorte = {
                IdBiopsia: vIdBiopsia,
                Rotulo: $('#txtSufijoCorte').val(),
                Cantidad: 1
            }
            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdBiopsia}`,
                data: JSON.stringify(secuenciaCorte),
                contentType: 'application/json',
                dataType: 'json',
                success: function () {
                    toastr.success('Se ha creado el casete ' + $('#txtSufijoCorte').val());
                    getDataCortesApi(vIdBiopsia);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnSecuenciaLaminas').click(function (e) {
        if ($(this).attr('disabled') == 'disabled')
            return false;

        var bValido = true;
        if ($('#selCantLaminas').prop('value') == 0) {
            bValido = false;
            resaltaElemento($('#selCantLaminas'));
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
        }
        let cantidadCortes = 0;
        if (bValido) {
            let t = $('#tblListadoCasete').DataTable();
            let d = t.rows().nodes();
            let jsonPost = {};
            //revisa que exista algun checkbok seleccionado
            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkCorte')[0].checked) {
                    cantidadCortes++;
                    jsonPost = {
                        idBiopsia: vIdBiopsia,
                        rotulo: null,
                        cantidad: $('#selCantLaminas').val(),
                        idCorte: $(d[i]).find('.checkCorte')[0].id
                    };
                    $.ajax({
                        type: 'POST',
                        url: `${GetWebApiUrl()}ANA_Laminas/${vIdBiopsia}`,
                        data: JSON.stringify(jsonPost),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            toastr.success('Se han creado las láminas solicitadas');
                            getDataLaminasApi(data.IdBiopsia);
                        }
                    });
                }
            if (cantidadCortes == 0)
                toastr.info('Debe seleccionar al menos un casete');
        }
        e.preventDefault();
    });
    $('#btnRotuloLamina').click(function (e) {
        var bValido = true;
        if ($('#txtSufijoLaminas').val() == '' || $('#txtSufijoLaminas').val() == undefined || $('#txtSufijoLaminas').val() == null) {
            bValido = false;
            resaltaElemento($('#txtSufijoLaminas'));
        }

        let vIdBiopsia = $("#txtIdBiopsia").val();
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                ANA_dictadaBiopsia = data.ANA_dictadaBiopsia;
                if (data.Dictada != "SI") {
                    resaltaElemento($('#chkDictado'));
                    bValido = false;
                }
            }
        });



        if (bValido) {
            const json = {
                rotulo: $('#txtSufijoLaminas').val(),
                idBiopsia: vIdBiopsia,
                idCorte: 0,
                cantidad: 1
            };

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Laminas/${vIdBiopsia}`,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha creado la lámina');
                    getDataLaminasApi(data.IdBiopsia);
                }
            });


        }
        e.preventDefault();
    });

    $('#btnGuardarDetalleMacro').click(function (e) {
        if ($('#txtDescMacro').val() == '')
            resaltaElemento($('#txtDescMacro'));
        else {
            let json = {
                IdBiopsia: vIdBiopsia,
                Macroscopia: $('#txtDescMacro').val()
            };

            $.ajax({
                type: 'Patch',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Descripcion/Macroscopia`,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha actualizado la descripción Macroscópica');
                }
            });
        }
        e.preventDefault();
    });

    $('#btnEnviarProcesador').click(function (e) {
        $('#btnEnviarProcesador').attr('disabled', true);
        cambiarEstadoCasete(56);

        $('#btnEnviarProcesador').removeAttr('disabled');
        e.preventDefault();
    });

    $('#btnDescal').click(function (e) {
        $('#btnDescal').attr('disabled', true);
        cambiarEstadoCasete(54);
        $('#btnDescal').removeAttr('disabled');
        e.preventDefault();
    });
    $('#btnVolverInicio').click(function (e) {
        $('#btnVolverInicio').attr('disabled', true);
        cambiarEstadoCasete(50);
        $('#btnVolverInicio').removeAttr('disabled');
        e.preventDefault();
    });
    function obtenerCortesSeleccionados() {
        var t = $('#tblListadoCasete').DataTable();
        var d = t.rows().nodes();
        let idsCorte = [];
        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.checkCorte')[0].checked) {
                let idCasete = $(d[i]).find('.checkCorte')[0].id;
                idsCorte.push(idCasete);
            }
        }
        return idsCorte;
    }
    function cambiarEstadoCasete(idEstado) {
        const baseUrl = `${GetWebApiUrl()}ANA_Cortes_Muestras/Enviar/`;

        let sUrl = "";

        if (idEstado == 56)
            sUrl = `${baseUrl}ListoParaProcesador`;
        else if (idEstado == 54)
            sUrl = `${baseUrl}Descalcificacion`;
        else if (idEstado == 50)
            sUrl = `${baseUrl}Encasetado`;

        const idsCorte = obtenerCortesSeleccionados();

        if (idsCorte.length > 0) {
            $.ajax({
                type: 'PATCH',
                url: sUrl,
                contentType: 'application/json',
                data: JSON.stringify(idsCorte),
                success: function () {
                    toastr.success('Se han enviado los casetes seleccionados');
                    getDataCortesApi(vIdBiopsia);
                }
            });

        }
        else
            toastr.info("Debe seleccionar al menos un registro valido");
    }

    $('#btnEliminarCortes').click(function (e) {
        $('#btnEliminarCortes').attr('disabled', true);

        const idsCorteSeleccionado = obtenerCortesSeleccionados();
        let idsCorte = [];

        for (let i = 0; i < idsCorteSeleccionado.length; i++) {
            idCasete = idsCorteSeleccionado[i];
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${idCasete}/Estado`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    let estadoCorte = data.Id;
                    if (estadoCorte == 56 || estadoCorte == 50 || estadoCorte == 54 || perfilAccesoSistema.codigoPerfil == "2")//si esta encasetado o listo para procesador o es admin
                        idsCorte.push(idCasete);

                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }

        for (let i = 0; i < idsCorte.length; i++) {
            $.ajax({
                type: 'Delete',
                url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${idsCorte[i]}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    eliminarFilaDatatable("tblListadoCasete", idsCorte[i]);
                    toastr.success('Se han anulado los registros solicitados');
                },
                error: function (jqXHR, status) {
                    toastr.error("No se ha podido eliminar el elemento seleccionado.")
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }

        if (idsCorte.length == 0)
            toastr.info("Debe seleccionar al menos un registro");

        $('#btnEliminarCortes').removeAttr('disabled');
        e.preventDefault();
    });

    $('body').on('change', '#chkTodoLaminas', function () {
        var t = $('#tblLaminas').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.checkLamina')[0].checked = $(this).prop('checked');
    });
});

function cargarCombos(codigoPerfil) {
    let url;
    $('#selCantCortes, #selCantLaminas').empty();
    for (var i = 0; i <= 40; i++)
        $('#selCantCortes, #selCantLaminas').append("<option>" + i + "</option>");


    if (codigoPerfil == perfilAccesoSistema.administrador || codigoPerfil == perfilAccesoSistema.paramedico) {
        url = `${GetWebApiUrl()}ANA_Plantillas/Combo/SI/NO/NO`;
        setCargarDataEnComboAsync(url, true, $('#selNomPlantilla'));

        url = `${GetWebApiUrl()}ANA_Tecnicas_Especiales/combo`;
        setCargarDataEnComboAsync(url, true, $('#selTecnicasEspeciales'));

        url = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica/combo`;
        setCargarDataEnComboAsync(url, true, $('#selInmunoHistoquimica'));
    }

    url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));

    url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/4`;
    setCargarDataEnComboAsync(url, true, $('#selTecnologo'));

    url = `${GetWebApiUrl()}ANA_Tipo_Biopsia/combo`;
    setCargarDataEnComboAsync(url, false, $('#selTipoMuestra'));

    url = `${GetWebApiUrl()}ANA_Tipo_Macroscopia/combo`;
    setCargarDataEnComboAsync(url, false, $('#selTipoMacroscopia'));
}

function cargarGrillas() {
    getDataCortesApi();
    getDataLaminasApi();
    getDataInmunoApi();
    getDataTecnicasApi();
    grillaDetalleMuestras();
}
//controla que tipo de biopsia puede crear laminas por rotulo.
function permiteCrearlamina() {
    if ($('#selTipoMuestra').val() != 0) {
        $('#txtSufijoLaminas').removeAttr('disabled');
        $('#btnRotuloLamina').removeAttr('disabled');
        $('#selCantLaminas').removeAttr('disabled');
        $('#btnSecuenciaLaminas').removeAttr('disabled');
    }
    else {
        $('#txtSufijoLaminas').attr('disabled', true);
        $('#btnRotuloLamina').attr('disabled', true);
        $('#selCantLaminas').attr('disabled', true);
        $('#btnSecuenciaLaminas').attr('disabled', true);
    }
}

function cargarInformacion(idBiopsia) {

    llenarGrillaDetalleRegistroMuestras(idBiopsia);

    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.paramedico ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.secretaria ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.tecnologo ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.supervisorTecnologo ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.auxiliar)
        cargarCombos(perfilAccesoSistema.codigoPerfil);

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $("#txtIdBiopsia").val(data.Id);
            $('#txtBiopsiaN').val(data.Numero + "-" + data.Folio);

            $("#txtOrgano").val(data.ANA_organoBiopsia);
            $("#txtEstado").val(data.Estado.Valor);

            if (data.Estado.Id == 44 || data.Estado.Id == 45)
                habilitarBotonesMuestraDespachadaoValidada();
            else if (data.Estado.Id == 43 && perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.paramedico)
                habilitarBotonesMuestraenMacroscopia(data)


            $("#txtAlmacenado").val(data.Almacenada);
            if (data.Almacenada == "SI")
                $('#btnSolicitarMacro').show();


            $("#chkDictado").attr("checked", data.Dictada == "SI" ? true : false);
            $("#chkTumoral").attr("checked", data.Tumoral == "SI" ? true : false);
            if (data.Usuario.Tecnologo != null)
                $('#selTecnologo').prop('value', data.Usuario.Tecnologo.IdUsuario ?? 0);

            if (data.Usuario.Patologo !== null)
                $('#selPatologo').prop('value', data.Usuario.Patologo.IdUsuario ?? 0);

            if (data.TipoBiopsia != null) {
                $('#selTipoMuestra').prop('value', data.TipoBiopsia.Id);
                $('#selTipoMuestra').change();
            }

            if (data.TipoMacroscopia != null)
                $('#selTipoMacroscopia').prop('value', data.TipoMacroscopia.Id);

            if (data.Estado.Id != 42) {
                cargarDetalleMacroscopia(idBiopsia);
            }
            else {
                $("#divbtnCortes").hide()
                $("#divAgregar").hide()
            }

            revisarBotones(data.Estado.Id);


        }
    });


    
}
function cargarDetalleMacroscopia(idBiopsia) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Descripcion/Macroscopica`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (dataMacro) {
            $("#txtDescMacro").val(dataMacro.Valor);
        }, error: function (err) {
            console.log("No se ha encontrado informacion relacionada");
        }
    });
    $('#btnGuardarMacro').attr('data-id', idBiopsia);
    $('#btnSolicitarMacro').attr('data-id', idBiopsia);
    $('#btnGuardarMacro').attr('data-name', 'guardarMacro');
    $('#btnGuardarMacro').show();


    getDataCortesApi(idBiopsia);
    getDataTecnicasApi(idBiopsia);
    getDataLaminasApi(idBiopsia);
    getDataInmunoApi(idBiopsia);
}

function habilitarBotonesMuestraDespachadaoValidada() {
    $('#divAgregar').hide();

}
function llenarGrillaDetalleRegistroMuestras(idBiopsia) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Detalle`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data && data.length > 0) {
                let adataset = [];
                data.forEach(function (val) {
                    adataset.push([
                        val.ANA_cantidadMuestras || '',
                        val.ANA_detalleCatalogo_Muestras || '',
                        val.ANA_descripcionBiopsia || ''
                    ]);
                });
                // Si deseas inicializar la tabla DataTable, descomenta esta parte del código
                $('#tblMuestrasBiopsias').addClass('nowrap').DataTable({
                    data: adataset,
                    order: [],
                    columns: [
                        { title: 'Muestras' },
                        { title: 'Detalle' },
                        { title: 'Descripción' }
                    ],
                    destroy: true // Cambio 'bDestroy' a 'destroy'
                });

                $('#bdgDetalleMuestra').html(adataset.length);
            } else {
                console.log("No se ha encontrado información relacionada.");
            }
        },
        error: function (err) {
            console.log("Ha ocurrido un error al intentar obtener la información relacionada.");
        }
    });
}
