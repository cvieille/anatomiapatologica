﻿$(document).ready(function () {
    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));

    $('#btnBuscar').click(function (e) {
        grillaCritico();
        e.preventDefault();
    });

    grillaCritico();
});


function grillaCritico() {
    let ruta = `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/CRITICOS/${$("#txtDesde").val()}/${$('#txtHasta').val()}`;
    if ($("#selVisto").val() == "NO")
        ruta += `?leido=NO`
    else if ($("#selVisto").val() == "SI")
        ruta += `?leido=SI`

    ShowModalCargando(true);
    $.ajax({
        type: 'GET',
        url: ruta,
        success: function (data) {
            console.log(data)
            let adataset = []
            adataset = data.map((Biopsia, index) => {
                return [
                    index + 1,
                    Biopsia.Paciente !== null ? `${Biopsia.Paciente.Nombre ?? ""} ${Biopsia.Paciente.PrimerApellido ?? ""} ${Biopsia.Paciente.SegundoApellido ?? ""}` : "Paciente es nulo",
                    Biopsia.Paciente !== null ? `${Biopsia.Paciente.NumeroDocumento}-${Biopsia.Paciente.Digito}` : "Paciente es nulo",
                    Biopsia.NumeroBiopsias,
                    Biopsia.FechaValidacion !== null ? moment(Biopsia.FechaValidacion).format('DD/MM/YYYY HH:mm:ss') : "No registra fecha",
                    moment(Biopsia.FechaCritico).format('DD/MM/YYYY HH:mm'),
                    Biopsia.Diagnostico !== null ? Biopsia.Diagnostico : "Sin diagnóstico",//ACa deberia ir el diagnostico
                    Biopsia.DiasNotificacion,
                    Biopsia.Patologo !== null ? `${Biopsia.Patologo.Nombre} ${Biopsia.Patologo.PrimerApellido}` : "Sin asignar patólogo",
                    Biopsia.FechaLeido !== null ? moment(Biopsia.FechaLeido).format('DD/MM/YYYY HH:mm') : "Sin leer",
                    Biopsia.DiasLeido !== null ? Biopsia.DiasLeido : "Sin leer",
                    Biopsia.RespuestaCritico !== null ? Biopsia.RespuestaCritico : "Sin respuesta",
                    Biopsia.Notificado !== null ? `${Biopsia.Notificado.Nombre} ${Biopsia.Notificado.PrimerApellido}` : "Sin notificar",
                    ''
                ]
            })



            if (adataset.length == 0)
                $('#btnExportar').attr('disabled', true);
            else
                $('#btnExportar').removeAttr('disabled');

            $('#tblCritico').addClass('text-wrap').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        className: 'btn btn-info',
                        title: `Informe Biopsias Criticas desde ${moment($('#txtDesde').val()).format("DD-MM-YYYY")}, hasta ${moment($('#txtHasta').val()).format("DD-MM-YYYY")}`,
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "Biopsias criticas"
                        }
                    }
                ],
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: 4, sType: 'date-ukLong' },
                        { targets: [0, 5, 6,/* 8, */11, 12], visible: false, searchable: false },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info"><span class="glyphicon glyphicon-list"></span></a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ID' },
                    { title: 'Nombre paciente' },
                    { title: 'Rut' },
                    { title: 'N° de examen' },
                    { title: 'Fecha validación' },
                    { title: 'Fecha notificación' },
                    { title: 'Diagnóstico' },
                    { title: 'Días not.' },
                    { title: 'Patólogo' },
                    { title: 'Fecha de lectura' },
                    { title: 'Dias leido' },
                    { title: 'Respuesta' },
                    { title: 'UsuarioNotificado' },
                    { title: 'Detalle', className: 'dt-control' }
                ],
                bDestroy: true
            });

            //Esta funcion es la que dibuja los childrow.. para que el usuario obtenga informacion extra
            function format(celda) {
                // celda, es la celda de la tabla que contiene toda la informacion de la celda seleccionada
                if (celda !== undefined) {
                    return `<table id='table-${celda[0]}'  class="table table-bordered table-striped w-100" >
                <tr>
                    <td><b>Usuario notificado</b></td>
                    <td >${celda[12]}</td>
                    <td><b>Fecha notificación</b></td>
                    <td>${celda[5]}</td>
                </tr>
                <tr>
                    <td colspan="4"><b>Diagnóstico</b></td>
                </tr>
                <tr>
                    <td colspan="4" style="white-space: pre-wrap;">${celda[6]}</td>
                </tr>

                <tr>
                    <td colspan="4"><b>Respuesta</b></td>
                </tr>
                <tr>
                    <td colspan="4" style="white-space: pre-wrap;">${celda[11]}</td>
                </tr>

                </table>`;
                }
            }

            $('#tblCritico tbody').unbind().on('click', 'td.dt-control', function () {
                var table = $(this).parent().parent().parent().DataTable();
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                if (row.child.isShown()) {
                    //la fila ya esta abierta.. click para cerrar
                    row.child.hide();
                    tr.removeClass('shown');
                    //Id de la tabla par destruirla y crearla nuevamente
                    let idTable = row.data()[0]
                    $('#table-' + idTable).DataTable().destroy();
                    $('#table' + idTable + ' tbody').empty();
                } else {
                    //Abre esta fila
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
            ShowModalCargando(false);
        }, error: function (err) {
            console.error("Ha ocurrido un error al intentar buscar el informe criticos")
            console.log(err)
        }
    });

}