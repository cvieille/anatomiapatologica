﻿$(document).ready(function () {
    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').attr("max", moment().format('YYYY-MM-DD'));
    $('#txtDesde').attr("max", moment().format('YYYY-MM-DD'));

    grillaRecepcionadas();

    $('#btnBuscar').click(function (e) {
        grillaRecepcionadas();

    });
});

async function grillaRecepcionadas() {
    ShowModalCargando(true)
    let adataset = [];
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?recepcionDesde=${$('#txtDesde').val()}&recepcionHasta=${$('#txtHasta').val()}&orden=id&descendente=false`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.length > 0) {
                $.each(data, function (key, val) {
                    adataset.push([
                        val.Id,
                        val.Numero,
                        val.Paciente.Nombre + " " + (val.Paciente.ApellidoPaterno ?? "") + " " + (val.Paciente.ApellidoMaterno ?? ""),
                        moment(val.FechaRecepcion, 'YYYY/MM/DD hh:mm:ss').format('DD-MM-YYYY'),
                        val.ServicioOrigen,
                        val.ServicioDestino,
                        val.OrganoBiopsia.length > 0 ? val.OrganoBiopsia[0].Valor : "",
                        val.Despacho == null ? "" : (moment(val.FechaDespacho).format('DD-MM-YYYY')),
                        val.Despacho == null ? "" : val.Despacho.NombreRecibe
                    ]);
                });
            }
            ShowModalCargando(false)
        },        
        error: function (err) {
            ShowModalCargando(false)
            console.log("No se encontro data")
        }
    })
    
    $('#tblRecepcionadas').empty()
    $('#tblRecepcionadas').DataTable({
        dom: 'Bfrtipl',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',

            }
        ],
        data: adataset,
        order: [],
        columnDefs:
            [
                //{ targets: [4], sType: 'date-ukShort' },
                { targets: 0, visible: false, searchable: false },
            ],
        columns: [
            { title: 'ANA_IdBiopsia' },
            { title: 'N° biopsia' },
            { title: 'Nombre paciente' },
            { title: 'Fecha recepción' },
            { title: 'Origen' },
            { title: 'Destino' },
            { title: 'Organo' },
            //{ title: 'Dependencia' },
            { title: 'Fecha despacho' },
            { title: 'Recibe' }
        ],
        bDestroy: true
    });
}