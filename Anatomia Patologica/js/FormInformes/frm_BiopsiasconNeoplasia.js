﻿$(document).ready(function () {

    ShowModalCargando(false);
    $("#btnBuscar").on('click', () => {
        descargarReporte();
    });
});

const descargarReporte = async () => {

    ShowModalCargando(true);

    const url = `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/NEOPLASIA/${$("#txtDesde").val()}/${$("#txtHasta").val()}`;
    const nombreArchivo = `Neoplasias_Codificadas_${$("#txtDesde").val()}_${$("#txtHasta").val()}.xlsx`;
    console.log(url);
    const existeReporte = await descargarReporteEnExcel(url, nombreArchivo);

    showMensajeAlert(existeReporte);
    ShowModalCargando(false);

}

const showMensajeAlert = (existeReporte) => {
    if (existeReporte) {
        $("#divResultadoReporte").attr("class", "alert alert-success text-center m-4");
        $("#divResultadoReporte").html("<i class='glyphicon glyphicon-ok'></i> El reporte se ha descargado exitosamente.");
    } else {
        $("#divResultadoReporte").attr("class", "alert alert-warning text-center m-4");
        $("#divResultadoReporte").html("<i class='glyphicon glyphicon-remove'></i> No existen datos a mostrar.");
    }
}