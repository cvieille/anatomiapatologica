﻿$(document).ready(function () {
    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));

    $('#btnBuscar').click(function (e) {
        grillaConsolidado();
        e.preventDefault();
    });
    grillaConsolidado();
});

function grillaConsolidado()
{
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/CONSOLIDADO/${$('#txtDesde').val()}/${$('#txtHasta').val()}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idBiopsia,
                    val.ANA_NumeroRegistro_Biopsias,
                    val.GEN_siglaModalidad_Fonasa,
                    val.ANA_cod001Codificacion_Biopsia,
                    val.ANA_cod002Codificacion_Biopsia,
                    val.ANA_cod003Codificacion_Biopsia,
                    val.ANA_cod004Codificacion_Biopsia,
                    val.ANA_cod005Codificacion_Biopsia,
                    val.ANA_cod006Codificacion_Biopsia,
                    val.ANA_cod007Codificacion_Biopsia,
                    val.ANA_cod008Codificacion_Biopsia,
                    val.ANA_Cortes_Muestrascount
                ]);
            });

            if (adataset.length == 0)
                $('#btnExportar').attr('disabled', true);
            else
                $('#btnExportar').removeAttr('disabled');

            $('#tblConsolidado').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        text: 'Exportar a excel',// mostrar texto
                        className: 'btn btn-info',
                        title: `Informe consolidado desde ${moment($('#txtDesde').val()).format("DD-MM-YYYY")}, hasta ${moment($('#txtHasta').val()).format("DD-MM-YYYY")}`,
                        exportOptions: {
                            // Personalizar las opciones de exportación
                            // Como por ejemplo: exportación personalizada de esas columnas y filas
                            //TODO...
                            fileName: "BiopsiaNeoplasia"
                        }
                    }
                ],
                data: adataset,
                order: [],
                columnDefs:
                [
                    //{ targets: [3, 7], sType: 'date-ukShort' },
                    { targets: 0, visible: false, searchable: false },
                ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'N° biopsia' },
                    { title: 'Tipo paciente' },
                    { title: '08-01-001' },
                    { title: '08-01-002' },
                    { title: '08-01-003' },
                    { title: '08-01-004' },
                    { title: '08-01-005' },
                    { title: '08-01-006' },
                    { title: '08-01-007' },
                    { title: '08-01-008' },
                    { title: 'Cortes' }
                ],
                bDestroy: true
            });
        }
    });
}