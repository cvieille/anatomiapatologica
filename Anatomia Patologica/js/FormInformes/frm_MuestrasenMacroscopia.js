﻿$(document).ready(function () {
    grillaMuestras();
});

async function grillaMuestras()
{
    let adataset = [];
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/buscar?idTipoEstadoSistema=43`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            
            $.each(data, function (key, val) {
                let stringBiopsia = ""
                val.OrganoBiopsia.map(a => {
                    stringBiopsia = a.Valor + "   "
                })
                let tc = [];
                if (val.Tecnicas.length > 0) {
                    for (let i = 0; i < val.Tecnicas.length; i++) {
                        tc.push(' ' + val.Tecnicas[i].Valor.substring(0, 4))
                    }
                }
                
                adataset.push([
                    val.Numero,
                    val.TipoBiopsia !== null ? val.TipoBiopsia.Valor:"Sin informacion",
                    stringBiopsia,
                    val.Cortes.length,
                    val.TipoMacroscopia !== null ? val.TipoMacroscopia.Valor : "Sin informacion",
                    (val.Patologo === null) ? "" : `${val.Patologo.Nombre} ${val.Patologo.ApellidoPaterno} ${val.Patologo.ApellidoMaterno}`,
                    tc,
                    val.Descacificaciones
                ]);
            });
        }
        
    });
    $('#tblMuestrasenMacroscopia').DataTable({
        dom: 'Bfrtipl',
         buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info'
           }
        ],
        data: adataset,
        order: [],
        columns: [
            { title: 'N° biopsia' },
            { title: 'Tipo Biopsia' },
            { title: 'Órgano' },
            { title: 'Cortes' },
            { title: 'Tipo Macroscopia' },
            { title: 'Patólogo' },
            { title: 'Técnicas' },
            { title: 'Descalc.' }
        ],
        bDestroy: true
    });
}
