﻿$(document).ready(function () {
    $('#txtDesde').val(moment().add(-1, 'month').format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));
})

async function buscarInformesWinsig() {
    let dataset=[]
    let url = `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?recepcionDesde=${$('#txtDesde').val()}&recepcionHasta=${$('#txtHasta').val()}`

    await $.ajax({
        url: url,
        method: "GET",
        success: function (res) {
            console.log(res)
            dataset = [...new Set(res.map(x => x.ServicioOrigen ))].map(z => {
                return {
                    CodigoWinsig: res.find(y => y.ServicioOrigen == z).CodigoWinSigOrigen??"",
                    Servicio: z??"Sin especificar",
                    Origen: res.find(y => y.ServicioOrigen == z).DependenciaOrigen,
                    Cantidad: res.filter(y => y.ServicioOrigen == z).length
                }
            })
            
        }, error: function (error) {
            console.error("Error al buscar biopsias")
            console.log(error)
        }
    })
    $("#tblInformWing").DataTable({
        dom: 'Bfrtipl',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',
                title: `Informe Winsig desde ${moment($('#txtDesde').val()).format("DD-MM-YYYY")}, hasta ${moment($('#txtHasta').val()).format("DD-MM-YYYY") }`,
                exportOptions: {
                    // Personalizar las opciones de exportación
                    // Como por ejemplo: exportación personalizada de esas columnas y filas
                    //TODO...
                    fileName: "BiopsiaNeoplasia"
                }
            }
        ],
        data: dataset,
        destroy:true,
        columns: [
            {title: "Codigo Winsig", data: "CodigoWinsig" },
            {title: "Origen", data: "Origen" },
            {title:"Servicio", data:"Servicio"},
            {title:"Cantidad", data:"Cantidad"}
        ]
    })
}