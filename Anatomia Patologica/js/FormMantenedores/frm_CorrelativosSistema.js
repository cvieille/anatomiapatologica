﻿$(document).ready(function () {

    valoresIniciales();

    $('#btnGuardar').click(function (e) {

        try {
            GuardarConf()


        } catch (e) {
            toastr.error('No se han podido guardar los cambios');
            console.log(e);
        }
        e.preventDefault();
    });

    $('#btnCancelar').click(function (e) {
        valoresIniciales();
        toastr.success('Se cargaron los valores iniciales');
        e.preventDefault();
    });
});
function GuardarConf() {
    let bValido = true;

    // Obtener valores
    const numeroBiopsia = parseInt($('#txtNBiopsia_').val(), 10);
    const añoBiopsia = parseInt($('#txtABiopsia_').val(), 10);
    const numeroSolicitud = parseInt($('#txtNSolNBiopsia_').val(), 10);
    const añoSolicitud = parseInt($('#txtASolBiopsia_').val(), 10);

    // Validaciones
    if (isNaN(numeroBiopsia) || numeroBiopsia <= 0) {
        bValido = false;
        resaltaElemento($('#txtNBiopsia_'));
    }
    if (isNaN(añoBiopsia) || añoBiopsia <= 0) {
        bValido = false;
        resaltaElemento($('#txtABiopsia_'));
    }
    if (isNaN(numeroSolicitud) || numeroSolicitud <=0) {
        bValido = false;
        resaltaElemento($('#txtNSolNBiopsia_'));
    }
    if (isNaN(añoSolicitud) || añoSolicitud <= 0) {
        bValido = false;
        resaltaElemento($('#txtASolBiopsia_'));
    }
    if (bValido) {
        $('#txtNBiopsia_').removeClass("_bordeError");
        $('#txtABiopsia_').removeClass("_bordeError");
        $('#txtNSolNBiopsia_').removeClass("_bordeError");
        $('#txtASolBiopsia_').removeClass("_bordeError");

        const configuracion = {
            'n_biopsia': numeroBiopsia,
            'a_biopsia': añoBiopsia,
            'n_solicitud': numeroSolicitud,
            'a_solicitud': añoSolicitud
        }
        $.ajax({
            type: "POST",
            url: ObtenerHost() + "/Clases/MetodosGenerales.ashx?method=GuardarConfiguracion",
            dataType: 'json',
            data: JSON.stringify(configuracion),
            contentType: 'application/json',
            success: function (data) {
                valoresIniciales()
                toastr.success('Se han guardado los cambios');
            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
    }
}
function valoresIniciales() {
    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Clases/MetodosGenerales.ashx?method=LeerConfiguracion",
        dataType: 'json',
        success: function (data) {
            $('#txtNBiopsia').val(data.n_biopsia);
            $('#txtABiopsia').val(data.a_biopsia);
            $('#txtNSolNBiopsia').val(data.n_solicitud);
            $('#txtASolBiopsia').val(data.a_solicitud);

            $('#txtNBiopsia_').val($('#txtNBiopsia').val());
            $('#txtABiopsia_').val($('#txtABiopsia').val());
            $('#txtNSolNBiopsia_').val($('#txtNSolNBiopsia').val());
            $('#txtASolBiopsia_').val($('#txtASolBiopsia').val());
        }
    });
}