﻿$(document).ready(function () {
    let vIdPlantilla = getUrlParameter('id');

    let sSession = getSession();
    let vIdUsuario = sSession.id_usuario;
    let vCreacionPlantilla;

    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Plantillas/${vIdPlantilla}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            if (!$.isEmptyObject(data)) {

                $('#txtNombrePlantilla').val(data.Nombre);
                //vCreacionPlantilla = data.ANA_fec_creacionPlantilla ?? "";

                if (data.AntecedentesClinicos == '')
                    $('#aAnteClinicos').first().addClass('_bordeError1px');
                else
                    $('#txtAntecedentesClinicos').val(data.AntecedentesClinicos);

                if (data.Macroscopia == '')
                    $('#aDescMacro').first().addClass('_bordeError1px');
                else
                    $('#txtDescMacro').val(data.Macroscopia);

                if (data.Microscopia == '')
                    $('#aExamenMicro').first().addClass('_bordeError1px');
                else
                    $('#txtExamenMicro').val(data.Microscopia);

                if (data.Diagnostico == '')
                    $('#aDiag').first().addClass('_bordeError1px');
                else
                    $('#txtDiag').val(data.Diagnostico);

                if (data.Inmunohistoquimica == '')
                    $('#aInmuno').first().addClass('_bordeError1px');
                else
                    $('#txtInmuno').val(data.Inmunohistoquimica);

                if (data.Nota == '')
                    $('#aNotaAdicional').first().addClass('_bordeError1px');
                else
                    $('#txtNotaAdicional').val(data.Nota);
            }
        }
    });

    $('#btnGuardar').click(function (e) {
        var bValido = true;
        if ($('#txtNombrePlantilla').val() == '' || $('#txtNombrePlantilla').val() == undefined || $('#txtNombrePlantilla').val() == null) {
            bValido = false;
            resaltaElemento($('#txtNombrePlantilla'));
        }

        if (bValido) {
            let validator = {
                set: function (obj, prop, value) {
                    //El nombre no puede estar vacio
                    if (prop === 'Nombre') {
                        if (value == "") {
                            toastr.error("El campo nombre no puede estar vacio.")
                            throw new Error('El campo nombre no puede estar vacio');
                        }
                    }
                    if (value == "") {
                        obj[prop] = null
                    } else {
                        obj[prop] = value;
                    }
                    return true;
                },
            };
            const jsonApi = new Proxy({}, validator)
            jsonApi.Nombre = $('#txtNombrePlantilla').val()
            jsonApi.AntecedentesClinicos = $('#txtAntecedentesClinicos').val()
            jsonApi.Macroscopia = $('#txtDescMacro').val()
            jsonApi.Microscopia = $('#txtExamenMicro').val()
            jsonApi.Diagnostico = $('#txtDiag').val()
            jsonApi.Nota = $('#txtNotaAdicional').val()
            jsonApi.Inmunohistoquimica = $('#txtInmuno').val()

            let vMetodo;
            let vUrl = `${GetWebApiUrl()}ANA_Plantillas`
            if (vIdPlantilla == 0) {
                vMetodo = 'POST';
            } else {
                vMetodo = 'PUT';
                vUrl = `${vUrl}/${vIdPlantilla}`
                jsonApi.Id = vIdPlantilla;
            }

            $.ajax({
                type: vMetodo,
                url: vUrl,
                contentType: 'application/json',
                data: JSON.stringify(jsonApi),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la plantilla <br/> <u>Click</u> en este mensaje para volver', '',
                            {
                                onclick: function () { window.location.replace('frm_ListaPlantillas.aspx'); },
                                onHidden: function () { window.location.replace('frm_ListaPlantillas.aspx'); }
                            });
                    else
                        toastr.success('Se ha creado la plantilla <br/> <u>Click</u> en este mensaje para volver', '',
                            {
                                onclick: function () { window.location.replace('frm_ListaPlantillas.aspx'); },
                                onHidden: function () { window.location.replace('frm_ListaPlantillas.aspx'); }
                            });

                    $('#btnGuardar').attr('disabled', true);
                    $('#btnCancelar').attr('disabled', true);
                }, error: function (err) {
                    console.error("Ha ocurrido un error al intentar guardar la plantilla")
                    console.log(err)
                }
            });
        }
        e.preventDefault();
    });
    $('#btnCancelar').click(function (e) {
        window.location.replace('frm_ListaPlantillas.aspx');
        e.preventDefault();
    });
});