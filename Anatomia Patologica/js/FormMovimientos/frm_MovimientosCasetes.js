﻿var vCodigoPerfil;
var vIdUsuario;
$(document).ready(function () {
    var vSession = getSession();
    vCodigoPerfil = vSession.GEN_CodigoPerfil;
    vIdUsuario = vSession.id_usuario;
    comboTipoBusqueda();
    comboPatologo();
    grillaMovimientos();
    

    $('#btnEnviarProcesador').click(function (e) {
        cambiarEstadoCasete(`${GetWebApiUrl()}ANA_Cortes_Muestras/Enviar/Procesador`);
        e.preventDefault();
    });

    $('#btnRecibir').click(function (e) {
        cambiarEstadoCasete(`${GetWebApiUrl()}ANA_Cortes_Muestras/Enviar/Inclusion`);
        e.preventDefault();
    });
    $('#btnTincion').click(function (e) {
        cambiarEstadoCasete(`${GetWebApiUrl()}ANA_Cortes_Muestras/Enviar/Tincion`);
        e.preventDefault();
    });

    $('#btnEnviarAlmacenamiento').click(function (e) {
        cambiarEstadoCasete(`${GetWebApiUrl()}ANA_Cortes_Muestras/Enviar/ParaAlmacenar`);
        e.preventDefault();
    });

    function cambiarEstadoCasete(sURL) {
        if ($('#tblMovimientosCasete tr').length > 1) {
            var a = [];
            let t = $('#tblMovimientosCasete').DataTable();
            let d = t.rows().nodes();

            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkCasete')[0].checked)
                    a.push($(d[i]).find('.checkCasete')[0].id);

            if (a.length > 0) {
                $.ajax({
                    type: 'PATCH',
                    contentType: 'application/json',
                    async: false,
                    data: JSON.stringify(a),
                    url: sURL,
                    success: function (data) {
                        toastr.success('Se han enviado los casetes seleccionados');
                        for (let f = 0; f < a.length; f++) {
                            eliminarFilaDatatable("tblMovimientosCasete", a[f])
                        }
                    }
                });
            }
            else
                toastr.info('Debe seleccionar al menos un registro');
        }
    }


    $('#btnBuscar').click(function (e) {
        if ($("#selTipoBusqueda").val() == 0)
            resaltaElemento($("#selTipoBusqueda"));
        else {
            $("#selTipoBusqueda").removeClass("_bordeError");
            grillaMovimientos();
        }
        e.preventDefault();
    });
    $('#selTipoBusqueda').change(function (e) {
        if (vCodigoPerfil == 6) //perfil paramédico
            $('#btnEnviarProcesador').show();
        else if (vCodigoPerfil == 4) {
            $('#btnEnviarProcesador').hide();

            if ($('#selTipoBusqueda').val() == 53) {
                $('#btnRecibir').show();
                $('#btnEnviarAlmacenamiento').hide();
                $('#btnTincion').hide();
            }
            else if ($('#selTipoBusqueda').val() == 62) {
                $('#btnRecibir').hide();
                $('#btnEnviarAlmacenamiento').hide();
                $('#btnTincion').show();
            }
            else if ($('#selTipoBusqueda').val() == 60) {
                $('#btnRecibir').hide();
                $('#btnEnviarAlmacenamiento').show();
                $('#btnTincion').hide();
            }
        }

        e.preventDefault();
    });
    $(document).on('change', '#chkTodoMovimientos', function () {
        var t = $('#tblMovimientosCasete').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.checkCasete')[0].checked = $(this).prop('checked');
    });
});

function selectAllMvmCasete(select) {
    let checkbox = $('#tblMovimientosCasete').find('.checkCasete')
    checkbox.map((index, item) => {
        if (select == true)
            item.checked = true
        else
            item.checked = false
    })
}

async function grillaMovimientos() {
    $("#tblMovimientosCasete").empty()
    $("#divSeleccion").hide()

    if ($('#selTipoBusqueda').val() != null) {
    let adataset = []
    await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=${$('#selTipoBusqueda').val()}&idPatologo=${$('#selPatologo').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data.length > 0) {
                    adataset = data.map(item => {
                        return [
                            /*0*/ '',
                            /*1*/ item.Id,
                            /*2*/ item.Nombre,
                            /*3*/'',
                            /*4*/'',
                            /*5*/ moment(item.FechaCorte).format('DD/MM/YYYY HH:mm:ss'),
                            /*6*/ (item.Patologo == '' || item.Patologo == undefined || item.Patologo == null) ? '' : item.Patologo.Login,
                            /*7*/ (item.Usuario == '' || item.Usuario == undefined || item.Usuario == null) ? '' : item.Usuario.Login,
                            /*8*/ item.TipoEstadoSistema.Id,
                            /*9*/ item.TipoEstadoSistema.Valor,
                            /*10*/ item.IdBiopsia,
                            /*11*/ '',

                        ]
                    })
                    inicializarTablaMovimientos(adataset)
                    //Solo cuando es perfil paramedico
                    if (vCodigoPerfil==6)
                        $("#divSeleccion").show()
                }
            }, error: function (err) {
                console.log("No se han encontrado resultados con la consulta")
                inicializarTablaMovimientos([])
            }
        });
    }
}

function inicializarTablaMovimientos(adataset) {
    let fechaActual = moment().format("DD-MM-YYYY")
    if (adataset.length == 0)
        $('#btnExportar').attr('disabled', true);
    else
        $('#btnExportar').removeAttr('disabled');

    $('#tblMovimientosCasete').addClass('nowrap').DataTable({
        dom: 'Bfrtipl',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',
                exportOptions: {
                    // Personalizar las opciones de exportación
                    // Como por ejemplo: exportación personalizada de esas columnas y filas
                    //TODO...
                    fileName: fechaActual + "-MovimientoCassete",
                    columns: [1, 2, 3, 4, 5, 7, 9, 10]
                }
            }
        ],
        data: adataset,
        fnCreatedRow: function (rowEl, adataset) {
            $(rowEl).attr('id', `row-${adataset[1]}`);
        },
        order: [],
        pageLength: 10,
        lengthMenu: [[20, 50, 100, -1], ["20", "50", "100", "Todos"]],
        columnDefs:
            [
                { targets: 1, searchable: false },
                { targets: 5, sType: 'date-ukLong' },
                { targets: 8, visible: false, searchable: false },
                //{ targets: -1, visible: false, searchable: false },
                {
                    targets: 0,
                    data: null,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var botones;
                        botones = '<label class="containerCheck">';
                        botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="checkCasete">';
                        botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                        botones += '</label>';
                        return botones;
                    }
                },
                {
                    targets: -1,
                    data: null,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var botones;
                        botones = `<a class="btn btn-info" data-id='${adataset[fila][10]}' onclick="linkBiopsia(this)">Ver</a>`;
                        return botones;
                    }
                },
                {targets: [3,4], visible:false}
            ],
        columns: [
            { title: '' },
            { title: 'IDCorte' },
            { title: 'N° corte' },
            { title: 'Checkeo 1' },
            { title: 'Checkeo 2' },
            { title: 'Fecha' },
            { title: 'Patólogo' },
            { title: 'Creado por' },
            { title: 'idTipoEstadoSistema' },
            { title: 'Estado' },
            { title: 'Cortes' }
        ],
        bDestroy: true
    });
}

function linkBiopsia(e) {
    let id = $(e).data('id');
    window.open(`../FormAnatomia/frm_Cortes.aspx?id=${id}`, '_blank');
}

function comboTipoBusqueda() {
    let url = `${GetWebApiUrl()}/GEN_Tipo_Estados_Sistemas/ANATOMIA/MovimientosCasete`;
    setCargarDataEnComboAsync(url, true, $('#selTipoBusqueda'));
}

function comboPatologo() {
    let url = `${GetWebApiUrl()}/GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));
}