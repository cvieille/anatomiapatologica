﻿$(document).ready(function () {

    $('#txtFechaDesde').val(moment().add(-3, 'month').format('YYYY-MM-DD'));
    $('#txtFechaHasta').val(moment().format('YYYY-MM-DD'));

    var vSession = getSession();

    if (vSession.GEN_CodigoPerfil == 1) //medico
    {
        $('#pnlBiopsiasSolicitadas').show();
        $('#pnlNotificaciones').hide();
        grillaBiopsias();
    }
    else if (vSession.GEN_CodigoPerfil == 14) //ges
    {
        $('#spanNotaGes').html("A continuación se presentan casos por <b>fecha de validación</b>:");
        $('#pnlBiopsiasSolicitadas').show();
        $('#pnlNotificaciones').hide();
        grillaGES();
    }
    else {
        grillaNotificaciones();
    }

    $('#btnBuscar').click(function (e) {
        if (vSession.GEN_CodigoPerfil == 1) //medico
            grillaBiopsias();
        else if (vSession.GEN_CodigoPerfil == 14) //ges
            grillaGES();
        e.preventDefault();
    });
    $('#btnBuscarNotificacion').click(function (e) {
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnLimpiarFiltro').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtFicha').val('');
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnGuardar').click(function (e) {
        //ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/guardarNotificacion',

        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${Ana_IdBiopsiaCritico}/Responder`,
            data: JSON.stringify({ Id: Ana_IdBiopsiaCritico, Respuesta: $('#txtRespuestaNotificacion').val() }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('La información fue guardada correctamente');
                $('#mdlNotificacion').modal('hide');
                grillaNotificaciones();
            }
        });
        e.preventDefault();
    });

    $('#btnCerrar').click(function (e) {
        let idUsuario = vSession.id_usuario;
        //ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/cerrarNotificacion',
        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${Ana_IdBiopsiaCritico}/Cerrar`,
            data: JSON.stringify({ Id: Ana_IdBiopsiaCritico, Respuesta: $('#txtRespuestaNotificacion').val() }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('Se ha cerrado correctamente la notificación');
                $('#mdlNotificacion').modal('hide');
                grillaNotificaciones();
            }
        });
        e.preventDefault();
    });

    $('#txtRut').blur(async function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(await DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    const paciente = data[0];
                    $('#txtNombre').val(paciente.Nombre);
                    $('#txtApellidoP').val(paciente.ApellidoPaterno);
                    $('#txtApellidoM').val(paciente.ApellidoMaterno);
                    $('#txtFicha').val(paciente.Nui);
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });
});

function grillaNotificaciones() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/Notificaciones',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.RegistroBiopsia.numero,
                    val.ANA_idBiopsia,
                    val.ANA_idBiopsias_Critico,
                    moment(val.RegistroBiopsia.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY/MM/DD').format('DD-MM-YYYY'),
                    val.Paciente.GEN_nombrePaciente,
                    val.RegistroBiopsia.PatologoValidador[0] != undefined ? val.RegistroBiopsia.PatologoValidador[0].GEN_NombreUsuarios : "",
                    val.RegistroBiopsia.Notificado[0] != undefined ? val.RegistroBiopsia.Notificado[0].GEN_NombreUsuarios : "",
                    val.RegistroBiopsia.GEN_nombreTipo_Estados_Sistemas,
                    val.ANA_idBiopsia,
                    val.Leido,
                    '',
                    '',
                    ''
                ]);
            });

            $('#tblNotificaciones').DataTable({
                data: adataset,
                order: [],
                stateSave: true,
                iStateDuration: 60,
                columnDefs:
                    [
                        { targets: 3, sType: 'date-ukShort' },
                        { targets: [1, 2, 8, 10], visible: false, searchable: false },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][1] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" data-idcritico="' + adataset[fila][2] + '" onclick="linkNotificacion(this)" class="btn btn-success">Ver</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Nº de biopsia' },//0//
                    { title: 'ANA_IdBiopsia' },//1//
                    { title: 'ANA_IdBiopsias_Critico' },//2//
                    { title: 'Recep. anatomía' },//3//
                    { title: 'Paciente' },//4//
                    { title: 'Patólogo' },//5//
                    { title: 'Notificado' },//6//
                    { title: 'Estado' },//7//
                    { title: 'id_biopsia' },//8//
                    { title: 'Leido' },//9//
                    { title: 'reporte' },//10//
                    { title: 'Información' },//11//                    
                    { title: '' }//12//
                ],
                bDestroy: true
            });
        }
    });
}

function linkInformacionB(e) {
    let id = $(e).data("id");
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $('#spanFechaRecepcion').html(moment(data.FechaRecepcion).format('DD-MM-YYYY'));
            if (data.Paciente !== null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_paciente/buscar?idPaciente=${data.Paciente.Id}`,
                    success: function (paciente) {
                        if (paciente.length > 0) {
                            $('#spanRutPaciente').html(`${paciente[0].NumeroDocumento}-${paciente[0].Digito}`);
                            $('#spanNombrePaciente').html(`${paciente[0].Nombre} ${paciente[0].ApellidoPaterno} ${paciente[0].ApellidoMaterno}`);
                        } else {
                            $('#spanNombrePaciente').html(`No se encontro data del paciente`);
                        }
                    }, error: function (err) {
                        console.error("Ha ocurridoun error al buscar el paciente")
                        console.log(err)
                    }
                })
            }
            $('#spanOrgano').html(data.ANA_organoBiopsia);
            if (data.Usuario !== null) {
                if (data.Usuario.Patologo)
                    $('#spanPatologo').html(`${data.Usuario.Patologo.Nombre} ${data.Usuario.Patologo.ApellidoPaterno} ${data.Usuario.Patologo.ApellidoMaterno}`);
            }
            if (data.Medico !== null) {
                const profesional = getProfesionalPorId(data.Medico.Id)
                $('#spanMedico').html(`${profesional.Nombre} ${profesional.ApellidoPaterno} ${profesional.ApellidoMaterno}`);
            }

            $('#spanOrigen').html(data.Origen.Valor);
            $('#spanDestino').html(data.Destino.Valor);
            $('#spanEstado').html(data.Estado.Valor);
        }
    });
    $('#mdlInformacionB').modal('show');
}

function linkImprimirB(idB) {
    ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${parseInt(idB)}/Imprimir`)
}
function imprimirPdf() {
    linkImprimirB($("#txtIdBiopsia").val());
}

async function linkNotificacion(e) {
    let id = $(e).data("id");
    let idCritico = $(e).data("idcritico");



    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + `ANA_registro_biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + `ANA_Biopsias_Critico/${idCritico}`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    Ana_IdBiopsiaCritico = data.Id
                    $('#chkVisto').attr('checked', true);

                    if (data.Leido != 'SI') {
                        marcarLeidoCasoCritico(idCritico);
                    }

                    $('#txtIdBiopsia').val(id);
                    $('#txtNota').val(data.Observacion);
                    $('#txtRespuestaNotificacion').val(data.Respuesta);
                }
            })

            $('#txtNumero').val(data.Numero + "-" + data.Folio);


            //Trabajando
            if (data.Paciente !== null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_paciente/buscar?idPaciente=${data.Paciente.Id}`,
                    success: function (paciente) {
                        if (paciente.length > 0) {
                            $('#txtUbInterna').val(paciente[0].Nui ?? "");
                            $('#txtDocumento').val(`${paciente[0].NumeroDocumento}-${paciente[0].Digito}`);
                            $('#txtNombrePaciente').val(`${paciente[0].Nombre} ${paciente[0].ApellidoPaterno} ${paciente[0].ApellidoMaterno}`);
                        } else {
                            $('#spanNombrePaciente').html(`No se encontro data del paciente`);
                        }
                    }, error: function (err) {
                        console.error("Ha ocurridoun error al buscar el paciente")
                        console.log(err)
                    }
                })
            }

            $('#txtServicioOrigen').val(data.Origen.Valor);
            if (data.Paciente !== null) {
                const profesional = getProfesionalPorId(data.Medico.Id)
                $('#txtSolicitadoPor').val(profesional.Nombre + " " + profesional.ApellidoPaterno + " " + profesional.ApellidoMaterno);
            }

            if (data.Usuario.Validador != null)
                $('#txtValidadoPor').val(data.Usuario.Validador.Nombre + " " + data.Usuario.Validador.ApellidoPaterno + " " + data.Usuario.Validador.ApellidoMaterno);//valdiador
            $('#txtEstadoInforme').val(data.Estado.GEN_nombreTipo_Estados_Sistemas);
            $('#txtOrgano').val(data.ANA_organoBiopsia);

            $('#mdlNotificacion').modal('show');
        }, error: function (err) {

        }
    })

    function marcarLeidoCasoCritico(idCritico) {
        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${idCritico}/Leido`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                grillaNotificaciones();
            }, error: function (err) {
                console.log("Ha ocurrido un error al intentar cambiar el estado a visto a una Biopsia", err);
            }
        });
    }
}
function grillaGES() {

    const fechaDesde = $('#txtFechaDesde').val();
    const fechaHasta = $('#txtFechaHasta').val();

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/GES/${fechaDesde}/${fechaHasta}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $('#tblGES').empty();

            let adataset = [];

            $.each(data, function (key, val) {
                let fechaValidacion = val.FechaValidacion ? moment(val.FechaValidacion).format("DD-MM-YYYY") : "";
                let patologo = val.Patologo ? val.Patologo : "";

                adataset.push([
                    val.Numero,
                    moment(val.FechaRecepcion).format("DD-MM-YYYY"),
                    val.Organo,
                    val.Descripcion,
                    val.Ges,
                    val.Tumoral,
                    val.Estado.Valor,
                    patologo,
                    fechaValidacion,
                    val.Id, // Id aquí para utilizarlo en la función linkVerGES pasarle por debajo el Id
                    val.Estado.Id,
                ]);
            });

            if ($.fn.DataTable.isDataTable('#tblGES')) {
                $('#tblGES').DataTable().destroy();
            }

            // Crear la tabla
            $('#tblGES').DataTable({
                responsive: true,
                data: adataset,
                order: [],
                columnDefs: [
                    {
                        targets: 2, // columna 'Órgano'
                        render: function (data, type, row, meta) {
                            // Si el tipo de renderizado es para mostrar en la tabla
                            if (type === 'display') {
                                return '<div style="max-width: 200px; overflow-wrap: break-word;">' + data + '</div>';
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        targets: 3, // columna 'Descripción'
                        render: function (data, type, row, meta) {
                            // Si el tipo de renderizado es para mostrar en la tabla
                            if (type === 'display') {
                                return '<div style="max-width: 200px; overflow-wrap: break-word;">' + data + '</div>';
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones;

                            if (adataset[fila][10] == 44 || adataset[fila][10] == 45)
                                botones = '<a onclick="linkImprimirB(' + adataset[fila][9] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                            else
                                botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';

                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'N°' },
                    { title: 'Fecha de Recepción' },
                    { title: 'Órgano' },
                    { title: 'Descripción' },
                    { title: 'GES' },
                    { title: 'Tumoral' },
                    { title: 'Estado' },
                    { title: 'Patólogo' },
                    { title: 'Fecha de Validación' },
                    { title: 'Imprimir', orderable: false } // Ocultar esta columna en la tabla
                ],
                bdestroy: true,
            });
        },
        error: function (error) {
            console.error('Error al obtener los datos del GES:', error);
        }
    });
}
function grillaBiopsias() {

    let iGEN_idProfesional = 0;
    let adataset = [];
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + '/GEN_Usuarios/LOGEADO',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            iGEN_idProfesional = data[0].Profesional.Id;
            const fechaInicio = moment($('#txtFechaDesde').val()).format("YYYY-MM-DD");
            const fechaTermino = moment($('#txtFechaHasta').val()).format("YYYY-MM-DD");
            ShowModalCargando(true)


            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?recepcionDesde=${fechaInicio}&recepcionHasta=${fechaTermino}&idProfesionalMedico=${iGEN_idProfesional}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if (data.length > 0) {
                        $.each(data, function (key, val) {
                            adataset.push([
                                val.Id,
                                val.Numero,
                                moment(val.FechaRecepcion).format("DD-MM-YYYY"),
                                val.Paciente.NumeroDocumento,
                                `${val.Paciente.Nombre} ${val.Paciente.ApellidoPaterno} ${val.Paciente.ApellidoMaterno}`,
                                val.Patologo == null ? "" : `${val.Patologo.Nombre} ${val.Patologo.ApellidoPaterno} ${val.Patologo.ApellidoMaterno}`,
                                val.LoginUsuariosTecnologo,
                                val.Estado.Id,
                                val.Estado.Valor,
                                val.ServicioOrigen,
                                val.FechaValidacion,
                                '',
                                ''
                            ]);
                        });
                        $('#tblBiopsias').addClass('nowrap').DataTable({
                            data: adataset,
                            order: [],
                            stateSave: true,
                            iStateDuration: 60,
                            columnDefs:
                                [
                                    { targets: 2, sType: 'date-ukShort' },
                                    { targets: [0, 3, 6, 7, 9, 10], visible: false, searchable: false },
                                    {
                                        targets: -2,
                                        data: null,
                                        orderable: false,
                                        render: function (data, type, row, meta) {
                                            let fila = meta.row;
                                            var botones;
                                            botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                            return botones;
                                        }
                                    },
                                    {
                                        targets: -1,
                                        data: null,
                                        orderable: false,
                                        render: function (data, type, row, meta) {
                                            let fila = meta.row;
                                            var botones;

                                            if (adataset[fila][7] == 44)
                                                botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                            else if (adataset[fila][7] == 45) {
                                                botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                            }
                                            else
                                                botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';

                                            return botones;
                                        }
                                    },
                                ],
                            columns: [
                                { title: 'ANA_IdBiopsia' },
                                { title: 'Nº de biopsia' },
                                { title: 'Recep. anatomía' },
                                { title: 'NumDocumentoPaciente' },
                                { title: 'Paciente' },
                                { title: 'Patólogo' },
                                { title: 'Gen_nombreTecnologo' },
                                { title: 'GEN_idTipo_Estados_Sistemas' },
                                { title: 'Estado' },
                                { title: 'GEN_nombreServicio' },
                                { title: 'ANA_fecValidaBiopsia' },
                                { title: 'Información' },
                                { title: 'Imprimir' }
                            ],
                            bDestroy: true
                        });
                    }
                    ShowModalCargando(false)
                }, error: function (err) {
                    ShowModalCargando(false)
                    console.log("No se  ha encontrado data")
                }
            });
        }, error: function (err) {
            console.log("No se ha encontrado data")
        }
    });
}