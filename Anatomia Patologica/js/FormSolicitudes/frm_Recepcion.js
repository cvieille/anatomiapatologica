﻿
var vID;
var vCodigoPerfil;

$(document).ready(function () {
    inicializarEventos()

    vID = getUrlParameter('id');
    var sSession = getSession();
    vCodigoPerfil = sSession.GEN_CodigoPerfil;

    deshabilitarElementosClase("datos-paciente");
    deshabilitarElementosClase("datos-paciente-adicional");

    $('#txtRecepcion').val(moment().format('YYYY-MM-DD'));

    $('#btnAgregar').click(function (e) {

        let bValido = validarElementosAgregar();

        if (bValido) {

            let attr = $('#btnAgregar').attr('disabled');
            if (!(typeof attr !== typeof undefined && attr !== false)) {
                let t = $('#tblDetalle').DataTable({
                    //data: adataset,
                    order: [],
                    columnDefs:
                        [
                            { targets: 0, visible: false, searchable: false },
                            {
                                targets: -1,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    if (vCodigoPerfil == 6 || vCodigoPerfil == 8)
                                        botones = '<a class="btn btn-danger" data-id="' + fila + '" onclick="linkQuitar(this)"><span class="glyphicon glyphicon-remove"></span></a>';
                                    else
                                        botones = '<a class="btn btn-danger" disabled><span class="glyphicon glyphicon-remove"></span></a>';

                                    return botones;
                                }
                            },
                        ],
                    bDestroy: true
                });
                // var d = t.rows().data().toArray();
                t.row.add([
                    $("#selDetalleMuestra").val(),
                    $('select[name="selTipoMuestra"] option:selected').text(),
                    $('#txtOrgano').val(),
                    parseInt($('#selMuestrasPorFrasco').val())
                ]).draw();
            }
        }
    });
    if (vID != 0) {
        cargarBiopsia(vID)
    }
    else {
        cargarCombosRecepcion();
        
        jsonBiopsia = {};
        $('#txtRecepcion').removeAttr('disabled');
        $('#btnAgregar').removeAttr('disabled');
        $('#selTipoOrigen').removeAttr('disabled');
        $('#selTipoDestino').removeAttr('disabled');
        $('#selServicioOrigen').removeAttr('disabled');
        $('#selServicioDestino').removeAttr('disabled');
        $('#selPrograma').removeAttr('disabled');
        $('#selSolicitado').removeAttr('disabled');


        $('#txtId').val('0');
        $('#txtNRegistro').val('0');
        if (vCodigoPerfil == perfilAccesoSistema.paramedico || vCodigoPerfil == perfilAccesoSistema.secretaria) {
            habilitarElementosClase("identificacion-Paciente");
        }
    }

    if (vCodigoPerfil == perfilAccesoSistema.secretaria || vCodigoPerfil == perfilAccesoSistema.paramedico) {
        //SECRETARIA O PARAMEDICO        
        habilitarElementosClase("datos-ingreso-muestra");
        deshabilitarCampos('#selTipoOrigen', '#selServicioOrigen', '#selTipoDestino', '#selServicioDestino', '#selPrograma', '#selSolicitado', '#btnAgregar')
        $('#btnGuardar').show();
    }

    $('#selTipoOrigen').change(function (e) {
        cargarComboServicio($('#selServicioOrigen'), $('#selTipoOrigen>option:selected').html());
    });
    $('#selTipoDestino').change(function (e) {
        cargarComboServicio($('#selServicioDestino'), $('#selTipoDestino>option:selected').html());
    });




});

function inicializarEventos() {
    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#selTipoMuestra').change(function () {
        if ($(this).val() != undefined && $(this).val() != 0) {
            comboDetalleCatalogo($(this).val());
        }
    });

    $('#selDocumento').change(function () {
        toggleDocumentoInputs($(this).val() == 1);
        $("#txtRut").val("");
        $("#txtDigito").val("");
        $("#txtNombre").val("");
        $("#txtApellidoP").val("");
        $("#txtApellidoM").val("");
        $("#txtEdad").val("");
        $("#txtNacimiento").val("");
        $("#txtNUI").val("");
        $("#txtNacionalidad").val(0);
    });

    $('#txtRut').blur(function () {
        if ($('#selDocumento').val() != 1) {
            buscarCargarPacienteDocumento($('#selDocumento').val(), $('#txtRut').val());
        }
    });

    $('#txtDigito').blur(async function (e) {
        if ($('#selDocumento').val() == 1 && $('#txtDigito').val() != "") {
            const digito = await DigitoVerificador($('#txtRut').val());
            if ($('#txtDigito').val().toUpperCase() == digito.toUpperCase())
                buscarCargarPacienteDocumento($('#selDocumento').val(), $('#txtRut').val())
            else
                toastr.error('Digito Verificar Incorrecto');
        }
        else
            resaltaElemento($('#txtDigito'));
    });
    $('#btnGuardar').click(function (e) {

        var bValido = true;

        if ($('#selDocumento').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDocumento'));
        }
        if ($('#txtOrgano').val() == '') {
            bValido = false;
            resaltaElemento($('#txtOrgano'));
        }

        if ($('#selPaciente').val() == '0') {
            bValido = false;
            resaltaElemento($('#selPaciente'));
        }
        if ($('#selTipoMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoMuestra'));
        }

        if ($('#selServicioOrigen').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioOrigen'));
        }
        if ($('#selServicioDestino').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioDestino'));
        }

        if ($('#selSolicitado').val() == '0') {
            bValido = false;
            resaltaElemento($('#selSolicitado'));
        }

        if ($('#selDetalleMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDetalleMuestra'));
        }

        if (bValido) {

            let jsonBiopsia = {
                FechaRecepcion: $('#txtRecepcion').val(),
                IdPaciente: $('#txtIdPaciente').val(),
                Organo: $('#txtOrgano').val(),
                Ges: $('#selGES').val(),
                IdServicioOrigen: parseInt($('#selServicioOrigen').val()),
                IdMedicoSolicita: parseInt($('#selSolicitado').val()),
                IdServicioDestino: parseInt($('#selServicioDestino').val()),
                IdPrograma: $('#selPrograma').val() > 0 ? $('#selPrograma').val() : null,
                IdMedicoSolicita: parseInt($('#selSolicitado').val()),
                IdCatalogoMuestra: parseInt($('#selDetalleMuestra').val()),
                IdModalidadFonasa: parseInt($('#selPaciente').val()),
                AntecedentesClinicos: $('#txtAntecedentesClinicos').val()
            };

            let parametrosGuardar = {
                Metodo: "",
                Url: "",
                Mensaje: "",
                Destino: `${ObtenerHost()}/FormPrincipales/frm_principal.aspx`
            }

            if (vID == 0) {
                parametrosGuardar.Metodo = 'POST';
                parametrosGuardar.Url = `${GetWebApiUrl()}ANA_Registro_Biopsias`;
                parametrosGuardar.Mensaje = 'Se ha ingresado solicitud de biopsia';
            }
            else {

                parametrosGuardar.Metodo = 'PUT';
                parametrosGuardar.Url = `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}`;
                parametrosGuardar.Mensaje = 'Se ha actualizado la solicitud de biopsia';
                jsonBiopsia.IdBiopsia = parseInt(vID);

            }

            //la tabla de muestras
            var t = $('#tblDetalle').DataTable();
            var d = t.rows().data().toArray();
            if (d.length == 0) {
                resaltaElemento($('#selTipoMuestra'));
                resaltaElemento($('#selDetalleMuestra'));
                resaltaElemento($('#txtOrgano'));
                return false;
            }

            jsonBiopsia.DetalleCatalogo = [];

            for (var p = 0; p < d.length; p++) {
                let json = {
                    IdCatalogoMuestra: parseInt(d[p][0]),
                    Descripcion: d[p][2],
                    CantidadMuestras: d[p][3]
                };
                jsonBiopsia.DetalleCatalogo.push(json);
            }

            $.ajax({
                type: parametrosGuardar.Metodo,
                url: parametrosGuardar.Url,
                data: JSON.stringify(jsonBiopsia),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    window.location.href = parametrosGuardar.Destino;
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnCancelar').click(function (e) {
        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
        e.preventDefault();
    });

    $('#btnMovimientos').click(function (e) {
        modalMovimiento(vID);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });

}
function toggleDocumentoInputs(habilitar) {
    if (habilitar) {
        $('#txtDigito').show();
        $('#txtguion').show();
        $("#txtRut").attr('maxlength', '8');
    } else {
        $('#txtDigito').hide();
        $('#txtguion').hide();
        $("#txtRut").attr('maxlength', '15');
    }
}

function validarElementosAgregar() {
    const clase = "requerido-agregar";
    const inputs = ['#selTipoMuestra', '#selDetalleMuestra', '#txtOrgano', '#selMuestrasPorFrasco'];

    let valido = true;

    inputs.forEach(selector => {
        const input = $(selector);
        if (input.val() == "" || input.val() == "0") {
            input.addClass("_bordeError");
            valido = false;
        } else {
            input.removeClass("_bordeError");
        }
    });

    return valido;
}

async function cargarBiopsia(id) {
    try {
        await cargarCombosRecepcion();
        const dataBio = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
            contentType: 'application/json',
            dataType: 'json'
        });

        $('#txtId').val(id);
        $('#txtNRegistro').val(`${dataBio.Numero}-${dataBio.Folio}`);
        $('#txtRecepcion').val(moment(dataBio.FechaRecepcion).format('YYYY-MM-DD'));
        $('#btnAgregar').removeAttr('disabled');
        const idPaciente = dataBio.Paciente.Id;
        buscarCargarPacienteId(idPaciente)

        //deshabilitar los campos de indentificacion
        deshabilitarElementosClase("identificacion-Paciente");

        $('#selPaciente').val(dataBio.GEN_idModalidad_Fonasa || 0);
        $('#selGES').val(dataBio.ANA_gesRegistro_Biopsias || 0);
        $('#txtEstado').val(dataBio.Estado.Valor);

        $('#selTipoMuestra').val(dataBio.IdCatalogoPadre || 0);
        comboDetalleCatalogo(dataBio.IdCatalogoPadre);

        $('#txtOrgano').val(dataBio.ANA_organoBiopsia);
        $('#selPrograma').val(dataBio.Programa?.Id || 0);

        $('#selTipoOrigen').val(dataBio.Origen.Dependencia).trigger('change');
        $('#selTipoDestino').val(dataBio.Destino.Dependencia).trigger('change');
        //////////////////////////////////////////////////////////////
        //objetos que hay que esperar a que carguen//
        //////////////////////////////////////////////////////////////
        await esperar(100)
        $('#selServicioOrigen').val(dataBio.Origen.Id);
        $('#selServicioDestino').val(dataBio.Destino.Id);
        $('#selDetalleMuestra').val(dataBio.IdCatalogo);
        $('#selSolicitado').val(dataBio.Medico?.Id || 0);
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////



        if (dataBio.AntecedentesClinicos !== null)
            $('#txtAntecedentesClinicos').val(dataBio.AntecedentesClinicos);

        if (dataBio.ANA_dictadaBiopsia == 'SI')
            $('#chkDictado').attr('checked', true);

        if (dataBio.ANA_tumoralBiopsia == 'SI')
            $('#chkTumoral').attr('checked', true);

        $('#btnMovimientos').removeAttr('disabled');

        grillaDetalle(id);
    } catch (error) {
        console.error("Error al cargar la biopsia:", error);
        toastr.error("No se pudo cargar la biopsia. Intente nuevamente.");
    }
}
function buscarCargarPacienteId(idPaciente) {
    const filtroPaciente = "idPaciente=" + idPaciente;
    cargarPaciente(filtroPaciente);
}
function buscarCargarPacienteDocumento(idIdentificacion, numeroDocumento) {
    if ($('#txtRut').val().length <= 6) {
        toastr.warning('Ingrese un número de documento valido');
    }
    else {
        const filtroPaciente = `idIdentificacion=${idIdentificacion}&numeroDocumento=${numeroDocumento}`;
        cargarPaciente(filtroPaciente);
    }
}
function cargarPaciente(filtroPaciente) {
    ShowModalCargando(true)
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?${filtroPaciente}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (!$.isEmptyObject(data[0])) {
                const paciente = data[0];
                $('#selDocumento').val(paciente.Identificacion.Id);
                $('#txtIdPaciente').val(paciente.IdPaciente);
                $('#txtRut').val(paciente.NumeroDocumento);
                $('#txtDigito').val(paciente.Digito);
                $('#txtNombre').val(paciente.Nombre);
                $('#txtApellidoP').val(paciente.ApellidoPaterno);
                $('#txtApellidoM').val(paciente.ApellidoMaterno);
                $('#txtNacimiento').val(moment(paciente.FechaNacimiento).format('YYYY-MM-DD'));
                $('#txtNUI').val(paciente.Nui);
                if (paciente.Edad != null)
                    $('#txtEdad').val(paciente.Edad.edad);
                $('#txtNombreSocial').val(paciente.NombreSocial);
                $('#selDocumento').val(paciente.Identificacion.Id);
                if (paciente.Nacionalidad != null)
                    $('#selNacionalidad').val(paciente.Nacionalidad.Id);

                if (paciente.Genero != null)
                    $('#selGenero').val(paciente.Genero.Id);

                if (paciente.Sexo != null)
                    $('#selSexo').val(paciente.Sexo.Id);

                habilitarElementosClase("datos-paciente-adicional");

            } else {
                $('#txtNombre').val('');
                $('#txtApellidoP').val('');
                $('#txtApellidoM').val('');
                $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));
                $('#txtEdad').val('');
                $('#txtNacionalidad').val('');
                toastr.warning('El Paciente no existe, debe crearlo en Mantenedor de Pacientes');
            }
            ShowModalCargando(false)
        }
    });
}


function linkQuitar(e) {
    let id = $(e).data("id");
    var t = $('#tblDetalle').DataTable();
    t.row(id).remove();
    var d = t.rows().data().toArray();
    t.clear();
    t.rows.add(d).draw();
}

function grillaDetalle(id) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/DetalleMuestras`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Catalogo.Id,
                    val.Catalogo.Valor,
                    val.Descripcion,
                    val.Cantidad,
                    ''
                ]);
            });
            $('#tblDetalle').DataTable({

                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: 0, visible: false, searchable: false },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                if (vCodigoPerfil == 6 || vCodigoPerfil == 8)
                                    botones = '<a class="btn btn-danger" data-id="' + fila + '" onclick="linkQuitar(this)"><span class="glyphicon glyphicon-remove"></span></a>';
                                else
                                    botones = '<a class="btn btn-danger" disabled><span class="glyphicon glyphicon-remove"></span></a>';

                                return botones;
                            }
                        },
                    ],
                bDestroy: true
            });
        }
    });
}
async function cargarCombosRecepcion() {

    await cargarComboMedicos();
    cargarComboProgramas();
    cargarComboIdentificacion();

    cargarComboModalidad();
    cargarComboCatalogo();

    cargarComboServicio($('#selServicioOrigen'), 'HCM');
    cargarComboServicio($('#selServicioDestino'), 'HCM');

    const baseApi = GetWebApiUrl();

    let url = `${baseApi}GEN_Sexo/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selSexo'))

    url = `${baseApi}GEN_Tipo_Genero/combo`;
    setCargarDataEnComboAsync(url, true, $('#selGenero'))

    url = `${baseApi}GEN_Nacionalidad/combo`;
    setCargarDataEnComboAsync(url, true, $('#selNacionalidad'))

}