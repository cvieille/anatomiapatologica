﻿function comboDetalleCatalogo(idCatalogo) {
    if (idCatalogo > 0) {
        const url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/${idCatalogo}/Detalle/combo`;
        setCargarDataEnComboAsync(url, false, $('#selDetalleMuestra'));
    }
}
function cargarComboModalidad() {
    const url = `${GetWebApiUrl()}GEN_Modalidad_Fonasa/Combo`;
    setCargarDataEnComboAsync(url, false, $('#selPaciente'));
}

function cargarComboCatalogo() {
    const url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoMuestra'));
}

function cargarComboProgramas() {
    const url = `${GetWebApiUrl()}GEN_Programa/combo`;
    setCargarDataEnComboAsync(url, true, $('#selPrograma'));
}

async function cargarComboServicio(cmbServicio, dependencia) {
    const url = `${GetWebApiUrl()}GEN_Servicio/combo/${dependencia}`;
    await setCargarDataEnComboAsync(url, false, cmbServicio);
}

async function cargarComboIdentificacion() {
    const url = `${GetWebApiUrl()}GEN_Identificacion/combo`;
    await setCargarDataEnComboAsync(url, false, $('#selDocumento'));
    $('#selDocumento option:first').remove();
}

async function cargarComboMedicos() {
    $('#selSolicitado').empty();

    // Obtener los datos de forma asincrónica
    const data = await getProfesionalesMedicos();

    // Verificar que los datos sean válidos
    if (!data || !Array.isArray(data)) {
        console.error("Error: datos inválidos recibidos.", data);
        $('#selSolicitado').append("<option value='0'>Error al cargar médicos</option>");
        return;
    }

    // Mapear y transformar los datos
    const datos = data.map(a => ({
        Id: a.Id,
        Valor: `${a.Persona.ApellidoPaterno} ${a.Persona.ApellidoMaterno} ${a.Persona.Nombre}`
    }));

    // Ordenar alfabéticamente en orden ascendente
    const datosOrdenados = datos.sort((a, b) => a.Valor.localeCompare(b.Valor));

    // Agregar las opciones al combo
    $('#selSolicitado').append("<option value='0'>-Seleccione-</option>");
    datosOrdenados.forEach(val => {
        $('#selSolicitado').append(`<option value="${val.Id}">${val.Valor}</option>`);
    });
}
