﻿function getMenu() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Menu/PorPerfil`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            sessionStorage.setItem('menuANA', JSON.stringify(data));
            dibujarMenu();
        }
    });

}
function dibujarMenu() {

    var html = "";
    var json = JSON.parse(sessionStorage.getItem("menuANA"));

    $('#divPerfil').show();
    $('#divPerfilN').html(perfilAccesoSistema.nombrePerfil)

    if (perfilAccesoSistema.codigoPerfil == 2)
        $('#navJS').append(`<ul class="nav navbar-nav"><li><a href="#/" id="linkIngresarComo">Entrar como...</a></li></ul>`);

    html += `<ul class="nav navbar-nav">`;
    for (var i = 0; i < json.length; i++) {
        if (json[i].GEN_MenuHijos.length === 0 || (json[i].GEN_MenuHijos.length === 1 && json[i].GEN_MenuHijos[0].GEN_MenuHijos.length === 1)) {
            html += `<li>
                                        <a href="${ObtenerHost() + json[i].GEN_contenidoMenu.replace('~/', '/')}">${json[i].GEN_tituloMenu}</a>
                                     </li>`;
        } else if (json[i].GEN_MenuHijos.length > 1) {
            html += `<li>
                                        <a href="/#" class="dropdown-toggle" data-toggle="dropdown">${json[i].GEN_tituloMenu}<b class="caret"></b></a>
                                        <ul class="dropdown-menu multi-level">`;

            for (var j = 0; j < json[i].GEN_MenuHijos.length; j++) {
                if (json[i].GEN_idMenu !== json[i].GEN_MenuHijos[j].GEN_idMenuHijo)
                    html += `<li>
                                                <a href="${ObtenerHost() + json[i].GEN_MenuHijos[j].GEN_contenidoMenuHijo.replace('~/', '/')}">${json[i].GEN_MenuHijos[j].GEN_tituloMenuHijo}</a>
                                            </li>`;
            }
            html += `</ul></li>`;
        }
    }
    $('#navJS').append(`${html}</ul>`);
}
