﻿
    function getPacientesPorDocumento(documento) {  
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${documento}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {                
                const paciente = data[0];
                if (paciente === null)
                    console.log("paciente no encontrado");
                
                return paciente;
            }
        });
    }
