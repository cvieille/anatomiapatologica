﻿function getNombrePersonaApi(id, element) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Personas/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            const nombre = `${data.Nombre} ${data.ApellidoPaterno} ${data.ApellidoMaterno}`
            $(element).val(nombre);
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}