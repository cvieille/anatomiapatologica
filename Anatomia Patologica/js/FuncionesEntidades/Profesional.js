﻿function getNombreCompletoProfesionalApi(id, element) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            const nombre = `${data.Nombre} ${data.ApellidoPaterno} ${data.ApellidoMaterno}`
            $(element).val(nombre);
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}
function getProfesionalPorId(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            const profesional = data;
            if (profesional === null)
                console.log("profesional no encontrado");

            return profesional;
        }
    });
}
async function getProfesionalesMedicos() {
    try {
        const response = await $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Profesional/buscar?idProfesion=1`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                return data;
            }
        });
        return response; // Retorna la respuesta correctamente
    } catch (error) {
        console.error("Error al obtener los datos:", error);
        return null; // Manejo de errores
    }
}
