﻿$(document).ready(function () {
    var v = getSession();
    vAdmin = false;
    if (v.ADMIN_LOGIN) {
        $('#txtClaveLoginModal').addClass('disabled').attr('disabled', true);
        vAdmin = true;
    }
    //Cada vez que se carga el TEMPLATE
    $('#btnEntrarLoginModal').unbind("click");
    $('#btnEntrarLoginModal').click(function (e) {
        LoginModal();
        e.preventDefault();
    });
});

function LoginModal() {
    //if (validarCampos("#div_login", false)) {
    //var success = false;
    sessionStorage.setItem("desdeModal", true);
    $('#btnEntrarLoginModal').addClass('disabled');
    $('#btnEntrarLoginModal').attr('disabled', 'disabled');
    $('#btnEntrarLoginModal').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $('#txtUsuarioLoginModal').attr('disabled', 'disabled');
    $('#txtClaveLoginModal').attr('disabled', 'disabled');
    try {
        var user = $('#txtUsuarioLoginModal').val();
        var pass = '';
        if (vAdmin) {
            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}gen_usuarios/autentificacion`,
                contentType: 'application/json',
                data: JSON.stringify({ login: user }),
                dataType: 'json',
                async: false,
                success: function (data) {
                    pass = data;
                    sessionStorage.setItem("usuario", user);
                }
            });
        }
        else
            pass = CryptoJS.MD5($('#txtClaveLoginModal').val());
        let resp = LogearUsuarioModal(user, pass, 0);
        if (resp) {
            sessionStorage.setItem("usuario", user);
            getUsuarioModal(user, pass);
            $("#mdlLogin").modal("hide");
        } else
            loginIncorrectoModal();
    } catch (err) {
        console.log(err.message);
    }
}

function loginIncorrectoModal() {
    toastr.error('Usuario o contraseña incorrectos');
    $('#btnEntrarLoginModal').removeClass('disabled');
    $('#btnEntrarLoginModal').removeAttr('disabled');
    $('#btnEntrarLoginModal').html('Entrar');
    $('#txtUsuarioLoginModal').removeAttr('disabled');
    $('#txtClaveLoginModal').removeAttr('disabled');
}

function getUsuarioModal(usuario, pass) {
    try {
        pass = pass + '';
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + '/GEN_Usuarios/Acceso',
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if ($.isEmptyObject(data)) {
                    loginIncorrecto();
                }
                else {
                    setSession('id_usuario', data[0].GEN_idUsuarios);
                    setSession('nom_usuario', usuario.toUpperCase());
                    setSession('rut_usuario', data[0].GEN_numero_documentoPersonas);
                    var sr = CryptoJS.MD5(data[0].GEN_numero_documentoPersonas) + '';
                    if (pass.toLowerCase() == sr.toLowerCase()) {
                        window.location.replace(`${ObtenerHost()}/FormMantenedores/frm_Clave.aspx`);
                    }
                    else {
                        if (data[0].GEN_Perfiles.length > 1) {
                            vPerfiles = data[0].GEN_Perfiles;
                            setSession('VPERFILES', JSON.stringify(vPerfiles));
                            $.each(data[0].GEN_Perfiles, function (key, val) { $("#selectPerfiles").append("<option value='" + val.GEN_idPerfil + "'>" + val.GEN_descripcionPerfil + "</option>"); });
                            $("#selectPerfiles").val(sessionStorage.getItem("perfil"));
                            $('#modalPerfiles').modal("show");
                            $("#selectPerfiles").prop("disabled", true);
                            return;
                        }
                        setSession('GEN_CodigoPerfil', data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                        setSession('perfil_del_usuario', data[0].GEN_Perfiles[0].GEN_idPerfil);
                        setSession('NombrePerfil', data[0].GEN_Perfiles[0].GEN_descripcionPerfil);
                        LogearUsuarioModal(usuario, pass, data[0].GEN_Perfiles[0].GEN_idPerfil);
                        $("mdlLoginModal").modal("hide");
                        //redirecciona(data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                    }
                }
            }
        });
    } catch (err) {
        console.log('Error: ' + err.message);
    }
}

////Arreglar esta función se copio para evitar sobreescritura del sessionStorage.perfil
function LogearUsuarioModal(user, pass, perfil = 0) {
    var resp;
    var success = false;
    const datos = {
        'grant_type': 'password',
        'username': user,
        'password': '' + pass,
        'clientid': '1',
    };
    if (perfil != 0) {
        datos.idperfil = perfil;
        sessionStorage.setItem("desdeModal", false);
    }

    if ($.ajaxSettings.headers['Authorization'] != undefined)
        delete $.ajaxSettings.headers['Authorization'];
    $.ajax({
        type: 'POST',
        url: GetWebApiUrl() + 'recuperarToken',
        data: datos,
        async: false,
        success: function (response) {
            resp = response;

            setSession('TOKEN', resp.access_token);
            $.ajaxSetup({
                headers: { 'Authorization': GetToken() }
            });

            return success;
        },
        error: function (err) {
            console.log('ERROR no se pudo obtener el token: ' + err.message);
        }
    });

}