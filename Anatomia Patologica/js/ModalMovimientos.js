﻿function modalMovimiento(idBiopsia)
{
    //console.log(`${GetWebApiUrl()}ANA_Movimientos/ANA_Registro_Biopsias/${idBiopsia}`);
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Movimientos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Descripcion,
                    val.Detalle,
                    val.Usuario
                ]);
            });

            $('#tblMovimientos').DataTable({
                data: adataset,
                order: [],
                columnDefs: [{ targets: 1, sType: 'date-ukLong' }],
                columns: [
                    { title: 'ID movimiento' },
                    { title: 'Fecha' },
                    { title: 'Tipo movimiento' },
                    { title: 'Descripción' },
                    { title: 'Login' }
                ],
                 
                bDestroy: true
            });
        }
    });
}