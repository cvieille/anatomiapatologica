var vIdBiopsia;
var vIdUsuario;
$(document).ready(function () {
    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    if (sSession.GEN_CodigoPerfil == 13)
        $('#btnCodificacion').show();

    $('#txtRut').blur(async function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(await DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (!$.isEmptyObject(data[0])) {
                        const paciente = data[0];
                        $('#txtNombre').val(paciente.Nombre);
                        $('#txtApellidoP').val(paciente.ApellidoPaterno);
                        $('#txtApellidoM').val(paciente.ApellidoMaterno);
                        $('#txtNUI').val(paciente.Nui);
                    }
                    else {
                        $('#txtNombre').val('');
                        $('#txtApellidoP').val('');
                        $('#txtApellidoM').val('');
                        $('#txtNUI').val('');
                    }
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });

    $('#btnBuscar').click(function (e) {
        if (($('#txtRut').val() == '' || $('#txtRut').val() == null) &&
            ($('#txtNombre').val() == '' || $('#txtNombre').val() == null) &&
            ($('#txtApellidoP').val() == '' || $('#txtApellidoP').val() == null) &&
            ($('#txtApellidoM').val() == '' || $('#txtApellidoM').val() == null) &&
            ($('#txtNUI').val() == '' || $('#txtNUI').val() == null)) {
            toastr.error('Debe llenar al menos un filtro de búsqueda');
        }
        else {

            ShowModalCargando(true)
            grillaPaciente();
            ShowModalCargando(false)
            $('#btnLimpiar').removeAttr('disabled');
        }
        e.preventDefault();
    });

    $('#btnLimpiar').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtNUI').val('');
        var t = $('#tblPaciente').DataTable();
        t.clear();
        t.draw(false);
        e.preventDefault();
    });

    $('#btnImprimirInforme').click(function (e) {

        let idBiopsiaImprimir = $('#spanIdBiopsia').text();

        ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${parseInt(idBiopsiaImprimir)}/Imprimir`)

    });
});

function detalleArchivo(e) {
    let id = $(e).data('id');
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Archivos_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let url = ObtenerHost() + '/Documento.aspx?nombre=' + data.Nombre + '&idBiopsia=' + data.IdBiopsia;
            window.open(url);
        }
    });
}
function cargarDatosBiopsiaPorId(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.Estado !== null) {
                if (data.Estado.Id !== 44 && data.Estado.Id !== 45) {
                    //Aca deberia ocultar el boton
                    $("#btnImprimirInforme").addClass("d-none")
                    $("#btnImprimirInforme").attr("disabled", true)
                }
            }
            $('#spanIdBiopsia').html(data.Id);
            $('#spanFechaRecepcion').html(moment(data.FechaRecepcion).format('DD-MM-YYYY'));
            if (data.Paciente !== null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_paciente/buscar?idPaciente=${data.Paciente.Id}`,
                    success: function (paciente) {
                        if (paciente.length > 0) {
                            $('#spanRutPaciente').html(`${paciente[0].NumeroDocumento}-${paciente[0].Digito}`);
                            $('#spanNombrePaciente').html(`${paciente[0].Nombre} ${paciente[0].ApellidoPaterno} ${paciente[0].ApellidoMaterno}`);
                        } else {
                            $('#spanNombrePaciente').html(`No se encontro data del paciente`);
                        }
                    }, error: function (err) {
                        console.error("Ha ocurridoun error al buscar el paciente")
                        console.log(err)
                    }
                })
            }

            $('#spanPatologo').html(data.Usuario.Patologo == null ? "" : data.Usuario.Patologo.Login);
            $('#spanTecnologo').html(data.Usuario.Tecnologo == null ? "" : data.Usuario.Tecnologo.Login);
            $('#spanServOrigen').html(`${data.Origen.Dependencia} - ${data.Origen.Valor}`);
            $('#spanServDestino').html(`${data.Destino.Dependencia} - ${data.Destino.Valor}`);

            $('#spanOrgano').html(data.ANA_organoBiopsia);

        }
    });
}
function cargarCodificacionBiopsia(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Codificacion`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.Codificacion !== null) {
                $('#txt0801001').val(data.Cod001);
                $('#txt0801002').val(data.Cod002);
                $('#txt0801003').val(data.Cod003);
                $('#txt0801004').val(data.Cod004);
                $('#txt0801005').val(data.Cod005);
                $('#txt0801006').val(data.Cod006);
                $('#txt0801007').val(data.Cod007);
                $('#txt0801008').val(data.Cod008);
            }
        }, error: function (error) {
            $('#tblArchivo').DataTable({
                data: [],
                columns: [
                    { title: 'Id' },
                    { title: 'Fecha' },
                    { title: 'Archivo' }
                ],
                bDestroy: true
            })
            console.error("Ha ocurrido un error al intentar buscar codigos, o no se encontro informacion")
            console.log(error)
        }
    });
}
function cargarArchivosBipsia(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Archivos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Nombre
                ]);
            });
            $('#tblArchivo').DataTable({
                data: adataset,
                order: [],
                dom: 't',
                columnDefs:
                    [
                        {
                            targets: 2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones = `<a data-id="${adataset[fila][0]}" href="#/" onclick="detalleArchivo(this)">${adataset[fila][2]}</a>`;
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id' },
                    { title: 'Fecha' },
                    { title: 'Archivo' }
                ],
                bDestroy: true
            });
        }, error: function (error) {
            $('#tblArchivo').DataTable({
                data: [],
                columns: [
                    { title: 'Id' },
                    { title: 'Fecha' },
                    { title: 'Archivo' }
                ],
                bDestroy: true
            })
            console.error("Ha ocurrido un error al intentar buscar archivos adjuntos, o no se encontro informacion")
            console.log(error)
        }
    });
}
function linkReporte(e) {
    $('#mdlBiopsia').modal('show');
    let id = $(e).data('id');
    $('#btnImprimirInforme').attr('data-id', id);
    cargarDatosBiopsiaPorId(id);
    if (sSession.GEN_CodigoPerfil == 13) {
        cargarCodificacionBiopsia(id);
        $("#dvAdjuntos").hide()
    }
    else {
        $("#dvCodificacion").hide()
        cargarArchivosBipsia(id);
    }

}

function grillaPaciente() {

    let adataset = [];
    let json = {
        nombre: $('#txtNombre').val(),
        rut: $('#txtRut').val(),
        app: $('#txtApellidoP').val(),
        apm: $('#txtApellidoM').val(),
        nui: $('#txtNUI').val()
    }
    //v `${ObtenerHost()}/frm_consulta_clinicos.aspx/grillaReportes`,
    let url = GetWebApiUrl() + 'ANA_Registro_Biopsias/Buscar?nombrePaciente=' + json.nombre
        + '&apellidoPaternoPaciente=' + json.app
        + '&apellidoMaternoPaciente=' + json.apm
        + '&nui=' + json.nui
        + '&numeroDocumentoPaciente=' + json.rut

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            ShowModalCargando(true)
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Estado.Id,
                    val.Paciente.NumeroDocumento,
                    val.Numero,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.Paciente.Nombre + " " + val.Paciente.ApellidoPaterno + " " + val.Paciente.ApellidoMaterno,
                    val.Patologo == null ? "" : val.Patologo.Nombre + " " + val.Patologo.ApellidoPaterno + " " + val.Patologo.ApellidoMaterno,
                    (val.LoginUsuariosTecnologo == null) ? 'Sin definir' : val.LoginUsuariosTecnologo,
                    val.Estado.Valor,
                    val.ServicioDestino,
                    val.FechaValidacion == null ? '' : (moment(val.FechaValidacion).format('DD/MM/YYYY HH:mm:ss')),
                    ''
                ]);
            });
            $('#tblPaciente').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2], visible: false, searchable: false },
                        { targets: 4, sType: 'date-ukShort' },
                        { targets: 10, sType: 'date-ukLong' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkReporte(this)"><span class="glyphicon glyphicon-list-alt"></span></a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'NumDocumentoPaciente' },
                    { title: 'N° de biopsia' },
                    { title: 'Fecha recepción' },
                    { title: 'Paciente' },
                    { title: 'Patólogo' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado' },
                    { title: 'Destino' },
                    { title: 'Fecha validación' },
                    { title: 'Reporte' }
                ],
                bDestroy: true
            });
        }, error: function (err) {
            console.log("No se ha encontrodado data")
        }
    });
}