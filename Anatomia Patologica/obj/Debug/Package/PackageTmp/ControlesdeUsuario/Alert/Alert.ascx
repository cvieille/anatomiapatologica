﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Alert.ascx.vb" Inherits="Anatomia_Patologica.Alert" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:UpdatePanel ID="Upd_modalAlerta" runat="server">
    <ContentTemplate>
        <!----- POPUP MENSAJE DE ALERTA ----->
        <asp:ModalPopupExtender ID="modal_alerta" runat="server" DropShadow="True" Enabled="True" PopupControlID="pnl_alerta"
            TargetControlID="cmd_alerta" BackgroundCssClass="modalBackgroundAlert" CancelControlID="cmd_alertaCerrar" >
        </asp:ModalPopupExtender>
        <asp:Button ID="cmd_alerta" runat="server" Style="display: none;" />
        <asp:Panel ID="pnl_alerta" runat="server" Style="display: none;">
            <asp:Panel ID="pnl_alertaEstilo" runat="server" CssClass="panel panel-danger">
                <div class="panel-heading text-center">
                    <asp:Label ID="lbl_alerta_titulo" runat="server" Text="Se produjo el siguiente error:"></asp:Label>
                </div>
                <div class="panel-body">
                    <asp:Panel ID="pnl_mensaje" runat="server" class="alert alert-danger" role="alert"
                        CssClass="text-center">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <asp:Label ID="lbl_alertaMensaje" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <div class="panel-footer">
                    <div class="centrado-horizontal">
                        <asp:Button 
                            ID="cmd_alertaAceptar" 
                            runat="server" 
                            CssClass="btn btn-success"
                            data-loading-text="Cargando..." 
                            Text="ACEPTAR" />
                        <asp:Button 
                            ID="cmd_alertaCerrar" 
                            runat="server" 
                            CssClass="btn btn-warning"
                            Text="CERRAR" />
                    </div>
                </div>
            </asp:Panel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
