﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ModalMovimientos.ascx.vb" Inherits="Anatomia_Patologica.ModalMovimientos" %>
<script type="text/javascript">
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblMovimientos').DataTable();
                __doPostBack("<%= btnExportarMovimiento.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
</script>
<div class="modal fade" id="mdlMovimientos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Historial de movimientos</h4>
                <button id="btnExportar" class="btn btn-success">Exportar a excel</button>
                <asp:Button runat="server" ID="btnExportarMovimiento" OnClick="btnExportarMovimiento_Click" Style="display: none;" />
            </div>
            <div class="modal-body">
                <table id="tblMovimientos" class="table table-bordered table-hover table-responsive" style="width: 100%;"></table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>

<script src="<%= ResolveClientUrl("~/js/ModalMovimientos.js") %>"></script>
