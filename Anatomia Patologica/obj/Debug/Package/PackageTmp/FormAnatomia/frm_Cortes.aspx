﻿<%@ Page Title="Cortes Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" MaintainScrollPositionOnPostback="true" CodeBehind="frm_Cortes.aspx.vb" Inherits="Anatomia_Patologica.frm_Cortes" %>

<%--<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>--%>
<%--<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>--%>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAnatomia/frm_cortes.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAnatomia/frm_cortes.botones.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAnatomia/GrillasFormAnatomia.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Gestión de cortes por biopsia</h2>
            <div style="text-align: right;">
                <button id="btnVerMovimientos" title="Ver Movimientos" class="btn btn-info">Ver Movimientos</button>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <label>Id. de biopsia:</label>
                    <input type="text" id="txtIdBiopsia" title="Id Biopsia" readonly="readonly" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Nº de biopsia:</label>
                    <input id="txtBiopsiaN" type="text" title="Número de Biopsia" disabled class="form-control" />
                </div>
                <div class="col-md-3">
                    <label>Órgano:</label>
                    <input id="txtOrgano" type="text" disabled  title="Organo" class="form-control" />
                </div>
                <div class="col-md-3">
                    <label>Estado:</label>
                    <input id="txtEstado" type="text" title="Estado" disabled class="form-control" />
                </div>
                <div class="col-md-1">
                    <label>Almacenado:</label>
                    <input id="txtAlmacenado" type="text" title="Almacenado" disabled class="form-control" />
                </div>
                <div class="col-md-1">
                    <br />
                    <button id="btnSolicitarMacro"  title="Solicitar Macroscopia"
                        style="color: white; display: none;" class="btn btn-warning">
                        <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-2">
                    <label>Patólogo:</label>
                    <select id="selPatologo" disabled class="form-control"></select>
                </div>
                <div class="col-md-2">
                    <label>Tecnólogo:</label>
                    <select id="selTecnologo" disabled class="form-control"></select>
                </div>
                <div class="col-md-2">
                    <br />
                    <label class="containerCheck">
                        Dictado
                        <input id="chkDictado" type="checkbox" disabled />
                        <span class="checkmark"></span>
                    </label>
                    <label class="containerCheck">
                        Tumoral
                        <input id="chkTumoral" type="checkbox" disabled />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="col-md-2">
                    <label>Tipo de Muestra:</label>
                    <select id="selTipoMuestra" disabled class="form-control"></select>
                </div>
                <div class="col-md-2">
                    <label>Corte:</label>
                    <select id="selTipoMacroscopia" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <br />
                    <button id="btnGuardarMacro" class="btn btn-success" disabled style="display:none;">Guardar macro</button>
                </div>
            </div>
            <hr />
            <div id="divbtnProcesar" style="display: none;" class="row">
                <div class="col-md-2">
                    <label>Secuencia cortes:</label>
                    <div class="form-inline">
                        <select id="selCantCortes" disabled class="form-control"></select>
                        <button id="btnSecuenciaCortes" class="btn btn-success" title="Secuencia de cortes"
                            style="color: white;" disabled><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Rótulo:</label>
                    <div class="form-inline">
                        <input id="txtSufijoCorte" maxlength="5" class="form-control" placeholder="Ingrese rótulo" disabled style="width: 70%;">
                        <button id="btnRotuloCorte" style="color: white;"  title="Rotulo de Corte"
                            class="btn btn-success" disabled><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Láminas:</label>
                    <div class="form-inline">
                        <select id="selCantLaminas" disabled class="form-control"></select>
                        <button id="btnSecuenciaLaminas" class="btn btn-success" title="Secuencia de laminas"
                            style="color: white;" disabled><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Rótulo láminas:</label>
                    <div class="form-inline">
                        <input id="txtSufijoLaminas" disabled maxlength="5" class="form-control" placeholder="Ingrese rótulo" style="width: 70%;" />
                        <button id="btnRotuloLamina" style="color: white;" title="Rotulo de lamina"
                            class="btn btn-success" disabled><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </div>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Listado de casete</h2>
        </div>
        <div class="panel-body">
            <table id="tblListadoCasete" class="table table-bordered  table-hover table-responsive"></table>

            <div class="clearfix"></div>
            <div class="text-center" id="divbtnCortes"  style="display:none;" >
                <div class="btn-group" role="group">
                    <button id="btnEnviarProcesador" disabled class="btn btn-success">Listo para procesador</button>
                </div>
                <div class="btn-group" role="group">
                    <button id="btnDescal" disabled class="btn btn-warning">Enviar a descalcificar</button>
                    <button id="btnVolverInicio" disabled class="btn btn-warning">Volver a encasetada</button>
                </div>
                <div class="btn-group" role="group">
                    <button id="btnEliminarCortes" disabled class="btn btn-danger">Eliminar selección</button>
                </div>
            </div>

            <hr />
            <div class="panel panel-success">
                <div class="row" id="divAgregar" style="padding: 8px;display:none;">
                    <div id="divTecnicas" class="col-md-6">
                        <div class="row h-100 align-items-end">
                            <div class="col-md-4">
                                <label>Técnica:</label>
                                <select id="selTecnicasEspeciales" class="form-control"></select>
                            </div>
                            <div class="col-md-4" style="margin-top: 25px;">
                                <button id="btnAgregarEspeciales" class="btn btn-success">Agregar técnica especial</button>
                            </div>
                        </div>
                    </div>
                    <div id="divInmuno" class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Inmuno Histoquímica:</label>
                                <select id="selInmunoHistoquimica" class="form-control"></select>

                            </div>
                            <div class="col-md-4" style="margin-top: 25px;">
                                <button id="btnAgregarInmuno" class="btn btn-success">Agregar inmunohistoquímica</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabTecnicasEspeciales" data-toggle="tab">
                                    <label style="color: black">Listado de técnicas especiales&nbsp;<span id="bdgListadoTecnicas" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabInmunoHistoquimica" data-toggle="tab">
                                    <label style="color: black">Listado de inmunohistoquímica&nbsp;<span id="bdgListadoInmuno" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabLaminas" data-toggle="tab">
                                    <label style="color: black">Listado de láminas&nbsp;<span id="bdgLaminas" class="badge">0</span></label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="divTabTecnicasEspeciales">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 class="text-center">Listado de técnicas especiales</h2>
                                    </div>
                                    <div class="panel-body">
                                        <table id="tblListadoTecEspeciales" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabInmunoHistoquimica">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 class="text-center">Listado de inmunohistoquímica</h2>
                                    </div>
                                    <div class="panel-body">
                                        <table id="tblListadoInmuno" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabLaminas">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 class="text-center">Listado de láminas</h2>
                                    </div>
                                    <div class="panel-body">
                                        <table id="tblLaminas" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabMacroscopia" data-toggle="tab">
                                    <label style="color: black">Detalle macroscopía</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabRecepcion" data-toggle="tab">
                                    <label style="color: black">Detalle de muestra&nbsp;<span id="bdgDetalleMuestra" class="badge">0</span></label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="divTabMacroscopia">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Plantilla:</label>
                                        <select id="selNomPlantilla" class="form-control" disabled></select>
                                        <button id="btnPlantillaAc" class="btn btn-success" style="display: none; margin-top: 5px;">Cargar plantilla</button>
                                    </div>
                                    <div class="col-md-9">
                                        <label>Descripción macroscópica:</label>
                                        <textarea id="txtDescMacro" rows="10" class="form-control" placeholder="Describa la muestra" maxlength="3000" style="resize: none;" disabled></textarea>
                                    </div>
                                </div>
                                <div class="row pull-right" style="margin: 8px;">
                                    <button id="btnGuardarDetalleMacro" type="button" class="btn btn-primary" style="display: none;">Guardar detalle macroscop</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabRecepcion">
                                <table id="tblMuestrasBiopsias" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalLaminas" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mensaje</h4>
                </div>
                <div class="modal-body">
                    <div id="divMensaje" style="display: none;">
                        <p>
                            <label id="lblModalMensaje"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalC"></label>
                            </b>
                        </p>
                    </div>
                    <hr id="hrSeparador" style="display: none;" />
                    <div id="divMensajeExtra" style="display: none;">
                        <p>
                            <label id="lblModalMensajeE"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalCE"></label>
                            </b>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlConfirmacion" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        &nbsp
                        <label id="lblConfirmacion">¿Esta seguro (a) de guardar esta macroscopia?</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="btnConfirmarModal" class="btn btn-danger" onclick="btnConfirmarModal();">Confirmar</a>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />
</asp:Content>
