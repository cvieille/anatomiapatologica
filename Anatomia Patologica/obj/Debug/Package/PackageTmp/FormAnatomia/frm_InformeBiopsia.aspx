<%@ Page Title="Informe de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master"
    MaintainScrollPositionOnPostback="true" CodeBehind="frm_InformeBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_InformeBiopsia" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAnatomia/GrillasFormAnatomia.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormAnatomia/frm_InformeBiopsia.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/uploader.js") %>'}?${new Date().getTime()}` });
    </script>
    <script src="<%= ResolveClientUrl("~/js/CryptoJS/md5.js") %>"></script>


    <script type="text/html" id="files-template">
        <li class="liElemento">
            <div class="media-body mb-1">
                <div class="mb-2" style="text-align: left; margin-left: 20px;">
                    <div class="pull-left nombreArchivo" style="margin-left: 5px;">
                        <label id="date-file" style="display: none; font-style: italic;">9999-99-99 - </label>
                        <strong>%%filename%%</strong><label id="progress-status" style="display: none;"> - Estado: <span class="text-muted">Esperando</span></label>
                    </div>
                    <button type="button" class="btnQuitar btn btn-danger btn-sm pull-right">
                        <span class="glyphicon glyphicon-remove"></span>Quitar
                    </button>
                </div>
                <div id="progress-bar-entire" style="display: none;">
                    <div class="progress mb-2">
                        <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                            role="progressbar"
                            style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
                <hr class="mt-1 mb-1" />
            </div>
        </li>
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de biopsia</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <label>N�</label>
                    <input type="text" id="txtNumero" class="form-control" disabled />
                </div>
                <div class="col-md-2">
                    <label>Fecha recepci�n</label>
                    <input type="text" id="txtRecepcion" class="form-control" disabled />
                </div>
                <div class="col-md-3">
                    <label>Solicitado por:</label>
                    <input type="text" id="txtSolicitado" class="form-control" disabled />
                </div>
                <div class="col-md-2">
                    <label>N� ID:</label>
                    <input type="text" id="txtIdBiopsia" class="form-control" disabled />
                </div>
                <div class="col-md-3 row" style="padding-top: 35px;">
                    <button id="btnVerMovimientos" class="btn btn-info">Ver movimientos</button>
                    <button id="btnVerCasos" class="btn btn-primary">Ver casos paciente</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>N� ubicaci�n interna</label>
                    <input type="text" id="txtFicha" class="form-control" disabled />
                </div>
                <div class="col-md-2">
                    <label>Rut paciente:</label>
                    <input type="text" id="txtRut" class="form-control" disabled />
                </div>
                <div class="col-md-4">
                    <label>Paciente:</label>
                    <input type="text" id="txtNombre" class="form-control" disabled />
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-5" style="padding-top: 0px;">
                            <label>F. nacimiento:</label>
                            <input type="text" id="txtFechaNac" class="form-control" disabled />
                        </div>
                        <div class="col-md-7" style="padding-top: 0px;">
                            <label>Edad:</label>
                            <input type="text" id="txtEdad" class="form-control" disabled />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label>Servicio origen:</label>
                    <input type="text" id="txtServicioOrigen" class="form-control" disabled />
                </div>
                <div class="col-md-3">
                    <label>Servicio destino:</label>
                    <input type="text" id="txtServicioDestino" class="form-control" disabled />
                </div>
                <div class="col-md-6">
                    <label>�rgano:</label>
                    <input type="text" id="txtOrgano" class="form-control" disabled />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>Fecha informe:</label>
                    <input type="text" id="txtFechaInforme" class="form-control" disabled />
                </div>
                <div class="col-md-4">
                    <label>Validado por:</label>
                    <input type="text" id="txtValidadoPor" class="form-control" disabled />
                </div>
                <div class="col-md-4">
                    <label>Estado:</label>
                    <input type="text" id="txtEstado" class="form-control" disabled />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Detalle informe</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabAnteClinicos" data-toggle="tab">
                                    <label style="color: black">Antecedentes cl�nicos</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabDescMacro" data-toggle="tab">
                                    <label style="color: black">Descripci�n macrosc�pica</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabExamenMicro" data-toggle="tab">
                                    <label style="color: black">Ex�men microsc�pico</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabDiag" data-toggle="tab">
                                    <label style="color: black">Diagn�stico</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabInmuno" data-toggle="tab">
                                    <label style="color: black">Estudio inmunohistoqu�mico</label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabNotaAdicional" data-toggle="tab">
                                    <label style="color: black">Nota adicional(opcional)</label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="divPlantillas" style="display: none;">
                                <div class="col-md-6">
                                    <label>Plantilla:</label>
                                    <select id="selNomPlantilla" class="form-control" disabled></select>
                                </div>
                                <div class="col-md-3">
                                    <label>Filtrar plantillas:</label>
                                    <div style="display: grid;">
                                        <label class="containerCheck">
                                            Con macroscop�a
                                        <input id="chkMacro" type="checkbox" disabled>
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="containerCheck">
                                            Con microscop�a
                                        <input id="chkMicro" type="checkbox" disabled>
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="containerCheck">
                                            Con diagn�stico
                                        <input id="chkDiag" type="checkbox" disabled>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade in active" id="divTabAnteClinicos">
                                <div class="col-md-9">
                                    <label>Antecedentes cl�nicos:</label>
                                    <textarea id="txtAntecedentesClinicos" maxlength="3000" rows="10" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaAnt" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabDescMacro">
                                <div class="col-md-9">
                                    <label>Descripci�n macrosc�pica:</label>
                                    <textarea id="txtDescMacro" rows="10" maxlength="3000" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaDescMacro" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabExamenMicro">
                                <div class="col-md-9">
                                    <label>Ex�men microsc�pico:</label>
                                    <textarea id="txtExamenMicro" rows="10" maxlength="3000" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaExamenMicro" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabDiag">
                                <div class="col-md-9">
                                    <label>Diagn�stico:</label>
                                    <textarea id="txtDiag" rows="10" maxlength="3000" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaDiag" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabInmuno">
                                <div class="col-md-9">
                                    <label>Estudio inmunohistoqu�mico:</label>
                                    <textarea id="txtInmuno" rows="10" maxlength="3000" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaInmuno" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabNotaAdicional">
                                <div class="col-md-9">
                                    <label>Nota adicional(opcional):</label>
                                    <textarea id="txtNotaAdicional" maxlength="3000" rows="10" class="form-control" disabled></textarea>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <button id="btnPlantillaNotaAdicional" class="btn btn-success" disabled style="display: none;">Cargar plantilla</button>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button id="btnGuardar" class="btn btn-success" disabled style="display: none;">Guardar borrador</button>
                                <button id="btnConvertirP" class="btn btn-primary">Convertir en plantilla</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Resumen de biopsia</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover table-responsive">
                        <tr>
                            <td>
                                <label>Total 08-01-001</label>
                                <input id="txt0801001" class="form-control" type="number" title="Citodiagn�stico corriente (PAP)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-002</label>
                                <input id="txt0801002" class="form-control" type="number" title="Citolog�a aspirativa (por punci�n)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-003</label>
                                <input id="txt0801003" class="form-control" type="number" title="Estudio histopatol�gico con microscop�a electr�nica (por cada �rgano)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-004</label>
                                <input id="txt0801004" class="form-control" type="number" title="Estudio histopatol�gico c/t�cnicas de inmunohistoqu�mica o inmunofluorescencia (por cada �rgano)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-005</label>
                                <input id="txt0801005" class="form-control" type="number" title="Estudio histopatol�gico con t�cnicas histoqu�micas especiales (incluye descalcificaci�n) (por cada �rgano)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-006</label>
                                <input id="txt0801006" class="form-control" type="number" title="Estudio histopatol. de biopsia contempor�nea (r�p.) a intervenc. quir�rgicas (X cada �rgano, no incl. biopsia diferida)" max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-007</label>
                                <input id="txt0801007" class="form-control" type="number" title="Estudio histopatol�gico con tinci�n corriente de biopsia 
                                    diferida con estudio seriado (m�nimo 10 muestras) de un �rgano o parte de �l
                                    (no incluye estudio con t�cnica habitual de otros �rganos incluidos en la muestra)"
                                    max="999" value="0" disabled />
                            </td>
                            <td>
                                <label>Total 08-01-008</label>
                                <input id="txt0801008" class="form-control" type="number" title="Estudio histopatol�gico corriente de biopsia diferida (por cada �rgano)" max="999" value="0" disabled />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <div id="divGuardarCodificacion">
                                    <button id="btnGuardarCodificacion" class="btn btn-primary">Guardar</button>
                                    <button id="btnRecalcularCodificacion" class="btn btn-success">Calcular</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#divTabListadoCasete" data-toggle="tab">
                                <label style="color: black">Listado de casete&nbsp;<span id="bdgListadoCasete" class="badge">0</span></label>
                            </a>
                        </li>
                        <li>
                            <a href="#divTabListadoTecEspeciales" data-toggle="tab">
                                <label style="color: black">
                                    Listado de t�cnicas especiales&nbsp;                                    
                                    <span id="bdgListadoTecnicasCreadas" class="badge">0</span> /
                                    <span id="bdgListadoTecnicas" class="badge">0</span>
                                </label>
                            </a>
                        </li>
                        <li>
                            <a href="#divTabListadoInmuno" data-toggle="tab">
                                <label style="color: black">
                                    Listado de inmunohistoqu�mica&nbsp;
                                    <span id="bdgListadoInmunoCreadas" class="badge">0</span> /
                                    <span id="bdgListadoInmuno" class="badge">0</span>

                                </label>
                            </a>
                        </li>
                        <li>
                            <a href="#divTabListadoLaminas" data-toggle="tab">
                                <label style="color: black">Listado de l�minas&nbsp;<span id="bdgLaminas" class="badge">0</span></label>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="divTabListadoCasete">
                            <div style="text-align: right;">
                                <a id="btnSolicitarMacro" title="Solicitar Macroscopia" style="color: white;" class="btn btn-warning" disabled>
                                    <span class="glyphicon glyphicon-repeat" aria-hidden="true">Solicitar macro</span>
                                </a>
                            </div>

                            <table id="tblListadoCasete" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>

                            <hr />
                            <div class="text-center _nada" style="display: none;">
                                <div class="btn-group" role="group">

                                    <button id="btnSolicitaMacro" class="btn btn-success">Solicitar macro</button>
                                    <button id="btnSolicitaCasete" class="btn btn-success">Solicitar casete</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button id="btnImprimirSeleccion" class="btn btn-primary">Imprimir selecci�n</button>
                                    <button id="btnEnviarAlmacenamiento" class="btn btn-primary">Enviar a almacenamiento</button>
                                </div>

                            </div>
                            <hr />
                            <div class="row" id="divAgregar">
                                <div class="row">
                                    <div id="divTecnicas" class="col-md-6">
                                        <div class="col-md-4">
                                            <label>T�cnica:</label>
                                            <select id="selTecnicasEspeciales" class="form-control"></select>
                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <button id="btnAgregarEspeciales" type="button" disabled class="btn btn-success">Agregar t�cnica especial</button>
                                        </div>
                                    </div>
                                    <div id="divInmuno" class="col-md-6">
                                        <div class="col-md-4">
                                            <label>Inmuno Histoqu�mica:</label>
                                            <select id="selInmunoHistoquimica" class="form-control"></select>

                                        </div>
                                        <div class="col-md-4">
                                            <br />
                                            <button id="btnAgregarInmuno" type="button" disabled class="btn btn-success">Agregar inmunohistoqu�mica</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="divTabListadoTecEspeciales">

                            <table id="tblListadoTecEspeciales" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>

                        </div>
                        <div class="tab-pane fade" id="divTabListadoInmuno">
                            <table id="tblListadoInmuno" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                        </div>
                        <div class="tab-pane fade" id="divTabListadoLaminas">
                            <table id="tblLaminas" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                            <div class="text-center">
                                <button id="btnEnviarAlmacenamientoLamina" class="btn btn-primary" style="display: none;">Enviar a almacenamiento</button>
                                <%--<button id="btnSolicitarLamina" class="btn btn-success" disabled>Solicitar l�mina</button>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#divTabCritico" data-toggle="tab">
                                <label style="color: black">Cr�tico&nbsp;<span id="bdgCritico" class="badge">0</span></label>
                            </a>
                        </li>
                        <li>
                            <a href="#divTabNeoplasia" data-toggle="tab">
                                <label style="color: black">Neoplasia</label>
                            </a>
                        </li>
                        <li id="liAdjuntos">
                            <a href="#divTabAdjuntos" data-toggle="tab">
                                <label style="color: black">Adjuntos&nbsp;<span id="bdgAdjuntos" class="badge">0</span></label>
                            </a>
                        </li>
                        <li>
                            <a href="#divTabProtocolos" data-toggle="tab">
                                <label style="color: black">Protocolos&nbsp;<span id="bdgProtocolos" class="badge">0</span></label>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="divTabCritico">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="text-center">Detalle cr�tico</h2>
                                </div>
                                <div class="panel-body">
                                    <div id="divNotificacionCritico">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Ubicacion a notificar</label>
                                                <%--Borrar opciones cuando este listo el endpoint--%>
                                                <select id="selUbicacionNoti"  class="form-control" onchange="comboNotificado(event.target.value)">
                                                    <option>--Seleccione--</option>
                                                </select>

                                                <label>Informado a:</label>
                                                <select id="selUsuarioNotificado"  class="form-control">
                                                    <option>--Seleccione--</option>
                                                </select>
                                                
                                            </div>
                                            <div class="col-md-9">
                                                <label>Observaci�n:</label>
                                                <textarea id="txtObservacion"  rows="4" maxlength="500" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="row pull-right" style="margin-right: 0px;">
                                            <button id="btnNotificarUsuario"  class="btn btn-warning">Notificar a usuario</button>
                                        </div>
                                    </div>
                                    <table id="tblNotificaciones" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="divTabNeoplasia">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="text-center">Detalle de neoplasia</h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Comportamiento:</label>
                                            <select id="selComportamiento" class="form-control" disabled></select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Morfolog�a:</label>
                                            <select id="selMorfologia" class="form-control" disabled></select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Grado de diferenciaci�n:</label>
                                            <select id="selGradoDif" class="form-control" disabled>
                                                <option value="0">-Seleccione-</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>�rgano:</label>
                                            <select id="selOrgano" class="form-control" disabled>
                                                <option value="0">-Seleccione-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Topograf�a:</label>
                                            <select id="selTopografia" class="form-control" disabled>
                                                <option value="0">-Seleccione-</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Lateralidad:</label>
                                            <select id="selLateralidad" class="form-control" disabled></select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Her2:</label>
                                            <select id="selHer2" class="form-control" disabled></select>
                                        </div>
                                    </div>
                                    <div class="row pull-right" style="margin-right: 0px;">
                                        <div class="col-md-6">
                                            <button id="btnGuardarNeoplasia" type="button" disabled class="btn btn-primary">Guardar neoplasia</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="divTabAdjuntos">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="text-center">Documentos adjuntos:</h2>
                                </div>
                                <div class="panel-body">
                                    <div style="margin: 0 auto; text-align: center;">
                                        <%--<h3 style="width: 100%">Documentos Adjuntos:</h3>--%>
                                        <div class="rowb4">
                                            <div id="adjuntosServerArrastrar" class="col-md-6 col-sm-12" runat="server">
                                                <div id="drag-and-drop-zone" class="dm-uploader p-5" style="height: 300px;">
                                                    <h3 class="mb-5 mt-5 text-muted">Arrastrar documentos ac�</h3>
                                                    <div class="btn btn-primary btn-block mb-5">
                                                        <span>Abrir explorador de archivos</span>
                                                        <input type="file" title='Click para seleccionar archivos' />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adjuntosServerEnviados" class="col-md-6 col-sm-12" runat="server">
                                                <div class="card h-100" style="max-height: 300px;">
                                                    <div class="card-header">
                                                        Documentos enviados
                                                    </div>
                                                    <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                                        <li class="text-muted text-center empty">No hay documentos cargados.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divError" class="rowb4" style="display: none;">
                                            <div class="col-12">
                                                <div class="card h-100">
                                                    <div class="card-header">
                                                        Mensaje
                                                    </div>
                                                    <ul class="list-group list-group-flush" id="debug">
                                                        <li class="list-group-item text-muted empty"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="divTabProtocolos">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="text-center">Protocolos del paciente</h2>
                                </div>
                                <div class="panel-body">
                                    <table id="tblProtocolo" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button id="btnValidarAutorizar" class="btn btn-danger" disabled>Validar y autorizar</button>
                    <button id="btnImprimir" type="button" class="btn btn-info">Imprimir</button>
                    <button id="btnCerrarInterconsulta" class="btn btn-warning" disabled>Cerrar interconsulta</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlCasosPaciente">
        <div class="modal-dialog modal-lg" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Casos anteriores del paciente</h4>
                </div>
                <div class="modal-body">
                    <table id="tblCasosPaciente" class="table table-bordered table-hover table-responsive" style="width: 100%;"></table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlConfimarCerrar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cerrar interconsulta</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning text-center">
                        �Realmente desea cerrar la interconsulta?
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnConfirmarCerrar" class="btn btn-warning">Cerrar interconsulta</button>
                    <button class="btn btn-info" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlQuitar" role="dialog" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-info">
                <div class="panel-heading">Confirmaci�n</div>
                <div class="panel-body">
                    <br />
                    <div class="alert alert-warning" role="alert">�Est� seguro que desea quitar el documento?</div>
                </div>
                <div class="panel-footer clearfix">
                    <div class="pull-right">
                        <button id="btnQuitarConfirmar" class="btn btn-danger">QUITAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlMensaje" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mensaje</h4>
                </div>
                <div class="modal-body">
                    <div id="divMensaje" style="display: none;">
                        <p>
                            <label id="lblModalMensaje"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalC"></label>
                            </b>
                        </p>
                    </div>
                    <hr id="hrSeparador" style="display: none;" />
                    <div id="divMensajeExtra" style="display: none;">
                        <p>
                            <label id="lblModalMensajeE"></label>
                        </p>
                        <p>
                            No se modificaron los siguientes registros: <b>
                                <label id="lblModalCE"></label>
                            </b>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlPlantilla" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Crear plantilla</h4>
                </div>
                <div class="modal-body">
                    <label>Nombre de la plantilla:</label>
                    <input type="text" id="txtNombrePlantilla" class="form-control" maxlength="100" />
                </div>
                <div class="modal-footer">
                    <button id="btnGuardarP" class="btn btn-success">Guardar plantilla</button>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlValidar" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Publicaci�n</h4>
                </div>
                <div class="modal-body">
                    <div id="alertValidar" class="alert alert-danger text-center" style="margin-bottom: 10px;">
                        Al validar este informe estar� enviando las l�minas a almacenamiento en forma autom�tica
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Nombre de usuario:</label>
                            <input type="text" id="txtNombreUsuario" maxlength="8" class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <label>Contrase�a:</label>
                            <input type="password" id="txtClave" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnPublicacion" class="btn btn-success">Aceptar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdlConfirmacion" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmaci�n</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        &nbsp
                        <label id="lblConfirmacion"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="btnConfirmarModal" class="btn btn-danger" onclick="btnConfirmarModal();">Confirmar</a>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />




    <link href="../CSS/jquery.dm-uploader.css" rel="stylesheet" />
    <script src="../JS/jquery.dm-uploader.js"></script>

    <style>
        .nombreArchivo:hover {
            transition: transform .2s;
            background-color: #f1f1f1;
            cursor: pointer;
            transform: translateX(3px);
        }
    </style>

    <script type="text/html" id="debug-template">
        <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>

    <script src="../JS/uploader.js"></script>
</asp:Content>
