﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm_ImprimeInforme.aspx.vb" Inherits="Anatomia_Patologica.frm_impr_informe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Informe de Biopsia</title>
    <%--<link rel="Stylesheet" href="seriousface/style.css" />--%>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <table style="background: #FFFFCC">
                    <tr>
                        <td>
                            <table style="width: 100%" class="TablaPrincipal">
                                <tr>
                                    <td>Nº</td>
                                    <td>
                                        <asp:TextBox ID="txt_numero" runat="server" Width="150px" ReadOnly="True" Enabled="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_recepcion" runat="server" Height="24px" Visible="False" Width="50px"></asp:TextBox>
                                        <asp:TextBox ID="txt_ficha" runat="server" MaxLength="10" TabIndex="2" Width="50px" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_edad" runat="server" Enabled="false" Visible="False" Width="50px"></asp:TextBox><asp:TextBox ID="txt_fnac" runat="server" Enabled="false" Visible="False" Width="50px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rut Paciente:</td>
                                    <td>
                                        <asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" Enabled="False" MaxLength="10" TabIndex="1" Width="150px"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Nombre:</td>
                                    <td>
                                        <asp:TextBox ID="txt_nombre" runat="server" Enabled="False" MaxLength="100" Width="200px"></asp:TextBox>
                                        <asp:TextBox ID="txt_ap_paterno" runat="server" Enabled="False" MaxLength="100" Width="290px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Servicio Origen:</td>
                                    <td>
                                        <asp:TextBox ID="txt_servicio" runat="server" Width="489px" ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Servicio Destino:</td>
                                    <td>
                                        <asp:TextBox ID="txt_servicio0" runat="server" Enabled="False" ReadOnly="True"
                                            Width="489px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Organo:</td>
                                    <td>
                                        <asp:TextBox ID="txt_organo" runat="server" Enabled="False" ReadOnly="True"
                                            Width="489px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Solicitado Por:</td>
                                    <td>
                                        <asp:TextBox ID="txt_solicitado_por" runat="server" Width="489px" ReadOnly="True" Enabled="False"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Validado Por:</td>
                                    <td>
                                        <asp:TextBox ID="txt_quien_valida" runat="server" Enabled="False" Height="24px" Width="370px"></asp:TextBox>
                                        <asp:TextBox ID="txt_finf" runat="server" Enabled="false" Height="24px" Width="114px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>Antecedentes Clinicos:</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_antecedentes_clinicos" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <th>Descripción Macroscopica:</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_desc_macro" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <th>Examen Microscópico:</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_desc_micro" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <th>Diagnostico:</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_desc_diag" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <th>Estudio Inmunohistoquímico:</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_estu_inmuno" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <th>Nota Adicional (opcional)</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_nota" runat="server" Height="120px" TextMode="MultiLine" Width="717px" Enabled="False"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_id" runat="server" Width="51px" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txt_id_descripcion" runat="server" Width="44px" Visible="False"></asp:TextBox></td>
                    </tr>
                </table>
            </asp:Panel>

            <br />


            <br />

        </div>
    </form>
</body>
</html>
