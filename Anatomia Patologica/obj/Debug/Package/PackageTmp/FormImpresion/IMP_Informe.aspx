﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IMP_Informe.aspx.vb"
    Inherits="Anatomia_Patologica.IMP_Informe" MasterPageFile="~/Master/IMP.master" %>

<asp:Content ContentPlaceHolderID="_HeadContent" runat="server">
    <link rel="stylesheet" href="<%= ResolveClientUrl("~/css/IMP_INFORME.css") %>" />

</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="_ContentPlaceHolder" runat="server">
 
            <article style="margin-left: 20px; margin-right: 20px;">
                <div class="row">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 33%;">
                                <img src="../imagenes/logo.jpg" style="width: 180px; height: 80px;" /></td>
                            <td style="width: 35%;">
                                <h5><b>INFORME DE BIOPSIA N°<asp:Label runat="server" ID="lblNumBiopsia"></asp:Label></b></h5>
                            </td>
                            <td>
                                <table>                                    
                                    <tr>
                                        <td>Recepción:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblFechaRecepcion"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Fec. Informe:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblFechaInforme"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div class="row">
                    <table style="width: 100%;">
                        <tr>
                            <td class="fondo-titulos contenido-celda">Nombre Paciente:</td>
                            <td class="contenido-celda">
                                <asp:Label runat="server" ID="lblNombre"></asp:Label></td>
                            <td class="fondo-titulos contenido-celda">Apellidos:</td>
                            <td class="contenido-celda">
                                <asp:Label runat="server" ID="lblApellidos"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="fondo-titulos contenido-celda">Rut:</td>
                            <td class="contenido-celda">
                                <asp:Label runat="server" ID="lblRut"></asp:Label></td>
                            <td class="fondo-titulos contenido-celda">Nº Ubicación Interna:</td>
                            <td class="contenido-celda">
                                <asp:Label ID="lblUbInterna" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fondo-titulos contenido-celda">Edad:</td>
                            <td class="contenido-celda">
                                <asp:Label runat="server" ID="lblEdad"></asp:Label></td>
                            <td class="fondo-titulos contenido-celda">Fecha Nacimiento:</td>
                            <td class="contenido-celda">
                                <asp:Label ID="lblFechaNac" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fondo-titulos contenido-celda">Servicio Origen:</td>
                            <td class="contenido-celda">
                                <asp:Label ID="lblServOrigen" runat="server"></asp:Label>
                            </td>
                            <td class="fondo-titulos contenido-celda">Servicio Destino:</td>
                            <td class="contenido-celda">
                                <asp:Label ID="lblServDestino" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fondo-titulos contenido-celda">Derivado por:</td>
                            <td class="contenido-celda" colspan="3">
                                <asp:Label ID="lblDerivado" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="fondo-titulos contenido-celda">Muestra:</td>
                            <td class="contenido-celda" colspan="3">
                                <asp:Label ID="lblMuestra" runat="server"></asp:Label>
                            </td>
                        </tr>                        
                    </table>
                </div>

                <div class="row">
                    <div id="descripciones" class="contenido-celda">
                        <asp:Label runat="server" ID="lblAntecedentes"></asp:Label>
                        <asp:Label runat="server" ID="lblDescripcionMacroscopica"></asp:Label>
                        <asp:Label runat="server" ID="lblDescripcionMicroscopica"></asp:Label>
                        <asp:Label runat="server" ID="lblDiagnostico"></asp:Label>
                        <asp:Label runat="server" ID="lblNotas"></asp:Label>
                        <asp:Label runat="server" ID="lblInmunoHistoquimica"></asp:Label>
                    </div>
                </div>

                <div>
                    <table>
                        <tr style="height:60px;">
                            <td class="fondo-titulos contenido-celda" style="width:35%;" >
                                <asp:Label runat="server" ID="lblResponsable"></asp:Label></td>
                            <td class="contenido-celda" colspan="3" >
                                <asp:Label runat="server" ID="lblFirmaNombre"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fondo-titulos contenido-celda text-center">El presente Informe deberá contar con firma del Médico Patálogo Responsable y 
                    timbre de Anatomía Patológica para su validez</td>
                        </tr>
                    </table>
                    <label class="text-center" style="border-bottom: thick"></label>
                </div>
            </article>


            <footer>
                <div class="text-center">
                    <div>
                        Datos de Impresión: Nº de Biopsia:
                        <asp:Label runat="server" ID="lblNumBiopsia2"></asp:Label>
                        - Fecha:
                        <asp:Label runat="server" ID="lblFechaHoy"></asp:Label>
                        - Usuario:
                        <asp:Label runat="server" ID="lblUsuario"></asp:Label>

                    </div>
                </div>
            </footer>
       



</asp:Content>
