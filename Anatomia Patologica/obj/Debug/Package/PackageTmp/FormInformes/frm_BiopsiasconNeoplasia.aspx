<%@ Page Title="Biopsias con Neoplasia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasconNeoplasia.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasconNeoplasia" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <%--<script src="/js/FormInformes/frm_BiopsiasconNeoplasia.js"></script>--%>
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasconNeoplasia.js") %>'}?${new Date().getTime()}` });
    </script>
    <style>
        ._row{
            border-bottom: solid 1px #00000010;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Biopsias con neoplasia</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control"/>
                    </div>
                    <div class="col-md-2" style="margin-top: 4px;">
                        <br />
                        <button id="btnBuscar" type="button" class="btn btn-success" style="width:100%;">Buscar</button>
                    </div>
                </div>
            </div>
            <hr />
            <div>
                <table id="tblNeoplasia" class="table table-bordered table-hover table-responsive" style="font-size:small;"></table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de biopsia</h4>
                </div>
                <div class="modal-body">
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Fecha de recepci�n:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanFechaRecepcion"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Rut paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanRutPaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Nombre paciente:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanNombrePaciente"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>�rgano:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanOrgano"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Pat�logo:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanPatologo"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Tecn�logo:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanTecnologo"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>M�dico solicitante:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanMedSolicitante"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio origen:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanServOrigen"></span>
                        </div>
                    </div>
                    <div class="row _row">
                        <div class="col-md-6">
                            <strong>Servicio destino:</strong>
                        </div>
                        <div class="col-md-6">
                            <span id="spanServDestino"></span>
                        </div>
                    </div>
                    <hr />
                    <h4><strong>Documentos adjuntos:</strong></h4>
                    <table id="tblArchivo" class="table table-bordered table-hover table-responsive" style="width:100%; font-size:small;"></table>
                </div>
                <div class="modal-footer">
                    <button id="btnCodificacion" class="btn btn-info" style="display:none;">Ver codificaci�n</button>
                    <button id="btnInforme" class="btn btn-info" data-dismiss="modal">Ver informe</button>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
