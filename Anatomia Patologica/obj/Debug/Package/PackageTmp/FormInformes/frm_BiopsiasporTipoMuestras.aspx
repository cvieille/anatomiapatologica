﻿<%@ Page Title="Informe por Tipo de Muestra" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasporTipoMuestras.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasporTipoMuestras" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasporTipoMuestras.js") %>'}?${new Date().getTime()}` });
        
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Detalle").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Detalle").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Informe por Tipo de Muestra</h2>
                </div>
                <div class="panel-body">
                    <div class="col-md-2">
                        <label>Desde:</label>
                        <input type="date" class="form-control" id="fechaInicio" value="" />
                        <%--<asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required" CssClass="form-control"></asp:TextBox>--%>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" class="form-control" id="fechaTermino" value="" />
                        <%--<asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required" CssClass="form-control"> </asp:TextBox>--%>
                    </div>
                    <div class="col-md-1">
                        <br />
                        <label>Todos:</label>
                        <input type="checkbox" class="form-check-input"  id="chkTodos"/>
                        <%--<asp:CheckBox ID="CheckBoxTodos" runat="server" Checked="false" AutoPostBack="true" />--%>
                    </div>
                    <div class="col-md-2">
                        <label>Tipo de Muestra:</label>
                        <select class="form-control" id="sltTipoMuestra">
                        </select>
                        <%--<asp:DropDownList ID="cmb_catalogo" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>--%>
                    </div>
                    <div class="col-md-2">
                        <label>Detalle de Muestra:</label>
                        <select class="form-control" id="sltDetalleMuestra">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <br />
                        <div class="form-inline">
                            <button type="button" class="btn btn-success" id="btnBuscar">Buscar</button>
                           
                        </div>

                    </div>

                    <br />
                    <hr />
                    <br />
                    <table id="tableBiopsias"></table>
                   

                </div>
            </div>
        </ContentTemplate> 
        <Triggers>
            
        </Triggers>
    </asp:UpdatePanel>   
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
