﻿<%@ Page Title="Dashboard" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Dashboard.aspx.vb" Inherits="Anatomia_Patologica.frm_Dashboard" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_dashboard.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Dashboard</h2>
        </div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Biopsias por Estado</h2>
                </div>
                <div class="panel-body">
                    <table id="tblResumenBiopsias" class="table table-bordered table-hover table-responsive"></table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Cortes por Estado</h2>
                </div>
            </div>
            <table id="tblResumenCortes" class="table table-bordered table-hover table-responsive"></table>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Laminas por Estado</h2>
                </div>
                <table id="tblResumenLaminas" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>
    <div id="mdlDetalle" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding: 10px 15px !important;">
                    <h3 class="modal-title m-1">Detalle</h3>
                </div>
                <div class="modal-body">
                    <table id="tblDetalle" class="table table-bordered table-hover table-responsive dataTable no-footer" style="width:100%;">
                        <thead><tr><th></th></tr></thead><tbody><tr><td></td></tr></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
