﻿<%@ Page Language="vb" Title="Biopsias según pre tabla" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_biopsiasPretabla.aspx.vb" Inherits="Anatomia_Patologica.frm_biopsiasPretabla" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_biopsiasPretabla.js") %>'}?${new Date().getTime()}` });
    </script>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Biopsias según pretabla</h1>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 col-sm-1 col-xs-1"></div>
                    <div class="col-md-2 col-sm-6 col-xs-6">
                        <label>Fecha inicio</label>
                        <input type="date" id="txtDesde" class="form-control"/>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6">
                        <label>Fecha limite</label>
                        <input type="date" id="txtHasta" class="form-control"/>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <label>&nbsp;</label><br />
                        <button class="btn btn-success" onclick="consultarPretabla()" type="button">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-hover table-responsive" id="tblBiopsiasPretabla" style="width:100%;"></table>
            </div>
        </div>
    </div>
</asp:Content>