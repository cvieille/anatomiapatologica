﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Clave.aspx.vb" Inherits="Anatomia_Patologica.frm_Clave" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script src="<%= ResolveClientUrl("~/js/CryptoJS/md5.js") %>"></script>
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_Clave.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Cnt_Principal">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Mantenedor de contraseña</h2>
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger text-center" role="alert">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        <strong>
                            &nbsp;Por políticas de seguridad debe cambiar su constraseña por una más segura<br>
                            Mínimo 8 caracteres<br>
                            Máximo 16 caracteres<br>
                            Debe contener al menos una letra<br>
                            Debe contener al menos un número
                        </strong>
                    </div>
                    <hr />

                    <div class="panel panel-default" style="width:50%; margin:auto;">
                        <div class="panel-heading">
                            <div>
                                <h4>Cambio de contraseña</h4>
                            </div>
                        </div>
                        <div class="panel-body" style="margin: 0px 30px 0px 30px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input id="txtPassActual" placeholder="CONTRASEÑA ACTUAL" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input id="txtPassNuevo" placeholder="CONTRASEÑA NUEVA" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input id="txtPassNuevoConfirmar" placeholder="REPETIR CONTRASEÑA NUEVA" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <button class="btn btn-success" id="btnActualizar" style="width:100%;">ACTUALIZAR</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div style="width:60%; margin:auto;">
                        <div class="alert alert-info col-sm-12">
                            <strong>
                                Recuerde que el cambio de su contraseña afecta a su usuario en los módulos/sistemas:
                                <ul>
                                    <li>Protocolo operatorio</li>
                                    <li>Epicrisis</li>
                                    <li>Receta e indicaciones médicas</li>
                                    <li>Sistema de pabellones</li>
                                    <li>Anatomia patológica</li>
                                    <li>Exámenes de laboratorio</li>
                                    <li>Categorización</li>
                                </ul>
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>