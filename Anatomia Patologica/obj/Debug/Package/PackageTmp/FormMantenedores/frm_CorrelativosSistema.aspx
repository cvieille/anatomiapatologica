﻿<%@ Page Title="Cambiar Correlativo" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_CorrelativosSistema.aspx.vb" Inherits="Anatomia_Patologica.frm_CorrelativosSistema" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_CorrelativosSistema.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Correlativos</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <blockquote class="blockquote">
                        <b>Valores actuales</b>
                    </blockquote>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>N° de biopsia:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtNBiopsia" class="form-control" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>Año de biopsia:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtABiopsia" class="form-control" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>N° de solicitud:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtNSolNBiopsia" class="form-control" disabled />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>Año de solicitud:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtASolBiopsia" class="form-control" disabled />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <blockquote class="blockquote">
                        <b>Valores nuevos</b>
                    </blockquote>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>N° de biopsia:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtNBiopsia_" type="number" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>Año de biopsia:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtABiopsia_" type="number" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>N° de solicitud:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtNSolNBiopsia_" type="number" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <label>Año de solicitud:</label>
                        </div>
                        <div class="col-md-5">
                            <input id="txtASolBiopsia_" type="number" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-footer" style="text-align: right;">
            <button id="btnGuardar" type="button" class="btn btn-info">Guardar</button>
            <button id="btnCancelar" type="button" class="btn btn-warning">Cancelar</button>
        </div>
    </div>
</asp:Content>
