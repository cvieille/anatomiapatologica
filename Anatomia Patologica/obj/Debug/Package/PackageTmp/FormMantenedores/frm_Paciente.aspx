<%@ Page Title="Gesti�n de Pacientes" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Paciente.aspx.vb" Inherits="Anatomia_Patologica.frm_Paciente" Culture="Auto" Buffer="true" UICulture="Auto" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_Paciente.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Gesti�n de pacientes</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <label>Tipo de documento:</label>
                    <select id="selTipoDocumento" class="form-control">
                      
                    </select>
                </div>
                <div class="col-md-3">
                    <label>N� de documento:</label>
                    <div class="input-group">
                        <input id="txtRut" type="text" maxlength="8" class="form-control">
                        <span id="txtDigito" class="input-group-addon">-</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>N� de ub. interna:</label>
                    <input id="txtNui" maxlength="6" type="text" disabled class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label>Estado:</label>
                    <select id="selEstado" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label>Nombre:</label>
                    <input id="txtNombre" maxlength="50" disabled type="text" class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label>Apellidos:</label>
                    <input id="txtApellidoP" maxlength="50" disabled type="text" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <input id="txtApellidoM" maxlength="50" disabled type="text" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label>Direcci�n:</label>
                    <input id="txtDireccion" maxlength="100" disabled type="text" class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label>N�mero:</label>
                    <input id="txtNumero" maxlength="50" disabled type="text" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>N�mero de tel�fono</label>
                    <input id="txtTel" maxlength="50" disabled type="text" class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label>Otros n�meros</label>
                    <input id="txtOtrosNumeros" maxlength="50" disabled type="text" class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label>Sexo:</label>
                    <select id="selSexo" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Fecha de nacimiento:</label>
                    <input type="date" id="txtNacimiento" disabled required class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label>Edad:</label>
                    <input id="txtEdad" type="text" disabled class="form-control"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label>Pa�s:</label>
                    <select id="selPais" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Regi�n:</label>
                    <select id="selRegion" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Provincia:</label>
                    <select id="selProvincia" name="selProvincia" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Ciudad:</label>
                    <select id="selCiudad" disabled class="form-control">
                        <option value="0">-Seleccione-</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-2 col-md-offset-8">
                    <button id="btnGuardar" disabled class="btn btn-success"  style="width:100%">Guardar</button>
                    
                </div>
                <div class="col-md-2">
                    <button id="btnVolver" class="btn btn-warning" style="width:100%">Volver</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
