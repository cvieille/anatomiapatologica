﻿<%@ Page Title="Despacho de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_despachar_biopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_despachar_biopsia" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <table style="margin: 0 auto; border: 0;">
        <tr>
            <td>Nº de Biopsia:</td>
            <td>
                <table style="width: 40%; margin-left:0;">
                    <tr >
                        <td>
                            <asp:TextBox ID="txt_biopsia_n" runat="server" Width="51px" Enabled="False"> </asp:TextBox>
                        </td>
                        <td>/</td>
                        <td>
                            <asp:TextBox ID="txt_biopsia_a" runat="server" Width="51px" Enabled="False"> </asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="txt_id" runat="server" Width="51px" Enabled="False" Visible="False"> </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td>
                <asp:TextBox ID="txt_estado" runat="server" Width="177px" Enabled="False"></asp:TextBox>
                <asp:CheckBox ID="chk_dictado" runat="server" Text="Dictado" Enabled="False" />
                <asp:CheckBox ID="chk_tumoral" runat="server" Text="Tumoral" Enabled="False" />
                <asp:CheckBox ID="chk_rapida" runat="server" Text="Rapida" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>Organo:</td>
            <td>
                <asp:TextBox ID="txt_organo" runat="server" Width="438px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Patologo:</td>
            <td>
                <asp:TextBox ID="txt_patologo" runat="server" Width="438px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Servicio de<br />Despacho:</td>
            <td>
                <asp:DropDownList ID="cmb_tipo_destino" runat="server" AutoPostBack="True" Width="100px">
                </asp:DropDownList>
                <asp:DropDownList ID="cmb_servicio_dest" runat="server" 
                    DataSourceID="SqlDataSource4" DataTextField="nombre" DataValueField="id" 
                    Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_despachar1" runat="server" Text="Fecha de Entrega:">
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_fecha" runat="server" MaxLength="10" type="date" TabIndex="4" CssClass="CuadroFecha">
                </asp:TextBox>
                
            </td>
        </tr>
        <tr>
            <td>Entregar Informe a:</td>
            <td>
                <asp:TextBox ID="txt_recibe_informe" runat="server" Width="438px">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" CssClass= "BtnA-MGrande" />
                <asp:Button ID="cmd_guardar0" runat="server" Text="Cancelar" CssClass="BtnR-MGrande" />
            </td>
        </tr>
    </table>
    <br />
        
            
    <asp:Image ID="Image1" runat="server" Height="105px" ImageUrl="~/imagenes/3dman-stop.jpg" Visible="False" Width="132px" />
    <asp:Label ID="lbl_debe_guardar" runat="server" Font-Bold="True" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
    

</asp:Content>
