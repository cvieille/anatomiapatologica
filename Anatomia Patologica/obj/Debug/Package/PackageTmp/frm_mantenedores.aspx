﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_mantenedores.aspx.vb" Inherits="Anatomia_Patologica.WebForm11" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <br />
    <table class="DivFullCentrado">
        <tr>
            <td valign="top">
                <asp:Image ID="Image1" runat="server" Height="223px" ImageUrl="~/imagenes/rueda.png" Width="214px" />
            </td>
            <td>&nbsp;</td>
            <td>
                <table>
                    <tr>
                        <td style="width: 1106px" >
                            Mantención de Médicos:</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td style="width: 338px">
                            <asp:Button ID="cmd_medicos" runat="server" Text="Gestionar" CssClass="btn btn-default" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 1106px">
                            Mantención de Técnicas Especiales</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="cmd_tecnicas" runat="server" Text="Gestionar" CssClass="btn btn-default" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 1106px">
                            Mantenedor de Programas:</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="cmd_programas" runat="server" Text="Gestionar" CssClass="btn btn-default" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 1106px">
                            Mantenedor de Servicios:</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="cmd_programas0" runat="server" Text="Gestionar" CssClass="btn btn-default" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 1106px">
                            &nbsp;</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 1106px">
                            &nbsp;</td>
                        <td style="width: 70px">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
