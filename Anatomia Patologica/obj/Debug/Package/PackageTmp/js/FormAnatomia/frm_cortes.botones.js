﻿const botonesCorteSecretaria = () => {
    $("#divTecnicas").hide();
    $("#divInmuno").hide();
    $("#btnSolicitarLamina").hide();
    $("#btnGuardarMacro").hide();
    $("#btnSolicitarMacro").hide();
    $("#divbtnCortes").hide();
}

const botonesCortesAuxiliar = () => {
    $('#divTecnicas').hide();
    $('#divInmuno').hide();
    $('#btnGuardarMacro').hide();
}
function botonesCortesParamedico(idEstado) {
    if (idEstado == 42 || idEstado == 43) {
        //si esta en macroscopia o ingresado     
        permiteCrearlamina();
        $('#btnEliminarCortes').removeAttr('disabled');
        $('#btnGuardarDetalleMacro').show();
        $('#btnGuardarMacro').removeAttr('disabled');

        $('#btnRotuloCorte').removeAttr('disabled');
        $('#btnRotuloLamina').removeAttr('disabled');
        $('#btnSecuenciaCortes').removeAttr('disabled');
        $('#btnSecuenciaLaminas').removeAttr('disabled');
        $('#btnVolverInicio').removeAttr('disabled');
        $('#chkDictado').removeAttr('disabled');
        $('#chkTumoral').removeAttr('disabled');
        $('#lblEstadoMacro').html('Muestra en macroscopía');
        $('#selCantCortes').removeAttr('disabled');
        $('#selCantLaminas').removeAttr('disabled');
        $('#selNomPlantilla').removeAttr('disabled');
        $('#selPatologo').removeAttr('disabled');
        $('#selTipoMacroscopia').removeAttr('disabled');
        $('#selTipoMuestra').removeAttr('disabled');
        $('#txtDescMacro').removeAttr('disabled');
        $('#txtSufijoCorte').removeAttr('disabled');
        $('#txtSufijoLaminas').removeAttr('disabled');
        $('#btnGuardarMacro').show();

        $('#divTecnicas').show();
        $('#divInmuno').show();

        $('#btnPlantillaAc').show();

    }
    else {
        //¡La muestra no se encuentra en macroscopia! alert
        $('#lblEstadoMacro').html('Debe solicitar muestra');
    }
}
const botonesCortesPatologo = () => {
    $('#divTecnicas').hide();
    $('#divInmuno').hide();
}
const botonesCortesTecnologo = () => {
    permiteCrearlamina();

    $('#divTecnicas').hide();
    $('#divInmuno').hide();
    $('#selCantLaminas').removeAttr('disabled');
    $('#btnParaInterconsulta').removeAttr('disabled');
    $('#btnRotuloLamina').removeAttr('disabled');
    $('#btnSecuenciaLaminas').removeAttr('disabled');
    $('#btnSolicitarLamina').removeAttr('disabled');
    $('#txtSufijoLaminas').removeAttr('disabled');
    $('#divbtnProcesar').show();
}
const botonesCortesAdministrador = () => {
    $('#chkDictado').removeAttr('disabled');
    $('#selCantCortes').removeAttr('disabled');
    $('#btnSecuenciaCortes').removeAttr('disabled');
    $('#selPatologo').removeAttr('disabled');
    $('#selTecnologo').removeAttr('disabled');

    $('#selTipoMuestra').removeAttr('disabled');

    $('#selTipoMacroscopia').removeAttr('disabled');
    $('#btnDescal').removeAttr('disabled');
    $('#btnEliminarCortes').removeAttr('disabled');
    $('#btnEnviarProcesador').removeAttr('disabled');
    $('#divTecnicas').show();
    $('#btnGuardarMacro').removeAttr('disabled');
    $('#btnInmuno').removeAttr('disabled');
    $('#txtSufijoCorte').removeAttr('disabled');
    $('#btnRotuloCorte').removeAttr('disabled');
    $('#txtSufijoLaminas').removeAttr('disabled');
    $('#btnRotuloLamina').removeAttr('disabled');
    $('#selCantLaminas').removeAttr('disabled');
    $('#btnSecuenciaLaminas').removeAttr('disabled');
    $('#btnSolicitarLamina').removeAttr('disabled');

    $('#divbtnProcesar').show();
    $('#btnVolverInicio').removeAttr('disabled');
}
function revisarBotones(idEstado) {

    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador)
        botonesCortesAdministrador();
    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo)
        botonesCortesPatologo();
    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.tecnologo)
        botonesCortesTecnologo();
    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.paramedico)
        botonesCortesParamedico(idEstado);
    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.auxiliar)
        botonesCortesAuxiliar();
    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.secretaria)
        botonesCorteSecretaria();

}
function habilitarBotonesMuestraenMacroscopia(data) {

    //SI LA MUESTRA ESTA EN MACRO, TODAVIA PUEDE ENVIAR CASETE A PROCESADOR.
    $('#btnEnviarProcesador').removeAttr('disabled');
    $('#btnVolverInicio').removeAttr('disabled');
    $('#btnDescal').removeAttr('disabled');
    $('#btnEliminarCortes').removeAttr('disabled');
    $('#selNomPlantilla').removeAttr('disabled');
    $('#txtDescMacro').removeAttr('disabled');
    $('#btnGuardarDetalleMacro').show();
    $('#btnPlantillaAc').show();

    if (data.Dictada == "SI") {
        $('#divbtnProcesar').show();
        $('#divTecnicas').show();
        $('#divInmuno').show();
        $('#divbtnCortes').show();
        $('#selCantCortes').removeAttr("disabled");
        $('#btnSecuenciaCortes').removeAttr("disabled");
        $('#txtSufijoCorte').removeAttr("disabled");
        $('#btnRotuloCorte').removeAttr("disabled");
    }
    else {
        $('#divTecnicas').hide();
        $('#divInmuno').hide();

        $("#divbtnCortes").hide()
        $("#divAgregar").hide()
    }
    $('#selPatologo').removeAttr("disabled");
    $('#selTecnologo').removeAttr("disabled");
    $('#chkDictado').removeAttr("disabled");
    $('#chkTumoral').removeAttr("disabled");
    $('#selTipoMuestra').removeAttr("disabled");
    $('#selTipoMacroscopia').removeAttr("disabled");
    $('#btnGuardarMacro').removeAttr("disabled");
}
