﻿var vIdBiopsia;
$(document).ready(function () {

    var sSession = getSession();
    var vIdUsuario = sSession.id_usuario;

    $('#txtDesde').val(moment().add(-1, 'month').format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));

    $('#btnBuscar').click(function (e) {
        grillaNeoplasia();
        e.preventDefault();
    });

    $('#btnCodificacion').click(function (e) {
        $.ajax({
            type: 'POST',
            url: `${ObtenerHost()}/FormInformes/frm_BiopsiasconNeoplasia.aspx/verCodificacion`,
            data: JSON.stringify({ idBiopsia: vIdBiopsia }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
            }
        });
        e.preventDefault();
    });
    $('#btnInforme').click(function (e) {
        ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${$("#btnInforme").data("idBiopsia")}/Imprimir`)
        
        e.preventDefault();
    });
    grillaNeoplasia();
});

function detalleArchivo(e)
{
    let archivo = $(e).data('id');
    let url = archivo.replace('/GIAP/dcto/adj/', '');
    window.open(ObtenerHost() + '/Documento.aspx?nombre=' + url + 'idBiopsia=' + data.Archivo.IdBiopsia );
}

async function detalleNeo(e)
{
    let id = $(e).data('id');    
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var o = '';
            //for (var i = 0; i < data[0].ANA_organoBiopsia.length; i++)
            //    o += data[0].ANA_organoBiopsia[i] + '<br/>';
            $('#spanFechaRecepcion').html(moment(data.FechaRecepcion).format('YYYY-MM-DD'));
            if (data.Paciente !== null) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}GEN_paciente/buscar?idPaciente=${data.Paciente.Id}`,
                    success: function (paciente) {
                        if (paciente.length > 0) {
                            $('#spanRutPaciente').html(`${paciente[0].NumeroDocumento}-${paciente[0].Digito}`);
                            $('#spanNombrePaciente').html(`${paciente[0].Nombre} ${paciente[0].ApellidoPaterno} ${paciente[0].ApellidoMaterno}`);
                        } else {
                            $('#spanNombrePaciente').html(`No se encontro data del paciente`);
                        }
                    }, error: function (err) {
                        console.error("Ha ocurridoun error al buscar el paciente")
                        console.log(err)
                    }
                })
            }

            $('#spanOrgano').html(data.ANA_organoBiopsia);
            if (data.Usuario !== null) {
                if (data.Usuario.Patologo)
                    $('#spanPatologo').html(`${data.Usuario.Patologo.Nombre} ${data.Usuario.Patologo.ApellidoPaterno} ${data.Usuario.Patologo.ApellidoMaterno}`);
                if (data.Usuario.Tecnologo !== null)
                    $('#spanTecnologo').html(`${data.Usuario.Tecnologo.Nombre} ${data.Usuario.Tecnologo.ApellidoPaterno} ${data.Usuario.Tecnologo.ApellidoMaterno}`);
            }
            if(data.Medico!==null)
                $('#spanMedSolicitante').html(`${data.Medico.Persona.Nombre} ${data.Medico.Persona.ApellidoPaterno} ${data.Medico.Persona.ApellidoMaterno}`);
            
            $('#spanServOrigen').html(data.Origen.Valor);
            $('#spanServDestino').html(data.Destino.Valor);
            $("#btnInforme").data("idBiopsia", data.Id)
        }
    });

    $.ajax({
        type: 'POST',
        url: `${ObtenerHost()}/FormInformes/frm_BiopsiasconNeoplasia.aspx/adjuntosBiopsia`,
        data: JSON.stringify({ idBiopsia: id }),
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            var json = JSON.parse(data.d);
            let adataset = [];
            //console.log(json);
            $.each(json, function (key, val) {
                adataset.push([
                    val.ANA_FecArchivos_Biopsias,
                    val.ANA_NomArchivos_Biopsias,
                    val.ruta_archivo
                ]);
            });
            $('#tblArchivo').DataTable({
                data: adataset,
                order: [],
                dom: 't',
                columnDefs:
                [
                    { targets: [2], visible: false, searchable: false },
                    { targets: 0, sType: 'date-ukLong' },
                    {
                        targets: 1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones = `<a data-id="${adataset[fila][2]}" href="#/" onclick="detalleArchivo(this)">${adataset[fila][1]}</a>`;
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'Fecha' },
                    { title: 'Archivo' },
                    { title: ''}
                ],
                bDestroy: true
            });
        }
    });
    $('#mdlDetalle').modal('show');
}

//grilla para informe de neoplasia
function grillaNeoplasia() {
    let url = `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/NEOPLASIA/${moment($('#txtDesde').val()).format('YYYY-MM-DD')}/${moment($('#txtHasta').val()).format('YYYY-MM-DD')}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                let previsionPaciente = `No informado`
                let stringOrganos = ""
                val.Organo.map(a => {
                    stringOrganos += a +" "
                })

                if (val.Paciente.Prevision !== null) {
                    previsionPaciente = paciente.Prevision.Valor
                }
                if (val.Paciente.PrevisionTramo !== null) {
                    previsionPaciente += ` ${val.Paciente.PrevisionTramo.Valor}`
                }

                adataset.push([
                    /*0*/val.IdBiopsia,
                    /*1*/val.NumeroBiopsia,
                    /*2*/val.Paciente !== null ? `${val.Paciente.NumeroDocumento.split("-")[0]}` : " ",
                    /*3*/val.Paciente !== null ? `${val.Paciente.NumeroDocumento.split("-")[1]}` : " ",
                    /*4*/val.Paciente.Nombre + " " + val.Paciente.ApellidoPaterno + " " + val.Paciente.ApellidoMaterno,
                    /*5*/val.Paciente.ApellidoPaterno,
                    /*6*/val.Paciente.ApellidoMaterno,
                    /*7*/val.Paciente.Nui??"Sin informacion",
                    /*8*/val.Paciente.FechaNacimiento !== null ? moment(val.Paciente.FechaNacimiento).format("DD-MM-YYYY") : "Sin fecha registrada",
                    /*9*/previsionPaciente,
                    /*10*/val.NumeroBiopsia.split("-")[0],
                    /*11*/val.NumeroBiopsia.split("-")[1],
                    /*12*/stringOrganos,
                    /*13*/val.FechaRecepcion == undefined ? '' : (moment(val.FechaRecepcion, 'YYYY/MM/DD').format('DD-MM-YYYY')),
                    /*14*/val.FechaDespacho == undefined ? '' : (moment(val.FechaDespacho, 'YYYY/MM/DD').format('DD-MM-YYYY')),
                    /*15*/val.Servicio.Destino.GEN_dependenciaServicioDestino,
                    /*16*/val.Servicio.Origen.GEN_dependenciaServicioOrigen,
                    /*17*/val.Servicio.Destino.GEN_nombreServicioDestino,
                    /*18*/val.Comportamiento ??"No especificado",
                    /*19*/val.Morfologia ??"No especificadoD",
                    /*20*/val.GradoDiferenciacion ?? "No especificado",
                    /*21*/val.Topografia != null ? `${val.Topografia.Descripcion}` : "No especificado",
                    /*22*/val.Topografia != null ? `${val.Topografia.Codigo}` : "No especificado",
                    /*23*/val.Lateralidad ?? "",
                    /*24*/val.Her2??"",
                    /*25*/val.OrganoCIE10 ?? "",
                    /*26*/val.OrganoCIE10.split("-")[0] ?? ""
                ]);
            });

            $('#tblNeoplasia').DataTable({
                dom: 'Bfrtipl',
                buttons: [
                        {
                           extend: 'excel',// Usa la extensión de Excel
                           text: 'Exportar a excel',// mostrar texto
                           className: 'btn btn-info',
                           exportOptions: {
                           // Personalizar las opciones de exportación
                           // Como por ejemplo: exportación personalizada de esas columnas y filas
                           //TODO...
                           fileName:"BiopsiaNeoplasia"
                           }
                        }
                ],
                data: adataset,
                columns: [
                /*0*/{ title: 'Id biopsia', className: "dt-control" },
                /*1*/{ title: 'Nº biopsia' },
                /*2*/{ title: 'Rut' },
                /*3*/{ title: 'Digito' },
                /*4*/{ title: 'Paciente' },
                /*5*/{ title: 'A_Paterno' },
                /*6*/{ title: 'A_Materno' },
                /*7*/{ title: 'Nui' },
                /*8*/{ title: 'Fecha nacimiento' },
                /*9*/{ title: 'Prevision actual' },
                /*10*/{ title: 'Num_Reg_Biopsia' },
                /*11*/{ title: 'Año_Registro' },
                /*12*/{ title: 'Órgano' },
                /*13*/{ title: 'Fecha recepción' },
                /*14*/{ title: 'Fecha despacho' },
                /*15*/{ title: 'Dependencia' },
                /*16*/{ title: 'Origen' },
                /*17*/{ title: 'Destino' },
                /*18*/{ title: 'Comportamiento' },
                /*19*/{ title: 'Morfologia' },
                /*20*/{ title: 'Grado diferenciacion' },
                /*21*/{ title: 'Topografia' },
                /*22*/{ title: 'Cod_Topografia' },
                /*23*/{ title: 'Lateralidad' },
                /*24*/{ title: 'Her2' },
                /*25*/{ title: 'Organo CIE10' },
                /*26*/{ title: 'Codigo CIE10' },
                ],
                columnDefs:
                [
                    {
                        targets: [0],
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones = `<button data-id="${adataset[fila][0]}" type="button" class="btn btn-info"><span class="glyphicon glyphicon-info-sign"></span> ${adataset[fila][0]}</button>`;
                            return botones;
                        }
                    },
                    { targets: [2,3,5,6,7,8,9,10,11,18,19,20,21,22,23,24,25,26], visible: false, searchable: false },
                    {
                        targets: 1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones = `<a data-id="${adataset[fila][0]}" href="#/" onclick="detalleNeo(this)">${adataset[fila][1]}</a>`;
                            return botones;
                        }
                    }
                ],
                bDestroy: true
            });
            //Esta funcion es la que dibuja los childrow.. para que el usuario obtenga informacion extra
            function format(celda) {
                // celda, es la celda de la tabla que contiene toda la informacion de la celda seleccionada
                if (celda !== undefined) {
                    return `<table id='table-${celda[0]}' cellpadding="10" cellspacing="0" border="0" class="table table-bordered table-striped w-100" >
                <tr>
                <td><b>Nombre paciente:</b></td>
                <td class="overflow-auto">${celda[4]}</td>
                <td><b>Rut paciente:</b></td>
                <td>${celda[2]}-${celda[3]}</td>
                <td><b>Comportamiento</b></td>
                <td>${celda[18]}</td>
                </tr>
                <tr>
                <td><b>Morfología:</b></td>
                <td  class="overflow-auto">${celda[19]}</td>
                <td><b>Grado diferenciación</b></td>
                <td>${celda[20]}</td>
                <td><b>Topografía</b></td>
                <td class="overflow-auto">${celda[21]} - ${celda[22]}</td>
                </tr>
                <tr>
                <td ><b>Lateralidad</b></td>
                <td class="overflow-auto">${celda[23]}</td>
                <td><b>Her2</b></td>
                <td class="overflow-auto">${celda[24]}</td>
                <td><b>Diagnóstico CIE10</b></td>
                <td class="overflow-auto">${celda[25]}</td>
                </tr>
                
                
                </table>`;
                }
            }


            $('#tblNeoplasia tbody').unbind().on('click', 'td.dt-control', function () {
                var table = $(this).parent().parent().parent().DataTable();
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                if (row.child.isShown()) {
                    //la fila ya esta abierta.. click para cerrar
                    row.child.hide();
                    tr.removeClass('shown');
                    //Id de la tabla par destruirla y crearla nuevamente
                    let idTable = row.data()[0]
                    $('#table-' + idTable).DataTable().destroy();
                    $('#table' + idTable + ' tbody').empty();
                }
                else {
                    //Abre esta fila
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        }
    });
}