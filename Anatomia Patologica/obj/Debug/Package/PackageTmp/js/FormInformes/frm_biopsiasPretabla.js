﻿$(document).ready(function () {
    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().add(7, 'days').format('YYYY-MM-DD'));
    
    //txtDesde
    consultarPretabla()
})

async function consultarPretabla() {
    let url = `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/PreTabla/${$('#txtDesde').val()}/${$('#txtHasta').val()}`
    let dataset = []
    await $.ajax({
        method: "GET",
        url: url,
        success: function (data) {
            dataset = data
        }, error: function (error) {
            console.error("ha ocurrido un erroe al intentar consultar la pretabla")
            console.log(error)
        }
    })
    console.log(dataset)
    $("#tblBiopsiasPretabla").DataTable({
        dom: 'Bfrtipl',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',
                exportOptions: {
                    // Personalizar las opciones de exportación
                    // Como por ejemplo: exportación personalizada de esas columnas y filas
                    //TODO...
                    fileName: "Biopsias por pretabla"
                }
            }
        ],
        destroy: true,
        data: dataset,
        columns: [
            { "title": "Fecha propuesta", data: "FechaPropuesta" },
            { "title": "Id formulario", data:"IdFormulario"},
            { "title": "Diagnóstico", data: "Diagnostico"},
            {"title":"Prestación", data: "Prestacion"},
            {"title":"Especialidad", data: "Especialidad.Valor"},
            { "title": "Tipo macroscopia", data:"TipoMacroscopia.Valor" },
        ],
        columnDefs: [
            {
                targets: 0,
                render: function (fecha) {
                    return moment(fecha).format("DD-MM-YYYY")
                }
            },
            {
                render: function (data) {
                    if (data == null || data == undefined)
                        return `Sin información`
                    else
                        return data
                }
            }
        ]
    })
}