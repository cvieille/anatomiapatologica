﻿$(document).ready(function () {
    $('body').on('change', 'input:password._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#btnActualizar').click(function (e) {
        var bValido = true;
        if ($('#txtPassActual').val() == '' || $('#txtPassActual').val() == null) {
            resaltaElemento($('#txtPassActual'));
            bValido = false;
        }
        if ($('#txtPassNuevo').val() == '' || $('#txtPassNuevo').val() == null) {
            resaltaElemento($('#txtPassNuevo'));
            bValido = false;
        }
        if ($('#txtPassNuevoConfirmar').val() == '' || $('#txtPassNuevoConfirmar').val() == null) {
            resaltaElemento($('#txtPassNuevoConfirmar'));
            bValido = false;
        }
        if ($('#txtPassNuevo').val() != $('#txtPassNuevoConfirmar').val()) {
            resaltaElemento($('#txtPassNuevo'));
            resaltaElemento($('#txtPassNuevoConfirmar'));
            toastr.error('Las contraseñas nuevas no coinciden');
            bValido = false;
        }
        ////////////
        if (bValido) {
            var v1 = $('#txtPassActual').val();
            var v2 = $('#txtPassNuevo').val();
            var v3 = $('#txtPassNuevoConfirmar').val();

            var bValidoConfirmar = true;
            var vSesion = getSession();

            var pass = CryptoJS.MD5($('#txtPassActual').val()) + '';
            //$.ajax({
            //    type: 'POST',
            //    url: `${ObtenerHost()}/FormMantenedores/frm_Clave.aspx/claveActual`,
            //    data: JSON.stringify({ sPass: pass, idUsuario: vSesion.id_usuario }),
            //    contentType: 'application/json',
            //    dataType: 'json',
            //    async: false,
            //    success: function (data) {
            //        if (!data.d) {
            //            bValidoConfirmar = false;
            //            toastr.error('La contraseña ingresada no coincide con la contraseña actual');
            //            resaltaElemento($('#txtPassActual'));
            //        }
            //    }
            //});

            //if (vSesion.rut_usuario != $('#txtPassActual').val()) {
            //}

            if ($('#txtPassNuevo').val().length < 8) {
                bValidoConfirmar = false;
                toastr.error('La contraseña nueva debe tener 8 o más caracteres');
                resaltaElemento($('#txtPassNuevo'));
                resaltaElemento($('#txtPassNuevoConfirmar'));
            }
            if ($('#txtPassNuevo').val().length > 16) {
                bValidoConfirmar = false;
                toastr.error('La contraseña nueva debe tener 16 o menos caracteres');
                resaltaElemento($('#txtPassNuevo'));
                resaltaElemento($('#txtPassNuevoConfirmar'));
            }

            if (!$('#txtPassNuevo').val().match(/[a-z]/i)) {
                bValidoConfirmar = false;
                toastr.error('La contraseña nueva debe tener al menos una letra');
                resaltaElemento($('#txtPassNuevo'));
                resaltaElemento($('#txtPassNuevoConfirmar'));
            }
            if (!$('#txtPassNuevo').val().match(/[0-9]/i)) {
                bValidoConfirmar = false;
                toastr.error('La contraseña nueva debe tener al menos un número');
                resaltaElemento($('#txtPassNuevo'));
                resaltaElemento($('#txtPassNuevoConfirmar'));
            }

            if (bValidoConfirmar) {
                var json = {
                    "GEN_claveUsuariosNueva": '' + CryptoJS.MD5($("#txtPassNuevo").val()),
                    "GEN_claveUsuariosAntigua": '' + CryptoJS.MD5($('#txtPassActual').val())
                };
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}GEN_Usuarios/${vSesion.id_usuario}/CambiarClave`,
                    data: JSON.stringify(json),
                    contentType: "application/json",
                    dataType: "json",
                    async: false,
                    success: function (data, status, jqXHR) {
                        $('#btnActualizar').attr('disabled', true);
                        toastr.success('La contraseña ha sido cambiada correctamente');
                        window.location.replace(`${ObtenerHost()}/frm_login.aspx`);
                    },
                    error: function (jqXHR, status) {
                        if (jqXHR.status === 409) {
                            toastr.error("Ha ocurrido un error al cambiar la contraseña: " + JSON.parse(jqXHR.responseText).Message);
                        }
                    }
                });
               
            }
        }
        e.preventDefault();
    });
});
