﻿$(document).ready(function () {

    valoresIniciales();

    $('#btnGuardar').click(function (e) {
        console.log("askjdaskl")
        try {
            GuardarConf('n_biopsia', $('#txtNBiopsia_').val());
            GuardarConf('a_biopsia', $('#txtABiopsia_').val());
            GuardarConf('n_solicitud', $('#txtNSolNBiopsia_').val());
            GuardarConf('a_solicitud', $('#txtASolBiopsia_').val());

            $('#txtNBiopsia').val($('#txtNBiopsia_').val());
            $('#txtABiopsia').val($('#txtABiopsia_').val());
            $('#txtNSolNBiopsia').val($('#txtNSolNBiopsia_').val());
            $('#txtASolBiopsia').val($('#txtASolBiopsia_').val());

            toastr.success('Se han guardado los cambios');
        } catch (e) {
            toastr.error('No se han podido guardar los cambios');
            console.log(e);
        }
        e.preventDefault();
    });

    $('#btnCancelar').click(function (e) {
        valoresIniciales();
        toastr.success('Se cargaron los valores iniciales');
        e.preventDefault();
    });
});


function valoresIniciales() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/NumeracionActual`,
        contentType: 'application/json',
        
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data)
            $('#txtNBiopsia').val(data.n_biopsia);
            $('#txtABiopsia').val(data.a_biopsia);
            $('#txtNSolNBiopsia').val(CargarConf('n_solicitud'));
            $('#txtASolBiopsia').val(CargarConf('a_solicitud'));

            $('#txtNBiopsia_').val($('#txtNBiopsia').val());
            $('#txtABiopsia_').val($('#txtABiopsia').val());
            $('#txtNSolNBiopsia_').val($('#txtNSolNBiopsia').val());
            $('#txtASolBiopsia_').val($('#txtASolBiopsia').val());
        }
    });


}