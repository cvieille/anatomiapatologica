﻿var vIdUsuario;
$(document).ready(function () {
    var vSession = getSession();
    vIdUsuario = vSession.id_usuario;

    comboPatologo();

    $('#btnBuscarMovLaminas').click(function () {
        getDataLaminasApi($("#selPatologo").val());
    });

    $('#btnEntregarMedico').click(function (e) {
        var a = [];
        var t = $('#tblMovimientosLaminas').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.checkLamina')[0].checked) {
                a.push($(d[i]).find('.checkLamina')[0].id);
            }
        if (a.length > 0) {
            for (var i = 0; i < a.length; i++) {
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}ANA_Laminas/Enviar/Almacenamiento/${a[i]}`,
                    contentType: 'application/json',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        if (i == a.length - 1) //actualiza cuando llega al ultimo POST
                        {
                            toastr.success('Se han entregado las láminas solicitadas');
                            getDataLaminasApi($('#selPatologo').val());
                        }
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            }
        }
        else
            toastr.info('Debe seleccionar al menos un registro');
        e.preventDefault();
    });
});

function comboPatologo() {
    let url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));
}

function getDataLaminasApi(idPatologo) {
    let fechaActual = moment().format("DD-MM-YYYY")
    var vUrl;
    if (idPatologo > 0) {
        vUrl = `ANA_Laminas/Buscar?idPatologo=${idPatologo}&idTipoEstadoSistema=58`;

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}${vUrl}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                let adataset = [];                
                $.each(data, function (key, val) {
                    adataset.push([
                        '',
                        val.Id,
                        val.Nombre,
                        moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                        val.Estado.Id,
                        val.Estado.Valor,
                        val.Patologo !== null ? val.Patologo.Login : "",
                        val.Tecnologo !== null ? val.Tecnologo.Login : "",
                        val.IdBiopsia
                    ]);
                });

                if (adataset.length == 0)
                    $('#btnExportar').attr('disabled', true);
                else
                    $('#btnExportar').removeAttr('disabled');

                $('#tblMovimientosLaminas').addClass('nowrap').DataTable({
                    dom: 'Bfrtipl',
                    buttons: [
                        {
                            extend: 'excel',// Usa la extensión de Excel
                            text: 'Exportar a excel',// mostrar texto
                            className: 'btn btn-info',
                            exportOptions: {
                                // Personalizar las opciones de exportación
                                // Como por ejemplo: exportación personalizada de esas columnas y filas
                                //TODO...
                                fileName: fechaActual + "-MovimientoLaminas",
                                columns: [1, 2, 3, 5, 6]
                            }
                        }
                    ],
                    data: adataset,
                    order: [],
                    pageLength: 20,
                    lengthMenu: [[20, 50, 100, -1], ["20", "50", "100", "Todos"]],
                    columnDefs:
                        [
                            { targets: 1, searchable: false },
                            { targets: 3, sType: 'date-ukLong' },
                            { targets: [1, 4,-2], visible: false, searchable: false },
                            {
                                targets: 0,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    botones = '<label class="containerCheck">';
                                    botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="checkLamina">';
                                    botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                                    botones += '</label>';
                                    return botones;
                                }
                            },
                            {
                                targets: -1,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    botones = `<a class="btn btn-info" data-id='${adataset[fila][7]}' onclick="linkBiopsia(this)">Ver</a>`;
                                    return botones;
                                }
                            },
                        ],
                    columns: [
                        { title: '' },
                        { title: 'IDLamina' },
                        { title: 'Lámina' },
                        { title: 'Fecha' },
                        { title: 'IDEstadoSistema' },
                        { title: 'Estado' },                        
                        { title: 'Usuario patólogo' },                        
                        { title: 'Usuario tecnologo' },                        
                        { title: 'idbiopsia' },
                        { title: '' }
                    ],
                    bDestroy: true
                });
            }, error: function (err) {
                console.error("Ha ocurrido un erro al intentar buscar las laminas")
                console.log(err)
            }
        });
    }
}

function linkBiopsia(e) {
    const id = $(e).data('id');
    window.open(`../FormAnatomia/frm_Cortes.aspx?id=${id}`, '_blank');
}