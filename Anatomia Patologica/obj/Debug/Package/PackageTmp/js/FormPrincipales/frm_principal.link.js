﻿function linkVerMacroscopia(e) {
    const id = $(e).data('id');
    window.open(`../FormAnatomia/frm_Cortes.aspx?id=${id}`, '_blank');
}

function linkVerBiopsia(e) {
    const id = $(e).data('id');
    window.open(`../FormSolicitudes/frm_recepcion.aspx?id=${id}&id_soli=0`, '_self');
}

function linkVerCortes(e) {

    const id = $(e).data('id');
    window.open(`../FormAnatomia/frm_cortes.aspx?id=${id}`, '_blank');
}

function linkVerNotas(e) {
    const biopsia = {
        id: $(e).data('id'),
        bio: $(e).data('bio'),
        organo: $(e).data('organo')
    }

    $('#txtNota').removeClass('_bordeError');
    $('#txtIdNota').val(biopsia.bio);
    $('#txtOrgano').val(biopsia.organo);
    $('#txtIdBiopsia').val(biopsia.id);

    grillaNotas(biopsia.id);
    $("#mdlNotas").modal('show');
}

function linkVerMovimientosBiopsia(e) {
    const id = $(e).data('id');
    modalMovimiento(id);
    $('#mdlMovimientos').modal('show');
}

function linkVerReporte(e) {
    const id = $(e).data('id');
    window.open(`../FormAnatomia/frm_InformeBiopsia.aspx?id=${id}`, '_self');
}

function linkAnularBiopsia(e) {
    $("#btnConfirmarModalAnular").data("id", $(e).data("id"));
    $("#txtMotivoAnulacion").removeAttr("style");
    $("#txtMotivoAnulacion").val("");
    $("#mdlConfirmacionAnular").modal("show");
}