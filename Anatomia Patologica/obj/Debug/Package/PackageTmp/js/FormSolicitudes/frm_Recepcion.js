﻿
var vID;
var vIdPaciente;
var vCodigoPerfil;

$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    vID = getUrlParameter('id');
    var sSession = getSession();
    vCodigoPerfil = sSession.GEN_CodigoPerfil;

    deshabilitarElementosClase("datos-paciente");
    deshabilitarElementosClase("datos-paciente-adicional");

    cargarCombosRecepcion();

    $('#txtRecepcion').val(moment().format('YYYY-MM-DD'));

    $('#selTipoMuestra').change(function (e) {
        if ($(this).val() != undefined && $(this).val() != 0)
            comboDetalleCatalogo($(this).val());
    });
    function validarElementosAgregar() {
        //Asignar clases a los elementos
        let clase = "requerido-agregar";
        $('#selTipoMuestra').addClass(clase);
        $('#selDetalleMuestra').addClass(clase);
        $('#txtOrgano').addClass(clase);
        $('#selMuestrasPorFrasco').addClass(clase);

        let valido = true, inputsError = []

        $('.datos-ingreso-muestra').each(function (index, item) {
            if ($(item).val() == "" || $(item).val() == "0") {
                inputsError.push(item)
            }
        })
        //Hay items con error
        if (inputsError.length > 0) {
            valido = false
            inputsError.forEach(function (item, index) {
                $(item).addClass("_bordeError")
            })
        }
        $('#selTipoMuestra').removeClass(clase);
        $('#selDetalleMuestra').removeClass(clase);
        $('#txtOrgano').removeClass(clase);
        $('#selMuestrasPorFrasco').removeClass(clase);
        return valido;

    }
    $('#btnAgregar').click(function (e) {

        let bValido = validarElementosAgregar();

        if (bValido) {

            let attr = $('#btnAgregar').attr('disabled');
            if (!(typeof attr !== typeof undefined && attr !== false)) {
                let t = $('#tblDetalle').DataTable({
                    //data: adataset,
                    order: [],
                    columnDefs:
                        [
                            { targets: 0, visible: false, searchable: false },
                            {
                                targets: -1,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    if (vCodigoPerfil == 6 || vCodigoPerfil == 8)
                                        botones = '<a class="btn btn-danger" data-id="' + fila + '" onclick="linkQuitar(this)"><span class="glyphicon glyphicon-remove"></span></a>';
                                    else
                                        botones = '<a class="btn btn-danger" disabled><span class="glyphicon glyphicon-remove"></span></a>';

                                    return botones;
                                }
                            },
                        ],
                    bDestroy: true
                });
                // var d = t.rows().data().toArray();
                t.row.add([
                    $("#selDetalleMuestra").val(),
                    $('select[name="selTipoMuestra"] option:selected').text(),
                    $('#txtOrgano').val(),
                    parseInt($('#selMuestrasPorFrasco').val())
                ]).draw();
            }
        }
    });

    if (vID != 0) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (dataBio) {
                $('#txtId').val(vID);
                $('#txtNRegistro').val(dataBio.Numero + "-" + dataBio.Folio);
                $('#txtRecepcion').val(moment(dataBio.FechaRecepcion).format('YYYY-MM-DD'));
                vIdPaciente = dataBio.Paciente.Id;
                buscarCargarPacienteId(dataBio.Paciente.Id)
                //deshabilitar los campos de indentificacion
                deshabilitarElementosClase("identificacion-Paciente");

                $('#selPaciente').val(dataBio.GEN_idModalidad_Fonasa);
                $('#selGES').val(dataBio.ANA_gesRegistro_Biopsias);
                $('#txtEstado').val(dataBio.Estado.Valor);

                $('#selTipoMuestra').val(dataBio.ANA_idCatalogo_Muestras);
                comboDetalleCatalogo(dataBio.ANA_idCatalogo_Muestras);
                $('#selDetalleMuestra').val(dataBio.ANA_idDetalleCatalogo_Muestras);

                $('#txtOrgano').val(dataBio.ANA_organoBiopsia);

                comboServicio($('#selServicioOrigen'), dataBio.Origen.Dependencia);
                $('#selTipoOrigen').val(dataBio.Origen.Dependencia);
                $('#selServicioOrigen').val(dataBio.Origen.Id);


                $('#selTipoDestino').val(dataBio.Destino.Dependencia);
                comboServicio($('#selServicioDestino'), dataBio.Destino.Dependencia);
                $('#selServicioDestino').val(dataBio.Destino.Id);


                if (dataBio.Programa !== null)
                    $('#selPrograma').val(dataBio.Programa.Id);
                else
                    $('#selPrograma').val(0);

                if (dataBio.Medico !== null)
                    $('#selSolicitado').val(dataBio.Medico.Id);

                if (dataBio.Usuario.Patologo !== null)
                    $('#selPatologo').val(valCampo(dataBio.Usuario.Patologo.GEN_idUsuario));

                if (dataBio.Usuario.Tecnologo !== null)
                    $('#selTecnologo').val(valCampo(dataBio.Usuario.Tecnologo.GEN_idUsuario));

                if (dataBio.TipoBiopsia !== null) {
                    $('#selTipoBiopsia').val(valCampo(dataBio.TipoBiopsia.ANA_idTipo_Biopsia));
                    $('#selTipoMacroscopia').val(valCampo(dataBio.TipoBiopsia.ANA_idTipo_Macroscopia));
                }

                if (dataBio.AntecedentesClinicos !== null)
                    $('#txtAntecedentesClinicos').val(dataBio.AntecedentesClinicos);

                if (dataBio.ANA_dictadaBiopsia == 'SI')
                    $('#chkDictado').attr('checked', true);
                if (dataBio.ANA_tumoralBiopsia == 'SI')
                    $('#chkTumoral').attr('checked', true);

                $('#btnMovimientos').removeAttr('disabled');
            }
        });
        grillaDetalle(vID);
    }
    else {
        jsonBiopsia = {};
        $('#txtRecepcion').removeAttr('disabled');

        $('#txtId').val('0');
        $('#txtNRegistro').val('0');
        if (vCodigoPerfil == perfilAccesoSistema.paramedico || vCodigoPerfil == perfilAccesoSistema.secretaria) {
            habilitarElementosClase("identificacion-Paciente");
        }
    }

    if (vCodigoPerfil == perfilAccesoSistema.secretaria || vCodigoPerfil == perfilAccesoSistema.paramedico) {
        //SECRETARIA O PARAMEDICO        
        habilitarElementosClase("datos-ingreso-muestra");

        $('#selTipoOrigen').removeAttr('disabled');
        $('#selServicioOrigen').removeAttr('disabled');
        $('#selTipoDestino').removeAttr('disabled');
        $('#selServicioDestino').removeAttr('disabled');
        $('#selPrograma').removeAttr('disabled');
        $('#selSolicitado').removeAttr('disabled');

        $('#btnAgregar').removeAttr('disabled');
        $('#btnGuardar').show();
    }

    $('#selDocumento').change(function (e) {
        if ($('#selDocumento').val() == 1) {
            $('#txtDigito').show();
            $('#txtguion').show();
            $("#txtRut").attr('maxlength', '8');
        }
        else {
            $('#txtDigito').hide();
            $('#txtguion').hide();
            $("#txtRut").attr('maxlength', '15');
        }

        $("#txtRut").val("");
        $("#txtDigito").val("");
        $("#txtNombre").val("");
        $("#txtApellidoP").val("");
        $("#txtApellidoM").val("");
        $("#txtEdad").val("");
        $("#txtNacimiento").val("");
        $("#txtNUI").val("");
        $("#txtNacionalidad").val(0);
    });

    $('#txtRut').blur(function (e) {
        if ($('#selDocumento').val() != 1)
            buscarCargarPacienteDocumento($('#selDocumento').val(), $('#txtRut').val());
    });
    $('#txtDigito').blur(async function (e) {
        if ($('#selDocumento').val() == 1 && $('#txtDigito').val() != "")
            if ($('#txtDigito').val() == await DigitoVerificador($('#txtRut').val()))
                buscarCargarPacienteDocumento($('#selDocumento').val(), $('#txtRut').val())
            else
                toastr.error('Digito Verificar Incorrecto');
        else
            resaltaElemento($('#txtDigito'));

    });

    $('#selTipoOrigen').change(function (e) {
        comboServicio($('#selServicioOrigen'), $('#selTipoOrigen>option:selected').html());
    });
    $('#selTipoDestino').change(function (e) {
        comboServicio($('#selServicioDestino'), $('#selTipoDestino>option:selected').html());
    });

    $('#btnGuardar').click(function (e) {

        var bValido = true;

        if ($('#selDocumento').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDocumento'));
        }
        if ($('#txtOrgano').val() == '') {
            bValido = false;
            resaltaElemento($('#txtOrgano'));
        }

        if ($('#selPaciente').val() == '0') {
            bValido = false;
            resaltaElemento($('#selPaciente'));
        }
        if ($('#selTipoMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoMuestra'));
        }

        if ($('#selServicioOrigen').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioOrigen'));
        }
        if ($('#selServicioDestino').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioDestino'));
        }

        if ($('#selSolicitado').val() == '0') {
            bValido = false;
            resaltaElemento($('#selSolicitado'));
        }

        if ($('#selDetalleMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDetalleMuestra'));
        }

        if (bValido) {

            let jsonBiopsia = {
                FechaRecepcion: $('#txtRecepcion').val(),
                IdPaciente: $('#txtIdPaciente').val(),
                Organo: $('#txtOrgano').val(),
                Ges: $('#selGES').val(),
                IdServicioOrigen: parseInt($('#selServicioOrigen').val()),
                IdMedicoSolicita: parseInt($('#selSolicitado').val()),
                IdServicioDestino: parseInt($('#selServicioDestino').val()),
                IdPrograma: $('#selPrograma').val() > 0 ? $('#selPrograma').val() : null,
                IdMedicoSolicita: parseInt($('#selSolicitado').val()),
                IdCatalogoMuestra: parseInt($('#selTipoMuestra').val()),
                IdDetalleCatalogoMuestra: parseInt($('#selDetalleMuestra').val()),
                IdModalidadFonasa: parseInt($('#selPaciente').val()),
                AntecedentesClinicos: $('#txtAntecedentesClinicos').val()
            };

            let parametrosGuardar = {
                Metodo: "",
                Url: "",
                Mensaje: "",
                Destino: `${ObtenerHost()}/FormPrincipales/frm_principal.aspx`
            }

            if (vID == 0) {
                parametrosGuardar.Metodo = 'POST';
                parametrosGuardar.Url = `${GetWebApiUrl()}ANA_Registro_Biopsias`;
                parametrosGuardar.Mensaje = 'Se ha ingresado solicitud de biopsia';
            }
            else {

                parametrosGuardar.Metodo = 'PUT';
                parametrosGuardar.Url = `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}`;
                parametrosGuardar.Mensaje = 'Se ha actualizado la solicitud de biopsia';
                jsonBiopsia.IdBiopsia = parseInt(vID);

            }

            //la tabla de muestras
            var t = $('#tblDetalle').DataTable();
            var d = t.rows().data().toArray();
            if (d.length == 0) {
                resaltaElemento($('#selTipoMuestra'));
                resaltaElemento($('#selDetalleMuestra'));
                resaltaElemento($('#txtOrgano'));
                return false;
            }

            jsonBiopsia.DetalleCatalogo = [];

            for (var p = 0; p < d.length; p++) {
                let json = {
                    IdDetalleCatalogoMuestra: parseInt(d[p][0]),
                    Descripcion: d[p][2],
                    CantidadMuestras: d[p][3]
                };
                jsonBiopsia.DetalleCatalogo.push(json);
            }

            $.ajax({
                type: parametrosGuardar.Metodo,
                url: parametrosGuardar.Url,
                data: JSON.stringify(jsonBiopsia),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {

                    toastr.success(parametrosGuardar.Mensaje, '', {
                        timeOut: 100, extenderTimeOut: 100, onShown: function () { window.location.replace(parametrosGuardar.Destino); }
                    });
                    $('#btnGuardar', '#btnCancelar').attr('disabled', true);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnCancelar').click(function (e) {
        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
        e.preventDefault();
    });

    $('#btnMovimientos').click(function (e) {
        modalMovimiento(vID);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });
});
function buscarCargarPacienteId(idPaciente) {
    const filtroPaciente = "idPaciente=" + idPaciente;
    cargarPaciente(filtroPaciente);
}
function buscarCargarPacienteDocumento(idIdentificacion, numeroDocumento) {
    if ($('#txtRut').val().length <= 6) {
        toastr.warning('Ingrese un número de documento valido');
    }
    else {
        const filtroPaciente = `idIdentificacion=${idIdentificacion}&numeroDocumento=${numeroDocumento}`;
        cargarPaciente(filtroPaciente);
    }
}
function cargarPaciente(filtroPaciente) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Paciente/Buscar?${filtroPaciente}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (!$.isEmptyObject(data[0])) {
                const paciente = data[0];
                $('#txtIdPaciente').val(paciente.IdPaciente);
                $('#txtRut').val(paciente.NumeroDocumento);
                $('#txtDigito').val(paciente.Digito);
                $('#txtNombre').val(paciente.Nombre);
                $('#txtApellidoP').val(paciente.ApellidoPaterno);
                $('#txtApellidoM').val(paciente.ApellidoMaterno);
                $('#txtNacimiento').val(moment(paciente.FechaNacimiento).format('YYYY-MM-DD'));
                $('#txtNUI').val(paciente.Nui);
                if (paciente.Edad != null)
                    $('#txtEdad').val(paciente.Edad.edad);
                $('#txtNombreSocial').val(paciente.NombreSocial);
                $('#selDocumento').val(paciente.Identificacion.Id);
                if (paciente.Nacionalidad != null)
                    $('#selNacionalidad').val(paciente.Nacionalidad.Id);

                if (paciente.Genero != null)
                    $('#selGenero').val(paciente.Genero.Id);

                if (paciente.Sexo != null)
                    $('#selSexo').val(paciente.Sexo.Id);

                habilitarElementosClase("datos-paciente-adicional");

            } else {
                $('#txtNombre').val('');
                $('#txtApellidoP').val('');
                $('#txtApellidoM').val('');
                $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));
                $('#txtEdad').val('');
                $('#txtNacionalidad').val('');
                toastr.warning('El Paciente no existe, debe crearlo en Mantenedor de Pacientes');
            }
        }
    });
}


function linkQuitar(e) {
    let id = $(e).data("id");
    var t = $('#tblDetalle').DataTable();
    t.row(id).remove();
    var d = t.rows().data().toArray();
    t.clear();
    t.rows.add(d).draw();
}

function grillaDetalle(id) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/DetalleMuestras`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idDetalleCatalogo_Muestras,
                    val.ANA_detalleCatalogo_Muestras,
                    val.ANA_descripcionBiopsia,
                    val.ANA_cantidadMuestras,
                    ''
                ]);
            });
            $('#tblDetalle').DataTable({

                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: 0, visible: false, searchable: false },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                if (vCodigoPerfil == 6 || vCodigoPerfil == 8)
                                    botones = '<a class="btn btn-danger" data-id="' + fila + '" onclick="linkQuitar(this)"><span class="glyphicon glyphicon-remove"></span></a>';
                                else
                                    botones = '<a class="btn btn-danger" disabled><span class="glyphicon glyphicon-remove"></span></a>';

                                return botones;
                            }
                        },
                    ],
                bDestroy: true
            });
        }
    });
}
function cargarCombosRecepcion() {
    comboMedicos();
    comboProgramasAsync();
    comboIdentificacion();

    comboModalidad();
    comboCatalogo();

    comboServicio($('#selServicioOrigen'), 'HCM');
    comboServicio($('#selServicioDestino'), 'HCM');

    const baseApi = GetWebApiUrl();

    let url = `${baseApi}GEN_Sexo/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selSexo'))

    url = `${baseApi}GEN_Tipo_Genero/combo`;
    setCargarDataEnComboAsync(url, true, $('#selGenero'))

    url = `${baseApi}GEN_Nacionalidad/combo`;
    setCargarDataEnComboAsync(url, true, $('#selNacionalidad'))

}