﻿function comboDetalleCatalogo(idCatalogo) {
    if (idCatalogo > 0) {
        const url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/${idCatalogo}/Detalle/combo`;
        setCargarDataEnComboAsync(url, false, $('#selDetalleMuestra'));
    }
}
function comboModalidad() {
    const url = `${GetWebApiUrl()}GEN_Modalidad_Fonasa/Combo`;
    setCargarDataEnComboAsync(url, false, $('#selPaciente'));
}

function comboCatalogo() {
    const url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoMuestra'));
}

function comboProgramasAsync() {    
    const url = `${GetWebApiUrl()}GEN_Programa/combo`;
    setCargarDataEnComboAsync(url, true, $('#selPrograma'));
}

function comboServicio(cmbServicio, dependencia) {
    //debe ser sincrono pq es una cascada
    const url = `${GetWebApiUrl()}GEN_Servicio/combo/${dependencia}`;
    setCargarDataEnComboAsync(url, false, cmbServicio);
}

function comboIdentificacion() {
    const url = `${GetWebApiUrl()}GEN_Identificacion/combo`;
    setCargarDataEnComboAsync(url, false, $('#selDocumento'));
}
function comboMedicos() {
    $('#selSolicitado').empty();
    $.ajax({
        type: 'GET',
        //url: `${GetWebApiUrl()}GEN_Profesional/Medico`,
        url: `${GetWebApiUrl()}GEN_Profesional/buscar?idProfesion=1`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            datos = data.map(a => {
                return {
                    Id: a.Id,
                    Valor: a.Persona.ApellidoPaterno + " " + a.Persona.ApellidoMaterno + " " + a.Persona.Nombre
                }
            })
            //Ordenar alfabeticamente ascendente

            datos = datos.sort(function (a, b) {
                if (a.Valor > b.Valor) { //comparación lexicogŕaficaa
                    return 1;
                } else if (a.Valor < b.Valor) {
                    return -1;
                }
                return 0;
            });
            $('#selSolicitado').append("<option value='0'>-Seleccione-</option>");
            $.each(datos, function (key, val) {
                $('#selSolicitado').append("<option value='" + val.Id + "'>" + val.Valor + "</option>");
            });
        }
    });
}