function GetWebApiUrl() {
    if (sessionStorage.getItem("UrlApiANA") === undefined || sessionStorage.getItem("UrlApiANA") === null) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=GeturlWebApi`,
            contentType: "application/json",
            async: false,
            success: function (data) {
                console.log(data)
                sessionStorage.setItem("UrlApiANA", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });
    }
    return sessionStorage.getItem("UrlApiANA");
}


function GetWebApiFuncionesUrl() {
    if (sessionStorage.getItem("UrlApiFuncionesANA") === undefined || sessionStorage.getItem("UrlApiFuncionesANA") === null) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=GeturlWebApiFunciones`,
            contentType: "application/json",
            async: true,
            success: function (data) {
                sessionStorage.setItem("UrlApiFuncionesANA", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la  Api funciones: " + JSON.stringify(err));
            }
        });
    }
    return sessionStorage.getItem("UrlApiFuncionesANA");
}

function GetRutaAdjunto() {
    if (sessionStorage.getItem("RutaAdjuntoANA") === undefined || sessionStorage.getItem("RutaAdjuntoANA") === null) {
        $.ajax({
            type: "GET",
            url: `${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=GetRutaAdjuntoFront`,
            contentType: "application/json",
            async: false,
            success: function (data) {
                sessionStorage.setItem("RutaAdjuntoANA", data);
            },
            error: function (err) {
                console.log("Error al obtener la URL de la WebApi: " + JSON.stringify(err));
            }
        });
    }
    return sessionStorage.getItem("RutaAdjuntoANA");
}


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function ObtenerHost() {
    if (window.location.origin.indexOf("localhost") > -1)
        return window.location.origin;
    return window.location.origin + "/ANA";
}

function resaltaElemento(elemento) {
    switch (elemento[0].localName.toLowerCase()) {
        case 'select':
        case 'input':
        case 'textarea':
            if (elemento[0].type.toLowerCase() == 'checkbox')
                elemento.parent().children('span').addClass('_bordeError');
            else
                elemento.addClass('_bordeError');
            break;
        default:
            break;
    }
}

function CalcularEdad(fechaNac) {
    var edad;
    $.ajax({
        type: "GET",
        url: GetWebApiUrl() + "GEN_Paciente/CalcularEdad?GEN_fec_nacimientoPaciente=" + fechaNac,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            edad = data.edad;
        }
    });
    return edad;
}

function GetToken() {
    if (sessionStorage.getItem("BearerANA") === undefined || sessionStorage.getItem("BearerANA") === null)
        sessionStorage.setItem("BearerANA", 'Bearer ' + getSession().TOKEN);

    return sessionStorage.getItem("BearerANA");
}

function saltoDeLinea(t, c) {
    var r = '';
    var res = t.split(' ');
    for (var i = c; i < res.length; i = i + (c + 1)) {
        res.splice(i, 0, '<br/>');
    }
    return res.join(' ');
}

function deleteSession(key) {
    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=DeleteSession&key=${key}`,
        async: false,
        success: function (data) {
        }, error: function (err,xhr) {
            console.error(`Ha ocurrido un error al intentar eliminar un elemento (${key}) de la sesion`)
            console.log(xhr)
            console.log(err)
        }

    });
}
function valCampo(val) {
    return (val === undefined || val === null) ? '0' : val;
}

function setSession(key, value) {
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=SetSession&key=' + key + '&value=' + value,
        async: false,
        success: function (data) {
        }, error: function (err, xhr) {
            console.error(`Ha ocurrido un error al intentar agregar un elemento (${key}) en la sesion`)
            console.log(xhr)
            console.log(err)
        }
    });
}

function getSession() {
    var sSession;
    $.ajax({
        type: 'GET',
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=GetSession',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            sSession = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error:' + xhr.responseText);
        }
    });
    return sSession;
}



function redirecciona(idPerfil) {
    $.ajax({
        type: "GET",
        url: `${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=RutaRedirecciona&idPerfil=${idPerfil}`,
        contentType: "application/json",
        async: false,
        success: function (data) {
            if (data !== "-")
                window.location.replace(ObtenerHost() + data);
        },
        error: function (err) {
            console.log("Excepci�n al redireccionar: " + JSON.stringify(err));
        }
    });
}

function GetLoginPass() {
    var sSession = getSession();
    var json = {};
    //OBTIENE LOGIN Y CONTRASE�A ENCRIPTADA
    $.ajax({
        type: "GET",
        url: `${GetWebApiUrl()}GEN_Usuarios/Login`,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
            json.login = data.GEN_loginUsuarios
            json.pass = data.GEN_claveUsuarios
            //$.each(data, function () {
            //    json.login = this.GEN_loginUsuarios;
            //    json.pass = this.GEN_claveUsuarios;
            //});
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error:' + xhr.responseText);
        }
    });

    return json;
}

async function DigitoVerificador(rut) {

    let resultado
    await $.ajax({
             url: `${GetWebApiFuncionesUrl()}Persona/digitoVerificador?numeroDocumento=${rut}`,
             method: "GET",
             success: function (res) {
                resultado =  res.resultado
             }, error: function (err) {
                console.log("Ha ocurrido un error al intentar obtener el digito verificador")
                console.log(err)
             }
        })

    return resultado
    
}

//Se pasa dos veces por esta funci�n
function LogearUsuario(user, pass, perfil) {
    var resp;
    var success = false;
    var datos = {
        grant_type: 'password',
        username: user,
        password: '' + pass,
        clientid: '1',
    };

    if (perfil != 0)
        datos.idperfil = perfil;

    if ($.ajaxSettings.headers['Authorization'] != undefined)
        delete $.ajaxSettings.headers['Authorization'];

    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}recuperarToken`,
        data: datos,
        async: false,
        success: function (response) {
            resp = response;
            success = true;
        },
        error: function (err) {
            console.error(err)
            console.error('ERROR no se pudo obtener el token: ' + err.message);
        }
    });
    if (success) {
        setSession('TOKEN', resp.access_token);
        $.ajaxSetup({
            headers: { 'Authorization': GetToken() }
        });

        getMenu()
        sessionStorage.setItem("perfil", datos.idperfil);
        sessionStorage.setItem("desdeModal", false);
    }
    return success;
}

// METODOS DE ENCRIPTACI�N AES /////////////////////////////////
function GetTextoEncriptado(txt) {
    var txtEncriptado;
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=Encriptar&texto=' + txt,
        async: false,
        success: function (data) {
            txtEncriptado = data;
        },
        error: function (err) {
            console.log("error");
        }
    })
    return txtEncriptado;
}

//https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


//https://stackoverflow.com/questions/35645296/how-do-i-get-year-month-and-day-between-two-dates-with-moment-js
function momentDiff(oldDate, newDate, bDays) {
    //console.log(moment(oldDate, 'DD/MM/YYYY').format('DD-MM-YYYY') + ' | ' + moment(newDate, 'DD/MM/YYYY').format('DD-MM-YYYY'));
    var oldDateMoment, newDateMoment, numYears, numMonths, numDays;
    oldDateMoment = moment(oldDate, 'DD/MM/YYYY');
    newDateMoment = moment(newDate, 'DD/MM/YYYY');

    var totalNumDays = newDateMoment.diff(oldDateMoment, 'days');

    numYears = newDateMoment.diff(oldDateMoment, 'years');
    oldDateMoment = oldDateMoment.add(numYears, 'years');
    numMonths = newDateMoment.diff(oldDateMoment, 'months');
    oldDateMoment = oldDateMoment.add(numMonths, 'months');
    numDays = newDateMoment.diff(oldDateMoment, 'days');

    var sFecha = '';
    if (numYears > 0)
        if (numYears > 1)
            sFecha = numYears + ' a�os';
        else
            sFecha = numYears + ' a�o';
    if (numMonths > 0)
        if (numMonths > 1)
            sFecha += (numYears > 0 ? ', ' : '') + numMonths + ' meses';
        else
            sFecha += (numYears > 0 ? ', ' : '') + numMonths + ' mes';
    if (numDays > 0)
        if (numDays > 1)
            sFecha += ((numYears > 0 || numMonths > 0) ? ', ' : '') + numDays + ' d�as';
        else
            sFecha += ((numYears > 0 || numMonths > 0) ? ', ' : '') + numDays + ' d�a';

    if (bDays) {
        //console.log(totalNumDays);
        return totalNumDays;
    }
    else
        return sFecha;
}

function setCargarDataEnComboAsync(url, asincrono, selector) {
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: asincrono,
        success: function (data) {
            $(selector).append("<option value='0'> -- Seleccione --</option>");
            $.each(data, function (key, val) {
                $(selector).append("<option value='" + val.Id + "'>" + val.Valor + "</option>");
            });
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}

function reiniciarDatatable(tableId) {
    let tableObj = $('#tblDetalle').DataTable();
    if (tableObj != null) {
        tableObj.clear().destroy();
    }
    $(tableId + " tbody").empty();
    $(tableId + " thead").empty();
}
function ShowModalCargando(esVisible) {
    if (esVisible===true) {
        $("#modalCargando").show();
    } else
        $("#modalCargando").hide();
}
function ImprimirApiExterno(url, elemento = null) {
    ShowModalCargando(true);
    try {
        sToken = GetToken()
        var req = new XMLHttpRequest
        req.open('GET', url, true)
        req.responseType = 'blob'
        req.onload = function (res) {
            let blob = new Blob([req.response], { type: 'application/pdf' });
            let URL = window.URL || window.webkitURL;
            let downloadUrl = URL.createObjectURL(blob);
            window.open(downloadUrl);
            ShowModalCargando(false);
        }
        req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setRequestHeader('Authorization', sToken);
        req.send()
    } catch (err) {
        ShowModalCargando(false);
        console.log("Ocurrio un error al solicitar la impresion")
        console.error(err)
    }
}

function ImprimirDocumento(id, tipoDocumento) {

    let url = `${GetWebApiUrl()}hl7/Patient/${tipoDocumento}/${id}/Imprimir`
    try {
        sToken = GetToken()
        var req = new XMLHttpRequest
        req.open('GET', url, true)
        req.responseType = 'blob'
        req.onload = function (res) {
            let blob = new Blob([req.response], { type: 'application/pdf' });
            let URL = window.URL || window.webkitURL;
            let downloadUrl = URL.createObjectURL(blob);
            window.open(downloadUrl);
            ShowModalCargando(false);
        }
        req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setRequestHeader('Authorization', sToken);
        req.send()
    } catch (err) {
        ShowModalCargando(false);
        console.log("Ocurrio un error al solicitar la impresion")
        console.error(err)
    }
}

function eliminarFilaDatatable(tableId, dataId) {
    let tablaOriginal = $('#' + tableId).DataTable()
    tablaOriginal.row("#row-" + dataId).remove().draw(false);
}
function habilitarElementosClase(clase) {
    $('.' + clase).prop('disabled', false);
}
function deshabilitarElementosClase(clase) {
    $('.' + clase).prop('disabled', true);
}