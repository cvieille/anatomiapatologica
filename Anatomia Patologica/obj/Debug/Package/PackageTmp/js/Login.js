﻿var vPerfiles;
var vAdmin;
let userSecret;
$(document).ready(function () {
    ShowModalCargando(false)
    var v = getSession();
    vAdmin = false;
    if (v.ADMIN_LOGIN) {
        $('#txtClave').addClass('disabled').attr('disabled', true);
        vAdmin = true;
    }
    deleteSession('GEN_CodigoPerfil');
    deleteSession('id_usuario');
    deleteSession('nom_usuario');
    deleteSession('rut_usuario');
    deleteSession('perfil_del_usuario');
    deleteSession('NombrePerfil');
    deleteSession('VPERFILES');
    deleteSession('ADMIN_LOGIN');

    sessionStorage.removeItem('menuANA');
    sessionStorage.removeItem('BearerANA');
    sessionStorage.removeItem("UrlApiANA")
    sessionStorage.clear;

    //$('#btnEntrar').click(function (e) {
    //    Login();
    //    e.preventDefault();
    //});
    $('#selectPerfiles').prop("disabled", false);
});


async function buscarUsuario(input) {
    //Por si ingresa primero la password y luego el usuario
    let valido = false
    let otroInput = document.getElementById("txtClave")
    let botonLogin = document.getElementById("btnEntrar")
    if (input.id == "txtClave") {
        otroInput = document.getElementById("txtLogin")
    }

    //validacion de campos vacios
    if (input.value !== "" && otroInput.value !== "") {
        valido = true
    } else {
        if (!botonLogin.hasAttribute("disabled")) {
            botonLogin.setAttribute("disabled", true)
        }
    }

    if (input.value !== "" && vAdmin) {
        valido = true
        //Aca deberia buscar la password cuando es admin
        await $.ajax({
            method: "POST",
            url: `${GetWebApiUrl()}GEN_Usuarios/Autentificacion?login=${input.value}`,
            success: function (res) {
                userSecret = res
                $("txtClave").blur()
            }, error: function (err) {
                console.error(`Ha ocurrido un error al intentar ingresar como ${input.value}`)
                console.log(err)
            }
        })
    }
    
    if (valido) {
        let usuarioBuscar = {
            idSistema: 1,
            Login: document.getElementById("txtLogin").value,
            Clave: document.getElementById("txtClave").value !== "" ? "" + CryptoJS.MD5(document.getElementById("txtClave").value) : userSecret
        }

        $.ajax({
            method: "POST",
            url: `${GetWebApiUrl()}GEN_usuarios/perfiles`,
            data: usuarioBuscar,
            async: true,
            success: function (res) {
                let contenido = ""
                //Listar los perfiles del usuario
                res.map(item => { contenido += `<option value="${item.Id}">${item.Valor}</option>` })
                $("#sltPerfiles").empty()
                $("#sltPerfiles").append(contenido)
                //Tiene mas de un perfil
                if (res.length > 1) {
                    $("#divSltPerfiles").slideDown()
                    setTimeout(() => {
                        botonLogin.removeAttribute("disabled")
                    }, 1000)
                } else {
                    //solo tiene un perfil
                    botonLogin.removeAttribute("disabled")
                    if ($("#divSltPerfiles").is(":visible"))
                        $("#divSltPerfiles").slideUp()
                }
                //Peticion es exitosa permite iniciar sesión

            }, error: function (error) {
                console.error("Ha ocurrido un error al buscar el usuario y los perfiles")
                console.log(error)
                //Error no permite iniciar sesión
                if (!botonLogin.hasAttribute("disabled")) {
                    botonLogin.setAttribute("disabled", true)
                }
                //vaciar lista de perfiles
                $("#sltPerfiles").empty()
                //ocultar listado de perfiles
                if ($("#divSltPerfiles").is(":visible"))
                    $("#divSltPerfiles").slideUp()
                //Error cuando no encuentra el usuario
                if (error.status == 404) {
                    toastr.error(error.responseJSON.message)
                }
            }
        })

    }
    
}

/** Nueva funcion de inicio de sesion **/
function iniciarSesion() {
    //Validacion de campos
    let handler = {
        set: function (obj, prop, value) {
            if (value == "") {
                toastr.error(`Error: faltan campos obligatorios.`)
                return false
            }
            obj[prop] = value;
        }
    }
    const usuarioLogin = new Proxy({}, handler)
    //Armando usuario a logear
    usuarioLogin.grant_type = "password"
    usuarioLogin.username = document.getElementById("txtLogin").value
    usuarioLogin.password = document.getElementById("txtClave").value !== "" ? "" + CryptoJS.MD5(document.getElementById("txtClave").value) : userSecret
    usuarioLogin.clientid = 1
    usuarioLogin.idPerfil = document.getElementById("sltPerfiles").value
    delete $.ajaxSettings.headers['Authorization'];
    $.ajax({
        type: "POST",
        url: `${GetWebApiUrl()}recuperarToken`,
        data: usuarioLogin,
        async: false,
        success: function (resp) {
            //console.log("respeusta del token", resp)
            setSession('TOKEN', resp.access_token);
            $.ajaxSetup({
                headers: { 'Authorization': GetToken() }
            });

            getMenu()
            sessionStorage.setItem("perfil", usuarioLogin.idPerfil);
            sessionStorage.setItem("desdeModal", false);
            sessionStorage.setItem("usuario", usuarioLogin.username);

            setSession("LOGIN_USUARIO", usuarioLogin.username);
            setSession("FECHA_FINAL_SESION", moment().add(parseInt(resp.expires_in), 'seconds').format('YYYY-MM-DD HH:mm:ss'));

            getUsuario(usuarioLogin, usuarioLogin.username, usuarioLogin.password);
            //resp = response;
            //setSession("TOKEN", resp.access_token);
            

            //$.ajaxSetup({
            //    headers: { 'Authorization': GetToken() }
            //});
            //userlogged = usuarioLogin
            //getUsuario(false);
        },
        error: function (err) {
            LoginIncorrecto();
            console.log("ERROR no se pudo obtener el token: " + JSON.stringify(err));
        }
    });
}
//deprecated
//function Login() {

//    //if (validarCampos("#div_login", false)) {
//    var success = false;
//    $('#btnEntrar').addClass('disabled');
//    $('#btnEntrar').attr('disabled', 'disabled');
//    $('#btnEntrar').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
//    $('#txtLogin').attr('disabled', 'disabled');
//    $('#txtClave').attr('disabled', 'disabled');

//    try {
//        var user = $('#txtLogin').val();
//        var pass = '';
//        if (vAdmin) {
//            $.ajax({
//                type: 'POST',
//                url: `${GetWebApiUrl()}gen_usuarios/autentificacion`,
//                contentType: 'application/json',
//                data: JSON.stringify({ login: user }),
//                dataType: 'json',
//                async: false,
//                success: function (data) {
//                    sessionStorage.setItem("usuario", user);
//                    pass = data;
//                }
//            });
//        }
//        else
//            pass = CryptoJS.MD5($('#txtClave').val());
//        var resp = LogearUsuario(user, pass, 0);
//        if (resp) {
//            sessionStorage.setItem("usuario", user);
//            getUsuario(user, pass);
//        } else
//            loginIncorrecto();
//    } catch (err) {
//        console.log(err.message);
//    }
//}

function loginIncorrecto() {
    toastr.error('Usuario o contraseña incorrectos');
    $('#btnEntrar').removeClass('disabled');
    $('#btnEntrar').removeAttr('disabled');
    $('#btnEntrar').html('Entrar');
    $('#txtLogin').removeAttr('disabled');
    $('#txtClave').removeAttr('disabled');
}

function getUsuario(usuarioLogin, usuario, pass) {
    try {
        pass = usuarioLogin.password;
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + '/GEN_Usuarios/Acceso',
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if ($.isEmptyObject(data)) {
                    loginIncorrecto();
                }
                else {
                    setSession('id_usuario', data[0].GEN_idUsuarios);
                    setSession('nom_usuario', usuarioLogin.username.toUpperCase());
                    setSession('rut_usuario', data[0].GEN_numero_documentoPersonas);
                    var sr = CryptoJS.MD5(data[0].GEN_numero_documentoPersonas) + '';
                    
                    if (pass.toLowerCase() == sr.toLowerCase()) {
                        window.location.replace(`${ObtenerHost()}/FormMantenedores/frm_Clave.aspx`);
                    }
                    else {
                        vPerfiles = data[0].GEN_Perfiles;
                        setSession('VPERFILES', JSON.stringify(vPerfiles));
                        perfilSeleccionado = vPerfiles.find(z => z.GEN_idPerfil == usuarioLogin.idPerfil)
                        setSession('GEN_CodigoPerfil', perfilSeleccionado.GEN_codigoPerfil);
                        setSession('perfil_del_usuario', perfilSeleccionado.GEN_idPerfil);
                        setSession('NombrePerfil', perfilSeleccionado.GEN_descripcionPerfil);
                        redirecciona(perfilSeleccionado.GEN_codigoPerfil);
                    }
                }
            }
        });
    } catch (err) {
        console.log('Error: ' + err.message);
    }
}