﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Modal_Cargando.ascx.vb" Inherits="Anatomia_Patologica.Modal_Cargando" %>
<asp:UpdateProgress runat="server" id="upd_Cargando" ng-show="showModalCargando" ng-init="showModalCargando = true">
    <ProgressTemplate>
        <asp:Panel id="divCargando" runat="server" cssClass="cargando centrado-horizontal-vertical">
            <div class="sk-rotating-plane">
                <img src="<%= ResolveClientUrl("~/imagenes/logo.png") %>" />
            </div>
            <h1>Cargando...</h1>
        </asp:Panel>
    </ProgressTemplate> 
</asp:UpdateProgress>