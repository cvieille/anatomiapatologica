﻿<%@ Page Title="Historial de Movimientos" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="_sin.uso.frm_MovimientosBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosBiopsia" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            //oculta menu para esta ventana.
            $("#navMenu").hide();
            let adatasetidBiopsia = getUrlParameter('id');

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Movimientos/ANA_Registro_Biopsias/${idBiopsia}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    let adataset = [];
                    $.each(data, function (key, val) {
                        adataset.push([
                            val.ANA_idMovimientos,
                            moment(val.ANA_fechaMovimientos).format('DD/MM/YYYY HH:mm:ss'),
                            val.GEN_descripcionTipo_Movimientos_Sistemas,
                            val.ANA_descMovimientos,
                            val.GEN_loginUsuarios
                        ]);
                    });

                    $('#tbl_movimientos').DataTable({
                        data: adataset,
                        order: [],
                        columnDefs: [{ targets: 1, sType: 'date-ukLong' }],
                        columns: [
                            { title: 'ID movimiento' },
                            { title: 'Fecha' },
                            { title: 'Tipo movimiento' },
                            { title: 'Descripción' },
                            { title: 'Login' }
                        ],
                        bDestroy: true
                    });
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div style="margin: 20px;">
        <!-------------------- Panel Principal -------------------->
        <div class="panel panel-default">
            <!-------------------- Default panel contents -------------------->
            <div class="panel-heading">
                <h2 class="text-center">Historia de Movimientos</h2>
            </div>
            <div class="panel-body">
                <div class="text-left">
                    <table id="tbl_movimientos" class="table table-bordered  table-hover table-responsive" style="font-size: smaller"></table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
