﻿<%@ Page Title="Inf. Biopsias Criticas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasCriticas.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasCriticas" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasCriticas.js") %>'}?${new Date().getTime()}` });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblCritico').DataTable();
                __doPostBack("<%= btnExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
 
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de críticos</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Patólogo:</label>
                        <select id="selPatologo" class="form-control">
                            <option value="0">-Seleccione-</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Visto:</label>
                        <select id="selVisto" class="form-control">
                            <option value="0">-Seleccione-</option>
                            <option value="SI">SI</option>
                            <option value="NO">NO</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <button id="btnBuscar" class="btn btn-success" style="width:100%;">Buscar</button>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <button id="btnExportar" class="btn btn-success" style="width:100%;">Exportar a excel</button>
                        <asp:Button runat="server" ID="btnExportarH" OnClick="btnExportarH_Click" style="display:none;"/>
                    </div>
                </div>
            </div>
            <hr />
            <div>
                <table id="tblCritico" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlDetalle" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de crítico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Usuario notificado:</label>
                            <input id="txtUsuarioNotificado" disabled class="form-control"/>
                        </div>
                        <div class="col-md-6">
                            <label>Fecha notificación:</label>
                            <input type="date" id="txtFechaNotificado" disabled class="form-control"/>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="col-md-12">
                            <label>Nombre del patólogo:</label>
                            <input id="txtNombrePatologo" disabled class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Diagnóstico:</label>
                            <textarea id="txtDiagnostico" disabled rows="3" class="form-control" style="resize:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Respuesta notificación:</label>
                            <textarea id="txtRespuestaNotificacion" disabled rows="3" class="form-control" style="resize:none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>