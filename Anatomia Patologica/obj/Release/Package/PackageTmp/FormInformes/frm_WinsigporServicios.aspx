﻿<%@ Page Title="Informe Winsig" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_WinsigporServicios.aspx.vb" Inherits="Anatomia_Patologica.frm_WinsigporServicios" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_datos").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_datos").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe Winsig - Por Servicio</h2>
        </div>
        <div class="panel-body">
            <div class="col-md-2">
                <label>Desde:</label>
                <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required">
                </asp:TextBox>
            </div>
            <div class="col-md-2">
                <label>Hasta:</label>
                <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required">
                </asp:TextBox>
            </div>
            <div class="col-md-3">
                <br />
                <div class="form-inline">
                    <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                    <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                </div>
            </div>
        </div>


        <br />

        <p>
            <asp:Label ID="Lbl_error" runat="server" Text="Revise el rango de fechas a consultar ....."
                CssClass="alert alert-danger" Visible="False"></asp:Label>
        </p>
    </div>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false"></asp:Label>
            <asp:GridView ID="gdv_datos" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover table-responsive"
                DataSourceID="sqldata_cuenta_biopsias">
                <RowStyle BorderStyle="None" />
                <Columns>
                    <asp:BoundField DataField="GEN_codwinsigServicio" HeaderText="Cód. WinSig" SortExpression="GEN_codwinsigServicio" />
                    <asp:BoundField DataField="origen" HeaderText="Origen" SortExpression="origen" ReadOnly="True" />
                    <asp:BoundField DataField="serv_origen" HeaderText="Servicio" SortExpression="serv_origen" ReadOnly="True" />
                    <asp:BoundField DataField="tot_biopsia" HeaderText="Total" SortExpression="tot_biopsia" ReadOnly="True" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="sqldata_cuenta_biopsias" runat="server"
                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
