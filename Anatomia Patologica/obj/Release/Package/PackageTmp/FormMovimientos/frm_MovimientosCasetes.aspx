﻿<%@ Page Title="Movimientos Casete" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" MaintainScrollPositionOnPostback="true" CodeBehind="frm_MovimientosCasetes.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosCasetes" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMovimientos/frm_MovimientosCasetes.js") %>'}?${new Date().getTime()}` });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblMovimientosCasete').DataTable();
                __doPostBack("<%= btnExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Movimiento de casete</h2>
        </div>
        <div class="panel-body">
            <div style="padding: 10px">
                <div class="row">
                    <div class="col-md-4">
                        <label>Estado a buscar:</label>
                        <select id="selTipoBusqueda" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Patólogo:</label>
                        <select id="selPatologo" class="form-control"></select>
                    </div>
                    <div class="col-md-4">
                        <br />
                        <button id="btnBuscar" class="btn btn-success">Buscar</button>
                        <asp:Button runat="server" ID="btnExportarH" OnClick="btnExportarH_Click" Style="display: none;" />
                        <button id="btnExportar" class="btn btn-success">Exportar tabla</button>
                    </div>
                </div>
                <hr />
            </div>
            <table id="tblMovimientosCasete" class="table table-bordered table-hover table-responsive"></table>

            <div class="text-center">
                <button id="btnEnviarProcesador" class="btn btn-primary" style="display: none;">Enviar a procesador</button>
                <button id="btnRecibir" class="btn btn-primary" style="display: none;">Recibir de procesador</button>
                <button id="btnTincion" class="btn btn-primary" style="display: none;">Tinción y montaje</button>
                <button id="btnEnviarAlmacenamiento" class="btn btn-primary" style="display: none;">Enviar a almacenamiento</button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMensaje" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mensaje</h4>
                </div>
                <div class="modal-body">
                    <div id="divMensaje" style="display: none;">
                        <p>
                            <label id="lblModalMensaje"></label>
                        </p>
                        <p>No se modificaron los siguientes registros: <b>
                            <label id="lblModalC"></label>
                        </b></p>
                    </div>
                    <hr id="hrSeparador" style="display: none;" />
                    <div id="divMensajeExtra" style="display: none;">
                        <p>
                            <label id="lblModalMensajeE"></label>
                        </p>
                        <p>No se modificaron los siguientes registros: <b>
                            <label id="lblModalCE"></label>
                        </b></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
