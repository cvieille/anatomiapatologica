<%@ Page Title="Pagina de Inicio" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_principal.aspx.vb"
    Inherits="Anatomia_Patologica.frm_principal" %>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormPrincipales/frm_principal.js") %>'}?${new Date()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormPrincipales/frm_principal.link.js") %>'}?${new Date()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/Funciones.js") %>'}?${new Date()}` });
        
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row" id="divPesta�as" style="display: none;">
                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#divTabTecnicasEspeciales" data-toggle="tab">
                                    <label style="color: black">T�cnicas especiales pendientes&nbsp;<span id="bdgListadoTecnicas" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabInmunoHistoquimica" data-toggle="tab">
                                    <label style="color: black">Inmunohistoqu�micas pendientes&nbsp;<span id="bdgListadoInmuno" class="badge">0</span></label>
                                </a>
                            </li>
                            <li>
                                <a href="#divTabInterconsultas" data-toggle="tab">
                                    <label style="color: black">Interconsultas&nbsp;<span id="bdgInterconsultas" class="badge">0</span></label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="divTabTecnicasEspeciales">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <%--<div class="col-md-2 col-md-offset-10">
                                                <button id="btnExportarTec" disabled class="btn btn-success" style="width: 100%; margin-top: 4px;">Exportar</button>
                                                <asp:Button runat="server" ID="btnExportarTecH" OnClick="btnExportarTecH_Click" Style="display: none;" />
                                            </div>--%>
                                        </div>
                                        <hr />
                                        <table id="tblListadoTecEspeciales" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabInmunoHistoquimica">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <%--<div class="col-md-2 col-md-offset-10">
                                                <button id="btnExportarInmuno" disabled class="btn btn-success" style="width: 100%; margin-top: 4px;">Exportar</button>
                                                <asp:Button runat="server" ID="btnExportarInmunoH" OnClick="btnExportarInmunoH_Click" Style="display: none;" />
                                            </div>--%>
                                        </div>
                                        <hr />
                                        <table id="tblListadoInmuno" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="divTabInterconsultas">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <%--<div class="col-md-2 col-md-offset-10">
                                                <button id="btnExportarInterconsultas" disabled class="btn btn-success" style="width: 100%; margin-top: 4px;">Exportar</button>
                                                <asp:Button runat="server" ID="btnExportarInterconsultasH" OnClick="btnExportarInterconsultasH_Click" Style="display: none;" />
                                            </div>--%>
                                        </div>
                                        <hr />
                                        <table id="tblInterconsultas" class="table table-bordered  table-hover table-responsive" style="width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">M�dulo de b�squeda</h2>
                </div>
                <div class="panel-body">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Mostrar:</label>
                                <select id="selMostrar" class="form-control">
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Pat�logo:</label>
                                <select id="selPatologo" class="form-control">
                                    <option value="0">-Seleccione-</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Desde:</label>
                                <input type="date" id="txtDesde" class="form-control" />
                            </div>
                            <div class="col-md-2">
                                <label>Hasta:</label>
                                <input type="date" id="txtHasta" class="form-control" />
                            </div>
                            <div class="col-md-2">
                                <label>N� biopsia:</label>
                                <input type="number" id="txtNumero" class="form-control" value="0" />
                            </div>
                            <div class="col-md-2">
                                <label>A�o biopsia:</label>
                                <input type="number" id="txtA�o" class="form-control" value="0" />
                            </div>
                        </div>
                        <div class="card row" style="margin-top: 10px;">
                            <div class="col-md-2">
                                <label>Interconsulta:</label>
                                <select id="selInterconsulta" class="form-control">
                                    <option value="0">-Seleccione-</option>
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Tipo biopsia:</label>
                                <select id="selTipoBiopsia" class="form-control">
                                    <option value="0">-Seleccione-</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>N� de documento:</label>
                                <div class="input-group">
                                    <input id="txtRut" type="text" maxlength="8" class="form-control">
                                    <span id="txtDigito" class="input-group-addon">-</span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Nombre:</label>
                                <input id="txtNombre" maxlength="100" type="text" class="form-control" />
                            </div>
                            <div class="col-md-2">
                                <label>Apellidos:</label>
                                <input id="txtApellidoP" type="text" maxlength="100" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <input id="txtApellidoM" type="text" maxlength="100" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-md-offset-8">
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-2">
                                <label class="containerCheck" style="display:none;">
                                    Sin codificar
                                    <input id="chkCodificar" type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="containerCheck" style="display:none;">
                                    Cr�tico
                                    <input id="chkCritico" type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <button id="btnBuscar" class="btn btn-success" style="width: 100%;">Buscar</button>
                            </div>
                            <div class="col-md-2">
                                <button id="btnLimpiar" class="btn btn-warning" style="width: 100%;">Limpiar</button>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <table id="tblPrincipal" class="table table-bordered  table-hover table-responsive"></table>
                    <div style="padding: 15px; background: #f0f0f0;">
                        <div>
                            <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: rgb(223, 240, 216); border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                            <label style="font-size: medium;">REGISTRO GES</label>
                            <br />
                            <span style="margin-left: 20px; height: 20px; width: 20px; margin-right: 10px; background-color: rgb(216, 218, 240); border-radius: 50%; display: inline-block; border: 1px solid black;"></span>
                            <label style="font-size: medium;">CITOLOG�A</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlConfirmacion" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmaci�n</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="display: contents;">
                        &nbsp<label id="lblConfirmacion"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnConfirmarModal" class="btn btn-success" onclick="confirmaEntregar(this); return false;">Confirmar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlNotas">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Notas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid lightgray;">
                            <blockquote class="blockquote">
                                <b>Nueva nota</b>
                            </blockquote>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Id Biopsia:</label>
                                    <input id="txtIdBiopsia" type="text" disabled class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>N� de biopsia:</label>
                                    <input id="txtIdNota" type="text" disabled class="form-control" />
                                </div>
                                <div class="col-md-7">
                                    <label>�rgano:</label>
                                    <input id="txtOrgano" type="text" disabled class="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nota:</label>
                                    <textarea id="txtNota" class="form-control" style="resize: none;" rows="5" maxlength="300"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding-top: 10px;">
                                    <button id="btnGuardarNota" class="btn btn-success pull-right">Guardar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <blockquote class="blockquote">
                                <b>Notas de la biopsia</b>
                            </blockquote>
                            <table id="tblNotas" style="width: 100%;" class="table table-bordered  table-hover table-responsive"></table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mdlConfirmacionAnular" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmaci�n anulaci�n</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Motivo de Anulaci�n</label><br />
                            <textarea id="txtMotivoAnulacion" class="form-control" rows="4" maxlength="100"
                                placeholder="Escriba motivo de anulaci�n">
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnConfirmarModalAnular" class="btn btn-danger" onclick="anularBiopsia(); return false;">
                        Anular
                    </button>
                    <button class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />   
    
</asp:Content>
