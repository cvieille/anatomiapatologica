﻿<%@ Page Language="vb" AutoEventWireup="false" Title="Sistema de Anatomia Patologica" CodeBehind="frm_login.aspx.vb"
    Inherits="Anatomia_Patologica.WebForm1" MasterPageFile="~/Master/MasterCrAnatomia.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/Funciones.js") %>'}?${new Date().getTime()}` });
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/Login.js") %>'}?${new Date().getTime()}` });
    </script>

    <script src="js/jquery-1.11.3.js"></script>    
    <script src="js/CryptoJS/md5.js"></script>

    <script src="<%= ResolveClientUrl("~/css/bootstrap-3.3.6-dist/js/bootstrap.js") %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="centrado-horizontal-vertical">
        <div id="div_login" class="container" style="margin-top: 20px; width:600px; display: block;">
            <div class="panel">
                <div class="panel-heading" style="background: linear-gradient(#374458, #262D37);">
                    <h2 style="padding-bottom: 5px; color: White;">Iniciar sesión
                    </h2>
                </div>
                <div class="panel-body" style="border: 1px solid black;">
                    <fieldset>
                        <div class="center-block" style="padding-bottom: 15px; padding-top: 5px;">
                            <img id="img_usuario" style="width: 80px; height: 80px;"
                                src="imagenes/candado.png" alt="Imagen de usuario no conectado">
                        </div>
                        <div class="form-group">
                            <div class="has-naranjo input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-user"></i>
                                </span>
                                <input class="form-control" id="txtLogin" type="text" placeholder="NOMBRE DE USUARIO" data-required="true" maxlength="8"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="has-naranjo input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-lock"></i>
                                </span>
                                <input class="form-control" id="txtClave" type="password" placeholder="CONSTRASEÑA" data-required="true" />
                            </div>
                            <br />
                            <div class="bg-danger text-white" style="font-size: 12pt; padding-top: 10px; display:none;">
                                <strong>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span> Usuario o Contraseña Incorrecto
                                </strong>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-lg btn-block btn-entrar" id="btnEntrar" style="background: linear-gradient(#374458, #262D37); color: White;">
                                ENTRAR
                            </button>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div id="modalPerfiles" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Perfiles</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="md-form">
                                <div class="col-sm-12 col-md-12">
                                    <label>Perfil:</label>
                                    <select id="selectPerfiles" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnEntrarLogin" type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>