﻿
var vIdBiopsia;
var vIdUsuario;
var vNombreUsuario;
var vANA_fecValidaBiopsia;
var vANA_InterconsultaBiopsia;
var vANA_solicitadaBiopsia;
var vFECHA_ACTUAL;

var vIDArchivo;
var vNombreArchivo;
var vFechaArchivo;

$(document).ready(function () {
    vIdBiopsia = getUrlParameter('id');
    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    vNombreUsuario = sSession.nom_usuario;
    vFECHA_ACTUAL = sSession.FECHA_ACTUAL;
    vPerfilDelUsuario = sSession.perfil_del_usuario;

    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo ||
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador)
        cargarCombos();


    cargarBiopsiaDesdeDatos();
    ContarAdjuntos(false);

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });


    $('#chkMacro').change(function (e) {
        comboPlantillas();
    });
    $('#chkMicro').change(function (e) {
        comboPlantillas();
    });
    $('#chkDiag').change(function (e) {
        comboPlantillas();
    });

    {
        $('#liAdjuntos').click(function () {
            ContarAdjuntos(false);
        });
    }

    $('body').on('click', '#btnQuitarConfirmar', function (e) {
        console.log(vIDArchivo)
        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Archivos_Biopsias/${vIDArchivo}`,
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                Id: vIDArchivo,
                Motivo: "Retirado por Patologo"
            }),
            success: function (data) {
                toastr.success("el archivo fue eliminado con exito")
                console.log(data)
                eliminarArchivo($("#btnQuitarConfirmar").data("nombrearchivo"), $("#btnQuitarConfirmar").data("idbiopsia"));
                ContarAdjuntos(false);
            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
        e.preventDefault();
        $("#mdlQuitar").modal('hide');

    });
    $('body').on('click', '.btnQuitar', function (e) {
        vIDArchivo = $(this).parents('.liElemento').attr('id');        //para sacar de la base de datos
        vNombreArchivo = $(this).parent().children().find('strong')[0].innerHTML;   //para sacar del disco
        vFechaArchivo = $(this).parent().children().find('#date-file')[0].innerHTML;

        $('#btnQuitarConfirmar').attr('data-idBiopsia', getUrlParameter('id'));
        $('#btnQuitarConfirmar').attr('data-nombreArchivo', vNombreArchivo);
        $('#mdlQuitar').modal('show');
    });
    //Con esto se redirige al documento
    $('body').on('click', '.nombreArchivo', function (e) {
        GetRutaAdjunto()
        window.open(ObtenerHost() + '/Documento.aspx?nombre=' + $(this).children('strong').html() + '&idBiopsia=' + getUrlParameter('id'));
        
    });

    $('#btnPlantillaAnt').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 0);
        e.preventDefault();
    });
    $('#btnPlantillaDescMacro').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 1);
        e.preventDefault();
    });
    $('#btnPlantillaExamenMicro').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 2);
        e.preventDefault();
    });
    $('#btnPlantillaDiag').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 3);
        e.preventDefault();
    });
    $('#btnPlantillaInmuno').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 4);
        e.preventDefault();
    });
    $('#btnPlantillaNotaAdicional').click(function (e) {
        if ($('#selNomPlantilla').prop('value') == 0)
            resaltaElemento($('#selNomPlantilla'));
        else
            cargarPlantilla($('#selNomPlantilla').prop('value'), 5);
        e.preventDefault();
    });

    $('#selOrgano').change(function (e) {
        comboTopografia($('#selOrgano').val());
    });


    $('#btnGuardarCodificacion').click(function (e) {
        var bValido = true;
        for (var i = 1; i <= 8; i++) {
            if ($('#txt080100' + i).val() == '' || $('#txt080100' + i).val() == undefined || $('#txt080100' + i).val() == null) {
                resaltaElemento($('#txt080100' + i));
                bValido = false;
            }
        }
        if (bValido) {
            let json = {
                C001: $('#txt0801001').val(),
                C002: $('#txt0801002').val(),
                C003: $('#txt0801003').val(),
                C004: $('#txt0801004').val(),
                C005: $('#txt0801005').val(),
                C006: $('#txt0801006').val(),
                C007: $('#txt0801007').val(),
                C008: $('#txt0801008').val(),
                IdBiopsia: vIdBiopsia
            };

            $.ajax({
                type: 'PATCH',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${$("#txtIdBiopsia").val()}/Codificacion`,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha guardado la codificación');
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnRecalcularCodificacion').click(function (e) {
        $('#txt0801001').val('0');
        $('#txt0801002').val('0');
        $('#txt0801003').val('0');
        $('#txt0801004').val('0');
        $('#txt0801005').val('0');
        $('#txt0801006').val('0');
        $('#txt0801007').val('0');
        $('#txt0801008').val('0');

        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Codificar`,
            dataType: 'json',
            success: function (data) {
                $.ajax({
                    type: 'GET',
                    url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Codificacion`,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        $('#txt0801005').val(data.ANA_cod005Codificacion_Biopsia);
                        $('#txt0801006').val(data.ANA_cod006Codificacion_Biopsia);
                        $('#txt0801007').val(data.ANA_cod007Codificacion_Biopsia);
                        $('#txt0801008').val(data.ANA_cod008Codificacion_Biopsia);
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
        e.preventDefault();
    });
    $('#btnVerMovimientos').click(function (e) {
        if ($('#mdlValidar').hasClass('in'))
            return false;

        modalMovimiento(vIdBiopsia);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });

    $('#btnGuardar').click(function (e) {
        guardarDescripcionBiopsia();
        e.preventDefault();
    });

    $('#btnConvertirP').click(function (e) {
        $('#txtNombrePlantilla').val('');
        $('#txtNombrePlantilla').removeClass('_bordeError');
        $('#mdlPlantilla').modal('show');
        e.preventDefault();
    });

    $('#btnGuardarP').click(function (e) {
        if ($('#txtNombrePlantilla').val() != '' && $('#txtNombrePlantilla').val() != null && $('#txtNombrePlantilla').val() != undefined) {
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Plantillas/Existe/${$('#txtNombrePlantilla').val()}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if (data > 0)
                        toastr.error('Ya existe una plantilla con este nombre');
                    else {
                        json = {
                            ANA_nombreplantilla: $('#txtNombrePlantilla').val(),
                            ANA_desc_ant_clinicosPlantilla: $('#txtAntecedentesClinicos').val() != "" ? $('#txtAntecedentesClinicos').val() : null,
                            ANA_desc_macroscopicaPlantilla: $('#txtDescMacro').val() != "" ? $('#txtDescMacro').val() : null,
                            ANA_desc_microscopicaPlantilla: $('#txtExamenMicro').val() != "" ? $('#txtExamenMicro').val() : null,
                            ANA_desc_diagnosticoPlantilla: $('#txtDiag').val() != "" ? $('#txtDiag').val() : null,
                            ANA_estu_inmunoPlantilla: $('#txtInmuno').val() != "" ? $('#txtInmuno').val() : null,
                            ANA_desc_notaPlantilla: $('#txtNotaAdicional').val() != "" ? $('#txtNotaAdicional').val() : null
                        };

                        $.ajax({
                            type: 'POST',
                            url: `${GetWebApiUrl()}ANA_Plantillas`,
                            contentType: 'application/json',
                            data: JSON.stringify(json),
                            dataType: 'json',
                            success: function (data) {
                                toastr.success('La plantilla ha sido guardada');
                                $('#mdlPlantilla').modal('hide');
                            }
                        });
                    }
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        } else
            resaltaElemento($('#txtNombrePlantilla'));
        e.preventDefault();
    });

    $('#btnNotificarUsuario').click(function (e) {
        var bValido = true;
        if ($('#selUsuarioNotificado').prop('value') == 0) {
            resaltaElemento($('#selUsuarioNotificado'));
            bValido = false;
        }

        if ($('#selUbicacionNoti').prop('value') == 0) {
            resaltaElemento($('#selUbicacionNoti'));
            bValido = false;
        }
        if ($('#txtObservacion').val() == '' || $('#txtObservacion').val() == undefined || $('#txtObservacion').val() == null) {
            resaltaElemento($('#txtObservacion'));
            bValido = false;
        }
        if (bValido) {
            let json = {
                IdBiopsia: vIdBiopsia,
                IdUsuarioNotificado: $('#selUsuarioNotificado').val(),
                Observacion: $('#txtObservacion').val()
            };

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Biopsias_Critico`,
                data: JSON.stringify(json),
                contentType: 'application/json',
                success: function (data) {
                    $('#selUsuarioNotificado').val('0');
                    $('#txtObservacion').val('');
                    grillaNotificaciones();
                    toastr.success('Se ha enviado una nueva notificación');
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnGuardarNeoplasia').click(function (e) {
        var bValido = true;

        if ($('#selComportamiento').val() == 0) {
            resaltaElemento($('#selComportamiento'));
            bValido = false;
        }
        if ($('#selMorfologia').val() == 0) {
            resaltaElemento($('#selMorfologia'));
            bValido = false;
        }
        if ($('#selGradoDif').val() == 0) {
            resaltaElemento($('#selGradoDif'));
            bValido = false;
        }
        if ($('#selOrgano').val() == 0) {
            resaltaElemento($('#selOrgano'));
            bValido = false;
        }
        if ($('#selTopografia').val() == 0) {
            resaltaElemento($('#selTopografia'));
            bValido = false;
        }
        if ($('#selLateralidad').val() == 0) {
            resaltaElemento($('#selLateralidad'));
            bValido = false;
        }
        if (bValido) {

            let json = {
                ANA_idBiopsia: $('#txtIdBiopsia').val(),
                ANA_idComportamiento: $('#selComportamiento').val(),
                ANA_idMorfologia: $('#selMorfologia').val(),
                ANA_idGrado_Diferenciacion: $('#selGradoDif').val(),
                ANA_idOrganoCie: $('#selOrgano').val(),
                ANA_idTopografia: $('#selTopografia').val(),
                ANA_idTipo_Lateralidad: $('#selLateralidad').val(),
                ANA_idTipo_Her2: $('#selHer2').val()
            };

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${$('#txtIdBiopsia').val()}/Neoplasia`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    let parametrizacion = {
                        metodo:"POST",
                        url: `${GetWebApiUrl()}ANA_Neoplasia_Biopsia`
                    };
                    if (data != null && data != undefined) {
                        parametrizacion.metodo = "PUT"
                        parametrizacion.url += `/${data.ANA_idNeoplasia_Biopsia}`;

                        json.ANA_idNeoplasia_Biopsia = data.ANA_idNeoplasia_Biopsia;
                    }

                    $.ajax({
                        type: parametrizacion.metodo,
                        url: parametrizacion.url,
                        data: JSON.stringify(json),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            toastr.success('Se ha guardado la información de neoplasia');
                        },
                        error: function (jqXHR, status) {
                            console.log(JSON.stringify(jqXHR));
                        }
                    });



                    e.preventDefault();
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnValidarAutorizar').click(function (e) {
        var bValido = true;
        if ($('#txtDescMacro').val() == "") {
            resaltaElemento($('#txtDescMacro'));
            toastr.info('Falta Descripción Macroscopica');
            bValido = false;
        }
        if ($('#txtDiag').val() == "") {
            resaltaElemento($('#txtDiag'));
            toastr.info('Falta Descripción de Diagnostico');
            bValido = false;
        }
        if ($('#bdgListadoTecnicasCreadas').text() != $('#bdgListadoTecnicas').text()) {
            toastr.warning('Existen Técnicas Pendientes');
            bValido = false;
        }
        if ($('#bdgListadoInmunoCreadas').text() != $('#bdgListadoInmuno').text()) {
            toastr.warning('Existen Inmunos Pendientes');
            bValido = false;
        }

        if (bValido) {
            if ($('#btnValidarAutorizar').text() == 'Validar y autorizar')
                $('#alertValidar').html('Al validar este informe estará enviando las láminas a almacenamiento en forma automática');
            else
                $('#alertValidar').html('Confirme sus credenciales para realizar esta acción');

            $('#txtNombreUsuario').val('');
            $('#txtClave').val('');
            $('#mdlValidar').modal('show');
        }
        e.preventDefault();
    });

    $('#btnVerCasos').click(function (e) {
        //tblCasosPaciente
        let id = $('#btnVerCasos').data('id');
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?idPaciente=${id}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                let adataset = [];
                $.each(data, function (key, val) {
                    let stringBiopsia = ""
                    val.OrganoBiopsia.map((a, index) => {
                        stringBiopsia += a.Valor
                    })
                    adataset.push([
                        val.Id,
                        moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                        val.Numero,
                        stringBiopsia,
                        val.Estado.Valor,
                        val.FechaValidacion == null ? null : moment(val.FechaValidacion).format('DD-MM-YYYY HH:mm:ss')
                    ]);
                });

                $('#tblCasosPaciente').DataTable({
                    data: adataset,
                    order: [],
                    columnDefs:
                        [
                            {
                                targets: 0,
                                data: null,
                                searchable: false,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    botones = `<a class="btn btn-default" data-id='${adataset[fila][0]}' onclick="imprimirCaso(this)">${adataset[fila][0]}</a>`;
                                    return botones;
                                }
                            },
                        ],
                    columns: [
                        { title: 'ID biopsia' },
                        { title: 'Fecha registro' },
                        { title: 'N° biopsia' },
                        { title: 'Descripción' },
                        { title: 'Estado' },
                        { title: 'Fecha validación' }
                    ],
                    bDestroy: true
                });
            }
        });
        $('#mdlCasosPaciente').modal('show');
        e.preventDefault();
    });

    $('body').on('click', '#btnPublicacion', function (e) {
        let clave = CryptoJS.MD5($('#txtClave').val());
        const json = {
            GEN_loginUsuarios: $('#txtNombreUsuario').val(),
            GEN_claveUsuarios: clave + ''
        };

        if ($('#btnValidarAutorizar').text() == 'Validar y autorizar') {
            //guarda las descripciones por si el medico no guardo borrador.

            $.ajax({
                type: 'Patch',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Validar`,
                contentType: 'application/json',
                data: JSON.stringify(json),
                success: function (data) {

                    guardarDescripcionBiopsia();
                    $('#mdlValidar').modal('hide');
                    toastr.success('El informe se ha validado correctamente');
                    $('#btnValidarAutorizar').text('Desvalidar Informe');
                    $("#txtEstado").val("Validada y Autorizada para Publicar")
                },
                error: function (jqXHR, status) {
                    toastr.error('Los datos ingresados no permiten utilizar esta opción');
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        else {
            $.ajax({
                type: 'Patch',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Desvalidar`,
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(json),
                success: function (data) {
                    $('#mdlValidar').modal('hide');
                    $('#btnValidarAutorizar').text('Validar y autorizar');
                    toastr.warning('El informe se ha desvalidado');
                    $('#txtNombreUsuario').val("Entregada a Patologo")
                },
                error: function (jqXHR, status) {
                    toastr.error('Los datos ingresados no permiten utilizar esta opción');
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }

        e.preventDefault();
    });

    $('#btnImprimir').click(function (e) {
        /*window.open('../FormImpresion/IMP_Informe.aspx?idb=' + vIdBiopsia + '&idu=' + vIdUsuario, '_blank');*/
        e.preventDefault();
        ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Imprimir`)
    });

    $('#btnCerrarInterconsulta').click(function (e) {
        $('#mdlConfimarCerrar').modal('show');
        e.preventDefault();
    });

    $('#btnConfirmarCerrar').click(function (e) {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/CERRARINTERCONSULTA/${vIdBiopsia}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha cerrado la interconsulta');
                $('#mdlConfimarCerrar').modal('hide');
                $('#btnCerrarInterconsulta').attr('disabled', true);
            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
        e.preventDefault();
    });

    $('#btnSolicitarLamina').click(function (e) {
        var a = [];
        var t = $('#tblListadoLamina').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.checkLamina')[0].checked)
                a.push($(d[i]).find('.checkLamina')[0].id);
        if (a.length > 0) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormAnatomia/frm_Cortes.aspx/laminasSolicitarLaminas',
                data: JSON.stringify({ s: JSON.stringify(a), id_usuario: vIdUsuario, ANA_IdBiopsia: vIdBiopsia }),
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    var d = JSON.parse(data.d);
                    if (d.length > 0)//tengo los id almacenados
                    {
                        var v = '';
                        for (var i = 0; i < d.length; i++)
                            v += ', ' + d[i];
                        $('#lblModalC').text(v.substr(1));
                        $('#lblModalMensaje').text('Solo se pueden solicitar láminas que se encuentren almacenadas');
                        $('#divMensaje').show();
                        $('#mdlMensaje').modal('show');
                    }
                    else
                        toastr.success('Se han solicitados los registros seleccionados');

                    getDataLaminasApi();
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        else
            toastr.info('Debe seleccionar al menos un registro');
        e.preventDefault();
    });

    $('body').on('change', '#chkTodoLaminas', function () {
        var t = $('#tblListadoLamina').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.checkLamina')[0].checked = $(this).prop('checked');
    });
});

function imprimirCaso(e) {
    let id = $(e).data('id');
    ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Imprimir`)
}

//Obtener/Contar Adjuntos...
function ContarAdjuntos(soloContar) {
    var vID = getUrlParameter('id');
    var json;

    $.ajax({
        type: 'GET', 
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}/archivos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data !== null) {
                console.log(" data de los archivos encontrados",data)
                json = data;

                $('#bdgAdjuntos').html(json.length);

                if (!soloContar) {
                    $('#files').html('<li class="text-muted text-center empty">No hay documentos cargados.</li>');
                    for (var i = 0; i < json.length; i++) {
                        moverArchivo(json[i].Nombre, vID);

                        var template = $('#files-template').text();
                        var fn = json[i].Nombre;
                        var idf = json[i].Id;
                        //

                        fn = fn.replace(/\+/g, ' ');
                        fn = fn.replace(/°/g, ' ');
                        fn = fn.replace(/,/g, ' ');
                        fn = fn.replace(/  /g, ' ');
                        fn = fn.replace(/   /g, ' ');

                        template = template.replace('%%filename%%', fn);
                        template = $(template);

                        template.find('.progress-bar').attr('style', 'width: 100%');
                        template.find('#date-file').show();
                        //console.log(json[i].ANA_FecArchivos_Biopsias);
                        var f = moment(json[i].Fecha, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');
                        var d = moment.duration(moment(new Date()).diff(f));
                        var h = d.asHours();
                        if (h < 24)
                            template.find('.btnQuitar').show();

                        template.find('#date-file').html(f);

                        template.prop('id', idf);
                        template.data('file-id', idf);
                        $('#files').find('li.empty').fadeOut();
                        $('#files').prepend(template);
                    }
                }
            }
        },error: function (err) {
            console.error("Ha ocurrido un erro al intentar traer los archivos adjuntos")
            console.log(err)
        }
    });
}

function revisarBotones(idEstado, almacenada) {
    if (perfilAccesoSistema.codigoPerfil != perfilAccesoSistema.patologo &&
        perfilAccesoSistema.codigoPerfil != perfilAccesoSistema.administrador &&
        perfilAccesoSistema.codigoPerfil != perfilAccesoSistema.secretaria)
        window.location = "../frm_login.aspx";

    if (vANA_InterconsultaBiopsia == 'SI' && perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo)
        $('#btnCerrarInterconsulta').removeAttr('disabled');

    if (idEstado == 46 &&
        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo) {

        //Entregada a Patologo
        $('#divPlantillas').show();
        $('#btnGuardar').show();
        $('#btnPlantillaAnt').show();
        $('#btnPlantillaDescMacro').show();
        $('#btnPlantillaExamenMicro').show();
        $('#btnPlantillaDiag').show();
        $('#btnPlantillaInmuno').show();
        $('#btnPlantillaNotaAdicional').show();

        $('#btnGuardar').removeAttr('disabled');
        $('#btnValidarAutorizar').text('Validar y autorizar');
        $('#btnValidarAutorizar').removeAttr('disabled');
        $('#selNomPlantilla').removeAttr('disabled');
        $('#chkMacro').removeAttr('disabled');
        $('#chkMicro').removeAttr('disabled');
        $('#chkDiag').removeAttr('disabled');
        $('#txtAntecedentesClinicos').removeAttr('disabled');
        $('#txtDescMacro').removeAttr('disabled');
        $('#txtExamenMicro').removeAttr('disabled');
        $('#txtDiag').removeAttr('disabled');
        $('#txtInmuno').removeAttr('disabled');
        $('#txtNotaAdicional').removeAttr('disabled');
        $('#btnPlantillaAnt').removeAttr('disabled');
        $('#btnPlantillaDescMacro').removeAttr('disabled');
        $('#btnPlantillaExamenMicro').removeAttr('disabled');
        $('#btnPlantillaDiag').removeAttr('disabled');
        $('#btnPlantillaInmuno').removeAttr('disabled');
        $('#btnPlantillaNotaAdicional').removeAttr('disabled');

        if (almacenada == "SI")
            $('#btnSolicitarMacro').removeAttr('disabled');

        $('#selComportamiento').removeAttr('disabled');
        $('#selMorfologia').removeAttr('disabled');
        $('#selGradoDif').removeAttr('disabled');
        $('#selOrgano').removeAttr('disabled');
        $('#selTopografia').removeAttr('disabled');
        $('#selLateralidad').removeAttr('disabled');
        $('#selHer2').removeAttr('disabled');
        $('#btnGuardarNeoplasia').removeAttr('disabled');

        $('#txt0801001').removeAttr('disabled');
        $('#txt0801002').removeAttr('disabled');
        $('#txt0801003').removeAttr('disabled');
        $('#txt0801004').removeAttr('disabled');
        $('#txt0801005').removeAttr('disabled');
        $('#txt0801006').removeAttr('disabled');
        $('#txt0801007').removeAttr('disabled');
        $('#txt0801008').removeAttr('disabled');

        $('#btnGuardarCodificacion').show();
        $('#btnRecalcularCodificacion').show();

    } else if (idEstado == 45) {
        //Validada y Autorizada para Publicar
        if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.patologo ||
            perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador)
            $('#btnNotificarUsuario').removeAttr('disabled');
        $('#selUsuarioNotificado').removeAttr('disabled');
        $('#selUbicacionNoti').removeAttr('disabled');
        $('#txtObservacion').removeAttr('disabled');

        $('#btnValidarAutorizar').text('Desvalidar informe');
        $('#btnValidarAutorizar').removeAttr('disabled');
        var momentFechadeAyer = moment(vFECHA_ACTUAL, 'DD/MM/YYYY').add(-1, 'days');
        var momentFechaValidada = moment(vANA_fecValidaBiopsia, 'DD/MM/YYYY');
        if (momentFechadeAyer >= momentFechaValidada) {
            $('#btnGuardar').removeAttr('disabled');
        }
        else {
            //margen de un día para ingresar nuevos datos
            $('#btnGuardar').attr('disabled', true);
            $('#btnPlantillaAnt').removeAttr('disabled');
            $('#btnPlantillaDescMacro').removeAttr('disabled');
            $('#btnPlantillaExamenMicro').removeAttr('disabled');
            $('#btnPlantillaDiag').removeAttr('disabled');
            $('#btnPlantillaInmuno').removeAttr('disabled');
            $('#btnPlantillaNotaAdicional').removeAttr('disabled');
        }

        $('#btnGuardarCodificacion').hide();
        $('#btnRecalcularCodificacion').hide();
        $('#divTecnicas').hide();
        $('#divInmuno').hide();

    } else if (idEstado == 44) {
        //despachado
        $('#divTecnicas').hide();
        $('#divInmuno').hide();
        $('#btnGuardarCodificacion').hide();
        $('#btnRecalcularCodificacion').hide();
        $('#btnNotificarUsuario').hide();
        $('#divPlantillas').hide();
        $('#divAccionesCasete').hide();
        $('#btnGuardar').hide();
        $('#btnValidarAutorizar').removeAttr('disabled');
        $('#btnValidarAutorizar').text('Desvalidar');
    }
    else {
        $('#btnGuardarCodificacion').hide();
        $('#btnRecalcularCodificacion').hide();
    }

    if (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.secretaria) {
        botonesCorteSecretaria();
    }
}

function botonesCorteSecretaria() {
    $('#btnCerrarInterconsulta').hide();
    $('#btnConvertirP').hide();
    $('#btnGuardar').hide();
    $('#btnGuardarCodificacion').hide();
    $('#btnPlantillaAnt').hide();
    $('#btnPlantillaDescMacro').hide();
    $('#btnPlantillaDiag').hide();
    $('#btnPlantillaExamenMicro').hide();
    $('#btnPlantillaInmuno').hide();
    $('#btnPlantillaNotaAdicional').hide();
    $('#btnRecalcularCodificacion').attr('disabled', true);
    $('#btnSolicitarMacro').attr('disabled', true);
    $('#btnSolicitarMacro').hide();
    $('#btnValidarAutorizar').hide();
    $('#divGuardarCodificacion').hide();
    $('#divInmuno').hide();
    $('#divPlantillas').hide();
    $('#divTecnicas').hide();
    $('#divNotificacionCritico').hide();
}
function buscarProtocolosporIdPaciente(idPaciente) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}PAB_Protocolo_Operatorio/Buscar?idPaciente=${idPaciente}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            try {

                $.each(data, function (key, val) {
                    adataset.push([
                        val.Id,
                        val.Ambito.Valor,
                        val.OperacionRealizada,
                        moment(val.Fecha).format('DD/MM/YYYY'),
                        "<input type='button' value='Imprimir' class='btn btn-info' data-id='" + val.Id + "' onclick='imprimeProtocolo(" + val.Id + ")' />"
                    ]);
                });

                $('#tblProtocolo').addClass('wrap').DataTable({
                    data: adataset,
                    order: [],
                    //columnDefs: [{ sType: 'date-ukLong' }],
                    columns: [
                        { title: 'Id ' },
                        { title: 'Ambito' },
                        { title: 'Operación realizada' },
                        { title: 'Fecha de Protocolo' },
                        { title: '' }
                    ],
                    bDestroy: true
                });

                $('#bdgProtocolos').html(adataset.length);

            } catch (err) {
                console.log("error:" + err.message);
            }
        },
        error: function (jqXHR, status) {
            //toastr.error('Los datos ingresados no permiten utilizar esta opción');
            console.log(JSON.stringify(jqXHR));
        }
    });
}

function grillaNotificaciones() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/UsuariosNotificados`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                let usuarioPatologo ="No informado", usuarioNotificado ="No informado"
                if (val.Usuarios != null) {
                    if (val.Usuarios.Patologo !== null)
                        usuarioPatologo = val.Usuarios.Patologo[0].Nombre
                    if (val.Usuarios.Notificado !== null)
                        usuarioNotificado = val.Usuarios.Notificado[0].Nombre
                }


                adataset.push([
                    val.Id,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Observacion,
                    val.Leido,
                    usuarioPatologo,
                    usuarioNotificado,
                    val.Respuesta??"Sin respuesta"
                ]);
            });

            $('#tblNotificaciones').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 3, 6], visible: false, searchable: false }
                    ],
                columns: [
                    { title: 'idBiopsiaCritico' },
                    { title: 'Fecha' },
                    { title: 'Observación' },
                    { title: 'leidoBiopsiaCritico' },
                    { title: 'Patólogo' },
                    { title: 'Notificado' },
                    { title: 'respuestaBiopsiaCritico' }
                ],
                bDestroy: true
            });
            $('#bdgCritico').html(adataset.length);
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}

function cargarCombos() {
    comboPlantillas();
    //comboNotificado();
    comboComportamiento();
    comboMorfologia();
    comboGradoDif();
    comboOrgano();
    comboTopografia();
    comboLateralidad();
    comboHer2();
    comboTecnicasEspeciales();
    comboInmunoHistoquimica();
    comboUbicacionSupervisorCritico()
}

function comboPlantillas() {
    let url = `${GetWebApiUrl()}ANA_Plantillas/Combo/${$('#chkMacro').prop('checked') ? 'SI' : 'NO'}/${$('#chkMicro').prop('checked') ? 'SI' : 'NO'}/${$('#chkDiag').prop('checked') ? 'SI' : 'NO'}`;
    setCargarDataEnComboAsync(url, true, $('#selNomPlantilla'));
}

function comboNotificado(idUbicacion) {
    let selector = '#selUsuarioNotificado'
    $(selector).empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Supervisor_Critico/${parseInt(idUbicacion)}`,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {
            $(selector).append("<option value='0'> -- Seleccione --</option>");
            $.each(data, function (key, val) {
                $(selector).append(`<option value='${val.IdUsuario}'> ${val.Nombre} ${val.ApellidoPaterno} ${val.ApellidoMaterno} </option>`);
            });
        },
        error: function (jqXHR, status) {
            $(selector).append("<option value='0'> -- Seleccione --</option>");
            console.log(JSON.stringify(jqXHR));
        }
    });
}

function comboComportamiento() {
    let url = `${GetWebApiUrl()}ANA_Comportamiento/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selComportamiento'));
}

function comboMorfologia() {
    let url = `${GetWebApiUrl()}ANA_Morfologia/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selMorfologia'));
}

function comboTecnicasEspeciales() {
    let url = `${GetWebApiUrl()}ANA_Tecnicas_Especiales/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selTecnicasEspeciales'));
}

function comboInmunoHistoquimica() {
    let url = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selInmunoHistoquimica'));
}

function comboLateralidad() {
    let url = `${GetWebApiUrl()}ANA_Tipo_Lateralidad/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selLateralidad'));
}

function comboHer2() {
    let url = `${GetWebApiUrl()}ANA_Tipo_Her2/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selHer2'));
}

function comboTopografia(ANA_IdOrganoCie) {

    $('#selTopografia').empty();
    $('#selTopografia').append("<option value='0'>-Seleccione-</option>");

    if (ANA_IdOrganoCie == undefined)
        return false;

    let url = `${GetWebApiUrl()}ANA_Topografia/Combo/${ANA_IdOrganoCie}`;
    setCargarDataEnComboAsync(url, false, $('#selTopografia'));
}

function comboOrgano() {
    let url = `${GetWebApiUrl()}ANA_Organo_Cie/combo`;
    setCargarDataEnComboAsync(url, false, $('#selOrgano'));
}

function comboGradoDif() {
    let url = `${GetWebApiUrl()}ANA_Grado_Diferenciacion/combo`;
    setCargarDataEnComboAsync(url, true, $('#selGradoDif'));
}
function comboUbicacionSupervisorCritico() {
    setCargarDataEnComboAsync(`${GetWebApiUrl()}ANA_Supervisor_Critico/combo`, true, $('#selUbicacionNoti'));
}

function cargarPlantilla(idPlantilla, elem) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Plantillas/${idPlantilla}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            switch (elem) {
                case 0:
                    if (data.AntecedentesClinicos != null) {
                        $('#txtAntecedentesClinicos').val($('#txtAntecedentesClinicos').val() + ' ' + data.AntecedentesClinicos);
                    }
                    else {
                        toastr.info('No existe descripción de antecedentes clinicos');
                    }
                    break;
                case 1:
                    if (data.Macroscopia != null) {
                        $('#txtDescMacro').val($('#txtDescMacro').val() + ' ' + data.Macroscopia);
                    }
                    else {
                        toastr.info('No existe descripción de Macroscopica');
                    }
                    break;
                case 2:
                    if (data.Microscopia != null) {
                        $('#txtExamenMicro').val($('#txtExamenMicro').val() + ' ' + data.Microscopia);
                    }
                    else {
                        toastr.info('No existe descripción de Microscopica');
                    }
                    break;
                case 3:
                    if (data.Diagnostico != null) {
                        $('#txtDiag').val($('#txtDiag').val() + ' ' + data.Diagnostico);
                    }
                    else {
                        toastr.info('No existe descripción de Diagnostico');
                    }
                    break;
                case 4:
                    if (data.Inmunohistoquimica != null) {
                        $('#txtInmuno').val($('#txtInmuno').val() + ' ' + data.Inmunohistoquimica);
                    }
                    else {
                        toastr.info('No existe descripción de InmunoHistoquimica');
                    }
                    break;
                case 5:
                    if (data.Nota != null) {
                        $('#txtNotaAdicional').val($('#txtNotaAdicional').val() + ' ' + data.ANA_desc_notaPlantilla);
                    }
                    else {
                        toastr.info('No existe descripción de Notas Adicionales');
                    }
                    break;
            }
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });

}

function cargarNeoplastia(id) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Neoplasia`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data != null) {
                $('#selComportamiento').val(valCampo(data.ANA_idComportamiento));
                $('#selMorfologia').val(valCampo(data.ANA_idMorfologia));
                $('#selGradoDif').val(valCampo(data.ANA_idGrado_Diferenciacion));
                $('#selOrgano').val(valCampo(data.ANA_idOrganoCie));

                comboTopografia(data.ANA_idOrganoCie);
                $('#selTopografia').val(valCampo(data.ANA_idTopografia));
                $('#selLateralidad').val(valCampo(data.ANA_idTipo_Lateralidad));
                $('#selHer2').val(valCampo(data.ANA_idTipo_Her2));
                vIdNeoplasia = data.ANA_idNeoplasia_Biopsia;
            }
            
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });

}

function cargarBiopsiaDesdeDatos() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            console.log(data)
            ///////////////////////////////////////
            ////////// Datos de Paciente //////////
            ///////////////////////////////////////
            $('#txtFicha').val(data.Paciente.Nui);
            $('#txtRut').val(data.Paciente.NumeroDocumento);

            let nombrePaciente = `${data.Paciente.Nombre} ${data.Paciente.ApellidoPaterno} ${data.Paciente.ApellidoMaterno}`;
            $('#txtNombre').val(nombrePaciente);
            $('#txtFechaNac').val(moment(data.Paciente.FechaNacimiento).format("DD/MM/YYYY"));

            $('#txtEdad').val(data.Paciente.Edad.edad);
            ///////////////////////////////////////////////
            ////////// Datos de Registro Muestra //////////
            ///////////////////////////////////////////////

            $('#txtIdBiopsia').val(data.Id);
            $('#txtNumero').val(`${data.Numero}-${data.Folio}`);
            $('#txtRecepcion').val(moment(data.FechaRecepcion).format('DD/MM/YYYY'));
            $('#txtServicioOrigen').val(`${data.Origen.Dependencia} - ${data.Origen.Valor}`);
            $('#txtServicioDestino').val(`${data.Destino.Dependencia} - ${data.Destino.Valor}`);
            $('#txtOrgano').val(data.ANA_organoBiopsia);

            const medicoSolicita = `${data.Medico.Persona.Nombre} ${data.Medico.Persona.ApellidoPaterno} ${data.Medico.Persona.ApellidoMaterno}`
            $('#txtSolicitado').val(medicoSolicita);

            $('#txtEstado').val(data.Estado.Valor);

            if (data.ANA_fecValidaBiopsia != null)
                $('#txtFechaInforme').val(moment(data.ANA_fecValidaBiopsia).format('DD/MM/YYYY HH:mm:ss'));

            $('#btnSolicitarMacro').attr('data-id', vIdBiopsia);
            $('#btnVerCasos').attr('data-id', data.Paciente.IdPaciente);
            
            if (data.Usuarios !== null && data.Usuarios!==undefined)
                
                if (data.Usuarios.Validador != undefined) {
                    const nombreValidador = `${data.Usuarios.Validador.Nombre} ${data.Usuarios.Validador.ApellidoPaterno} ${data.Usuarios.Validador.ApellidoMaterno}`
                    $('#txtValidadoPor').val(nombreValidador);
                }

            if (data.Descripcion != null) {
                $('#txtAntecedentesClinicos').val(data.Descripcion.Antecedentes);
                $('#txtDescMacro').val(data.Descripcion.Macroscopia);
                $('#txtExamenMicro').val(data.Descripcion.Microscopia);
                $('#txtDiag').val(data.Descripcion.Diagnostico);
                $('#txtInmuno').val(data.Descripcion.Inmuno);
                $('#txtNotaAdicional').val(data.Descripcion.Nota);
            }

            if (data.Codificacion != null) {

                $('#txt0801001, #txt0801002, #txt0801003, #txt0801004, #txt0801005, #txt0801006, #txt0801007, #txt0801008').val('0');

                if (data.Codificacion.Cod001 > 0)
                    $('#txt0801001').val(data.Codificacion.Cod001);
                if (data.Codificacion.Cod002 > 0)
                    $('#txt0801002').val(data.Codificacion.Cod002);
                if (data.Codificacion.Cod003 > 0)
                    $('#txt0801003').val(data.Codificacion.Cod003);
                if (data.Codificacion.Cod004 > 0)
                    $('#txt0801004').val(data.Codificacion.Cod004);
                if (data.Codificacion.Cod005 > 0)
                    $('#txt0801005').val(data.Codificacion.Cod005);
                if (data.Codificacion.Cod006 > 0)
                    $('#txt0801006').val(data.Codificacion.Cod006);
                if (data.Codificacion.Cod007 > 0)
                    $('#txt0801007').val(data.Codificacion.Cod007);
                if (data.Codificacion.Cod008 > 0)
                    $('#txt0801008').val(data.Codificacion.Cod008);

            }
            vANA_fecValidaBiopsia = data.ANA_fecValidaBiopsia;
            vANA_InterconsultaBiopsia = data.ANA_interconsultaBiopsia;

            //CARGA INFORMACIÓN ADICIONAL DE LA BIOPSIA
            cargarGrillasInforme(vIdBiopsia, data.Paciente.IdPaciente);

            cargarNeoplastia(vIdBiopsia);

            //HABILITA BOTONES SEGÚN PERFIL
            revisarBotones(data.Estado.Id, data.Almacenada)

        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });

    function cargarGrillasInforme(idBiopsia, idPaciente) {
        getDataCortesApi(idBiopsia);
        getDataLaminasApi(idBiopsia);
        getDataInmunoApi(idBiopsia);
        getDataTecnicasApi(idBiopsia);
        grillaNotificaciones(idBiopsia);
        buscarProtocolosporIdPaciente(idPaciente);
    }
}

function imprimeProtocolo(id) {
    //obtiene usuario y contraseña
    var login;
    var pass;
    var json = GetLoginPass();
    login = json.login;
    pass = json.pass;

    //concatena el parametro a enviar al sistema de protocolo
    txt = login + '|' + pass + '|' + id;
    var txtEncrypt = GetTextoEncriptado(txt);
    window.open(`${ObtenerHost()}/Clases/MetodosGenerales.ashx?method=ObtenerApiPdf&id=${id}`, '_blank');
    return false;
}

function guardarDescripcionBiopsia() {
    const json = {
        ANA_idBiopsia: vIdBiopsia,
        ANA_descAntClinicos: $('#txtAntecedentesClinicos').val(),
        ANA_descMacroscopica: $('#txtDescMacro').val(),
        ANA_descMicroscopica: $('#txtExamenMicro').val(),
        ANA_descDiagnostico: $('#txtDiag').val(),
        ANA_estuInmuno: $('#txtInmuno').val(),
        ANA_descNota: $('#txtNotaAdicional').val()
    };

    let vMetodo;
    let vUrl = `${GetWebApiUrl()}ANA_Descripcion_biopsia`;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Descripcion`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if ($.isEmptyObject(data)) {
                vMetodo = 'POST';
            }
            else {
                vMetodo = 'PUT';
                vUrl = `${vUrl}/${data[0].id}`;
                json.ANA_idDescripcion = data[0].id;
            }
            $.ajax({
                type: vMetodo,
                url: vUrl,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la descripción del caso');
                    else
                        toastr.success('Se ha ingresado la descripción del caso');
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}
function moverArchivo(nombreArchivo, idBiopsia) {
    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Clases/MetodosGenerales.ashx?method=MoverArchivo&nombreArchivo=" + nombreArchivo + "&idBiopsia=" + idBiopsia,
        dataType: 'json',
        success: function (data) {

        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}
function eliminarArchivo(nombreArchivo, idBiopsia) {
    $.ajax({
        type: "POST",
        url: ObtenerHost() + "/Clases/MetodosGenerales.ashx?method=EliminarArchivo&nombreArchivo=" + nombreArchivo + "&idBiopsia=" + idBiopsia,
        dataType: 'json',
        success: function (data) {

        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}