﻿$(document).ready(function () {
    comboPatologo();

    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));

    $('#btnBuscar').click(function (e) {
        grillaCritico();
        e.preventDefault();
    });

    grillaCritico();
});
function comboPatologo() {
    let url = `${GetWebApiUrl()}/GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));
}

function linkDetalle(e)
{
    let id = $(e).data('id');
    $.ajax({
        type: 'POST',
        url: ObtenerHost() + '/FormInformes/frm_BiopsiasCriticas.aspx/detalleBiopsia',
        data: JSON.stringify({ idBiopsiaCritico: id }),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            $('#txtUsuarioNotificado').val(json.usuarioNotificado);
            $('#txtFechaNotificado').val(moment(json.fechaNotificacion, 'DD/MM/YYYY').format('YYYY-MM-DD'));
            $('#txtDiagnostico').val(json.diagnostico);
            $('#txtRespuestaNotificacion').val(json.respuestaNotificacion);
            $('#txtNombrePatologo').val(json.nombrePatologo);
        }
    });
    $('#mdlDetalle').modal('show');
}

function grillaCritico()
{
    $.ajax({
        type: 'POST',
        url: ObtenerHost() + '/FormInformes/frm_BiopsiasCriticas.aspx/grillaCriticos',
        data: JSON.stringify({ fechaInicio: $('#txtDesde').val(), fechaFin: $('#txtHasta').val(), idPatologo: $('#selPatologo').val(), visto: $('#selVisto').val() }),
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            var json = JSON.parse(data.d);
            
            $.each(json, function (key, val) {
                adataset.push([
                    val.ANA_IdBiopsias_Critico,
                    val.NomPaciente,
                    val.RutPaciente,
                    val.n_biopsia,
                    (moment(val.ANA_fecValidaBiopsia, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm:ss')),
                    val.ANA_FechaBiopsias_Critico,
                    val.ANA_DescDiagnostico,
                    val.DiasNotificacion,
                    val.NomPatologo,
                    val.ANA_FechaLeidoBiopsias_Critico,
                    val.ANA_RespuestaBiopsias_Critico,
                    val.UsuarioNotificado,
                    ''
                ]);
            });

            if (adataset.length == 0)
                $('#btnExportar').attr('disabled', true);
            else
                $('#btnExportar').removeAttr('disabled');

            $('#tblCritico').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [
                    { targets: 4, sType: 'date-ukLong' },
                    { targets: [0, 5, 6,/* 8, */10, 11], visible: false, searchable: false },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones;
                            botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkDetalle(this)"><span class="glyphicon glyphicon-list"></span></a>';
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'ANA_IdBiopsias_Critico' },
                    { title: 'Nombre paciente' },
                    { title: 'Rut' },
                    { title: 'N° de examen' },
                    { title: 'Fecha validación' },
                    { title: 'Fecha notificación' },
                    { title: 'ANA_DescDiagnostico' },
                    { title: 'Días not.' },
                    { title: 'Patólogo' },
                    { title: 'Fecha de lectura' },
                    { title: 'ANA_RespuestaBiopsias_Critico' },
                    { title: 'UsuarioNotificado' },
                    { title: 'Detalle' }
                ],
                bDestroy: true
            });
        }
    });
}