﻿let fechaActual, fechaAutomatica

$(document).ready(function () {
    $("#chkTodos").attr('checked', true)

    fechaActual = moment().format("YYYY-MM-DD")
    fechaAutomatica = moment(fechaActual).subtract(1, 'month').format("YYYY-MM-DD");

    $("#fechaInicio").val(fechaAutomatica)
    $("#fechaTermino").val(fechaActual)
    $("#fechaTermino").attr('max', fechaActual)

    cargarCombos()
    reiniciarCombo()
    $("#sltTipoMuestra").attr('disabled', true)
    $("#sltDetalleMuestra").attr('disabled', true)

    $("#chkTodos").on('click', function () {
        if (this.checked) {

            $("#sltTipoMuestra").val(0)

            $("#sltDetalleMuestra").val(0)
            $("#sltTipoMuestra").attr('disabled', true)
            $("#sltDetalleMuestra").attr('disabled', true)
            reiniciarCombo()
        } else {
            $("#sltTipoMuestra").attr('disabled', false)
            $("#sltDetalleMuestra").attr('disabled', false)
        }
    })
    $("#sltTipoMuestra").on('change', function () {

        if ($(this).val() !== 0 && $(this).val() !== '0') {
            cargarComboDetalleMuestra($(this).val())
        } else {
            reiniciarCombo()
            $(this).val(0)
        }
    })
    //Trabajandoaca


    $("#btnBuscar").on('click', function () {
        consultarInfo()
    })
});

function cargarCombos() {
    cargarComboTipoMuestra()
}

function cargarComboTipoMuestra() {
    let url = GetWebApiUrl() + "ANA_Catalogo_Muestras/Combo"
    setCargarDataEnComboAsync(url, true, "#sltTipoMuestra")
}
function cargarComboDetalleMuestra(id) {
    let url = GetWebApiUrl() + `ANA_Catalogo_Muestras/${id}/Detalle/combo`
    setCargarDataEnComboAsync(url, true, "#sltDetalleMuestra")
}
function reiniciarCombo() {
    $("#sltDetalleMuestra").empty()
    $("#sltDetalleMuestra").append("<option val=0>--Seleccione--</option>")
}

async function consultarInfo() {
    //Validacion de campos vacios(fechas)
    if ($("#fechaInicio").val() !== "" && $("#fechaTermino").val() !== "") {
        let url = GetWebApiUrl() + 'ANA_Registro_Biopsias/Buscar?recepcionDesde=' + $("#fechaInicio").val()
            + '&recepcionHasta=' + $("#fechaTermino").val()

        if (!$("#chkTodos").is(':checked')) {
            //Validar los selects si no realiza una busqueda general (todos)
            if ($("#sltTipoMuestra").val() !== '0' && $("#sltDetalleMuestra").val() !== '0') {
                //Concatena los demas detalles de busqueda
                url += '&idCatalogo=' + $("#sltTipoMuestra").val() + '&idDetalleCatalogo=' + $("#sltDetalleMuestra").val()
            } else {
                toastr.error("Debe seleccionar tipo y detalle de muestra para continuar con la busqueda")
                return false
            }
        }
        console.log(url)
        let dataset = []
        await $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json",            
            success: function (data) {
                
                dataset = data.map(a => {
                    return [
                        a.Id,
                        a.Numero,
                        a.OrganoBiopsia[0].Catalogo.Valor,
                        a.OrganoBiopsia[0].Catalogo.Detalle.Valor,
                        a.Cortes.length,
                        a.Laminas.length,
                        a.Tecnicas.length,
                        a.Inmunos.length
                    ]
                })
                //llenarGrilla(dataset, "#tableBiopsias")

            },
            error: function (err) {
                console.log("No se ha encontrado data")
            }
        });
        
        llenarGrilla(dataset, "#tableBiopsias")

    } else {
        toastr.error("Debe ingresar un rango de fechas para realizar busqueda")
    }


}

function llenarGrilla(dataset, table) {
    $(table).addClass("no-wrap table table-hover").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',
                exportOptions: {
                    // Personalizar las opciones de exportación
                    // Como por ejemplo: exportación personalizada de esas columnas y filas
                    //TODO...
                    fileName: fechaActual + "-InformeTipoMuestra"
                }
            }
        ],
        "pageLength": 10,
        "order": [[0, "desc"]],
        data: dataset,
        columns: [
            { title: "Id" },
            { title: "Numero Biopsia" },
            { title: "Catalogo muestras" },
            { title: "Detalle catalogo" },
            { title: "Cortes" },
            { title: "Laminas" },
            { title: "Tecnicas", class: "overflow-auto" },
            { title: "Inmuno", class: "text-center" },
            { title: "", class: "text-center" },
        ],
        columnDefs: [
            { targets: 0, visible: false },
            {
                targets: -1,
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    //La variable data es la fila actual
                    var fila = meta.row;

                    var botones = `
                        <a id='' class='btn btn-info btn-circle' href='../FormAnatomia/frm_cortes.aspx?id=${data[0]}'>
                           <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a><br>
                    `;
                    return botones;
                }
            }
        ],
        "bDestroy": true
    })
}