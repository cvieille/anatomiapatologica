﻿$(document).ready(function () {
    getBiopsiasApi();
    getCortespi();
    getLaminasApi();
});

///Llamados a la api///
function getCortespi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getCortesPorEstado(data);
        }
    });
}

function getBiopsiasApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getBiopsiasPorEstado(data);
        }
    });
}

function getLaminasApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
              getLaminasPorEstado(data);
        }
    });
}
/// Fin de llamados a la api///

function getBiopsiasPorEstado(data) {
    const estados = [42, 43, 47, 46, 45];

    const table = document.querySelector('#tblResumenBiopsias');
    const header = `
                    <tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    </tr>
                    `;
    table.innerHTML = header;

    estados.forEach(function (key, i) {
        const registros = data.filter(d => d.Estado.Id == key);
        if (registros.length > 0) {
            let estadoActual = registros[0].Estado.Valor;
            const row = `
                        <tr>
                          <td>${estadoActual}</td>
                          <td>${registros.length}</td>
                          <td class="text-center">
                            <button 
                              id="aBiopsiaDashboard_${i}"
                              type="button"
                              onclick="getDetalleBiopsiaApi(0, ${key})" 
                              class="btn btn-info">
                              Ver
                            </button>
                          </td>
                        </tr>
                      `;
            table.innerHTML += row;
        }
    });
}

function getDetalleBiopsiaApi(id, idEstado) {
    let parametros = "";
    let urlApi;
    if (idEstado > 0) {
        parametros = `idTipoEstadoSistema=${idEstado}`;
        urlApi = `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?${parametros}`;
    }
    else {
        urlApi = `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`;
    }

    $.ajax({
        type: 'GET',
        url: urlApi,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                let stringBiopsia = ""
                val.OrganoBiopsia.map(a => {
                    stringBiopsia += a.Valor + " "
                })
                adataset.push([
                    val.Id,
                    val.Estado.Id,
                    val.Numero,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.Patologo != null ? val.Patologo.Login : '',
                    stringBiopsia,
                    val.TipoBiopsia != null ? val.TipoBiopsia.Valor : "N/A",
                    val.Estado.Valor
                ]);
            });
            reiniciarDatatable("#tblDetalle");
            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                    ],
                columns: [
                    { title: 'Id biopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'N°' },
                    { title: 'Recepción' },
                    { title: 'Patologo' },
                    { title: 'Órgano' },
                    { title: 'Tipo Biopsia' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}


function getCortesPorEstado(data) {
    
    const estados = [50, 56, 53, 60, 54, 55, 59, 57, 61, 51];

    const table = document.querySelector('#tblResumenCortes');
    const header = `
                    <tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    </tr>
                    `;
    table.innerHTML = header;

    estados.forEach(function (key, i) {
        const registros = data.filter(d => d.Estado.Id == key);
        if (registros.length > 0) {
            let estadoActual = registros[0].Estado.Valor;
            const row = `
                        <tr>
                          <td>${estadoActual}</td>
                          <td>${registros.length}</td>
                          <td class="text-center">
                            <button 
                              id="aCorteDashboard_${i}"
                              type="button"
                              onclick="getDetalleCortesApi(${key})" 
                              class="btn btn-info">
                              Ver
                            </button>
                          </td>
                        </tr>
                      `;
            table.innerHTML += row;
        }
    });

}

function getDetalleCortesApi(idEstado) {
    let parametros = "";
    let urlApi;
    if (idEstado > 0) {
        parametros = `idTipoEstadoSistema=${idEstado}`;
        urlApi = `${GetWebApiUrl()}/ANA_Cortes_Muestras/Buscar?${parametros}`;
    }
    $.ajax({
        type: 'GET',
        url: urlApi,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdBiopsia,
                    val.Patologo.Nombre + " " + val.Patologo.ApellidoPaterno + " " + val.Patologo.ApellidoMaterno,
                    val.Patologo.Login,
                    moment(val.FechaCorte).format('DD-MM-YYYY'),
                    val.TipoEstadoSistema.Valor
                ]);
            });
            reiniciarDatatable("#tblDetalle");

            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                    ],
                columns: [
                    { title: 'id biopsia' },
                    { title: 'Id Corte' },
                    { title: 'Nombre' },
                    { title: 'Patologo' },
                    { title: 'Fecha Corte' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}
function getLaminasPorEstado(data) {
    const estados = [42, 43, 47, 46, 45];

    const table = document.querySelector('#tblResumenLaminas');
    const header = `
                    <tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    </tr>
                    `;
    table.innerHTML = header;

    estados.forEach(function (key, i) {
        const registros = data.filter(d => d.Estado.Id == key);
        if (registros.length > 0) {
            let estadoActual = registros[0].Estado.Valor;
            const row = `
                        <tr>
                          <td>${estadoActual}</td>
                          <td>${registros.length}</td>
                          <td class="text-center">
                            <button 
                              id="aBiopsiaDashboard_${i}"
                              type="button"
                              onclick="getDetalleLaminasApi(${key})"
                              class="btn btn-info">
                              Ver
                            </button>
                          </td>
                        </tr>
                      `;
            table.innerHTML += row;
        }
    });
}
function getDetalleLaminasApi(idEstado) {
    let parametros = "";
    let urlApi;
    if (idEstado > 0) {
        parametros = `idTipoEstadoSistema=${idEstado}`;
        urlApi = `${GetWebApiUrl()}/ANA_Laminas/Buscar?${parametros}`;
    }
    $.ajax({
        type: 'GET',
        url: urlApi,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdBiopsia,
                    val.Patologo!=null?val.Patologo.Nombre + " " + val.Patologo.ApellidoPaterno + " " + val.Patologo.ApellidoMaterno:"No informado",
                    val.Patologo!==null?val.Patologo.Login:"No informado",
                    moment(val.FechaCorte).format('DD-MM-YYYY'),
                    val.Estado.Valor
                ]);
            });
            reiniciarDatatable("#tblDetalle");

            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                    ],
                columns: [
                    { title: 'id biopsia' },
                    { title: 'Id Corte' },
                    { title: 'Nombre' },
                    { title: 'Patologo' },
                    { title: 'Fecha Corte' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}

