﻿$(document).ready(function () {

    grillaPlantillas();

    $('#btnNuevoRegistro').click(function (e) {
        let url = 'frm_plantilla.aspx?id=0';
        location.href = url;
        e.preventDefault();
    });    
});

function lnkInactivarPlantilla(e)
{
    const id = $(e).data("id");
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Plantillas/${id}/Anular`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha inactivado la plantilla');
            grillaPlantillas();
        }
    });
}

function lnkActivarPlantilla(e) {
    const id = $(e).data("id");
    //como se vaya a llamar el controlador
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Plantillas/Activar/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha activado la plantilla');
            grillaPlantillas();
        }
    });
}

function lnkVerPlantilla(e)
{
    let id = $(e).data("id");
    let url = `frm_plantilla.aspx?id=${id}`;
    location.href = url;
}

async function grillaPlantillas() {
    let adataset = []

    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Plantillas`,
        contentType: 'application/json',
        dataType: 'json',
        async: true,
        success: function (data) {
            adataset = data.map(item => {
                return [
                    item.Id,
                    item.Nombre,
                    moment(item.FechaCreacion).format('DD/MM/YYYY HH:mm:ss'),                    
                    item.Estado==true?"Activo":"Inactivo",
                    '<input type="button" value="Ver" id="btnVerPlantilla" class="btn btn-info" data-id="' + item.Id + '" onclick="lnkVerPlantilla(this)" />',
                    ''
                ]
            })
        }, error: function (err) {
            console.error("Ha ocurrido un error al intentar listtar las plantillas")
            console.log(err)
        }
    });
    $('#tblPlantillas').DataTable({
        data: adataset,
        order: [],
        stateSave: true,
        iStateDuration: 60,
        columnDefs: [
            { targets: 2, sType: 'date-ukLong' },
            {
                targets: -1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    var botones;
                    if (adataset[fila][3] == 'Activo')
                        botones = '<input type="button" value="Inactivar" class="btn btn-danger" data-id="' + adataset[fila][0] + '" onclick="lnkInactivarPlantilla(this)" />';
                    else
                        botones = '<input type="button" value="Activar" class="btn btn-success" data-id="' + adataset[fila][0] + '" onclick="lnkActivarPlantilla(this)" />'
                    return botones;
                }
            },
        ],
        columns: [
            { title: 'N° plantilla' },
            { title: 'Nombre' },
            { title: 'Fecha de creación' },
            { title: 'Estado' },
            { title: '' },
            { title: '' }
        ],
        bDestroy: true
    });
}