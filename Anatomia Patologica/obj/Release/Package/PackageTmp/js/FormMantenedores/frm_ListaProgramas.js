﻿$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    tablaProgramas();

    $('#btnNuevoRegistro').click(function (e) {
        modalEditar(0);
        e.preventDefault();
    });

    $('#btnGuardarPrograma').click(function (e) {
        var bValido = true;
        if ($('#txtNombrePrograma').val() == '' || $('#txtNombrePrograma').val() == null) {
            resaltaElemento($('#txtNombrePrograma'));
            bValido = false;
        }       

        if (bValido) {
            var m = 'POST';
            var u = `${GetWebApiUrl()}GEN_Programa`;
            let json = {
                Nombre : $('#txtNombrePrograma').val(),                
            };
            if ($('#txtIdPrograma').val() != '0') {
                m = 'PUT';
                u = `${GetWebApiUrl()}GEN_Programa/${$('#txtIdPrograma').val()}`;
                json.Id = $('#txtIdPrograma').val();
            }

            $.ajax({
                type: m,
                url: u,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado el programa');
                    else
                        toastr.success('Se ha creado el programa');
                    tablaProgramas();
                    $('#mdlEditarPrograma').modal('hide');
                }
            });
        }

        e.preventDefault();
    });
});

function modalEditar(id) {
    if (id != 0) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Programa/${id}`,
            contentType: 'application/json',
            dataType: 'json',            
            success: function (data) {
                $('#txtIdPrograma').val(data[0].Id);
                $('#txtNombrePrograma').val(data[0].Nombre);
            }
        });
        $('#selEstadoPrograma').removeAttr('disabled');
    }
    else {
        $('#txtIdPrograma').val('0');
        $('#txtNombrePrograma').val('');
        $('#selEstadoPrograma').attr('disabled', true);
        $('#selEstadoPrograma').val('Activo');
    }
    $('#txtNombrePrograma').removeClass('_bordeError');
    $('#selEstadoPrograma').removeClass('_bordeError');
    $('#mdlEditarPrograma').modal('show');
}

function tablaProgramas() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Programa/Buscar`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Nombre,
                    val.Activo == true ? "SI" : "NO",
                    ''
                ]);
            });
            $('#tblProgramas').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a onclick="modalEditar(' + adataset[fila][0] + ')" class="btn btn-info">Editar</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ID' },
                    { title: 'Descripción' },
                    { title: 'Activo' },
                    { title: 'Editar' },
                ],
                bDestroy: true
            });
        }
    });
}