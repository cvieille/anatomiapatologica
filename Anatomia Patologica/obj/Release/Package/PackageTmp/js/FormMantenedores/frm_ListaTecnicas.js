﻿var vIdTecnica;
$(document).ready(function () {      
    $('#btnNuevoRegistro').click(function (e) {
        vIdTecnica = 0;
        $('#txtIdTecnica').val('0');
        $('#txtNombreTecnica').val('');
        $('#selEstadoTecnica').prop('value', '0');

        $('#selEstadoTecnica').removeClass('_bordeError');
        $('#txtNombreTecnica').removeClass('_bordeError');

        $('#mdlTecnicas').modal('show');
        e.preventDefault();
    });

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('body').on('click', '#btnGuardarTecnica', function (e) {
        var bValido = true;

        if ($('#selEstadoTecnica').prop('value') == '0') {
            resaltaElemento($('#selEstadoTecnica'));
            bValido = false;
        }
        if ($('#txtNombreTecnica').val() == '' || $('#txtNombreTecnica').val() == undefined || $('#txtNombreTecnica').val() == null) {
            resaltaElemento($('#txtNombreTecnica'));
            bValido = false;
        }
        if (bValido)
        {
            let json = {};
            json.ANA_nomTecnica = $('#txtNombreTecnica').val();
            json.ANA_estadoTecnica = $('#selEstadoTecnica').val();

            var vMetodo;
            var vUrl = `${GetWebApiUrl()}ANA_Tecnicas_Especiales/${vIdTecnica}`
            if (vIdTecnica == 0) {
                vMetodo = 'POST';
                vUrl = `${GetWebApiUrl()}ANA_Tecnicas_Especiales`
            }
            else {
                vMetodo = 'PUT';
                json.ANA_idTecnica = vIdTecnica;
            }
            console.log(JSON.stringify(json))
            $.ajax({
                type: vMetodo,
                url: vUrl,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la técnica');
                    else
                        toastr.success('Se ha creado la técnica');
                    getDataTecnicasApi();
                    $('#mdlTecnicas').modal('hide');
                }
            });
        }
        e.preventDefault();
    });
    getDataTecnicasApi();
});

function lnkEditarTecnica(e) {
    let id = $(e).data("id");
    vIdTecnica = id;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Tecnicas_Especiales/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#txtIdTecnica').val(data.ANA_idTecnica);
            $('#txtNombreTecnica').val(data.ANA_nomTecnica);
            $('#selEstadoTecnica').val(data.ANA_estadoTecnica.trim());

            $('#selEstadoTecnica').removeClass('_bordeError');
            $('#txtNombreTecnica').removeClass('_bordeError');
        }
    });
    
    $('#mdlTecnicas').modal('show');
}

async function getDataTecnicasApi() {
    let adataset =[]
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Tecnicas_Especiales`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            console.info(data)
            adataset = data.map(item => {
                return [
                    item.ANA_idTecnica,
                    item.ANA_nomTecnica,
                    item.ANA_estadoTecnica,
                    ''
                ]
            })
        },
        error: function (err) {
            console.error("Ha ocurrido un error a buscar las tecnicas especiales")
            console.log(err)
            console.log(JSON.stringify(err))
        }
    });
    $('#tblTecnicas').DataTable({
        data: adataset,
        order: [],
        stateSave: true,
        iStateDuration: 60,
        columnDefs: [
            {
                targets: -1,
                data: null,
                orderable: false,
                render: function (data, type, row, meta) {
                    let fila = meta.row;
                    var botones = '<input type="button" value="Editar técnica" class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="lnkEditarTecnica(this)" />';
                    return botones;
                }
            },
        ],
        columns: [
            { title: 'ID técnica' },
            { title: 'Nombre técnica' },
            { title: 'Estado' },
            { title: '' }
        ],
        bDestroy: true
    });
}