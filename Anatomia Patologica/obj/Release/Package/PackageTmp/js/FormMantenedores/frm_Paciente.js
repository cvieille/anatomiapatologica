﻿var idPaciente;
$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    comboPaciente();
    comboSexo();
    comboPais();

    $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));

    $('#selPais').change(function (e) {
        comboRegion($(this).val(), 0);
    });
    $('#selRegion').change(function (e) {
        comboProvincia($(this).val(), 0);
    });
    $('#selProvincia').change(function (e) {
        comboCiudad($(this).val(), 0);
    });
    $('#txtNacimiento').on('change', function (e) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/CalcularEdad?GEN_fec_nacimientoPaciente=${$('#txtNacimiento').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                $('#txtEdad').val(data.edad);
            }
        });
    });

    $('#selTipoDocumento').change(function (e) {
        cargarPaciente();
        if ($(this).val() == '2' || $(this).val() == '3')
            $('#txtRut').attr('maxlength', 10);
        else {
            $('#txtRut').attr('maxlength', 8).val($('#txtRut').val().substring(0, 8));
        }
    });

    $('#txtRut').blur(function (e) {
        cargarPaciente();
    });

    $('#btnGuardar').click(function (e) {
        let bValido = true;

        $('#selEstado').removeClass('_bordeError');
        $('#txtRut').removeClass('_bordeError');
        $('#txtNombre').removeClass('_bordeError');
        $('#txtApellidoP').removeClass('_bordeError');
        $('#txtDireccion').removeClass('_bordeError');
        $('#txtNumero').removeClass('_bordeError');
        $('#txtTel').removeClass('_bordeError');
        $('#selSexo').removeClass('_bordeError');
        $('#selPais').removeClass('_bordeError');
        $('#selRegion').removeClass('_bordeError');
        $('#selProvincia').removeClass('_bordeError');
        $('#selCiudad').removeClass('_bordeError');

        if ($('#selTipoDocumento').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoDocumento'));
        }

        if ($('#txtRut').val().length < 6) {
            bValido = false;
            resaltaElemento($('#txtRut'));
        }
        if ($('#selEstado')[0].hasAttribute('disabled')) {
            if ($('#selEstado').val() == '0') {
                bValido = false;
                resaltaElemento($('#selEstado'));
            }
        }
        if ($('#txtNombre').val() == '' || $('#txtNombre').val() == null || $('#txtNombre').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtNombre'));
        }
        if ($('#txtApellidoP').val() == '' || $('#txtApellidoP').val() == null || $('#txtApellidoP').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtApellidoP'));
        }
        if ($('#txtDireccion').val() == '' || $('#txtDireccion').val() == null || $('#txtDireccion').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtDireccion'));
        }
        if ($('#txtNumero').val() == '' || $('#txtNumero').val() == null || $('#txtNumero').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtNumero'));
        }
        if ($('#txtTel').val() == '' || $('#txtTel').val() == null || $('#txtTel').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtTel'));
        }
        if ($('#selSexo').val() == '0') {
            bValido = false;
            resaltaElemento($('#selSexo'));
        }
        const fechaHoy = moment().format('YYYY-MM-DD');
        const fechaNac = moment($('#txtNacimiento').val()).format('YYYY-MM-DD');
        if (fechaHoy < fechaNac) {
            bValido = false;
            resaltaElemento($('#txtNacimiento'));
        }
        if ($('#selPais').val() == '0') {
            bValido = false;
            resaltaElemento($('#selPais'));
        }
        if ($('#selRegion option').length > 1 && $('#selRegion').val() == '0') {
            bValido = false;
            resaltaElemento($('#selRegion'));
        }

        if ($('#selProvincia').val() === "0") {            
            bValido = false;
            resaltaElemento($('#selProvincia'));
        }
        if ( $('#selCiudad').val() === "0") {
            bValido = false;
            resaltaElemento($('#selCiudad'));
        }

        
        if (bValido) {
            const json = {
                GEN_idPaciente: idPaciente,
                GEN_numero_documentoPaciente: $('#txtRut').val(),
                GEN_digitoPaciente: $('#txtDigito').html(),
                GEN_nombrePaciente: $('#txtNombre').val(),
                GEN_ape_paternoPaciente: $('#txtApellidoP').val(),
                GEN_ape_maternoPaciente: $('#txtApellidoM').val() == '' ? null : $('#txtApellidoM').val(),
                GEN_dir_callePaciente: $('#txtDireccion').val(),
                GEN_dir_numeroPaciente: $('#txtNumero').val(),
                GEN_dir_ruralidadPaciente: 'NO',
                GEN_idPais: $('#selPais').val(),
                GEN_idRegion: $('#selRegion').val(),
                GEN_idProvincia: $('#selProvincia').val(),

                GEN_idCiudad: $('#selCiudad').val(),
                GEN_telefonoPaciente: $('#txtTel').val(),
                GEN_idSexo: $('#selSexo').val(),
                GEN_fec_nacimientoPaciente: $('#txtNacimiento').val(),
                GEN_otros_fonosPaciente: $('#txtOtrosNumeros').val() == '' ? null : $('#txtOtrosNumeros').val(),
                GEN_nuiPaciente: $('#txtNui').val(),
                GEN_idIdentificacion: $('#selTipoDocumento').val(),
                GEN_estadoPaciente: $('#selEstado').val()
            };

            let parametrizacion = {
                tipo: 'POST',
                url: `${GetWebApiUrl()}GEN_Paciente`
            }
            if (idPaciente > 0) {
                parametrizacion.tipo = 'PUT';
                parametrizacion.url = `${parametrizacion.url}/${idPaciente}`;
            }

            $.ajax({
                type: parametrizacion.tipo,
                url: parametrizacion.url,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                   toastr.success("Se ha actualizado la información")
                    window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $(document).keypress(function (e) {
        if (e.which == '13') {
            e.preventDefault();
        }
    });

    $('#btnVolver').click(function (e) {
        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
        e.preventDefault();
    });
});

function cargarPaciente() {
    if ($('#txtRut').val().length > 6 && (/^[0-9]*$/.test($('#txtRut').val()) || $('#selTipoDocumento').val() == '2' || $('#selTipoDocumento').val() == '3')) {
        if ($('#selTipoDocumento').val() == '2' || $('#selTipoDocumento').val() == '3')
            $('#txtDigito').html('-');
        else
            $('#txtDigito').html(await DigitoVerificador($('#txtRut').val()));
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=${$('#selTipoDocumento').val()}&numeroDocumento=${$('#txtRut').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {

                if (data.length > 0) {
                    const paciente = data[0];
                    idPaciente = paciente.IdPaciente;
                    $('#txtNui').val(paciente.Nui);                    
                    $('#txtNombre').val(paciente.Nombre);
                    $('#txtApellidoP').val(paciente.ApellidoPaterno);
                    $('#txtApellidoM').val(paciente.ApellidoMaterno);
                    $('#txtDireccion').val(paciente.DireccionCalle);
                    $('#txtNumero').val(paciente.DireccionNumero);
                    $('#txtTel').val(paciente.Telefono);
                    $('#txtOtrosNumeros').val(paciente.OtrosFonos);
                    if (paciente.Sexo!=null)
                        $('#selSexo').val(paciente.Sexo.Id);
                    $('#txtNacimiento').val(moment(paciente.FechaNacimiento).format('YYYY-MM-DD'));
                    $('#txtNacimiento').change();                    

                    if (paciente.Pais !== null) {
                        $('#selPais').val(paciente.Pais.Id);
                        comboRegion(paciente.Pais.Id);

                        if (paciente.Region !== null) {
                            $('#selRegion').val(paciente.Region.Id);
                            comboProvincia(paciente.Region.Id);

                            if (paciente.Provincia !== null) {
                                $('#selProvincia').val(paciente.Provincia.Id);
                                comboCiudad(paciente.Provincia.Id);

                                if (paciente.Ciudad !== null)
                                    $('#selCiudad').val(paciente.Ciudad.Id);
                                else
                                    $('#selCiudad').val(0);
                            }
                            else
                                $('#selProvincia').val(0);
                        }
                        else
                            $('#selRegion').val(0);
                    }
                    else
                        $('#selPais').val(0);

                    toastr.success('Se ha cargado un paciente existente');

                    habilitarCampos();
                }
                else {

                    idPaciente = 0;
                    $('#txtNui').val('');
                    $('#txtNombre').val('');
                    $('#txtApellidoP').val('');
                    $('#txtApellidoM').val('');
                    $('#txtDireccion').val('Sin Información');
                    $('#txtNumero').val('0');
                    $('#txtTel').val('0');
                    $('#txtOtrosNumeros').val('');
                    $('#selSexo').val(1);
                    $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));


                    toastr.success('Se está creando un paciente nuevo');

                    habilitarCampos();
                    $('#selEstado').val('Activo');
                    $('#selPais').val('1').change();
                    $('#selRegion').val(14).change();
                    $('#selProvincia').val(45)
                    $('#selProvincia').change();
                    $('#selCiudad').val(284);
                    $('#txtNacimiento').change();
                }
            }
        });
    }
    else
        $('#txtDigito').html('-');
}

function habilitarCampos() {
    $('#txtNombre').removeAttr('disabled');
    $('#txtApellidoP').removeAttr('disabled');
    $('#txtApellidoM').removeAttr('disabled');
    $('#txtDireccion').removeAttr('disabled');
    $('#txtNumero').removeAttr('disabled');
    $('#txtTel').removeAttr('disabled');
    $('#txtOtrosNumeros').removeAttr('disabled');
    $('#selSexo').removeAttr('disabled');
    $('#txtNacimiento').removeAttr('disabled');
    $('#selPais').removeAttr('disabled');
    $('#selRegion').removeAttr('disabled');
    $('#selProvincia').removeAttr('disabled');
    $('#selCiudad').removeAttr('disabled');

    $('#btnGuardar').removeAttr('disabled');
}

function inhabilitarCampos() {
    $('#selEstado').attr('disabled', true);
    $('#txtNombre').attr('disabled', true);
    $('#txtApellidoP').attr('disabled', true);
    $('#txtApellidoM').attr('disabled', true);
    $('#txtDireccion').attr('disabled', true);
    $('#txtNumero').attr('disabled', true);
    $('#txtTel').attr('disabled', true);
    $('#txtOtrosNumeros').attr('disabled', true);
    $('#selSexo').attr('disabled', true);
    $('#txtNacimiento').attr('disabled', true);
    $('#selPais').attr('disabled', true);
    $('#selRegion').attr('disabled', true);
    $('#selProvincia').attr('disabled', true);
    $('#selCiudad').attr('disabled', true);

    $('#btnGuardar').attr('disabled', true);
}

function comboPaciente() {
    $('#selTipoDocumento').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                $('#selTipoDocumento').append("<option value='" + val.GEN_idIdentificacion + "'>" + val.GEN_nombreIdentificacion + "</option>");
            });
        }
    });
}

function comboSexo() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnComboAsync(url, false, $('#selSexo'));
}

function comboPais() {
    let url = `${GetWebApiUrl()}GEN_Pais/Combo`
    let elemento = "#selPais"
    setCargarDataEnComboAsync(url, true, elemento)
}

function comboRegion(idPais, idSel) {

    $('#selRegion').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Region/GEN_idPais/${idPais}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selRegion').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selRegion').append("<option value='" + val.Id + "'>" + val.Valor + "</option>");
            });
            $('#selRegion').val(idSel);
        }
    });
}

function comboProvincia(idRegion, idSel) {
    $('#selProvincia').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Provincia/GEN_idRegion/${idRegion}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

            $('#selProvincia').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selProvincia').append("<option value='" + val.GEN_idProvincia + "'>" + val.GEN_nombreProvincia + "</option>");
            });
            $('#selProvincia').val(idSel);
        }
    });
}

function comboCiudad(idProvincia, idSel) {
    $('#selCiudad').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Ciudad/Buscar?idProvincia=${idProvincia}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selCiudad').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selCiudad').append("<option value='" + val.Ciudad.Id + "'>" + val.Ciudad.Valor + "</option>");
            });
            $('#selCiudad').val(idSel);
        }
    });
}