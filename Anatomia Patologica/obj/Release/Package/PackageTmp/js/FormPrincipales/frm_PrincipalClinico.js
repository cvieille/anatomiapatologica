﻿$(document).ready(function () {
    $('#txtFechaDesde').val(moment().add(-3, 'month').format('YYYY-MM-DD'));
    $('#txtFechaHasta').val(moment().format('YYYY-MM-DD'));

    var vSession = getSession();

    if (vSession.GEN_CodigoPerfil == 9) //clinico
    {
        $('#pnlBiopsiasSolicitadas').show();
        $('#pnlNotificaciones').hide();
        grillaBiopsias();
    }
    else {
        grillaNotificaciones();
    }

    $('#btnBuscar').click(function (e) {
        grillaBiopsias();
        e.preventDefault();
    });
    $('#btnBuscarNotificacion').click(function (e) {
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnLimpiarFiltro').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtFicha').val('');
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnGuardar').click(function (e) {
        //ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/guardarNotificacion',

        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${Ana_IdBiopsiaCritico}/Responder`,
            data: JSON.stringify({ Id: Ana_IdBiopsiaCritico, Respuesta: $('#txtRespuestaNotificacion').val() }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('La información fue guardada correctamente');
                $('#mdlNotificacion').modal('hide');
            }
        });
        e.preventDefault();
    });

    $('#btnCerrar').click(function (e) {
        let idUsuario = vSession.id_usuario;
        //ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/cerrarNotificacion',
        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${Ana_IdBiopsiaCritico}/Cerrar`,
            data: JSON.stringify({ Id: Ana_IdBiopsiaCritico, Respuesta: $('#txtRespuestaNotificacion').val() }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('Se ha cerrado correctamente la notificación');
                $('#mdlNotificacion').modal('hide');
                grillaNotificaciones();
            }
        });
        e.preventDefault();
    });

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    const paciente = data[0];
                    $('#txtNombre').val(paciente.Nombre);
                    $('#txtApellidoP').val(paciente.ApellidoPaterno);
                    $('#txtApellidoM').val(paciente.ApellidoMaterno);
                    $('#txtFicha').val(paciente.Nui);
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });
});

function grillaNotificaciones() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/Notificaciones',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.RegistroBiopsia.numero,
                    val.ANA_idBiopsia,
                    val.ANA_idBiopsias_Critico,
                    moment(val.RegistroBiopsia.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY/MM/DD').format('DD-MM-YYYY'),
                    val.Paciente.GEN_nombrePaciente,
                    val.RegistroBiopsia.PatologoValidador[0] != undefined ? val.RegistroBiopsia.PatologoValidador[0].GEN_NombreUsuarios : "",
                    val.RegistroBiopsia.GEN_nombreTipo_Estados_Sistemas,
                    val.ANA_idBiopsia,
                    'Ver',
                    '',
                    '',
                    ''
                ]);
            });

            $('#tblNotificaciones').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                stateSave: true,
                iStateDuration: 60,
                columnDefs:
                    [
                        { targets: 3, sType: 'date-ukShort' },
                        { targets: [1, 2, 7, 8], visible: false, searchable: false },
                        {
                            targets: -3,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][1] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a onclick="linkImprimirB(' + adataset[fila][1] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" data-idcritico="' + adataset[fila][2] + '" onclick="linkNotificacion(this)" class="btn btn-success">Ver</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Nº de biopsia' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'ANA_IdBiopsias_Critico' },
                    { title: 'Recep. anatomía' },
                    { title: 'Paciente' },
                    { title: 'Patólogo' },
                    { title: 'Estado' },
                    { title: 'id_biopsia' },
                    { title: 'reporte' },
                    { title: 'Información' },
                    { title: 'Imprimir' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function linkInformacionB(e) {
    let id = $(e).data("id");
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            
            $('#spanFechaRecepcion').html(moment(data.FechaRecepcion).format('YYYY-MM-DD'));
            $('#spanRutPaciente').html(data.Paciente.NumeroDocumento);
            $('#spanNombrePaciente').html(`${data.Paciente.Nombre} ${data.Paciente.ApellidoPaterno} ${data.Paciente.ApellidoMaterno}`);
            $('#spanOrgano').html(data.ANA_organoBiopsia);
            if (data.Usuario !== null) {
                if (data.Usuario.Patologo)
                    $('#spanPatologo').html(`${data.Usuario.Patologo.Nombre} ${data.Usuario.Patologo.ApellidoPaterno} ${data.Usuario.Patologo.ApellidoMaterno}`);
            }
            if (data.Medico !== null)
                $('#spanMedico').html(`${data.Medico.Persona.Nombre} ${data.Medico.Persona.ApellidoPaterno} ${data.Medico.Persona.ApellidoMaterno}`);

            $('#spanOrigen').html(data.Origen.Valor);
            $('#spanDestino').html(data.Destino.Valor);
            $('#spanEstado').html(data.Estado.Valor);
        }
    });
    $('#mdlInformacionB').modal('show');
}

function linkImprimirB(idB) {
    let idUsuario = getSession().id_usuario;
    ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${parseInt(idB)}/Imprimir`)
}

async function linkNotificacion(e) {
    let id = $(e).data("id");
    let idCritico = $(e).data("idcritico");



    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + `ANA_registro_biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + `ANA_Biopsias_Critico/${idCritico}`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    Ana_IdBiopsiaCritico = data.Id
                    $('#chkVisto').attr('checked', true);

                    if (data.Leido != 'SI')
                        marcarLeidoCasoCritico(idCritico);

                    $('#txtNota').val(data.Observacion);
                    $('#txtRespuestaNotificacion').val(data.Respuesta);
                }
            })

            $('#txtNumero').val(data.Numero + "-" + data.Folio);
            $('#txtUbInterna').val(data.Paciente.Nui);
            $('#txtDocumento').val(data.Paciente.NumeroDocumento);
            $('#txtNombrePaciente').val(data.Paciente.Nombre + " " + data.Paciente.ApellidoPaterno + " " + data.Paciente.ApellidoMaterno);
            $('#txtServicioOrigen').val(data.Origen.Valor);
            $('#txtSolicitadoPor').val(data.Medico.Persona.Nombre + " " + data.Medico.Persona.ApellidoPaterno + " " + data.Medico.Persona.ApellidoMaterno);
            if (data.Usuario.Validador != null)
                $('#txtValidadoPor').val(data.Usuario.Validador.Nombre + " " + data.Usuario.Validador.ApellidoPaterno + " " + data.Usuario.Validador.ApellidoMaterno);//valdiador
            $('#txtEstadoInforme').val(data.Estado.GEN_nombreTipo_Estados_Sistemas);
            $('#txtOrgano').val(data.ANA_organoBiopsia);

            $('#mdlNotificacion').modal('show');
        }, error: function (err) {

        }
    })

    function marcarLeidoCasoCritico(idCritico) {
        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + `ANA_Biopsias_Critico/${idCritico}/Leido`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
            }, error: function (err) {
                console.log("Ha ocurrido un error al intentar cambiar el estado a visto a una Biopsia", err);
            }
        });
    }
}

function grillaBiopsias() {

    let iGEN_idProfesional = 0;
    let adataset = [];
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + '/GEN_Usuarios/LOGEADO',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            iGEN_idProfesional = data[0].Profesional.Id;
            const fechaInicio = moment($('#txtFechaDesde').val()).format("YYYY-MM-DD");
            const fechaTermino = moment($('#txtFechaHasta').val()).format("YYYY-MM-DD");

            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + `ANA_Registro_Biopsias/Buscar?recepcionDesde=${fechaInicio}&recepcionHasta=${fechaTermino}&idProfesionalMedico=${iGEN_idProfesional}`,
                //data: JSON.stringify({ idProfesional: iGEN_idProfesional, sDesde: $('#txtFechaDesde').val(), sHasta: $('#txtFechaHasta').val() }),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {

                    if (data.length > 0) {

                        $.each(data, function (key, val) {
                            adataset.push([
                                val.Id,
                                val.Numero,
                                moment(val.FechaRecepcion).format("DD-MM-YYYY"),
                                val.Paciente.NumeroDocumento,
                                `${val.Paciente.Nombre} ${val.Paciente.ApellidoPaterno} ${val.Paciente.ApellidoMaterno}`,
                                val.Patologo == null ? "" : `${val.Patologo.Nombre} ${val.Patologo.ApellidoPaterno} ${val.Patologo.ApellidoMaterno}`,
                                val.LoginUsuariosTecnologo,
                                val.Estado.Id,
                                val.Estado.Valor,
                                val.ServicioOrigen,
                                val.FechaValidacion,
                                '',
                                ''
                            ]);
                        });
                        $('#tblBiopsias').addClass('nowrap').DataTable({
                            data: adataset,
                            order: [],
                            stateSave: true,
                            iStateDuration: 60,
                            columnDefs:
                                [
                                    { targets: 2, sType: 'date-ukShort' },
                                    { targets: [0, 3, 6, 7, 9, 10], visible: false, searchable: false },
                                    {
                                        targets: -2,
                                        data: null,
                                        orderable: false,
                                        render: function (data, type, row, meta) {
                                            let fila = meta.row;
                                            var botones;
                                            botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                            return botones;
                                        }
                                    },
                                    {
                                        targets: -1,
                                        data: null,
                                        orderable: false,
                                        render: function (data, type, row, meta) {
                                            let fila = meta.row;
                                            var botones;

                                            if (adataset[fila][7] == 44)
                                                botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                            else if (adataset[fila][7] == 45) {
                                                //var date1 = moment().add(-1, 'days');
                                                //var date2 = moment(adataset[fila][10], 'DD/MM/YYYY');

                                                botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                                //if (date1 > date2)
                                                //    botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                                //else {
                                                //    botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';
                                                //    alert(adataset[fila][10])
                                                //}
                                            }
                                            else
                                                botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';
                                            return botones;
                                        }
                                    },
                                ],
                            columns: [
                                { title: 'ANA_IdBiopsia' },
                                { title: 'Nº de biopsia' },
                                { title: 'Recep. anatomía' },
                                { title: 'NumDocumentoPaciente' },
                                { title: 'Paciente' },
                                { title: 'Patólogo' },
                                { title: 'Gen_nombreTecnologo' },
                                { title: 'GEN_idTipo_Estados_Sistemas' },
                                { title: 'Estado' },
                                { title: 'GEN_nombreServicio' },
                                { title: 'ANA_fecValidaBiopsia' },
                                { title: 'Información' },
                                { title: 'Imprimir' }
                            ],
                            bDestroy: true
                        });
                    }
                }, error: function (err) {
                    console.log("No se  ha encontrado data")
                }
            });
        }, error: function (err) {
            console.log("No se ha encontrado data")
        }
    });

}