﻿//var iTecnicaBiopsia;
//var sTipoAlerta;
var vIdBiopsia;
var vSession;
$(document).ready(function () {
    vSession = getSession();

    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    mostrarPestañasdePendientes();
    inicializarGrilla();

    $('#btnBuscar').click(function (e) {
        grillaPrincipal();
        e.preventDefault();
    });
    $('#btnLimpiar').click(function (e) {
        limpiarFiltrosyRecargarGrilla(e);
    });

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(await DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if (!$.isEmptyObject(data[0])) {
                        const paciente = data[0];
                        $('#txtNombre').val(paciente.Nombre);
                        $('#txtApellidoP').val(paciente.ApellidoPaterno);
                        $('#txtApellidoM').val(paciente.ApellidoMaterno);
                    }
                    else {
                        $('#txtNombre').val('');
                        $('#txtApellidoP').val('');
                        $('#txtApellidoM').val('');
                    }
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });

    $('#btnGuardarNota').click(function (e) {
        let bValido = true;
        // Eliminamos los espacios en blanco del inicio y del final de la cadena
        let nota = $('#txtNota').val().trim();

        // Verificamos si la cadena está vacía
        if (nota === '') {
            resaltaElemento($('#txtNota'));
            bValido = false;
        }

        if (bValido) {
            let json = {
                IdBiopsia: $('#txtIdBiopsia').val(),
                Detalle: $('#txtNota').val()
            };

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Notas`,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha ingresado la nota');
                    $('#txtNota').val('');
                    grillaNotas($('#txtIdBiopsia').val());
                }
            });
        }
        e.preventDefault();
    });
});

function limpiarFiltrosyRecargarGrilla(e) {
    getFiltrosBandejaPrincipal();
    $('#selMostrar').val(0); // lo deja en lista de trabajo
    grillaPrincipal();
    e.preventDefault();
}

function inicializarGrilla() {
    getFiltrosBandejaPrincipal();
    grillaPrincipal();
}

function mostrarPestañasdePendientes() {
    const codigoPerfil = perfilAccesoSistema.codigoPerfil;
    if (codigoPerfil != perfilAccesoSistema.paramedico && codigoPerfil != perfilAccesoSistema.auxiliar && codigoPerfil != perfilAccesoSistema.secretaria) {
        $('#divPestañas').show();
        grillaTecnicasPendientes();
        grillaInmunoPendientes();
        grillaInterconsultas();
    }
}

function solicitarCaseteAlmacenado(idCorte) {
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${idCorte}/Solicitar`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha Solicitado Casete');
            grillaInmunoPendientes();
            grillaTecnicasPendientes();
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}

function grillaTecnicasPendientes() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Tecnica_Biopsia/PENDIENTES`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Tecnica.Id,
                    val.IdBiopsia,
                    val.TipoEstadoSistemaCorte.Id,
                    val.Corte.Id,
                    moment(val.FechaTecnica).format('DD/MM/YYYY HH:mm:ss'),
                    val.Corte.Valor,
                    val.Tecnica.Valor,
                    val.LoginUsuariosPatologo,
                    val.LoginUsuariosTecnologo,
                    val.TipoEstadoSistemaCorte.Valor,
                    '',
                    ''
                ]);
            });
            $('#bdgListadoTecnicas').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarTec').attr('disabled', true);
            else
                $('#btnExportarTec').removeAttr('disabled');

            $('#tblListadoTecEspeciales').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        title: 'Listado tecnicas especiales',
                        text: 'Exportar a excel',// mostrar texto
                        className: 'btn btn-info',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                ],
                fnCreatedRow: function (rowEl, adataset) {
                    $(rowEl).attr('id', `row-${adataset[0]}`);
                },
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2, 3], visible: false, searchable: false },
                        {
                            targets: 10,
                            data: null,
                            visible: perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador ||
                                perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.tecnologo ||
                                perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.supervisor,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones = "";
                                let estadoCasete = adataset[fila][2];
                                if (estadoCasete == '52')//almacenado
                                {
                                    botones = '<a data-id="' + adataset[fila][3] + '" onclick="solicitarCaseteAlmacenado(' + adataset[fila][3] + ')" class="btn btn-warning">Solicitar casete</a>';
                                }
                                else if (estadoCasete == '60')// tincion y montaje
                                {
                                    botones = `<a data-id="${adataset[fila][0]}" data-fila=${adataset[fila][0]} data-name="entregarTecnica" onclick="MostrarModalConfirmacion(this)" class="btn btn-success">Entregar</a>`;
                                }
                                else
                                    botones = '';

                                return botones;
                            }
                        },
                        {
                            targets: 11,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdTecnicaBiopsia' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'ANA_idCortes_Muestras' },
                    { title: 'Fecha' },
                    { title: 'Nº corte' },
                    { title: 'Técnica' },
                    { title: 'Solicitado por' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado casete' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

async function grillaInmunoPendientes() {
    let adataset = []
    await $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Inmuno_Histoquimica_Biopsia/PENDIENTES`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key, val) {
                adataset.push([
                    val.InmunoBiopsia.Valor,  //0
                    val.IdBiopsia,                      //1
                    val.TipoEstadoSistemaCorte.Id,//2
                    val.Corte.Id,//3
                    moment(val.FechaTecnica).format('DD/MM/YYYY HH:mm:ss'),//4
                    val.Corte.Valor,//5
                    val.InmunoBiopsia.Valor,//6
                    val.LoginUsuariosPatologo,//7
                    val.LoginUsuariosTecnologo,//8
                    val.TipoEstadoSistemaCorte.Valor,//9
                    '',//10
                    '',//11
                    val.Acciones.Entregar,//12
                    val.Acciones.Solicitar,//13
                    val.Acciones.EntregarAtrasado,//14
                    val.Acciones.VerCaso//15

                ]);
            });
            $('#bdgListadoInmuno').html(adataset.length);
            $('#bdgListadoInmuno').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarInmuno').attr('disabled', true);
            else
                $('#btnExportarInmuno').removeAttr('disabled');
        }, error: function (err) {
            if (err.status == 404) {
                console.log("No se ha encontrado informacion al consultar ANA_Inmuno_Histoquimica_Biopsia/PENDIENTES")
                dataset = []
            }
        }
    });
    $('#tblListadoInmuno').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',// Usa la extensión de Excel
                title: 'Listado InmunoHistoquimicas',
                text: 'Exportar a excel',// mostrar texto
                className: 'btn btn-info',

            }
        ],
        fnCreatedRow: function (rowEl, adataset) {
            $(rowEl).attr('id', `rowInmuno-${adataset[0]}`);
        },
        data: adataset,
        order: [],
        columnDefs:
            [
                { targets: [0, 1, 2, 3], visible: false, searchable: false },
                { targets: 5, sType: 'date-ukLong' },
                {
                    targets: -1,
                    data: null,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        let botones;
                        botones = '<a data-id="' + adataset[fila][1] + '"  onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                        return botones;
                    }
                },
                {
                    targets: -2,
                    data: null,
                    orderable: false,
                    visible: perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador ||
                        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.tecnologo ||
                        perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.supervisor,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        let botones = "";

                        if (adataset[fila][13])//entregar                             
                            botones += '<a data-id="' + adataset[fila][0] + '" data-fila="' + adataset[fila][0] + '" data-name="entregarInmuno" onclick="MostrarModalConfirmacion(this)" class="btn btn-success">Entregar</a>';

                        if (adataset[fila][14])//Solicitar                             
                            botones += '<a data-id="' + adataset[fila][3] + '" onclick="solicitarCaseteAlmacenado(' + adataset[fila][3] + ')" class="btn btn-warning">Solicitar casete</a>';

                        //  if (adataset[fila][15] == true)//Entregar Atrasado                             
                        //    botones += '<a data-id="' + adataset[fila][0] + '" data-name="entregarInmunoAtrasada" onclick="MostrarModalConfirmacion(this)" class="btn btn-danger">Entrega Atrasada</a>';
                        return botones;
                    }
                },
            ],
        columns: [
            { title: 'ANA_IdInmuno_Histoquimica_Biopsia' },
            { title: 'ANA_IdBiopsia' },
            { title: 'GEN_idTipo_Estados_Sistemas' },
            { title: 'idCortes_Muestras' },
            { title: 'Fecha' },
            { title: 'Nº corte' },
            { title: 'Inmunohistoquímicas' },
            { title: 'Solicitado por' },
            { title: 'Tecnólogo' },
            { title: 'Estado casete	' },
            { title: '' },
            { title: '' }
        ],
        bDestroy: true
    });
}

function grillaInterconsultas() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/INTERCONSULTASPORDESPACHAR',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Numero,
                    val.DescripcionBiopsia,
                    moment(val.Fecha, 'YYYY-MM-DD').format('DD-MM-YYYY'),
                    val.Tecnologo.Nombre + " " + val.Tecnologo.ApellidoPaterno + " " + val.Tecnologo.ApellidoMaterno,
                    val.Estado.Valor,
                    ''
                ]);
            });
            $('#bdgInterconsultas').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarInterconsultas').attr('disabled', true);
            else
                $('#btnExportarInterconsultas').removeAttr('disabled');

            $('#tblInterconsultas').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',// Usa la extensión de Excel
                        title: 'Listado Interconsultas',
                        text: 'Exportar a excel',// mostrar texto
                        className: 'btn btn-info',
                    }
                ],
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: 3, sType: 'date-ukShort' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'N° de biopsia' },
                    { title: 'Órgano' },
                    { title: 'Fecha recepción' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function MostrarModalConfirmacion(e) {
    let id = $(e).data('id');
    let name = $(e).data('name');
    //estos id sirven para eliminar la fila sin recargar toda la tabla
    $('#btnConfirmarModal').removeAttr('data-fila')
    $('#btnConfirmarModal').data('fila', $(e).data('fila'));

    if (name == 'entregarTecnica') {
        $('#lblConfirmacion').text('¿Confirma la entrega de esta técnica?');
    }
    else if (name == 'entregarInmuno') {
        $('#lblConfirmacion').text('¿Confirma la entrega de esta Inmuno?');
    }
    else if (name == 'entregarInmunoAtrasada')
        $('#lblConfirmacion').text('¿Confirma la entrega de esta Inmuno en Forma Atrasada?');

    $('#btnConfirmarModal').removeAttr('data-id');  //asegurarse
    $('#btnConfirmarModal').removeAttr('data-name');

    $('#btnConfirmarModal').attr('data-id', id);
    $('#btnConfirmarModal').attr('data-name', name);

    $('#mdlConfirmacion').modal('show');
}
function entregarTecnicaEspecial(id) {
    $.ajax({
        type: 'POST',
        url: GetWebApiUrl() + 'ANA_Tecnica_Biopsia/Entregar/' + id,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data == undefined)//si es undefined es por que no devolvió error
                toastr.success('Lámina entregada correctamente.');
            else
                toastr.error(data);

            //Se elimina solo la fila y no se recarga la tabla completa
            eliminarFilaDatatable("tblListadoTecEspeciales", id);
            $('#mdlConfirmacion').modal('hide');
        }
    });
}
function entregarInmunoNormal(id) {
    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}ANA_Inmuno_Histoquimica_Biopsia/${id}/Entregar/Normal`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data == undefined)//si es undefined es por que no devolvió error
                toastr.success('¡Se ha entregado esta Inmuno!');
            else
                toastr.error(data);
            //grillaInmunoPendientes();
            let tablaTecnicasEspeciales = $('#tblListadoInmuno').DataTable()
            tablaTecnicasEspeciales.row(`#rowInmuno-${fila}`).remove().draw();
            $('#mdlConfirmacion').modal('hide');
        }
    });
}
function entregarInmunoAtrasada(id) {
    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}ANA_Inmuno_Histoquimica_Biopsia/${id}/Entregar/Atrasada`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data == undefined)//si es undefined es por que no devolvió error
                toastr.success('¡Se ha entregado esta Inmuno!');
            else
                toastr.error(data);
            //grillaInmunoPendientes();
            let tablaTecnicasEspeciales = $('#tblListadoInmuno').DataTable()
            tablaTecnicasEspeciales.row(`#rowInmuno-${fila}`).remove().draw();
            $('#mdlConfirmacion').modal('hide');
        }
    });
}
//crea lamina y entrega casete cuando tecnologo confirma.
function confirmaEntregar(e) {
    const id = $('#btnConfirmarModal').attr('data-id');
    const name = $('#btnConfirmarModal').attr('data-name');
    const fila = $(e).data('fila');
    

    if (name == "entregarTecnica") {
        entregarTecnicaEspecial(id);
    } else if (name == "entregarInmuno") {
        entregarInmunoNormal(id, fila);
    }
    else if (name == "entregarInmunoAtrasada") {
        entregarInmunoAtrasada(id, fila);
    }
}


function grillaNotas(idBiopsia) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Notas`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Login,
                    val.Detalle
                ]);
            });

            $('#tblNotas').DataTable({
                lengthMenu: [[5, -1], [5, "Todos"]],
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: 0, visible: false, searchable: false }
                    ],
                columns: [
                    { title: 'ANA_IdNota' },
                    { title: 'Fecha' },
                    { title: 'Creado por' },
                    { title: 'Detalle nota' },
                ],
                bDestroy: true
            });
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}



function anularBiopsia() {
    const motivoAnulacion = $.trim($("#txtMotivoAnulacion").val());
    if (motivoAnulacion != "") {

        const id = $("#btnConfirmarModalAnular").data('id');
        const json = {
            "observacion": motivoAnulacion
        };

        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
            contentType: 'application/json',
            data: JSON.stringify(json),
            success: function (data) {
                toastr.success('¡Se ha Anulado la biopsia!');
                grillaPrincipal();
                $('#mdlConfirmacionAnular').modal('hide');
            }
        });
    } else
        $("#txtMotivoAnulacion").css({ "border": "1px solid red" });
}

function getFiltrosBandejaPrincipal() {
    let url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));

    url = `${GetWebApiUrl()}ANA_TIPO_BIOPSIA/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoBiopsia'));

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/ANA_Registro_Biopsias/Bandeja/Filtros`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $('#selMostrar').empty();
            $('#selMostrar').append(`<option value='0'>Lista de Trabajo</option>`);
            $('#selMostrar').append(`<option value='-1'>Todas</option>`);
            $.each(data, function (key, val) {
                $('#selMostrar').append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });

            $('#selMostrar').val(0);
        },
        error: function (err) {
            console.log(`Error en bandeja/filtros`, JSON.stringify(err));
        }
    });

    inicializaRangodeFechas();
    limpiaFiltrosBandeja();

    function limpiaFiltrosBandeja() {
        $('#selPatologo').val(0);
        $('#selTipoBiopsia').val(0);
        $('#txtNumero').val(0);
        $('#txtAño').val(0);
        $('#chkInterconsulta').prop('checked', false);
        $('#chkCodificar').prop('checked', false);
        $('#chkCritico').prop('checked', false);
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
    }

    function inicializaRangodeFechas() {
        $('#txtDesde').val(moment().add(-6, 'month').format('YYYY-MM-DD'));
        $('#txtHasta').val(moment().format('YYYY-MM-DD'));
        $('#txtDesde').attr("max", moment().format('YYYY-MM-DD'));
        $('#txtHasta').attr("max", moment().format('YYYY-MM-DD'));
    }
}

function grillaPrincipal() {
    let iSelect = $('#selMostrar').val();
    let iPatologo = $('#selPatologo').val();
    let iTipoBiopsia = $('#selTipoBiopsia').val();
    let iNumero = (($('#txtNumero').val() > 0)) ? $('#txtNumero').val() : 0;
    let iFolio = (($('#txtAño').val() > 0)) ? $('#txtAño').val() : 0;
    let iInter = $('#selInterconsulta >option:selected').text() != "-Seleccione-" ? $('#selInterconsulta >option:selected').text() : ""
    let iDocumento = $("#txtRut").val()
    let iNombre = $("#txtNombre").val()
    let iApp = $("#txtApellidoP").val()
    let iApm = $("#txtApellidoM").val()
    let iFechaInicial = moment($("#txtDesde").val()).format("YYYY-MM-DD")
    let iFechaFinal = moment($("#txtHasta").val()).format("YYYY-MM-DD")

    let fechaActual = moment()

    if (fechaActual.diff(iFechaInicial, 'days') < 0 || fechaActual.diff(iFechaFinal, 'days') < 0) {
        toastr.error("El rango de  fechas no pueden ser mayor a la fecha actual")
        return false
    }
    let sUrl = GetWebApiUrl() + 'ANA_Registro_Biopsias/Bandeja?idEstado=' + iSelect
        + '&idPatologo=' + iPatologo
        + '&idTipoBiopsia=' + iTipoBiopsia
        + '&numero=' + iNumero
        + '&folio=' + iFolio
        + '&inter=' + iInter
        + '&documento=' + iDocumento
        + '&nombre=' + iNombre
        + '&ap1=' + iApp
        + '&ap2=' + iApm
        + '&fechaInicial=' + iFechaInicial
        + '&fechaFinal=' + iFechaFinal

    $.ajax({
        type: 'GET',
        url: sUrl,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                /*0*/  val.Id,
                /*1*/  val.Numero,
                /*2*/  val.TipoBiopsia == null ? 0 : val.TipoBiopsia.Id,
                /*3*/  val.Organo,
                /*4*/  val.FechaRecepcion == null ? '' : moment(val.FechaRecepcion).format('DD/MM/YYYY'),
                /*5*/  val.TipoEstadoSistema.Valor,
                /*6*/  val.Ges,
                /*7*/  val.ServicioSolicita.Valor,
                /*8*/  val.Dias,
                /*9*/  val.FechaValidacion == null ? '' : moment(val.FechaValidacion).format('DD/MM/YYYY HH:mm:ss'),
                /*10*/ val.Dictada,
                /*11*/ val.Acciones.Cortes,
                /*12*/ val.Acciones.Notas,
                /*13*/ val.Acciones.Movimientos,
                /*14*/ val.Acciones.Reporte,
                /*15*/ val.Acciones.Anular
                ]);
            });

            $('#tblPrincipal').DataTable({
                data: adataset,
                order: [],
                columns: [
                    /*0*/{ title: 'ANA_IdBiopsia' },
                    /*1*/{ title: 'N° biopsia' },
                    /*2*/{ title: 'ANA_idTipo_Biopsia' },
                    /*3*/{ title: 'Órgano' },
                    /*4*/{ title: 'Fecha recepción' },
                    /*5*/{ title: 'Estado' },
                    /*6*/{ title: 'GES' },
                    /*7*/{ title: 'Solicitado' },
                    /*8*/{ title: 'Días' },
                    /*9*/{ title: 'Fecha validación' },
                    /*10*/{ title: 'Dictada Biopsia' },
                    /*11*/{ title: 'Cortes' },
                    /*12*/{ title: 'Notas' },
                    /*13*/{ title: 'Movim. Biopsia' },
                    /*14*/{ title: 'Reporte' },
                    /*15*/{ title: 'Anular' }
                ],
                columnDefs:
                    [
                        { targets: [0, 2, 6, 10], visible: false, searchable: false },
                        {
                            targets: 1,
                            data: null,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return `<a data-id="${adataset[fila][0]}" href="#/" onclick="linkVerBiopsia(this)">${adataset[fila][1]}</a>`;
                            }
                        },
                        {
                            targets: 11,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let claseboton = "";
                                if (adataset[fila][11]) {
                                    if (adataset[fila][10] == 'NO') //dictada 'NO' = color rojo
                                        claseboton = "btn btn-danger";
                                    else
                                        claseboton = "btn btn-warning";

                                    return `
                                            <a class="${claseboton}" data-id="${adataset[fila][0]}"  onclick="linkVerCortes(this)">
                                                <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span>
                                            </a>`;
                                } else
                                    return `<a class="btn btn-disabled">
                                                <span class="glyphicon glyphicon-ban-circle"></span>
                                            </a>`;
                            }
                        },
                        {
                            targets: 12,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][12]) ?
                                    `<a class="btn btn-success" data-id="${adataset[fila][0]}" data-bio="${adataset[fila][1]}" 
                                        data-organo="${adataset[fila][3]}" onclick="linkVerNotas(this)">
                                        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 13,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][13]) ?
                                    `<a class="btn btn-info" data-id="${adataset[fila][0]}" onclick="linkVerMovimientosBiopsia(this)">
                                        <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 14,
                            data: null,
                            orderable: false,
                            searchable: false,
                            visible: (perfilAccesoSistema.codigoPerfil == 3 ||
                                perfilAccesoSistema.codigoPerfil == 8 ||
                                perfilAccesoSistema.codigoPerfil == 2) ? true : false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][14]) ?
                                    `<a class="btn btn-success" data-id="${adataset[fila][0]}" onclick="linkVerReporte(this)">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 15,
                            data: null,
                            orderable: false,
                            searchable: false,
                            visible: (perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.administrador ||
                                perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.secretaria ||
                                perfilAccesoSistema.codigoPerfil == perfilAccesoSistema.paramedico) ? true : false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][15]) ?
                                    `<a class="btn btn-danger" data-id="${adataset[fila][0]}" onclick="linkAnularBiopsia(this)">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;

                            }
                        }
                    ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                    let diasTranscurridos = aData[8];

                    if (aData[6].trim() == 'SI') //si es caso GES  pinta de color la fila
                        $('td', nRow).css('background-color', '#DFF0D8');

                    if (aData[2] == 3) //si es tipo "citología"
                        $('td', nRow).css('background-color', '#d8daf0');

                    if (diasTranscurridos >= 30) {
                        $(nRow).find('td:eq(5)').css('background-color', 'red');
                        $(nRow).find('td:eq(5)').css('color', 'white');
                    } else if (diasTranscurridos >= 20) {
                        $(nRow).find('td:eq(5)').css('background-color', 'yellow');
                    } else {
                        $(nRow).find('td:eq(5)').css('background-color', 'green');
                        $(nRow).find('td:eq(5)').css('color', 'white');
                    }
                },
                bDestroy: true
            });
        }
    });
}