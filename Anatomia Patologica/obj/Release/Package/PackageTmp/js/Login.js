﻿var vPerfiles;
var vAdmin;
$(document).ready(function () {
    ShowModalCargando(false)
    var v = getSession();
    vAdmin = false;
    if (v.ADMIN_LOGIN) {
        $('#txtClave').addClass('disabled').attr('disabled', true);
        vAdmin = true;
    }
    deleteSession('GEN_CodigoPerfil');
    deleteSession('id_usuario');
    deleteSession('nom_usuario');
    deleteSession('rut_usuario');
    deleteSession('perfil_del_usuario');
    deleteSession('NombrePerfil');
    deleteSession('VPERFILES');
    deleteSession('ADMIN_LOGIN');

    sessionStorage.removeItem('menuANA');
    sessionStorage.removeItem('BearerANA');
    sessionStorage.removeItem("UrlApiANA")
    sessionStorage.clear;

    $('#btnEntrar').click(function (e) {
        Login();
        e.preventDefault();
    });
    $('#selectPerfiles').prop("disabled", false);
});

function Login() {

    //if (validarCampos("#div_login", false)) {
    var success = false;
    $('#btnEntrar').addClass('disabled');
    $('#btnEntrar').attr('disabled', 'disabled');
    $('#btnEntrar').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $('#txtLogin').attr('disabled', 'disabled');
    $('#txtClave').attr('disabled', 'disabled');

    try {
        var user = $('#txtLogin').val();
        var pass = '';
        if (vAdmin) {
            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}gen_usuarios/autentificacion`,
                contentType: 'application/json',
                data: JSON.stringify({ login: user }),
                dataType: 'json',
                async: false,
                success: function (data) {
                    sessionStorage.setItem("usuario", user);
                    pass = data;
                }
            });
        }
        else
            pass = CryptoJS.MD5($('#txtClave').val());
        var resp = LogearUsuario(user, pass, 0);
        if (resp) {
            sessionStorage.setItem("usuario", user);
            getUsuario(user, pass);
        } else
            loginIncorrecto();
    } catch (err) {
        console.log(err.message);
    }
}

function loginIncorrecto() {
    toastr.error('Usuario o contraseña incorrectos');
    $('#btnEntrar').removeClass('disabled');
    $('#btnEntrar').removeAttr('disabled');
    $('#btnEntrar').html('Entrar');
    $('#txtLogin').removeAttr('disabled');
    $('#txtClave').removeAttr('disabled');
}

function getUsuario(usuario, pass) {
    try {
        pass = pass + '';
        $.ajax({
            type: 'GET',
            url: GetWebApiUrl() + '/GEN_Usuarios/Acceso',
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if ($.isEmptyObject(data)) {
                    loginIncorrecto();
                }
                else {
                    setSession('id_usuario', data[0].GEN_idUsuarios);
                    setSession('nom_usuario', usuario.toUpperCase());
                    setSession('rut_usuario', data[0].GEN_numero_documentoPersonas);
                    var sr = CryptoJS.MD5(data[0].GEN_numero_documentoPersonas) + '';

                    if (pass.toLowerCase() == sr.toLowerCase()) {
                        window.location.replace(`${ObtenerHost()}/FormMantenedores/frm_Clave.aspx`);
                    }
                    else {

                        if (data[0].GEN_Perfiles.length > 1) {
                            vPerfiles = data[0].GEN_Perfiles;
                            setSession('VPERFILES', JSON.stringify(vPerfiles));
                            $.each(data[0].GEN_Perfiles, function (key, val) { $("#selectPerfiles").append("<option value='" + val.GEN_idPerfil + "'>" + val.GEN_descripcionPerfil + "</option>"); });
                            $('#modalPerfiles').modal('show');
                            return;
                        }
                        setSession('GEN_CodigoPerfil', data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                        setSession('perfil_del_usuario', data[0].GEN_Perfiles[0].GEN_idPerfil);
                        setSession('NombrePerfil', data[0].GEN_Perfiles[0].GEN_descripcionPerfil);
                        LogearUsuario(usuario, pass, data[0].GEN_Perfiles[0].GEN_idPerfil);
                        redirecciona(data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                    }
                }
            }
        });
    } catch (err) {
        console.log('Error: ' + err.message);
    }
}