var vIdBiopsia;
var vIdUsuario;
$(document).ready(function () {
    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    if (sSession.GEN_CodigoPerfil == 13)
        $('#btnCodificacion').show();

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(await DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/Buscar?idIdentificacion=1&numeroDocumento=${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (!$.isEmptyObject(data[0])) {
                        const paciente = data[0];
                        $('#txtNombre').val(paciente.Nombre);
                        $('#txtApellidoP').val(paciente.ApellidoPaterno);
                        $('#txtApellidoM').val(paciente.ApellidoMaterno);
                        $('#txtNUI').val(paciente.Nui);
                    }
                    else {
                        $('#txtNombre').val('');
                        $('#txtApellidoP').val('');
                        $('#txtApellidoM').val('');
                        $('#txtNUI').val('');
                    }
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });

    $('#btnBuscar').click(function (e) {
        if (($('#txtRut').val() == '' || $('#txtRut').val() == null) &&
            ($('#txtNombre').val() == '' || $('#txtNombre').val() == null) &&
            ($('#txtApellidoP').val() == '' || $('#txtApellidoP').val() == null) &&
            ($('#txtApellidoM').val() == '' || $('#txtApellidoM').val() == null) &&
            ($('#txtNUI').val() == '' || $('#txtNUI').val() == null)) {
            toastr.error('Debe llenar al menos un filtro de búsqueda');
        }
        else {
            grillaPaciente();
            $('#btnLimpiar').removeAttr('disabled');
        }
        e.preventDefault();
    });

    $('#btnLimpiar').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtNUI').val('');
        var t = $('#tblPaciente').DataTable();
        t.clear();
        t.draw(false);
        e.preventDefault();
    });

    $('#btnImprimirInforme').click(function (e) {

        let idBiopsiaImprimir = $('#spanIdBiopsia').text();

        ImprimirApiExterno(`${GetWebApiUrl()}ANA_Registro_Biopsias/${parseInt(idBiopsiaImprimir)}/Imprimir`)

    });
});

function detalleArchivo(e) {
    let id = $(e).data('id');
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Archivos_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let url = ObtenerHost() + '/Documento.aspx?nombre=' + data.Nombre + '&idBiopsia=' + data.IdBiopsia;
            window.open(url);
        }
    });
}

function linkReporte(e) {
    let id = $(e).data('id');
    $('#btnImprimirInforme').attr('data-id', id);

    console.log(`${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`)
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $('#spanIdBiopsia').html(data.Id);
            $('#spanFechaRecepcion').html(moment(data.FechaRecepcion).format('DD-MM-YYYY'));
            $('#spanRutPaciente').html(data.Paciente.NumeroDocumento);
            const nombrePaciente = `${data.Paciente.Nombre} ${data.Paciente.ApellidoPaterno} ${data.Paciente.ApellidoMaterno}`;
            $('#spanNombrePaciente').html(nombrePaciente);

            $('#spanPatologo').html(data.Usuario.Patologo == null ? "" : data.Usuario.Patologo.Login);
            $('#spanTecnologo').html(data.Usuario.Tecnologo == null ? "" : data.Usuario.Tecnologo.Login);
            $('#spanMedSolicitante').html(`${data.Medico.Persona.Nombre} ${data.Medico.Persona.ApellidoPaterno} ${data.Medico.Persona.ApellidoMaterno}`);
            $('#spanServOrigen').html(`${data.Origen.Dependencia} - ${data.Origen.Valor}`);
            $('#spanServDestino').html(`${data.Destino.Dependencia} - ${data.Destino.Valor}`);
            //var o = '';
            //for (var i = 0; i < data.ANA_organoBiopsia.length; i++)
            //    o += data.ANA_organoBiopsia[i] + '<br/>';
            $('#spanOrgano').html(data.ANA_organoBiopsia);

            $('#txt0801001').val(data.Codificacion.Cod001);
            $('#txt0801002').val(data.Codificacion.Cod002);
            $('#txt0801003').val(data.Codificacion.Cod003);
            $('#txt0801004').val(data.Codificacion.Cod004);
            $('#txt0801005').val(data.Codificacion.Cod005);
            $('#txt0801006').val(data.Codificacion.Cod006);
            $('#txt0801007').val(data.Codificacion.Cod007);
            $('#txt0801008').val(data.Codificacion.Cod008);
        }
    });

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Archivos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    moment(val.Fecha).format('DD/MM/YYYY HH:mm:ss'),
                    val.Nombre
                ]);
            });
            $('#tblArchivo').DataTable({
                data: adataset,
                order: [],
                dom: 't',
                columnDefs:
                    [
                        {
                            targets: 2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones = `<a data-id="${adataset[fila][0]}" href="#/" onclick="detalleArchivo(this)">${adataset[fila][2]}</a>`;
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id' },
                    { title: 'Fecha' },
                    { title: 'Archivo' }
                ],
                bDestroy: true
            });
        }
    });

    $('#mdlBiopsia').modal('show');
}

function grillaPaciente() {
    let adataset = [];
    let json = {
        nombre: $('#txtNombre').val(),
        rut: $('#txtRut').val(),
        app: $('#txtApellidoP').val(),
        apm: $('#txtApellidoM').val(),
        nui: $('#txtNUI').val()
    }
    //v `${ObtenerHost()}/frm_consulta_clinicos.aspx/grillaReportes`,
    let url = GetWebApiUrl() + 'ANA_Registro_Biopsias/Buscar?nombrePaciente=' + json.nombre
        + '&apellidoPaternoPaciente=' + json.app
        + '&apellidoMaternoPaciente=' + json.apm
        + '&nui=' + json.nui
        + '&numeroDocumentoPaciente=' + json.rut
    console.log(url)
    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log("data de paciente", data)
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Estado.Id,
                    val.Paciente.NumeroDocumento,
                    val.Numero,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.Paciente.Nombre + " " + val.Paciente.ApellidoPaterno + " " + val.Paciente.ApellidoMaterno,
                    val.Patologo == null ? "" : val.Patologo.Nombre + " " + val.Patologo.ApellidoPaterno + " " + val.Patologo.ApellidoMaterno,
                    (val.LoginUsuariosTecnologo == null) ? 'Sin definir' : val.LoginUsuariosTecnologo,
                    val.Estado.Valor,
                    val.ServicioDestino,
                    val.FechaValidacion == null ? '' : (moment(val.FechaValidacion).format('DD/MM/YYYY HH:mm:ss')),
                    ''
                ]);
            });
            $('#tblPaciente').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2], visible: false, searchable: false },
                        { targets: 4, sType: 'date-ukShort' },
                        { targets: 10, sType: 'date-ukLong' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkReporte(this)"><span class="glyphicon glyphicon-list-alt"></span></a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'NumDocumentoPaciente' },
                    { title: 'N° de biopsia' },
                    { title: 'Fecha recepción' },
                    { title: 'Paciente' },
                    { title: 'Patólogo' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado' },
                    { title: 'Destino' },
                    { title: 'Fecha validación' },
                    { title: 'Reporte' }
                ],
                bDestroy: true
            });
        }, error: function (err) {
            console.log("No se ha encontrodado data")
        }
    });

}