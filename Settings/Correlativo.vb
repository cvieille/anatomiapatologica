Imports System.IO
Imports Newtonsoft.Json

Namespace Configuracion
    Public Class Correlativo
        Private Shared ReadOnly FolderPath As String = "C:\ArchivoConf"
        Private Shared ReadOnly FilePath As String = Path.Combine(FolderPath, "ANA.json")

        ''' <summary>
        ''' Obtiene la configuraci�n actual del archivo JSON.
        ''' Si no existe, lo crea con valores predeterminados.
        ''' </summary>
        ''' <returns>Objeto ConfiguracionAnaDto con los valores actuales</returns>
        Public Shared Function GetNumeracionActual() As ConfiguracionAnaDto
            Try
                VerificarDirectorio(FolderPath)
                VerificarArchivo(FilePath)

                Dim json As String = File.ReadAllText(FilePath)
                Return JsonConvert.DeserializeObject(Of ConfiguracionAnaDto)(json)
            Catch ex As Exception
                Throw New Exception("Error al obtener la numeraci�n actual.", ex)
            End Try
        End Function

        ''' <summary>
        ''' Guarda la configuraci�n actualizada en el archivo JSON.
        ''' </summary>
        ''' <param name="config">Objeto ConfiguracionAnaDto con los nuevos valores</param>
        Public Shared Sub GuardarNumeracion(ByVal config As ConfiguracionAnaDto)
            Try
                Dim json As String = JsonConvert.SerializeObject(config, Newtonsoft.Json.Formatting.Indented)
                File.WriteAllText(FilePath, json)
            Catch ex As Exception
                Throw New Exception("Error al guardar la numeraci�n.", ex)
            End Try
        End Sub

        ''' <summary>
        ''' Verifica si el directorio existe, si no, lo crea.
        ''' </summary>
        ''' <param name="path">Ruta del directorio</param>
        Private Shared Sub VerificarDirectorio(ByVal path As String)
            If Not Directory.Exists(path) Then
                Directory.CreateDirectory(path)
            End If
        End Sub

        ''' <summary>
        ''' Verifica si el archivo de configuraci�n existe. Si no, lo crea con valores predeterminados.
        ''' </summary>
        ''' <param name="path">Ruta del archivo</param>
        Private Shared Sub VerificarArchivo(ByVal path As String)
            If Not File.Exists(path) Then
                Dim yearStr As String = DateTime.Now.Year.ToString("yy")

                Dim defaultConfig As New ConfiguracionAnaDto With {
                    .NumeroBiopsia = "1",
                    .A�oBiopsia = yearStr,
                    .NumeroSolicitud = "1",
                    .A�oSolicitud = yearStr
                }

                Dim json As String = JsonConvert.SerializeObject(defaultConfig, Newtonsoft.Json.Formatting.Indented)
                File.WriteAllText(path, json)
            End If
        End Sub
    End Class

    ''' <summary>
    ''' Representa la configuraci�n de numeraci�n para biopsias y solicitudes.
    ''' </summary>
    Public Class ConfiguracionAnaDto
        ''' <summary>
        ''' N�mero consecutivo de la siguiente biopsia.
        ''' </summary>
        <JsonProperty("n_biopsia")>
        Public Property NumeroBiopsia As String

        ''' <summary>
        ''' A�o asociado al n�mero de biopsia.
        ''' </summary>
        <JsonProperty("a_biopsia")>
        Public Property A�oBiopsia As String

        ''' <summary>
        ''' N�mero consecutivo de la siguiente solicitud.
        ''' </summary>
        <JsonProperty("n_solicitud")>
        Public Property NumeroSolicitud As String

        ''' <summary>
        ''' A�o asociado al n�mero de solicitud.
        ''' </summary>
        <JsonProperty("a_solicitud")>
        Public Property A�oSolicitud As String
    End Class
End Namespace
